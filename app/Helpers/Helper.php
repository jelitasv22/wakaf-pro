<?php
namespace App\Helpers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Entities\Admin\core\Parameter;
use App\Entities\FrontPage\LogWebsite;

class Helper
{
	public static function sosmed_facebook()
	{
		$facebook = Parameter::where('key', 'facebook')->first();
		return $facebook->value;
	}

	public static function sosmed_twitter()
	{
		$twitter = Parameter::where('key', 'twitter')->first();
		return $twitter->value;
	}

	public static function sosmed_instagram()
	{
		
		$instagram = Parameter::where('key', 'instagram')->first();
		return $instagram->value;
	}

	public static function log_message($ip, $desc)
	{
		$log = new LogWebsite;
		$log->ip_pengunjung = $ip;
		$log->deskripsi = $desc;
		$log->created_at = date('Y-m-d H:i:s');
		$log->updated_at = null;
		$log->user_id = Session::get('id');
		$log->save();
	}

	public static function tanggal_indonesia($tgl, $tampil_hari=true){
		$nama_hari=array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu");
		$nama_bulan = array (
			1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus",
			"September", "Oktober", "November", "Desember");
		$tahun=substr($tgl,0,4);
		$bulan=$nama_bulan[(int)substr($tgl,5,2)];
		$tanggal=substr($tgl,8,2);
		$text="";
		if ($tampil_hari) {
			$urutan_hari=date('w', mktime(0,0,0, substr($tgl,5,2), $tanggal, $tahun));
			$hari=$nama_hari[$urutan_hari];
			$text .= $hari.", ";
		}
		$text .=$tanggal ." ". $bulan ." ". $tahun;
		return $text;
	}
}