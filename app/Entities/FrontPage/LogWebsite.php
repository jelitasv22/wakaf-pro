<?php

namespace App\Entities\FrontPage;

use Illuminate\Database\Eloquent\Model;

class LogWebsite extends Model
{
    protected $table = 'log_websites';
}
