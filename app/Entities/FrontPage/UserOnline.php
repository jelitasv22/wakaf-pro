<?php

namespace App\Entities\FrontPage;

use Illuminate\Database\Eloquent\Model;

class UserOnline extends Model
{
    protected $table = 'log_user_online';
    public $timestamps = false;
}
