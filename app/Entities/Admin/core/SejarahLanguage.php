<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class SejarahLanguage extends Model
{
    protected $table = 'sejarah_language';
}
