<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class InfoTerbaruDetail extends Model
{
    protected $table = 'info_terbaru_detail';
}
