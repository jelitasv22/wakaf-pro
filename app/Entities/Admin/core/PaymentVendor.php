<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class PaymentVendor extends Model
{
    protected $table = 'payment_vendors';
    public $timestamps = false;
}
