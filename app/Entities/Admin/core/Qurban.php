<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class Qurban extends Model
{
    protected $table = 'qurban';

    protected $fillable = [
        'id',
        'category_id',
        'id_produk_qurban',
        'no_invoice',
        'user_id',
        'guest_id',
        'member_id',
        'nama_pequrban',
        'alamat_pequrban',
        'harga_qurban',
        'sedekah_qurban',
        'total_qurban',
        'status',
        'metode_pembayaran',
        'batas_pembayaran',
        'snap_token',
        'mid_order_id',
        'device',
        'verifikasi'
    ];

    public function category()
    {
        return $this->hasOne('App\Entities\Admin\core\Category','id','id_category');
    }
    
    public function masterProdukQurban()
    {
        return $this->hasOne('App\Entities\Admin\core\MasterProdukQurban','id','id_produk_qurban');
    }

    public function user()
    {
        return $this->hasOne('App\Entities\Admin\core\User','id','user_id');
    }

    public function guest()
    {
        return $this->hasOne('App\Entities\Admin\modul\Guest','id','guest_id');
    }
}
