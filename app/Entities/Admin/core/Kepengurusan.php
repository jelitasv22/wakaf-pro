<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class Kepengurusan extends Model
{
    protected $table = 'kepengurusan';
}
