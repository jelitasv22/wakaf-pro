<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class MenuFrontPage extends Model
{
    protected $table = 'menu_front_page';
}
