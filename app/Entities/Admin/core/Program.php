<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
	protected $table = 'program';
	protected $primaryKey = 'id';
	protected $fillable = [
		'id',
		'penggagas',
		'tanggal',
		'foto',
		'dana_terkumpul',
		'dana_target'
	];

    public function detail()
    {
		return $this->hasMany('App\Entities\Admin\core\ProgramLanguage','id_program','id');
	}
	
    public function program_lang()
    {
        return $this->hasOne('App\Entities\Admin\core\ProgramLanguage','id_program','id');
    }
}
