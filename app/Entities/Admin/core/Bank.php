<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'bank';
}
