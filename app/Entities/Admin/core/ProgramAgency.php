<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class ProgramAgency extends Model
{
	protected $table = 'program_detail_agency';
	protected $primaryKey = 'id';
}
