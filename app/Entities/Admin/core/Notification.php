<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';

    public function notif_related()
    {
        return $this->hasOne('App\Entities\Admin\core\promosi','id','notification_related_id');
    }
}
