<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'users';

    protected $fillable = [
        'id','username','password','foto', 'name', 'email', 'alamat', 'no_telp', 'bio', 'role_id', 'email_verified'
    ];
}
