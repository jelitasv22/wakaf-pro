<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
    protected $table = 'access_token';

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }
}
