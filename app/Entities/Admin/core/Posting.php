<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class Posting extends Model
{
    protected $table = 'posting';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id','id_category','image',
        'status'
    ];


    public function category()
    {
        return $this->hasOne('App\Entities\Admin\core\category','id','id_category');
    }
}
