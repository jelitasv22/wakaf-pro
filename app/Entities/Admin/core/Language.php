<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'language';
}