<?php

namespace App\Entities\Admin\core;

use Illuminate\Database\Eloquent\Model;

class QDocumentation extends Model
{
    protected $table = 'qurban_documentation';
}
