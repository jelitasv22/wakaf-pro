<?php

namespace App\Entities\Admin\modul;

use Illuminate\Database\Eloquent\Model;

class DonasiBackup extends Model
{
    protected $table = 'donasi_backup';
    protected $fillable = [
        'id',
        'user_id',
        'guest_id',
        'total_donasi',
        'status',
        'metode_pembayaran',
        'batas_pembayaran',
        'snap_token',
        'mid_order_id',
        'device'
    ];

    public function detail()
    {
        return $this->hasOne('App\Entities\Admin\modul\DonasiDetailBackup','donasi_id','id');
    }
    
    public function user()
    {
        return $this->hasOne('App\Entities\Admin\core\User','id','user_id');
    }

    public function guest()
    {
        return $this->hasOne('App\Entities\Admin\modul\Guest','id','guest_id');
    }
}
