<?php

namespace App\Entities\Admin\modul;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table = 'guest';
    protected $fillable = [
        'nama_lengkap',
        'email',
        'alamat',
        'no_telp'
    ];
}
