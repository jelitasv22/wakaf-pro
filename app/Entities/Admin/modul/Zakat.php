<?php

namespace App\Entities\Admin\modul;

use Illuminate\Database\Eloquent\Model;

class Zakat extends Model
{
	protected $table = 'zakat';
	protected $fillable = [
		'id',
		'user_id',
		'guest_id',
		'member_id',
		'total_zakat',
		'status',
		'metode_pembayaran',
		'batas_pembayaran',
		'snap_token',
		'mid_order_id',
		'device'
	];
    
    public function user()
    {
        return $this->hasOne('App\Entities\Admin\core\User','id','user_id');
    }

    public function guest()
    {
        return $this->hasOne('App\Entities\Admin\modul\Guest','id','guest_id');
	}
	
	public function detail()
	{
		return $this->hasOne('App\Entities\Admin\modul\ZakatDetail','zakat_id','id');
	}
}
