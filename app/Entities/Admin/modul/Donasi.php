<?php

namespace App\Entities\Admin\modul;

use Illuminate\Database\Eloquent\Model;

class Donasi extends Model
{
    protected $table = 'donasi';
    protected $fillable = [
        'id',
        'user_id',
        'guest_id',
        'total_donasi',
        'status',
        'metode_pembayaran',
        'batas_pembayaran',
        'snap_token',
        'mid_order_id',
        'device'
    ];
    
    public function user()
    {
        return $this->hasOne('App\Entities\Admin\core\User','id','user_id');
    }

    public function guest()
    {
        return $this->hasOne('App\Entities\Admin\modul\Guest','id','guest_id');
    }

    /**
     * Set status to Pending
     *
     * @return void
     */
    public function setPending()
    {
        $this->attributes['status'] = 'pending';
        self::save();
    }

    /**
     * Set status to Success
     *
     * @return void
     */
    public function setSuccess()
    {
        $this->attributes['status'] = 'settlement';
        self::save();
    }

    /**
     * Set status to Failed
     *
     * @return void
     */
    public function setFailed()
    {
        $this->attributes['status'] = 'failed';
        self::save();
    }

    /**
     * Set status to Expired
     *
     * @return void
     */
    public function setExpired()
    {
        $this->attributes['status'] = 'expired';
        self::save();
    }

    public function detail()
    {
        return $this->hasOne('App\Entities\Admin\modul\DonasiDetail','donasi_id','id');
    }
}
