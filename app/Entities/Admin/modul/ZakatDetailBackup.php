<?php

namespace App\Entities\Admin\modul;

use Illuminate\Database\Eloquent\Model;

class ZakatDetailBackup extends Model
{
    protected $table = 'zakat_detail_backup';
    protected $fillable = [
        'id',
        'zakat_id',
        'program_id',
        'akad_id',
        'id_category',
        'nilai_zakat',
        'komentar',
        'anonim',
        'verifikasi'
    ];

    public function category()
    {
        return $this->hasOne('App\Entities\Admin\core\Category','id','id_category');
    }
}
