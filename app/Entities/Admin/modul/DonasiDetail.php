<?php

namespace App\Entities\Admin\modul;

use Illuminate\Database\Eloquent\Model;

class DonasiDetail extends Model
{
    protected $table = 'donasi_detail';
    protected $fillable = [
        'id',
        'donasi_id',
        'program_id',
        'akad_id',
        'agency_id',
        'nilai_donasi',
        'komentar',
        'anonim',
        'verifikasi'
    ];

    public function program()
    {
        return $this->hasOne('App\Entities\Admin\core\Program','id','program_id');
    }
}
