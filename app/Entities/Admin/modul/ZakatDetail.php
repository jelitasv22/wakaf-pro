<?php

namespace App\Entities\Admin\modul;

use Illuminate\Database\Eloquent\Model;

class ZakatDetail extends Model
{
    protected $table = 'zakat_detail';
    protected $fillable = [
        'id',
        'zakat_id',
        'program_id',
        'akad_id',
        'id_category',
        'nilai_zakat',
        'komentar',
        'anonim',
        'verifikasi'
    ];

    public function category()
    {
        return $this->hasOne('App\Entities\Admin\core\Category','id','id_category');
    }
}
