<?php

namespace App\Entities\Admin\modul;

use Illuminate\Database\Eloquent\Model;

class QurbanBackup extends Model
{
    protected $table = 'qurban_backup';
    protected $fillable = [
        'id',
        'category_id',
        'id_produk_qurban',
        'user_id',
        'guest_id',
        'member_id',
        'nama_pequrban',
        'alamat_pequrban',
        'harga_qurban',
        'sedekah_qurban',
        'total_qurban',
        'status',
        'metode_pembayaran',
        'batas_pembayaran',
        'snap_token',
        'mid_order_id',
        'device'
    ];

    public function category()
    {
        return $this->hasOne('App\Entities\Admin\core\Category','id','id_category');
    }

    public function user()
    {
        return $this->hasOne('App\Entities\Admin\core\User','id','user_id');
    }

    public function guest()
    {
        return $this->hasOne('App\Entities\Admin\modul\Guest','id','guest_id');
    }
}
