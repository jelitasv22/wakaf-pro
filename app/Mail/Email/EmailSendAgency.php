<?php

namespace App\Mail\Email;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailSendAgency extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $program;
    public $agency;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $program, $agency)
    {
        $this->email = $email;
        $this->program = $program;
        $this->agency = $agency;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        return $this->subject('Report Agency')->view('admin.email.EmailSendAgency');
    }
}
