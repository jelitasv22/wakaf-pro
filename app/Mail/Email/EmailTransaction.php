<?php

namespace App\Mail\Email;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Entities\Admin\modul\Donasi;
use App\Entities\Admin\modul\Zakat;

class EmailTransaction extends Mailable
{
    use Queueable, SerializesModels;
    public $email;
    public $triger;
    public $method;
    public $trans;
    public $title;
    public $total;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $triger, $method, $transaction_type, $transaction_id)
    {
        $this->email = $email;
        $this->triger = $triger;
        $this->method = $method;
        $this->trans = null;
        $this->title = "";
        $this->total = 0;
        
        if ($transaction_type == "DONASI") {
            $this->trans = Donasi::with('detail.program.detail')->find($transaction_id);
            $this->title = $this->trans->detail->program->detail[0]->judul;
            $this->total = $this->trans->total_donasi;

        } else if ($transaction_type == "ZAKAT") {
            $this->trans = Zakat::find($transaction_id);
            $this->title = "Zakat";
            $this->total = $this->trans->total_zakat;

        } else if ($transaction_type == "QURBAN") {
            $this->trans = null;
            $this->title = "Qurban";
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->triger == "transaction_created"){
            if ($this->method == "OVO")
                return $this->subject('Mizan Amanah - ' . $this->title)->view('front_page.email.transactions.steps_ovo');
            else
                return $this->subject('Mizan Amanah - ' . $this->title)->view('front_page.email.transactions.steps_ovo');
        }
    }
}
