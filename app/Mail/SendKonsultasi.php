<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Parameter;

class SendKonsultasi extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $emailguest;
    public $subjek;
    public $perihal;
    public $yayasan;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $emailguest, $subjek, $perihal, $yayasan)
    {
        $this->name = $name;
        $this->emailguest = $emailguest;
        $this->subjek = $subjek;
        $this->perihal = $perihal;
        $this->yayasan = $yayasan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Konsultasi')->view('front_page.email.konsultasi');
    }
}
