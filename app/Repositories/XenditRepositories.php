<?php

namespace App\Repositories;

use \Xendit\Xendit;
use \Xendit\Balance;
use \Xendit\Cards;
use \Xendit\CardlessCredit;
use \Xendit\EWallets;
use \Xendit\VirtualAccounts;
use \Xendit\Payouts;
use \Xendit\Retail;
use \Xendit\Invoice;

use App\Entities\Admin\modul\Donasi;
use App\Entities\Admin\modul\Zakat;
use App\Entities\Admin\core\Parameter;

/**
 * Interface UserRepository
 * @package namespace App\Repositories;
 */
class XenditRepositories extends BaseRepository
{
    public function __construct()
    {
        // if (env("APP_ENV") == "production")
        //     $get_id_xendit = Parameter::where('key', "key_xendit_prod")->first();
        // else
        //     $get_id_xendit = Parameter::where('key', "key_xendit")->first();
        
        // Xendit::setApiKey($get_id_xendit->value);
    }

    public function get_balance($account_type)
    {
        $getBalance = Balance::getBalance($account_type);
        
        return $getBalance;
    }


    // -------------------------------------------------------------------------------------------
    // Credit Cards ------------------------------------------------------------------------------
    public function cc_create_token($request)
    {
        if (env("APP_ENV") == "production")
            $get_xendit_public_public = Parameter::where('key', "key_xendit_public_prod")->first();
        else
            $get_xendit_public_public = Parameter::where('key', "key_xendit_public")->first();
        
        Xendit::setApiKey($get_xendit_public_public->value);

        $card_data = [
            'account_number' => $request['card_data']['account_number'],
            'exp_month' => $request['card_data']['exp_month'],
            'exp_year' => $request['card_data']['exp_year'],
            'cvn' => $request['card_cvn']
        ];
        $params = [
            'is_single_use' => $request['is_single_use'],
            'card_data' => json_encode($card_data),
            'should_authenticate' => $request['should_authenticate'],
            'amount' => $request['amount'],
            'card_cvn' => $request['card_cvn']
        ];

        $create_token = Cards::createToken($params);
        return $create_token;
    }

    public function cc_create_charge($request)
    {
        $params = [
            'token_id' => $request['token_id'],
            'external_id' => 'card-' . time(),
            'amount' => $request['amount'],
            'card_cvn' => $request['card_cvn'],
            'is_multiple_use'=> false,
            'capture' => true
        ];
        
        $create_charge = Cards::create($params);
        return $create_charge;
    }

    public function cc_create_refund($cc_charge_id, $request)
    {
        $params = [
            'external_id' => $request['external_id'],
            'amount' => $request['amount'],
            'X-IDEMPOTENCY-KEY' => uniqid()
        ];
        
        $refund = Cards::createRefund($cc_charge_id, $params);
        return $refund;
    }
    // End of Credit Cards -----------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------
    


    // -------------------------------------------------------------------------------------------
    // e-Wallets ---------------------------------------------------------------------------------
    public function ewallet_ovo($request)
    {
        $ovoParams = [
            'external_id' => 'ewallet-' . time(),
            'amount' => $request['amount'],
            'phone' => $request['phone'],
            'ewallet_type' => 'OVO'
        ];
        $createOvo = EWallets::create($ovoParams);
        
        return $createOvo;
    }

    public function ewallet_dana($request)
    {
        $plus7 = strtotime('+7 day');
        $expire_date = date('Y-m-d H:i:s', $plus7);

        $danaParams = [
            'external_id' => 'ewallet-' . time(),
            'amount' => $request['amount'],
            'phone' => $request['phone'],
            'expiration_date' => $expire_date,
            'callback_url' => env("APP_URL") . '/api/ewallet/callback',
            'redirect_url' => env("APP_URL") . '/notif/notif-wait',
            'ewallet_type' => 'DANA'
        ];
        $createDana = EWallets::create($danaParams);

        return $createDana;
    }

    public function ewallet_linkaja($request)
    {
        if ($request['transaction_type'] == "DONASI") {
            $transaction = Donasi::with('detail.program.detail')
                                ->find($request['transaction_id']);

            $item_name = $transaction->detail->program->detail[0]->judul;
            $item_price = $transaction->total_donasi;
        } else { // ZAKAT
            $transaction = Zakat::find($request['transaction_id']);

            $item_name = "Zakat";
            $item_price = $transaction->total_zakat;
        }

        $items[0] = [
            'id'        => uniqid(),
            'name'      => $item_name,
            'price'     => $item_price,
            'quantity'  => 1
        ];

        $linkajaParams = [
            'external_id' => 'ewallet-' . time(),
            'amount' => $request['amount'],
            'phone' => $request['phone'],
            'items' => $items,
            'callback_url' => env("APP_URL") . '/api/ewallet/callback',
            'redirect_url' => env("APP_URL") . '/notif/notif-wait',
            'ewallet_type' => 'LINKAJA'
        ];
        $createLinkaja = EWallets::create($linkajaParams);

        return $createLinkaja;
    }
    // End of e-Wallets --------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------



    // -------------------------------------------------------------------------------------------
    // Virtual Account ---------------------------------------------------------------------------
    public function fixed_virtual_account($request)
    {
        $params = [
            "external_id" => "va-".uniqid(),
            "bank_code" => $request['bank_code'],
            "name" => $request['name'],
            "expected_amount" => $request['expected_amount'],
            "is_single_use" => true
        ];

        $createVA = VirtualAccounts::create($params);
        
        return $createVA;
    }

    public function get_virtual_account_banks()
    {
        $getVABanks = VirtualAccounts::getVABanks();

        return $getVABanks ;
    }

    public function get_fixed_virtual_account($id)
    {
        $getVA = VirtualAccounts::retrieve($id);

        return $getVA;
    }

    public function update_fixed_virtual_account($va_id, $request)
    {
        $updateVA = VirtualAccounts::update($va_id, $request);
        
        if ($updateVA)
            return $updateVA;
        else
            return false;
    }

    public function get_payment_fixed_virtual_account($payment_id)
    {
        $getFVAPayment = VirtualAccounts::getFVAPayment($payment_id);

        return $getFVAPayment;
    }
    // End of Virtual Accounts -------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------



    // -------------------------------------------------------------------------------------------
    // Retail Outlets ----------------------------------------------------------------------------
    public function ro_create_payment($request)
    {
        $ro_param = [
            "external_id" => "ro-".uniqid(),
            "retail_outlet_name" => $request['retail_outlet_name'],
            "name" => $request['name'],
            "expected_amount" => $request['expected_amount'],
            'is_single_use' => true
        ];
        $create_payment = Retail::createPayment($ro_param);

        return $create_payment;
    }

    public function ro_update_payment($request, $payment_id)
    {
        $ro_param = [
            "name" => $request['name'],
            "expected_amount" => $request['expected_amount']
        ];
        $update_payment = Retail::updatePayment($payment_id, $ro_param);

        return $update_payment;
    }
    // End of Retail Outlets ---------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------



    // -------------------------------------------------------------------------------------------
    // Callback ----------------------------------------------------------------------------------    
    public function va_created_callback($request)
    {
        $xendit_payment = XenditPayment::where('va_id',$request['callback_virtual_account_id'])->first();
        $xendit_payment->status = "PENDING";
        $xendit_payment->save();

        $transaction = Transaksi::find($xendit_payment->transaksi_id);
        $transaction->status = "PENDING";
        $transaction->save();

        return "SUCCESS";
    }
    // End of Callback ---------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------
}