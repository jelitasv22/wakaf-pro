<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;

class LoginController extends Controller
{

    private $client;
    private $yayasan;
	private $language;
	private $theme;
	private $slider;


    public function __construct()
    {
        $language   = Session::get('language_id');
	    $client 	= new \GuzzleHttp\Client();
        $yayasan    = $client->request('GET', ENV('API_URL').'/api/ngo-yayasan/'.ENV('YAYASAN_KEY'));


        $this->client    = $client; 
	    $this->language  = $language;
        $this->yayasan   = json_decode($yayasan->getBody()->getContents());
    }

    function Login(Request $request){

        $client = $this->client;
        $_username = $request->post('username');
        $_password = $request->post('password');

//        dd($_username, $_password);

        $_id_yayasan = $this->yayasan->data->id;
        $_key_yayasan = ENV('YAYASAN_KEY');
        

        

		$_login = $client->post(ENV('API_URL') . "/api/login", [
            "headers" => [
                "Content-Type"  => "application/json"
            ],
            "json" => [
				"username" => $_username,
				"password" => $_password,
				"id_yayasan" => $_id_yayasan,
                "key_yayasan" => $_key_yayasan
            ]
        ]);
		$_login = json_decode($_login->getBody()->getContents());
//        dd($_login);

        if (isset($_login->status)){
            if ($_login->status == 400){
                Session::put('error', $_login->error);
                return redirect()->to('/login');
            } else if ($_login->status == 200){
                Session::put('success', "Login Berhasil");
                return redirect()->to('/login');
            }
        } else {
            $request->session()->put('token', $_login->access_token);            
            return redirect()->route('dashboard.donatur');
        }
    }
}
