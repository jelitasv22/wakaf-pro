<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;

class RegisterController extends Controller
{

    private $client;
    private $yayasan;
	private $language;
	private $theme;
	private $slider;


    public function __construct()
    {
        $language   = Session::get('language_id');
	    $client 	= new \GuzzleHttp\Client();
        $yayasan    = $client->request('GET', ENV('API_URL').'/api/ngo-yayasan/'.ENV('YAYASAN_KEY'));


        $this->client    = $client; 
	    $this->language  = $language;
        $this->yayasan   = json_decode($yayasan->getBody()->getContents());
    }

    function Register(Request $request){

        $client = $this->client;
        $_username = $request->post('username');
        $_password = $request->post('password');
        $_email = $request->post('email');
        $_name = $request->post('name');
        $_phone = $request->post('no_telp');
        $_address = $request->post('alamat');
        $_bio = $request->post('bio');

//        dd($_username, $_password);

        $_id_yayasan = $this->yayasan->data->id;
        $_key_yayasan = ENV('YAYASAN_KEY');

        
//        dd($_username, $_password, $_email, $_name, $_phone, $_address, $_bio, $_id_yayasan, $_key_yayasan);
        
		$_response = $client->post(ENV('API_URL') . "/api/register", [
            "headers" => [
                "Content-Type"  => "application/json"
            ],
            "json" => [
				"username" => $_username,
				"password" => $_password,
                "email" => $_email,
                "name" => $_name,
                "alamat" => $_address,
                "no_telp" => $_phone,
                "bio" => $_bio,
				"id_yayasan" => $_id_yayasan,
                "key_yayasan" => $_key_yayasan
            ]
        ]);
		$_response = json_decode($_response->getBody()->getContents());

        if (isset($_response->status)){
            if ($_response->status == 400){
                Session::put('error', $_response->error);
                return redirect('/register');
            } else {
                Session::put('success', $_response->message);
                return redirect('/login');
            }
        } else {
            Session::put('info', $_response->message);
            return redirect('/login');
        }
    }
}
