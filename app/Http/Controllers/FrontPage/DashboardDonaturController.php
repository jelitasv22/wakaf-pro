<?php

namespace App\Http\Controllers\Frontpage;

use Illuminate\Http\Request;
use App\Veritrans\Midtrans;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class DashboardDonaturController extends Controller
{

    private $client;
    private $yayasan;
    private $language;
    private $theme;
    private $slider;
    private $statistik;
    private $program;
    private $posting;
    private $menu;
    private $color;
    private $qurban;
    private $profile;
    private $_user_profile;
    private $_histori_zakat;

    public function __construct(Request $request)
    {
        if (Session::get('token') != null) {
            //return redirect()->route('login.index');

            $language   = Session::get('language_id');
            $client     = new \GuzzleHttp\Client();
            $yayasan    = $client->request('GET', ENV('API_URL') . '/api/ngo-yayasan/' . ENV('YAYASAN_KEY'));
            $theme         = $client->request('GET', ENV('API_URL') . '/api/ngo-theme?yayasan=' . ENV('YAYASAN_KEY'));
            $slider     = $client->request('GET', ENV('API_URL') . '/api/ngo-slider?lang=' . $language . '&yayasan=' . ENV('YAYASAN_KEY'));
            $program      = $client->request('GET', ENV('API_URL') . '/api/ngo-program?lang=' . $language . '&yayasan=' . ENV('YAYASAN_KEY'));
            $posting    = $client->request('GET', ENV('API_URL') . '/api/ngo-posting?lang=' . $language . '&yayasan=' . ENV('YAYASAN_KEY'));
            $menu        = $client->request('GET', ENV('API_URL') . '/api/ngo-menu?lang=' . $language . '&yayasan=' . ENV('YAYASAN_KEY'));
            $color        = $client->request('GET', ENV('API_URL') . '/api/ngo-theme/color?yayasan=' . ENV('YAYASAN_KEY'));
            $qurban     = $client->request('GET', ENV('API_URL') . '/api/ngo-qurban/pages?lang=' . $language . '&yayasan=' . ENV('YAYASAN_KEY'));
            $profile    = $client->request('GET', ENV('API_URL') . '/api/ngo-pages/get-profile?lang=' . $language . '&yayasan=' . ENV('YAYASAN_KEY'));

            $_user_profile = $client->get(ENV('API_URL') . "/api/user", [
                "headers" => [
                    // auth
                    "Authorization"  => Session::get('token')

                ]
            ]);


            $_histori_zakat = $client->get(ENV('API_URL') . "/api/zakat/history", [
                "headers" => [
                    // auth
                    "Authorization"  => Session::get('token')
                ]
            ]);


            $this->client    = $client;
            $this->language  = $language;
            $this->yayasan   = json_decode($yayasan->getBody()->getContents());
            $this->theme      = json_decode($theme->getBody()->getContents());
            $this->slider      = json_decode($slider->getBody()->getContents());
            $this->program   = json_decode($program->getBody()->getContents());
            $this->posting   = json_decode($posting->getBody()->getContents());
            $this->menu      = json_decode($menu->getBody()->getContents());
            $this->color     = json_decode($color->getBody()->getContents());
            $this->qurban    = json_decode($qurban->getBody()->getContents());
            $this->profile   = json_decode($profile->getBody()->getContents());
            $this->_user_profile = json_decode($_user_profile->getBody()->getContents());
            $this->_histori_zakat = json_decode($_histori_zakat->getBody()->getContents());
        }
    }


    public function index(Request $request)
    {

        $language_ses = Session::get('language_id');
        $theme        = $this->theme;
        $menu         = $this->menu;
        $color        = $this->color;
        $program      = $this->program;
        $qurban       = $this->qurban;
        $posting      = $this->posting;
        $profile      = $this->profile;
        $yayasan      = $this->yayasan;
        $list_yayasan = $this->yayasan;
        $slider       = $this->slider;
        $data_list    = $this->qurban;
        $yayasan_list = $this->yayasan;
        $category_zakat = $this->qurban;
        $setup_zakat    = $this->qurban;
        $user_profile   = $this->_user_profile;

        $request->session()->put('email', $user_profile->email);
        $request->session()->put('uid', $user_profile->id);
        $request->session()->put('no_telp', $user_profile->no_telp);
        $request->session()->put('name', $user_profile->name);

        return view('front_page.' . $theme->theme->key . '.pages.dashboard-donatur.index', compact(
            'slider',
            'program',
            'posting',
            'menu',
            'color',
            'language_ses',
            'yayasan',
            'user_profile'
        ));
    }


    public function Logout(Request $request)
    {
        // remove all existing session data
        $request->session()->flush();
        // redirect to home page
        return redirect()->route('login.index');
    }

    public function UpdateProfile(Request $request)
    {
        $client = new \GuzzleHttp\Client();

        $_name_profile = $request->nama_lengkap_field;
        $_address_field = $request->alamat_field;
        $_phone_profile = $request->phone_field;
        $_bio_profile = $request->bio_field;

        $response = $client->request('POST', ENV('API_URL') . '/api/edit/user', [
            'form_params' => [
                'name' => $_name_profile,
                'alamat' => $_address_field,
                'no_telp' => $_phone_profile,
                'bio' => $_bio_profile
            ],
            "headers" => [
                // auth
                "Authorization"  => Session::get('token')
            ]
        ]);


        $response = json_decode($response->getBody()->getContents());

        if ($response->status == 200) {
            Session::put('success', $response->message);
            return redirect()->to('/dashboard-donatur?menu=profile');
        } else {
            Session::put('error', $response->message);
            return redirect()->to('/dashboard-donatur?menu=profile');
        }
    }


    public function UpdatePassword(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $_email = Session::get('email');
        $_old_password = $request->old_password_field;
        $_new_password = $request->new_password_field;
        $_confirm_password = $request->confirm_password_field;

        $_check_old_password = $client->request('POST', ENV('API_URL') . '/api/user/checkPassword', [
            'form_params' => [
                'email' => $_email,
                'password' => $_old_password
            ],
            "headers" => [
                // auth
                "Authorization"  => Session::get('token')
            ]
        ]);

        $_check_old_password = json_decode($_check_old_password->getBody()->getContents());

        if ($_check_old_password->status == 400) {
            Session::put('error', $_check_old_password->message);
            return redirect()->to('/dashboard-donatur?menu=change-password');
        }

        if ($_new_password != $_confirm_password) {
            Session::put('error', 'Password baru dan konfirmasi password tidak sama');

            return redirect()->to('/dashboard-donatur?menu=change-password');
        }

        $response = $client->request('POST', ENV('API_URL') . '/api/user/changePassword', [
            'form_params' => [
                'password' => $_new_password
            ],
            "headers" => [
                // auth
                "Authorization"  => Session::get('token')
            ]
        ]);

        $response = json_decode($response->getBody()->getContents());

        if ($response->status == 200) {
            Session::put('success', $response->message);
            return redirect('/dashboard-donatur?menu=change-password');
        } else {
            Session::put('error', $response->message);
            return redirect('/dashboard-donatur?menu=change-password');
        }
    }
}
