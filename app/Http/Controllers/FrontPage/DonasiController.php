<?php

namespace App\Http\Controllers\FrontPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Veritrans\Midtrans;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use App\Entities\Admin\modul\Donasi;
use App\Entities\Admin\modul\DonasiDetail;
use App\Entities\Admin\modul\DonasiBackup;
use App\Entities\Admin\modul\DonasiDetailBackup;
use App\Mail\Email\EmailKonfirmasiPembayaran;
use App\Mail\Email\EmailTransaction;
use App\Entities\Admin\modul\Guest;
use App\Entities\Admin\core\Theme;
use App\Entities\Admin\core\MenuFrontPage;
use App\Entities\Admin\core\MenuFrontPageLanguage;
use App\Entities\Admin\core\Bank;
use App\Entities\Admin\core\Program;
use App\Entities\Admin\modul\KonfirmasiPembayaran as Pay;
use App\Entities\Admin\modul\Zakat;
use App\Entities\Admin\modul\ZakatDetail;
use App\Entities\Admin\modul\ZakatBackup;
use App\Entities\Admin\modul\ZakatDetailBackup;
use App\Entities\Admin\modul\Log_transaksi as Log;
use App\Repositories\XenditRepositories;
use App\Entities\Admin\modul\XenditCreditCard as CC;
use App\Entities\Admin\modul\XenditCreditCardRefund as CCRefund;
use App\Entities\Admin\modul\XenditEWallet as EW;
use App\Entities\Admin\modul\XenditRetailOutlet as RO;
use App\Entities\Admin\modul\XenditVirtualAccount as VA;
use App\Entities\Admin\modul\LogSMS;
use App\Entities\Admin\core\MasterProdukQurban as ProductQurban;
use App\Entities\Admin\core\Qurban;
use App\Entities\Admin\modul\QurbanBackup;
use App\Entities\Admin\core\ProgramAgency;
use App\Entities\Admin\core\DeviceToken;
use App\Entities\Admin\core\Parameter;
use App\Http\Controllers\FrontPage\BulkDriver;
use Image;
use File;
use Veritrans_Config;
use Veritrans_Snap;
use Veritrans_Notification;

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AdsPixel;
use FacebookAds\Object\ServerSide\Event;
use FacebookAds\Object\ServerSide\EventRequest;
use FacebookAds\Object\ServerSide\UserData;
use FacebookAds\Object\ServerSide\CustomData;

class DonasiController extends Controller
{
	private $client;
    private $yayasan;
	private $language;
	private $theme;
	private $slider;
	private $statistik;
	private $program;
	private $posting;
	private $menu;
    private $color;
    private $qurban;
	private $profile;
	
	public function __construct(Request $request, XenditRepositories $xendit)
	{
		$this->request = $request;

        //Definisi PATH Foto
		$this->path =  'admin/assets/media/img';

		// Set midtrans configuration
		Veritrans_Config::$serverKey = config('services.midtrans.serverKey');
		Veritrans_Config::$isProduction = config('services.midtrans.isProduction');
		Veritrans_Config::$isSanitized = config('services.midtrans.isSanitized');
		Veritrans_Config::$is3ds = config('services.midtrans.is3ds');

        $this->xendit = $xendit;

		$language   = Session::get('language_id');
	    $client 	= new \GuzzleHttp\Client();
        $yayasan    = $client->request('GET', ENV('API_URL').'/api/ngo-yayasan/'.ENV('YAYASAN_KEY'));
	    $theme 		= $client->request('GET', ENV('API_URL').'/api/ngo-theme?yayasan='.ENV('YAYASAN_KEY'));
	    $slider 	= $client->request('GET', ENV('API_URL').'/api/ngo-slider?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
	    $program  	= $client->request('GET', ENV('API_URL').'/api/ngo-program?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
	    $posting    = $client->request('GET', ENV('API_URL').'/api/ngo-posting?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
		$menu		= $client->request('GET', ENV('API_URL').'/api/ngo-menu?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
		$color		= $client->request('GET', ENV('API_URL').'/api/ngo-theme/color?yayasan='.ENV('YAYASAN_KEY'));
		$qurban     = $client->request('GET', ENV('API_URL').'/api/ngo-qurban/pages?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
        $profile    = $client->request('GET', ENV('API_URL').'/api/ngo-pages/get-profile?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));

        $this->client    = $client; 
	    $this->language  = $language;
        $this->yayasan   = json_decode($yayasan->getBody()->getContents());
	    $this->theme 	 = json_decode($theme->getBody()->getContents());
	    $this->slider 	 = json_decode($slider->getBody()->getContents());
	    $this->program   = json_decode($program->getBody()->getContents());
	    $this->posting   = json_decode($posting->getBody()->getContents());
	    $this->menu      = json_decode($menu->getBody()->getContents());
	    $this->color     = json_decode($color->getBody()->getContents());
        $this->qurban    = json_decode($qurban->getBody()->getContents());
        $this->profile   = json_decode($profile->getBody()->getContents());
	}

	public function token() 
	{
		
		// if($this->request->metode_pembayaran == 1){
			$agency_id = ProgramAgency::join('agency', 'agency.id', '=', 'program_detail_agency.id_agency')
                ->where('agency.seo', $this->request->agency)->where('program_detail_agency.deleted_at', null)->first();

			$total_donasi = $this->request->jumlah_donasi;
			// if(!session::has('id')){
			if(!isset($this->request->user_id) || $this->request->user_id == 0 || $this->request->user_id == null){
				$guest = Guest::create([
					'nama_lengkap' 	=> $this->request->nama,
					'no_telp'		=> $this->request->no_telp,
					'email'			=> $this->request->email,
					'alamat'		=> ''
				]);

				$donasi = Donasi::create([
					'user_id'           => 0,
					'guest_id'          => $guest->id,
					'total_donasi'      => $total_donasi,
					'metode_pembayaran' => $this->request->payment_method,
					'status'			=> 'pending',
					'batas_pembayaran'  => Null,
					'snap_token'        => '',
					'mid_order_id'      => uniqid(),
					'device'			=> $this->request->device
				]);
			}else{
				$donasi = Donasi::create([
					'user_id'           => $this->request->user_id,
					'guest_id'          => 0,
					'total_donasi'      => $total_donasi,
					'metode_pembayaran' => $this->request->payment_method,
					'status'			=> 'pending',
					'batas_pembayaran'  => Null,
					'snap_token'        => '',
					'mid_order_id'      => uniqid(),
					'device'			=> $this->request->device
				]);
			}
			$donasi_id = $donasi->id;

			$donasi_detail = DonasiDetail::create([
				'donasi_id'     => $donasi->id,
				'program_id'    => $this->request->program_id,
				'akad_id'       => 0,
                                                  'agency_id'     => ($agency_id) ? $agency_id->id_agency : 0,
				'nilai_donasi'  => $total_donasi,
				'komentar'      => ($this->request->komentar)?($this->request->komentar):'-',
				'anonim'        => ($this->request->anonim)?($this->request->anonim):0,
				'verifikasi'    => ($this->request->verifikasi)?($this->request->verifikasi):0,
			]);

			$log = new Log;
			$log->log_id 		     = null;
			$log->donasi_id 		 = $donasi_id;
			$log->zakat_id  		 = null;
			$log->qurban_id  		 = null;
			$log->status_transaction = "pending";
			$log->transaction_time   = $donasi->created_at;
			$log->status_code 		 = "200";
			$log->status_message 	 = "Success, transaction is found";
			$log->transaction_approve= null;
			$log->save();


			$this->response['snap_token'] = null;
			$vendor = $this->request->vendor; // midtrans/xendit
			$type = $this->request->payment_type; 
			$method = $this->request->payment_method; // gopay/akulaku/OVO/dll
			if ($vendor == "midtrans") {
				$transaction_details = array(
					'order_id'       => 'P'.$donasi_id,
					'gross_amount'   => $donasi->total_donasi
				);

				// Populate items
				$items = [
					array(
						'id'            => 'P'.$donasi_id,
						'price'         => $donasi->total_donasi,
						'quantity'      => 1,
						'name'          => $this->request->nama_produk
					)
				];

				// Populate customer's billing address
				$billing_address = array(
					'first_name'        => $this->request->nama,
					'last_name'         => '',
					'address'           => '',
					'city'              => '',
					'postal_code'       => '',
					'phone'             => $this->request->no_telp,
					'country_code'      => 'IDN'
				);

				// Populate customer's shipping address
				$shipping_address = array(
					'first_name'        => $this->request->nama,
					'last_name'         => '',
					'address'           => '',
					'city'              => '',
					'postal_code'       => '',
					'phone'             => $this->request->no_telp,
					'country_code'      => 'IDN'
				);

				// Populate customer's Info
				$customer_details = array(
					'first_name'        => $this->request->nama,
					'last_name'         => '',
					'email'             => $this->request->email,
					'phone'             => $this->request->no_telp,
					'billing_address'   => $billing_address,
					'shipping_address'  => $shipping_address
				);

				// Data yang akan dikirim untuk request redirect_url.
				$transaction_data = array(
					'transaction_details'=> $transaction_details,
					'item_details'       => $items,
					'customer_details'   => $customer_details,
					'enabled_payments'	 => array($method)
				);
				$snapToken = Veritrans_Snap::getSnapToken($transaction_data);
				$donasi->snap_token = $snapToken;
				$donasi->save();
				// Beri response snap token
				$this->response['snap_token'] = $snapToken;

			} else {

				if ($type == "credit_card") {
					$card_data = $this->request->card_data;
					$data_cc = [
						"transaction_type" => "DONASI",
						"transaction_id" => $donasi->id,
						// Request for create token credit card
						"is_single_use" =>true,
						"card_data" => $card_data,
						"should_authenticate" => true,
						"amount" => $total_donasi,
						"card_cvn" => $card_data['cvn']
					];
					$create_token = $this->xendit->cc_create_token($data_cc);
			
					// Store to DB
					$cc = new CC;
					$cc->transaction_type = "DONASI";
					$cc->transaction_id = $donasi->id;
					$cc->token_id = $create_token['id'];
					$cc->authentication_id = $create_token['authentication_id'];
					$cc->masked_card_number = $create_token['masked_card_number'];
					$cc->status = $create_token['status'];
					$cc->payer_authentication_url = $create_token['payer_authentication_url'];
					$cc->save();

					$this->response['data_cc'] = $cc;

				} else if ($type == "e_wallet") {
					$data_ew = [
						"transaction_type" => "DONASI",
						"transaction_id" => $donasi->id,
						"amount"=> $total_donasi,
						"phone"=> ($method == "DANA") ? $this->request->no_telp : $this->request->phone
					];
					if ($method == "OVO")
						$create_payment = $this->xendit->ewallet_ovo($data_ew);
					else if ($method == "DANA")
						$create_payment = $this->xendit->ewallet_dana($data_ew);
					else // LINKAJA
						$create_payment = $this->xendit->ewallet_linkaja($data_ew);

					$ew = new EW;
					$ew->transaction_type = "DONASI";
					$ew->transaction_id = $donasi->id;
					$ew->external_id = $create_payment['external_id'];
					$ew->amount = $create_payment['amount'];
					$ew->ewallet_type = $create_payment['ewallet_type'];
					$ew->status = "PENDING";
					if ($create_payment['ewallet_type'] == "OVO") {
						$ew->business_id = $create_payment['business_id'];
						$ew->phone = $create_payment['phone'];
					}
					if ($create_payment['ewallet_type'] != "OVO") {
						$ew->checkout_url = $create_payment['checkout_url'];
					}
					$ew->save();

					$this->response['data_ew'] = $ew;

				} else if ($type == "retail_outlet") {
					$data_ro = [
						"retail_outlet_name" => $method,
						"name" => $this->request->nama,
						"expected_amount" => $total_donasi
					];
					$create_payment = $this->xendit->ro_create_payment($data_ro);
			
					$ro = new RO;
					$ro->transaction_type = "DONASI";
					$ro->transaction_id = $donasi->id;
					$ro->retail_payment_id = $create_payment['id'];
					$ro->owner_id = $create_payment['owner_id'];
					$ro->external_id = $create_payment['external_id'];
					$ro->retail_outlet_name = $create_payment['retail_outlet_name'];
					$ro->prefix = $create_payment['prefix'];
					$ro->name = $create_payment['name'];
					$ro->payment_code = $create_payment['payment_code'];
					$ro->type = $create_payment['type'];
					$ro->status = $create_payment['status'];
					$ro->amount = $create_payment['expected_amount'];
					$ro->expiration_date = $create_payment['expiration_date'];
					$ro->save();

					$this->response['data_ro'] = $ro;

				} else if ($type == "virtual_account") {
					$data_va = [
						"bank_code" => $method,
						"name" => $this->request->nama,
						"expected_amount" => $total_donasi
					];
					$create_payment = $this->xendit->fixed_virtual_account($data_va);
			
					$va = new VA;
					$va->transaction_type = "DONASI";
					$va->transaction_id = $donasi->id;
					$va->va_id = $create_payment['id'];
					$va->owner_id = $create_payment['owner_id'];
					$va->external_id = $create_payment['external_id'];
					$va->bank_code = $create_payment['bank_code'];
					$va->merchant_code = $create_payment['merchant_code'];
					$va->name = $this->request->nama;
					$va->account_number = $create_payment['account_number'];
					$va->amount = $total_donasi;
					$va->status = $create_payment['status'];
					$va->save();

					$this->response['data_va'] = $va;
				}

			}
			
			if ($type != "e_wallet") {
				$email = $this->request->email;
				$triger = "transaction_created";
				// Mail::to($email)->send(new EmailTransaction($email, $triger, $method, "DONASI", $donasi_id));
			}
		// }
		return response()->json($this->response);
	}

	public function updateStatus()
	{
		$order_id = $this->request->id;

		$id = substr($order_id, 1);
		$donasi = Donasi::findOrFail($id);
		$donasi->status = $this->request->status;
		$donasi->metode_pembayaran = $this->request->payment_type;
		$donasi->snap_token = $this->request->snap_token;
		$donasi->save();

		return response()->json($donasi);
	}

	public function deleteDonasi()
	{
		$iDtoken = $this->request->iDtoken;
		$iDtransaction = $this->request->iDtransaction;
		$status = $this->request->status;
		if (isset($iDtoken))
			$donasi = Donasi::where('snap_token', $iDtoken)->first();
		else
			$donasi = Donasi::find($iDtransaction);
		$donasi_detail = DonasiDetail::where('donasi_id', $donasi->id)->first();

		if($status == 1){
			$log = new Log;
			$log->log_id 		     = $donasi->snap_token;
			$log->donasi_id 		 = $donasi->id;
			$log->zakat_id  		 = null;
			$log->status_transaction = 'Delete Transaction';
			$log->transaction_time   = date('Y-m-d H:i:s');
			$log->status_code 		 = '302';
			$log->status_message 	 = 'Delete';
			$log->transaction_approve= 'Administrator';
			$log->save();

			//backup data
			$donasiBackup = DonasiBackup::create([
				'id'			    => $donasi->id,
				'user_id'           => $donasi->user_id,
				'guest_id'          => $donasi->guest_id,
				'total_donasi'      => $donasi->total_donasi,					
				'metode_pembayaran' => $donasi->metode_pembayaran,
				'status'			=> $donasi->status,
				'batas_pembayaran'  => Null,
				'snap_token'        => $donasi->snap_token,
				'mid_order_id'      => $donasi->mid_order_id,
				'device'			=> $donasi->device
			]);

			$donasi_detail = DonasiDetailBackup::create([
				'id' 		    => $donasi_detail->id,
				'donasi_id'     => $donasi_detail->donasi_id,
				'program_id'    => $donasi_detail->program_id,
				'akad_id'       => $donasi_detail->akad_id,
				'agency_id'		=> $donasi_detail->agency_id,
				'nilai_donasi'  => $donasi_detail->nilai_donasi,
				'komentar'      => $donasi_detail->komentar,
				'anonim'        => $donasi_detail->anonim,
				'verifikasi'    => $donasi_detail->verifikasi
			]);

			Donasi::where('id', $donasi->id)->delete();
			DonasiDetail::where('donasi_id', $donasi->id)->delete();

			return response()->json('Data Berhasil di Hapus!');
		}else if($status == 0 && $donasi->metode_pembayaran == '-'){
			$log = new Log;
			$log->log_id 		     = $donasi->snap_token;
			$log->donasi_id 		 = $donasi->id;
			$log->zakat_id  		 = null;
			$log->status_transaction = 'Delete Transaction';
			$log->transaction_time   = date('Y-m-d H:i:s');
			$log->status_code 		 = '302';
			$log->status_message 	 = 'Delete';
			$log->transaction_approve= 'Administrator';
			$log->save();

			//backup data
			$donasiBackup = DonasiBackup::create([
				'id'			    => $donasi->id,
				'user_id'           => $donasi->user_id,
				'guest_id'          => $donasi->guest_id,
				'total_donasi'      => $donasi->total_donasi,					
				'metode_pembayaran' => $donasi->metode_pembayaran,
				'status'			=> $donasi->status,
				'batas_pembayaran'  => Null,
				'snap_token'        => $donasi->snap_token,
				'mid_order_id'      => $donasi->mid_order_id,
				'device'			=> $donasi->device
			]);

			$donasi_detail = DonasiDetailBackup::create([
				'id' 		    => $donasi_detail->id,
				'donasi_id'     => $donasi_detail->donasi_id,
				'program_id'    => $donasi_detail->program_id,
				'akad_id'       => $donasi_detail->akad_id,
				'agency_id'		=> $donasi_detail->agency_id,
				'nilai_donasi'  => $donasi_detail->nilai_donasi,
				'komentar'      => $donasi_detail->komentar,
				'anonim'        => $donasi_detail->anonim,
				'verifikasi'    => $donasi_detail->verifikasi
			]);

			Donasi::where('id', $donasi->id)->delete();
			DonasiDetail::where('donasi_id', $donasi->id)->delete();

			return response()->json('Data Berhasil di Hapus!');
		} else {
			return response()->json('Tidak ada Data di Hapus!');
		}
	}

	public function updateStatusZakat()
	{
		$order_id = $this->request->id;

		$id = substr($order_id, 1);
		$zakat = Zakat::findOrFail($id);
		$zakat->status = $this->request->status;
		$zakat->metode_pembayaran = $this->request->payment_type;
		$zakat->snap_token = $this->request->snap_token;
		$zakat->save();

		return response()->json($zakat);
	}

	public function deleteZakat()
	{
		$iDtoken = $this->request->iDtoken;
		$iDtransaction = $this->request->iDtransaction;
		if (isset($iDtoken))
			$zakat = Zakat::where('snap_token', $iDtoken)->first();
		else
			$zakat = Zakat::find($iDtransaction);
		$zakat_detail = ZakatDetail::where('zakat_id', $zakat->id)->first();

		$log = new Log;
		$log->log_id 		     = $zakat->snap_token;
		$log->donasi_id 		 = null;
		$log->zakat_id  		 = $zakat->id;
		$log->status_transaction = 'Delete Transaction';
		$log->transaction_time   = date('Y-m-d H:i:s');
		$log->status_code 		 = '302';
		$log->status_message 	 = 'Delete';
		$log->transaction_approve= 'Administrator';
		$log->save();

		$zakatBU = ZakatBackup::create([
			'id' 				=> $zakat->id,
			'user_id'           => $zakat->user_id,
			'guest_id'			=> $zakat->guest_id,
			'member_id'         => $zakat->member_id,
			'total_zakat'       => $zakat->total_zakat,
			'batas_pembayaran'  => Null,
			'status'			=> $zakat->status,
			'mid_order_id'      => $zakat->mid_order_id,
			'device'			=> $zakat->device
		]);

		$zakat_detailBU = ZakatDetailBackup::create([
			'id' 			=> $zakat_detail->id,
			'zakat_id'      => $zakat_detail->zakat_id,
			'program_id'    => $zakat_detail->program_id,
			'akad_id'       => $zakat_detail->akad_id,
			'id_category'   => $zakat_detail->id_category,
			'nilai_zakat'   => $zakat_detail->nilai_zakat,
			'komentar'      => $zakat_detail->komentar,
			'anonim'        => $zakat_detail->anonim,
			'verifikasi'    => $zakat_detail->verifikasi,
		]);

		$delete_zakat = Zakat::where('id', $zakat->id)->delete();
		$delete_detail = ZakatDetail::where('zakat_id', $zakat->id)->delete();

		// dd($delete_zakat, $delete_detail);
		if ($delete_zakat && $delete_detail)
			return response()->json('Data Berhasil di Hapus!');
		else
			return response()->json('Data Gagal di Hapus!');
	}

	public function token_zakat(){
		\DB::transaction(function(){
			$total_zakat = $this->request->jumlah_zakat;
			// if(!session::has('id')){
			if(!isset($this->request->user_id) || $this->request->user_id == 0 || $this->request->user_id == null) {
				$guest = Guest::create([
					'nama_lengkap' 	=> $this->request->nama,
					'no_telp'		=> $this->request->no_telp,
					'email'			=> $this->request->email,
					'alamat'		=> ''
				]);

				$zakat = Zakat::create([
					'user_id'           => 0,
					'guest_id'			=> $guest->id,
					'member_id'         => 0,
					'total_zakat'       => $total_zakat,
					'metode_pembayaran' => $this->request->payment_method,
					'batas_pembayaran'  => Null,
					'status'			=> 'pending',
					'mid_order_id'      => uniqid(),
					'device'			=> $this->request->device
				]);
			}else{
				$zakat = Zakat::create([
					'user_id'           => $this->request->user_id,
					'guest_id'			=> 0,
					'member_id'         => 0,
					'total_zakat'       => $total_zakat,
					'metode_pembayaran' => $this->request->payment_method,
					'batas_pembayaran'  => Null,
					'status'			=> 'pending',
					'mid_order_id'      => uniqid(),
					'device'			=> $this->request->device
				]);
			}
			$zakat_id = $zakat->id;

			$zakat_detail = ZakatDetail::create([
				'zakat_id'      => $zakat_id,
				'program_id'    => Null,
				'akad_id'       => 0,
				'id_category'   => $this->request->category_id,
				'nilai_zakat'   => $total_zakat,
				'komentar'      => ($this->request->komentar)?($this->request->komentar):'-',
				'anonim'        => ($this->request->anonim)?($this->request->anonim):0,
				'verifikasi'    => ($this->request->verifikasi)?($this->request->verifikasi):0,
			]);

			$log = new Log;
			$log->log_id 		     = null;
			$log->donasi_id 		 = null;
			$log->zakat_id  		 = $zakat_id;
			$log->qurban_id  		 = null;
			$log->status_transaction = "pending";
			$log->transaction_time   = $zakat->created_at;
			$log->status_code 		 = "200";
			$log->status_message 	 = "Success, transaction is found";
			$log->transaction_approve= null;
			$log->save();
			

			$this->response['snap_token'] = null;
			$vendor = $this->request->vendor; // midtrans/xendit
			$type = $this->request->payment_type; 
			$method = $this->request->payment_method; // gopay/akulaku/OVO/dll
			if ($vendor == "midtrans") {
				$transaction_details = array(
					'order_id'       => 'Z'.$zakat_id,
					'gross_amount'   => $zakat->total_zakat
				);
	
				// Populate items
				$items = [
					array(
						'id'            => 'Z'.$zakat_id,
						'price'         => $zakat->total_zakat,
						'quantity'      => 1,
						'name'          => $this->request->nama_produk
					)
				];
	
				// Populate customer's billing address
				$billing_address = array(
					'first_name'        => $this->request->nama,
					'last_name'         => '',
					'address'           => '',
					'city'              => '',
					'postal_code'       => '',
					'phone'             => $this->request->no_telp,
					'country_code'      => 'IDN'
				);
	
				// Populate customer's shipping address
				$shipping_address = array(
					'first_name'        => $this->request->nama,
					'last_name'         => '',
					'address'           => '',
					'city'              => '',
					'postal_code'       => '',
					'phone'             => $this->request->no_telp,
					'country_code'      => 'IDN'
				);
	
				// Populate customer's Info
				$customer_details = array(
					'first_name'        => $this->request->nama,
					'last_name'         => '',
					'email'             => $this->request->email,
					'phone'             => $this->request->no_telp,
					'billing_address'   => $billing_address,
					'shipping_address'  => $shipping_address
				);
	
				// Data yang akan dikirim untuk request redirect_url.
				$transaction_data = array(
					'transaction_details'=> $transaction_details,
					'item_details'       => $items,
					'customer_details'   => $customer_details,
					'enabled_payments'	 => array($method)
				);
	
				$snapToken = Veritrans_Snap::getSnapToken($transaction_data);
				$zakat->snap_token = $snapToken;
				$zakat->save();
	
				// Beri response snap token
				$this->response['snap_token'] = $snapToken;
			} else {

				if ($type == "credit_card") {
					$card_data = $this->request->card_data;
					$data_cc = [
						"transaction_type" => "ZAKAT",
						"transaction_id" => $zakat->id,
						// Request for create token credit card
						"is_single_use" =>true,
						"card_data" => $card_data,
						"should_authenticate" => true,
						"amount" => $total_zakat,
						"card_cvn" => $card_data['cvn']
					];
					$create_token = $this->xendit->cc_create_token($data_cc);
			
					// Store to DB
					$cc = new CC;
					$cc->transaction_type = "ZAKAT";
					$cc->transaction_id = $zakat->id;
					$cc->token_id = $create_token['id'];
					$cc->authentication_id = $create_token['authentication_id'];
					$cc->masked_card_number = $create_token['masked_card_number'];
					$cc->status = $create_token['status'];
					$cc->payer_authentication_url = $create_token['payer_authentication_url'];
					$cc->save();

					$this->response['data_cc'] = $cc;

				} else if ($type == "e_wallet") {
					$data_ew = [
						"transaction_type" => "ZAKAT",
						"transaction_id" => $zakat->id,
						"amount"=> $total_zakat,
						"phone"=> ($method == "DANA") ? $this->request->no_telp : $this->request->phone
					];
					if ($method == "OVO")
						$create_payment = $this->xendit->ewallet_ovo($data_ew);
					else if ($method == "DANA")
						$create_payment = $this->xendit->ewallet_dana($data_ew);
					else // LINKAJA
						$create_payment = $this->xendit->ewallet_linkaja($data_ew);

					$ew = new EW;
					$ew->transaction_type = "ZAKAT";
					$ew->transaction_id = $zakat->id;
					$ew->external_id = $create_payment['external_id'];
					$ew->amount = $create_payment['amount'];
					$ew->ewallet_type = $create_payment['ewallet_type'];
					$ew->status = "PENDING";
					if ($create_payment['ewallet_type'] == "OVO") {
						$ew->business_id = $create_payment['business_id'];
						$ew->phone = $create_payment['phone'];
					}
					if ($create_payment['ewallet_type'] != "OVO") {
						$ew->checkout_url = $create_payment['checkout_url'];
					}
					$ew->save();

					$this->response['data_ew'] = $ew;

				} else if ($type == "retail_outlet") {
					$data_ro = [
						"retail_outlet_name" => $method,
						"name" => $this->request->nama,
						"expected_amount" => $total_zakat
					];
					$create_payment = $this->xendit->ro_create_payment($data_ro);
			
					$ro = new RO;
					$ro->transaction_type = "ZAKAT";
					$ro->transaction_id = $zakat->id;
					$ro->retail_payment_id = $create_payment['id'];
					$ro->owner_id = $create_payment['owner_id'];
					$ro->external_id = $create_payment['external_id'];
					$ro->retail_outlet_name = $create_payment['retail_outlet_name'];
					$ro->prefix = $create_payment['prefix'];
					$ro->name = $create_payment['name'];
					$ro->payment_code = $create_payment['payment_code'];
					$ro->type = $create_payment['type'];
					$ro->status = $create_payment['status'];
					$ro->amount = $create_payment['expected_amount'];
					$ro->expiration_date = $create_payment['expiration_date'];
					$ro->save();

					$this->response['data_ro'] = $ro;

				} else if ($type == "virtual_account") {
					$data_va = [
						"bank_code" => $method,
						"name" => $this->request->nama,
						"expected_amount" => $total_zakat
					];
					$create_payment = $this->xendit->fixed_virtual_account($data_va);
			
					$va = new VA;
					$va->transaction_type = "ZAKAT";
					$va->transaction_id = $zakat->id;
					$va->va_id = $create_payment['id'];
					$va->owner_id = $create_payment['owner_id'];
					$va->external_id = $create_payment['external_id'];
					$va->bank_code = $create_payment['bank_code'];
					$va->merchant_code = $create_payment['merchant_code'];
					$va->name = $this->request->nama;
					$va->account_number = $create_payment['account_number'];
					$va->amount = $total_zakat;
					$va->status = $create_payment['status'];
					$va->save();

					$this->response['data_va'] = $va;
				}
			}
			
			if ($type != "e_wallet") {
				$email = $this->request->email;
				$triger = "transaction_created";
				// Mail::to($email)->send(new EmailTransaction($email, $triger, $method, "ZAKAT", $zakat_id));
			}
		});
		return response()->json($this->response);
	}

	public function notificationHandler(Request $request)
	{
		$notif = new Veritrans_Notification();
		$status 				= $notif->status_code;
		$status_message			= $notif->status_message;
		$transaction_id 		= $notif->transaction_id;  
		$order_id 				= $notif->order_id;
		$gross_amount 			= $notif->gross_amount;
		$payment_type 			= $notif->payment_type;
		$transaction_time		= $notif->transaction_time;
		$transaction_settlement = $notif->transaction_settlement;
		$transaction 			= $notif->transaction_status;
		$type 					= $notif->payment_type;
		$fraud 					= $notif->fraud_status;
		$signature_key 			= $notif->signature_key;

		if (substr($notif->order_id, 0,2) == 'TP') {
			$client = new \GuzzleHttp\Client();
			$check = $client->post("https://temanpeduli.com/notification/handler", [
				"headers" => [
					"Content-Type"  => "application/json",
				],
				"json" => $request->all()
			]);
			return "Redirected to Teman Peduli";
		}

		$id = substr($order_id, 1);
		if (substr($notif->order_id, 0,1) == 'P') {
			$log = new Log;
			$log->log_id 		     = $transaction_id;
			$log->donasi_id 		 = substr($order_id, 1);
			$log->zakat_id  		 = null;
			$log->qurban_id  		 = null;
			$log->status_transaction = $transaction;
			$log->transaction_time   = $transaction_time;
			$log->status_code 		 = $status;
			$log->status_message 	 = $status_message;
			$log->transaction_approve= $transaction_settlement;
			$log->save();

			$donation = Donasi::with('detail.program.program_lang', 'user', 'guest')->find($id);
		} else if (substr($notif->order_id, 0,1) == 'Q') {
			$log = new Log;
			$log->log_id 		     = $transaction_id;
			$log->donasi_id  		 = null;
			$log->zakat_id  		 = null;
			$log->qurban_id 		 = substr($order_id, 1);
			$log->status_transaction = $transaction;
			$log->transaction_time   = $transaction_time;
			$log->status_code 		 = $status;
			$log->status_message 	 = $status_message;
			$log->transaction_approve= $transaction_settlement;
			$log->save();

			$donation = Qurban::with('user', 'guest')->find($id);
		} else {
			$log = new Log;
			$log->log_id 		     = $transaction_id;
			$log->donasi_id 		 = null;
			$log->zakat_id  		 = substr($order_id, 1);
			$log->qurban_id  		 = null;
			$log->status_transaction = $transaction;
			$log->transaction_time   = $transaction_time;
			$log->status_code 		 = $status;
			$log->status_message 	 = $status_message;
			$log->transaction_approve= $transaction_settlement;
			$log->save();

			$donation = Zakat::with('user', 'guest', 'detail.category')->find($id);
		}

		if (!$donation) $this->missingTransaction($notif);

		if (substr($notif->order_id, 0,1) == 'P') {
			$donation = Donasi::with('detail.program.program_lang', 'user', 'guest')->findOrFail($id);
			$donation_type = ($donation->detail->program) ? $donation->detail->program->program_lang->judul : "Kami";

		} else if (substr($notif->order_id, 0,1) == 'Q') {
			$donation = Qurban::with('user', 'guest')->findOrFail($id);
			$donation_type = "Qurban";
			
		} else { // Zakat
			$donation = Zakat::with('user', 'guest', 'detail.category')->findOrFail($id);
			$donation_type = $donation->detail->category->category;
		}


		if ($donation->user) {
			$user_name = $donation->user->name;
			$phone_number = $donation->user->no_telp;
			$email = $donation->user->email;
		} else {
			$user_name = $donation->guest->nama_lengkap;
			$phone_number = $donation->guest->no_telp;
			$email = $donation->guest->email;
		}

		if (isset($notif->payment_type)) $type = $donation->metode_pembayaran;

		if ($transaction == 'capture') {
  			// For credit card transaction, we need to check whether transaction is challenge by FDS or not
			if ($type == 'credit_card'){
				if($fraud == 'challenge'){
      				// TODO set payment status in merchant's database to 'Challenge by FDS'
      				// TODO merchant should decide whether this transaction is authorized or not in MAP
      				// $donation->setPending();

					if($donation->status != "settlement"){
						$donation->status = $transaction;
						$donation->metode_pembayaran = $type;
						$donation->save();
					}
					echo "Transaction order_id: " . $order_id ." is challenged by FDS";
				}
				else {
      				// TODO set payment status in merchant's database to 'Success'
      				// $donation->setSuccess();
					if($donation->status != "settlement"){
						$donation->status = $transaction;
						$donation->metode_pembayaran = $type;
						$donation->save();
					}
					echo "Transaction order_id: " . $order_id ." successfully captured using " . $type;
				}
				
				$this->sendNotifAfterPaid($donation->id, $donation->user_id, $donation_type, "settlement", $user_name, $phone_number, $gross_amount, $email, $notif->order_id);
			}
		}
		else if ($transaction == 'settlement'){
  			// TODO set payment status in merchant's database to 'Settlement'
  			// $donation->setSuccess();
			if($donation->status != "settlement"){
				$donation->status = $transaction;
				$donation->metode_pembayaran = $type;
				$donation->save();

				$this->sendNotifAfterPaid($donation->id, $donation->user_id, $donation_type, "settlement", $user_name, $phone_number, $gross_amount, $email, $notif->order_id);
			}
			echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
		}
		else if($transaction == 'pending'){
  			// TODO set payment status in merchant's database to 'Pending'
			// $donation->setPending();
			if($donation->status != "settlement"){
				$donation->status = $transaction;
				$donation->metode_pembayaran = $type;
				$donation->save();
			}
			echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
		}
		else if ($transaction == 'deny') {
  			// TODO set payment status in merchant's database to 'Denied'
			// $donation->setFailed();
			if($donation->status != "settlement"){
				$donation->status = $transaction;
				$donation->metode_pembayaran = $type;
				$donation->save();
			}
			echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
		}
		else if ($transaction == 'expire') {
  			// TODO set payment status in merchant's database to 'expire'
			// $donation->setExpired();
			if($donation->status != "settlement"){
				$donation->status = $transaction;
				$donation->metode_pembayaran = $type;
				$donation->save();
				
				$this->sendNotifAfterPaid($donation->id, $donation->user_id, $donation_type, "expire", $user_name, $phone_number, $gross_amount, $email, $notif->order_id);
			}
			echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is expired.";
		}
		else if ($transaction == 'cancel') {
  			// TODO set payment status in merchant's database to 'Denied'
			// $donation->setFailed();
			if($donation->status != "settlement"){
				$donation->status = $transaction;
				$donation->metode_pembayaran = $type;
				$donation->save();
			}
			echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is canceled.";
		}
	}

	public function setSuccess()
	{
		$header = $this->header;
		$header_body = $this->header_body;
		$footer = $this->footer;
		$footer_sub = $this->footer_sub;
		$language = $this->language;

		return view('front_page.'.$this->theme->key.'.pages.notif.success', compact('header','header_body','footer','footer_sub','language'));
	}

	public function setWait()
	{
		$language_ses = $this->language;
        $yayasan      = $this->yayasan;
    	$theme        = $this->theme;
    	$menu 		  = $this->menu;
    	$color 		  = $this->color;
    	$program      = $this->program;
        $posting      = $this->posting;
        $qurban       = $this->qurban;
		$profile      = $this->profile;

		return view('front_page.'.$this->theme->theme->theme.'.pages.notif.wait', compact(
			'language_ses','theme','menu','color','program','qurban','posting','profile','yayasan'
		));
	}
	public function setWaitZakat()
	{
		$header = $this->header;
		$header_body = $this->header_body;
		$footer = $this->footer;
		$footer_sub = $this->footer_sub;
		$language = $this->language;

		return view('front_page.'.$this->theme->key.'.pages.notif.wait_zakat', compact('header','header_body','footer','language', 'footer_sub'));
	}
	public function setWaitQurban()
	{
		$header = $this->header;
		$header_body = $this->header_body;
		$footer = $this->footer;
		$footer_sub = $this->footer_sub;
		$language = $this->language;

		return view('front_page.'.$this->theme->key.'.pages.notif.wait_qurban', compact('header','header_body','footer','language', 'footer_sub'));
	}


	public function setPending()
	{
		$header = $this->header;
		$header_body = $this->header_body;
		$footer = $this->footer;
		$footer_sub = $this->footer_sub;
		$language = $this->language;

		return view('front_page.'.$this->theme->key.'.pages.notif.pending', compact('header','header_body','footer','footer_sub','language'));
	}

	public function setKonfirmasi($token)
	{
		if(\App::getLocale() == "id"){
			$id_lang = 1;
		}else{
			$id_lang = 2;
		}

		$header = $this->header;		
		$header_body = $this->header_body;
		$footer = $this->footer;
		$footer_sub = $this->footer_sub;
		$language = $this->language;

		$bank = Bank::where('deleted_at', null)->get();

		if(session::has('id') == null){
			$data = Program::select('program.id as programid','programlanguage.judul','donasi.*','guest.nama_lengkap')
			->join('programlanguage', 'programlanguage.id_program', '=', 'program.id')
			->leftjoin('category', 'category.id', '=', 'program.id_category')
			->leftjoin('donasi_detail', 'donasi_detail.program_id', '=', 'program.id')
			->leftjoin('donasi', 'donasi.id', '=', 'donasi_detail.donasi_id')
			->leftjoin('guest', 'guest.id', '=', 'donasi.guest_id')
			->where('programlanguage.id_language', $id_lang)
			->where('donasi.snap_token', $token)
			->orderBy('donasi.guest_id', 'DESC')
			->first();

		}else{
			$data = Program::select('program.id as programid','programlanguage.judul','donasi.*','users.name as nama_lengkap')
			->join('programlanguage', 'programlanguage.id_program', '=', 'program.id')
			->leftjoin('category', 'category.id', '=', 'program.id_category')
			->leftjoin('donasi_detail', 'donasi_detail.program_id', '=', 'program.id')
			->leftjoin('donasi', 'donasi.id', '=', 'donasi_detail.donasi_id')
			->leftjoin('users', 'users.id', '=', 'donasi.user_id')
			->where('programlanguage.id_language', $id_lang)
			->where('donasi.snap_token', $token)
			->orderBy('donasi.guest_id', 'DESC')
			->first();
		}

		return view('front_page.'.$this->theme->key.'.pages.notif.konfirmasi', compact('header','header_body','footer','footer_sub','language','data','bank'));
	}

	public function setKonfirmasi2()
	{
		$header = $this->header;
		$header_body = $this->header_body;
		$footer = $this->footer;
		$footer_sub = $this->footer_sub;
		$language = $this->language;

		return view('front_page.'.$this->theme->key.'.pages.notif.notif_konfirmasi', compact('header','header_body','footer','footer_sub','language'));
	}

	public function konfirmasi_pembayaran($id)
	{
		if(\App::getLocale() == "id"){
			$id_lang = 1;
		}else{
			$id_lang = 2;
		}

		$header = $this->header;
		$header_body = $this->header_body;
		$footer = $this->footer;
		$footer_sub = $this->footer_sub;
		$language = $this->language;

		$bank = Bank::select('*')->where('deleted_at', null)->get();

		if(session::has('id') == null){
			$detail_transfer = Program::select('program.id as programid','programlanguage.judul','donasi.*')
			->join('programlanguage', 'programlanguage.id_program', '=', 'program.id')
			->leftjoin('category', 'category.id', '=', 'program.id_category')
			->leftjoin('donasi_detail', 'donasi_detail.program_id', '=', 'program.id')
			->leftjoin('donasi', 'donasi.id', '=', 'donasi_detail.donasi_id')
			->where('programlanguage.id_language', $id_lang)
			->orderBy('donasi.guest_id', 'DESC')
			->limit(1)
			->first();

			$data = Program::select('program.id as programid','programlanguage.judul','donasi.*','guest.nama_lengkap')
			->join('programlanguage', 'programlanguage.id_program', '=', 'program.id')
			->leftjoin('category', 'category.id', '=', 'program.id_category')
			->leftjoin('donasi_detail', 'donasi_detail.program_id', '=', 'program.id')
			->leftjoin('donasi', 'donasi.id', '=', 'donasi_detail.donasi_id')
			->leftjoin('guest', 'guest.id', '=', 'donasi.guest_id')
			->where('programlanguage.id_language', $id_lang)
			->where('donasi.snap_token', $id)
			->orderBy('donasi.guest_id', 'DESC')
			->first();

			$totals = number_format($data->total_donasi);
			$total_donasis = str_replace(',','.',$totals);
		}else{
			$detail_transfer = Program::select('program.id as programid','programlanguage.judul','donasi.*')
			->join('programlanguage', 'programlanguage.id_program', '=', 'program.id')
			->leftjoin('category', 'category.id', '=', 'program.id_category')
			->leftjoin('donasi_detail', 'donasi_detail.program_id', '=', 'program.id')
			->leftjoin('donasi', 'donasi.id', '=', 'donasi_detail.donasi_id')
			->where('donasi.user_id', session::get('id'))
			->where('programlanguage.id_language', $id_lang)
			->orderBy('donasi.id', 'DESC')
			->first();

			$data = Program::select('program.id as programid','programlanguage.judul','donasi.*','users.name as nama_lengkap')
			->join('programlanguage', 'programlanguage.id_program', '=', 'program.id')
			->leftjoin('category', 'category.id', '=', 'program.id_category')
			->leftjoin('donasi_detail', 'donasi_detail.program_id', '=', 'program.id')
			->leftjoin('donasi', 'donasi.id', '=', 'donasi_detail.donasi_id')
			->leftjoin('users', 'users.id', '=', 'donasi.users_id')
			->where('programlanguage.id_language', $id_lang)
			->where('donasi.snap_token', $id)
			->orderBy('donasi.guest_id', 'DESC')
			->first();

			$totals = number_format($data->total_donasi);
			$total_donasis = str_replace(',','.',$totals);
		}

		$total = number_format($detail_transfer->total_donasi);
		$total_donasi = str_replace(',','.',$total);

		return view('front_page.'.$this->theme->key.'.pages.programs.detail_transfer', compact('header','header_body','footer','footer_sub','language','bank', 'detail_transfer','total_donasi','data','total_donasis'));
	}

	public function konfirmasi_pay(Request $request)
	{
		$header = $this->header;
		$header_body = $this->header_body;
		$footer = $this->footer;
		$footer_sub = $this->footer_sub;
		$language = $this->language;

		$total_i = $request->input('total');
		$total_s = substr($total_i, 4);
		$total   = str_replace('.','', $total_s);

		$total_is = $request->input('total_submit');
		$total_ss = substr($total_is, 4);
		$total_submit = str_replace('.','', $total_ss);

		$pay = new Pay;
		if($request->file('berkas') != NULL){
            #upload foto to database
			$file = $request->file('berkas');

            #JIKA FOLDERNYA BELUM ADA
			if (!File::isDirectory($this->path)) {
                #MAKA FOLDER TERSEBUT AKAN DIBUAT
				File::makeDirectory($this->path);
			}

            // #MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
			$fileName = 'Berkas' . '_' . uniqid() . '.' . $file->getClientOriginalExtension();

            #UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
			Image::make($file)->save($this->path . '/' . $fileName);
		}else{
			$fileName = '-';
		}

        #SIMPAN DATA IMAGE YANG TELAH DI-UPLOAD
		$pay->id_guest 			 = $request->guest_id;
		$pay->id_user 			 = $request->user_id;
		$pay->id_bank 			 = $request->id_bank;
		$pay->id_program		 = $request->id_program;
		$pay->id_donasi 		 = $request->donasi_id;
		$pay->total 			 = $total;
		$pay->total_submit  	 = $total_submit;
		$pay->tanggal_konfirmasi = $request->tanggal_konfirmasi;
		$pay->bank_pengirim 	 = $request->bank_pengirim;
		$pay->nama_pengirim 	 = $request->nama_pengirim;
		$pay->berkas 			 = $fileName;
		$pay->save();

		return view('front_page.'.$this->theme->key.'.pages.notif.notif_konfirmasi', compact('header','header_body','footer','footer_sub','language'));
	}

	public function get_invoice(Request $request, $id)
	{
		if(\App::getLocale() == "id"){
			$id_lang = 1;
		}else{
			$id_lang = 2;
		}

		if(session::has('id') == null){
			$data = Program::select('program.id as programid','programlanguage.judul','donasi.*','guest.nama_lengkap')
			->join('programlanguage', 'programlanguage.id_program', '=', 'program.id')
			->leftjoin('category', 'category.id', '=', 'program.id_category')
			->leftjoin('donasi_detail', 'donasi_detail.program_id', '=', 'program.id')
			->leftjoin('donasi', 'donasi.id', '=', 'donasi_detail.donasi_id')
			->leftjoin('guest', 'guest.id', '=', 'donasi.guest_id')
			->where('programlanguage.id_language', $id_lang)
			->where('donasi.snap_token', $id)
			->orderBy('donasi.guest_id', 'DESC')
			->first();
		}else{
			$data = Program::select('program.id as programid','programlanguage.judul','donasi.*','users.name as nama_lengkap')
			->join('programlanguage', 'programlanguage.id_program', '=', 'program.id')
			->leftjoin('category', 'category.id', '=', 'program.id_category')
			->leftjoin('donasi_detail', 'donasi_detail.program_id', '=', 'program.id')
			->leftjoin('donasi', 'donasi.id', '=', 'donasi_detail.donasi_id')
			->leftjoin('users', 'users.id', '=', 'donasi.user_id')
			->where('programlanguage.id_language', $id_lang)
			->where('donasi.snap_token', $id)
			->orderBy('donasi.guest_id', 'DESC')
			->first();
		}

		return response()->json($data);
	}

	#qurban modul
	public function token_qurban()
	{
		\DB::transaction(function(){
			$uniqid = date('His');
			$no_i = $this->request->no_invoice;
			$no_invoice = $no_i+1;

			$harga_qurban = $this->request->harga_qurban;
			$sedekah_qurban = $this->request->sedekah_qurban;
			if(!isset($this->request->user_id) || $this->request->user_id == 0 || $this->request->user_id == null){
				$guest = Guest::create([
					'nama_lengkap' 	=> $this->request->nama,
					'no_telp'		=> $this->request->no_telp,
					'email'			=> $this->request->email,
					'alamat'		=> '',
				]);

				$qurban = Qurban::create([
					'no_invoice'		=> 'KP033'.$uniqid.'0'.$no_invoice.'QRB',
					'category_id'		=> 0,
					'id_produk_qurban'  => $this->request->product_id,
					'user_id'           => 0,
					'guest_id'			=> $guest->id,
					'member_id'         => 0,
					'nama_pequrban'		=> $this->request->nama_pequrban,
					'alamat_pequrban'   => $this->request->alamat_pequrban,
					'harga_qurban'		=> $harga_qurban,
					'sedekah_qurban'	=> $sedekah_qurban,
					'total_qurban'      => $harga_qurban + $sedekah_qurban,
					'status'			=> 'pending',
					'metode_pembayaran' => '-',
					'batas_pembayaran'  => Null,
					'snap_token' 		=> '-',
					'mid_order_id'      => uniqid(),
					'device' 			=> $this->request->device,
					'verifikasi'		=> $this->request->verifikasi
				]);
			}else{
				$qurban = Qurban::create([
					'no_invoice'		=> 'KP033'.$uniqid.'0'.$no_invoice.'QRB',
					'category_id'		=> 0,
					'id_produk_qurban'  => $this->request->product_id,
					'user_id'           => $this->request->user_id,
					'guest_id'			=> 0,
					'member_id'         => 0,
					'nama_pequrban'		=> $this->request->nama_pequrban,
					'alamat_pequrban'   => $this->request->alamat_pequrban,
					'harga_qurban'		=> $harga_qurban,
					'sedekah_qurban'	=> $sedekah_qurban,
					'total_qurban'      => $harga_qurban + $sedekah_qurban,
					'status'			=> 'pending',
					'metode_pembayaran' => '-',
					'batas_pembayaran'  => Null,
					'snap_token' 		=> '-',
					'mid_order_id'      => uniqid(),
					'device' 			=> $this->request->device,
					'verifikasi'		=> $this->request->verifikasi
				]);
			}
			$qurban_id = $qurban->id;

			$log = new Log;
			$log->log_id 		     = null;
			$log->donasi_id 		 = null;
			$log->zakat_id  		 = null;
			$log->qurban_id  		 = $qurban_id;
			$log->status_transaction = "pending";
			$log->transaction_time   = $qurban->created_at;
			$log->status_code 		 = "200";
			$log->status_message 	 = "Success, transaction is found";
			$log->transaction_approve= null;
			$log->save();


			// $product = ProductQurban::findOrFail($qurban->id_produk_qurban);

			$this->response['snap_token'] = null;
			$vendor = $this->request->vendor; // midtrans/xendit
			$type = $this->request->payment_type; 
			$method = $this->request->payment_method; // gopay/akulaku/OVO/dll
			if ($vendor == "midtrans") {
				$transaction_details = array(
					'order_id'       => 'Q'.$qurban_id,
					'gross_amount'   => $qurban->total_qurban
				);

				// Populate items
				$items = [
					array(
						'id'            => 'Q'.$qurban->id,
						'price'         => $harga_qurban + $sedekah_qurban,
						'quantity'      => 1,
						'name'          => $this->request->nama_produk . " + Sedekah Pemotongan & Distribusi (Rp. ".number_format($sedekah_qurban).")"
					)
				];

				// Populate customer's billing address
				$billing_address = array(
					'first_name'        => $this->request->nama,
					'last_name'         => '',
					'address'           => '',
					'city'              => '',
					'postal_code'       => '',
					'phone'             => $this->request->no_telp,
					'country_code'      => 'IDN'
				);

				// Populate customer's shipping address
				$shipping_address = array(
					'first_name'        => $this->request->nama,
					'last_name'         => '',
					'address'           => '',
					'city'              => '',
					'postal_code'       => '',
					'phone'             => $this->request->no_telp,
					'country_code'      => 'IDN'
				);

				// Populate customer's Info
				$customer_details = array(
					'first_name'        => $this->request->nama,
					'last_name'         => '',
					'email'             => $this->request->email,
					'phone'             => $this->request->no_telp,
					'billing_address'   => $billing_address,
					'shipping_address'  => $shipping_address
				);

				// Data yang akan dikirim untuk request redirect_url.
				$transaction_data = array(
					'transaction_details'=> $transaction_details,
					'item_details'       => $items,
					'customer_details'   => $customer_details,
					'enabled_payments'	 => array($method)
				);

				$snapToken = Veritrans_Snap::getSnapToken($transaction_data);
				$qurban->snap_token = $snapToken;
				$qurban->save();

				// Beri response snap token
				$this->response['snap_token'] = $snapToken;
			} else {

				if ($type == "credit_card") {
					$card_data = $this->request->card_data;
					$data_cc = [
						"transaction_type" => "QURBAN",
						"transaction_id" => $qurban->id,
						// Request for create token credit card
						"is_single_use" =>true,
						"card_data" => $card_data,
						"should_authenticate" => true,
						"amount" => $harga_qurban + $sedekah_qurban,
						"card_cvn" => $card_data['cvn']
					];
					$create_token = $this->xendit->cc_create_token($data_cc);
			
					// Store to DB
					$cc = new CC;
					$cc->transaction_type = "QURBAN";
					$cc->transaction_id = $qurban->id;
					$cc->token_id = $create_token['id'];
					$cc->authentication_id = $create_token['authentication_id'];
					$cc->masked_card_number = $create_token['masked_card_number'];
					$cc->status = $create_token['status'];
					$cc->payer_authentication_url = $create_token['payer_authentication_url'];
					$cc->save();

					$this->response['data_cc'] = $cc;

				} else if ($type == "e_wallet") {
					$data_ew = [
						"transaction_type" => "QURBAN",
						"transaction_id" => $qurban->id,
						"amount"=> $harga_qurban + $sedekah_qurban,
						"phone"=> ($method == "DANA") ? $this->request->no_telp : $this->request->phone
					];
					if ($method == "OVO")
						$create_payment = $this->xendit->ewallet_ovo($data_ew);
					else if ($method == "DANA")
						$create_payment = $this->xendit->ewallet_dana($data_ew);
					else // LINKAJA
						$create_payment = $this->xendit->ewallet_linkaja($data_ew);

					$ew = new EW;
					$ew->transaction_type = "QURBAN";
					$ew->transaction_id = $qurban->id;
					$ew->external_id = $create_payment['external_id'];
					$ew->amount = $create_payment['amount'];
					$ew->ewallet_type = $create_payment['ewallet_type'];
					$ew->status = "PENDING";
					if ($create_payment['ewallet_type'] == "OVO") {
						$ew->business_id = $create_payment['business_id'];
						$ew->phone = $create_payment['phone'];
					}
					if ($create_payment['ewallet_type'] != "OVO") {
						$ew->checkout_url = $create_payment['checkout_url'];
					}
					$ew->save();

					$this->response['data_ew'] = $ew;

				} else if ($type == "retail_outlet") {
					$data_ro = [
						"retail_outlet_name" => $method,
						"name" => $this->request->nama,
						"expected_amount" => $harga_qurban + $sedekah_qurban
					];
					$create_payment = $this->xendit->ro_create_payment($data_ro);
			
					$ro = new RO;
					$ro->transaction_type = "QURBAN";
					$ro->transaction_id = $qurban->id;
					$ro->retail_payment_id = $create_payment['id'];
					$ro->owner_id = $create_payment['owner_id'];
					$ro->external_id = $create_payment['external_id'];
					$ro->retail_outlet_name = $create_payment['retail_outlet_name'];
					$ro->prefix = $create_payment['prefix'];
					$ro->name = $create_payment['name'];
					$ro->payment_code = $create_payment['payment_code'];
					$ro->type = $create_payment['type'];
					$ro->status = $create_payment['status'];
					$ro->amount = $create_payment['expected_amount'];
					$ro->expiration_date = $create_payment['expiration_date'];
					$ro->save();

					$this->response['data_ro'] = $ro;

				} else if ($type == "virtual_account") {
					$data_va = [
						"bank_code" => $method,
						"name" => $this->request->nama,
						"expected_amount" => $harga_qurban + $sedekah_qurban
					];
					$create_payment = $this->xendit->fixed_virtual_account($data_va);
			
					$va = new VA;
					$va->transaction_type = "QURBAN";
					$va->transaction_id = $qurban->id;
					$va->va_id = $create_payment['id'];
					$va->owner_id = $create_payment['owner_id'];
					$va->external_id = $create_payment['external_id'];
					$va->bank_code = $create_payment['bank_code'];
					$va->merchant_code = $create_payment['merchant_code'];
					$va->name = $this->request->nama;
					$va->account_number = $create_payment['account_number'];
					$va->amount = $harga_qurban;
					$va->status = $create_payment['status'];
					$va->save();

					$this->response['data_va'] = $va;
				}
			}
			
			if ($type != "e_wallet") {
				$email = $this->request->email;
				$triger = "transaction_created";
				// Mail::to($email)->send(new EmailTransaction($email, $triger, $method, "QURBAN", $qurban_id));
			}
		});
		return response()->json($this->response);
	}

	public function updateStatusQurban()
	{
		$order_id = $this->request->id;

		$id = substr($order_id, 1);
		$qurban = Qurban::findOrFail($id);
		$qurban->status = $this->request->status;
		$qurban->metode_pembayaran = $this->request->payment_type;
		$qurban->snap_token = $this->request->snap_token;
		$qurban->save();

		return response()->json($qurban);
	}

	public function deleteQurban()
	{
		$snapToken = $this->request->iDtoken;
		$iDtransaction = $this->request->iDtransaction;
		if (isset($iDtoken))
			$qurban = Qurban::where('snap_token', $snapToken)->first();
		else
			$qurban = Qurban::find($iDtransaction);

		if($qurban){
			$qurbanInsert = QurbanBackup::create([
				'id'				=> $qurban->id,
				'category_id'		=> $qurban->category_id,
				'id_produk_qurban'  => $qurban->id_produk_qurban,
				'user_id'           => $qurban->user_id,
				'guest_id'			=> $qurban->guest_id,
				'member_id'         => $qurban->member_id,
				'nama_pequrban'		=> $qurban->nama_pequrban,
				'alamat_pequrban'   => $qurban->alamat_pequrban,
				'total_qurban'      => $qurban->total_qurban,
				'status'			=> $qurban->status,
				'metode_pembayaran' => '-',
				'batas_pembayaran'  => Null,
				'snap_token' 		=> $qurban->snap_token,
				'mid_order_id'      => $qurban->mid_order_id,
				'device' 			=> $qurban->device
			]);
		}

		$log = new Log;
		$log->log_id 		     = $qurban->snap_token;
		$log->donasi_id 		 = null;
		$log->zakat_id  		 = null;
		$log->qurban_id 		 = $qurban->id;
		$log->status_transaction = 'Delete Transaction';
		$log->transaction_time   = date('Y-m-d H:i:s');
		$log->status_code 		 = '200';
		$log->status_message 	 = 'Delete';
		$log->transaction_approve= 'Administrator';
		$log->save();

		Qurban::findOrFail($qurban->id)->delete();

		return response()->json('Delete Transaction ........');
	}

	public function sendNotifAfterPaid($id, $user_id, $type, $status, $name, $phone_number, $donasi, $email, $order_id){
		$device = DeviceToken::where('user_id', $user_id)->orderBy('id','desc')->get();
		if($status == 'settlement'){
			$parameter = Parameter::where('key', 'notif_settlement')->first();
//			$split = explode('".$program."', $parameter->value);
//			$split2 = explode('".$donatur."', $split[0]);

			$param_template_wa = [
				"bapak/ibu", $name, $type, "Rp. ".number_format($donasi)
			];

            $body = $parameter->value;
            $body = str_replace('".$donatur."',$name,$body);
			if($type == "Qurban"){
                $body = str_replace('".$type."',$type,$body);
//				$body = $split2[0].$name.$split2[1]."atas ".$type." anda".$split[1];
			}
			else if($type == "Zakat"){
                $body = str_replace('".$type."',$type,$body);
//				$body = $split2[0].$name.$split2[1]."atas ".$type." anda".$split[1];
			}
			else{
                $body = str_replace('".$type."',"Donasi",$body);
//				$body = $split2[0].$name.$split2[1]."atas donasi anda untuk ".$type.$split[1];

				$param_template_wa[2] = "Program " . $type;
			}
            $body = str_replace('".$program."',$type,$body);
            $body = str_replace('".$donasi."',number_format($donasi),$body);
            
//            Pesan Notif di db
//            Terimakasih bapak/ibu ".$donatur." sudah ber".$type." di Mizan Amanah Lembaga Amil Zakat Nasional untuk Program ".$program." senilai Rp.".$donasi."
//            Semoga Alloh Menggatikannya dengan Kehidupan yang Barokah.
//            WA Center 08111873334
//            www.mizanamanah.or.id
			
			$this->sendMessageAfterPaidAPI($phone_number, $body, $param_template_wa);
			$this->pixelPurchase('IDR',$donasi,$email,$order_id);
		}else{
			$parameter = Parameter::where('key', 'notif_expire')->first();
			$split = explode('".$program."', $parameter->value);
			$split2 = explode('".$donatur."', $split[0]);

			if($type == "Qurban"){
				$body = $split2[0].$name.$split2[1]."atas ".$type." anda".$split[1];
			}
			else if($type == "Zakat"){
				$body = $split2[0].$name.$split2[1]."atas ".$type." anda".$split[1];
			}
			else{
				$body = $split2[0].$name.$split2[1]."donasi anda untuk ".$type.$split[1];
			}
		}

		if ($this->notif_app && $device) {
            foreach ($device as $d) {
                $data = array(
                    'to' => $d->device_token,
                    'notification' => array(
                                        "title"=> $parameter->judul,
                                        "body"=> $body,
                                        "sound" => "default",
                                        "click_action" =>"NANTI_GANTI_ACTION",
                                        "type" => 1
                                        ),
                    'data' => array(
                                "title"=> $parameter->judul,
                                "body"=> $parameter->value,
                                "sound" => "default",
                                "id" => $id,
                                "type_category" => "NantiGanti",
                                "type" => 1
                                ),
                    'priority' => 10
                );
                
                $options = array(
                    'http'      => array(
                    'method'    => 'POST',
                    'content'   => json_encode( $data ),
                    'header'    =>  "Content-Type: application/json\r\n" .
                                    "Authorization : key=AAAAp9EnSKE:APA91bFbvvy5hIpfg7OUdn9-V4aRXitJlOKoxuziDFpgWW5srq34IYfs9oUrhdx--JX1Fah_kQ7l6l_D9LzK58kdvRgUjtqAqz6cHM0MNaXMzwqAlX9-UMukZnOF3YGrLmVGQXMrKkUF/json\r\n" .
                                    "Accept: application/json\r\n"
                    )
                );
            
                $url = "https://fcm.googleapis.com/fcm/send";
                $context  = stream_context_create( $options );
                $result = file_get_contents( $url , false, $context );
                $response = json_decode( $result );
            }
		}
	}

	public function sendMessageAfterPaid($phone_number, $message, $param_template_wa)
	{
		$phone_number_62 = "62" . substr($phone_number, 1);
		$client = new \GuzzleHttp\Client();

		$url_api = env("APP_URL") . "/api/wabiz";
		// Check if phone number is valid WA contact
		$check_contact = $client->post($url_api . "/check-contact", [
            "headers" => [
                "Content-Type"  => "application/json"
            ],
            "json" => [
                "to" => $phone_number_62
            ]
        ]);
		$check_contact = json_decode($check_contact->getBody()->getContents());
		// End of check phone number
			
        if ($check_contact->status == 200) {
			/* ----- WA ----- */
			$send_wa = $client->post($url_api . "/send-template-message", [
				"headers" => [
					"Content-Type"  => "application/json"
				],
				"json" => [
					"to"=> $phone_number_62,
					"policy"=> "deterministic",
					"code"=> "id",
					"params"=> [
						[ "default" => $param_template_wa[0] ],
						[ "default" => $param_template_wa[1] ],
						[ "default" => $param_template_wa[2] ],
						[ "default" => $param_template_wa[3] ]
					]
				]
			]);
			/* ----- End of WA ----- */
		} else {
			/* ----- SMS ----- */
			header("content-type: text/plain");

			// Check for allowed provider
			$is_allowed = false;
			$allow_number = Parameter::where('key','sms_allow_phone_number')->first();
			$allow_number = explode(',', $allow_number->value);
			for ($i=0; $i<count($allow_number); $i++)
			{
				if ($allow_number[$i] === substr($phone_number, 0, 4)) {
					$is_allowed = true;
					break;
				}
			}

			if (!$is_allowed) return;

			/* Request To Bulk Server */
			$drv = new BulkDriver;
			$drv->setNormalPriority();
			$drv->addSMS($phone_number, $message);
			$response = $drv->request();
			$response_xml = simplexml_load_string($response['response_body']);

			$log = new LogSMS;
			$log->to_number = $phone_number;
			$log->message = $message;
			$log->response_code = $response['response_code'];
			$log->response_desc = $response['response_description'];
			$log->status_code = $response_xml->status_code;
			$log->status_text = $response_xml->status_text;
			$log->transaction_id = $response_xml->transaction_id;
			$log->save();
			
			/* Print Out the Bulk Server Response */
			// print "Response Code => " . $response['response_code'] . "\n\n" ; 
			// print "Response Code Description => " . $response['response_description'] . "\n\n" ; 
			// print "Response XML Body :\n" . $response['response_body'] . "\n\n" ; 
			/* ----- End of SMS ----- */
		}
	}

	public function sendMessageAfterPaidAPI($phone_number, $message, $param_template_wa)
	{	
		$client = new \GuzzleHttp\Client();

		$url_api = "https://api.mizanamanah.or.id/api";
		$send_message = $client->post($url_api . "/send-message", [
            "headers" => [
                "Content-Type"  => "application/json"
            ],
            "json" => [
				"phone_number" => $phone_number,
				"message" => $message,
				"param_template_wa" => $param_template_wa
            ]
        ]);
		$send_message = json_decode($send_message->getBody()->getContents());

		return $send_message;
	}

	private function pixelPurchase($currency, $value, $email, $order_id)
	{
		$app_secret = '368420bf6942ac85012cb0c32d7deca3';
		$app_id = '2711941588905598';
		$pixel_id = '598052197449945';

		// $access_token = 'a16b2a599284a90ecf813a1966bbcbdf';
		$access_token = "EAAmifxsO7n4BAPSECRgfJ9GCjf5cABaHbClO569YnV86GpjBJmC9lLQ5TrxuDQ7pP7p85mxcUf0kRqH3U7LavaZAlxptnfta1OBBeGQuoYjJmZCr1QNFLFEUH5nK66BVifKfgUaVLT7eOgsh4IeBJk9gBFT6H2poZCrg0EEA0FHrccoAZAC5ZAlMpDZAlwsGsZD";

		// $fb = new Facebook([
		//   'app_id' => $app_id,
		//   'app_secret' => $app_secret,
		// ]);

		// $helper = $fb->getRedirectLoginHelper();
		// if (!isset($_SESSION['facebook_access_token'])) {
		//   	$_SESSION['facebook_access_token'] = null;
		// }
		
		// if (!$_SESSION['facebook_access_token']) {
		// 	$helper = $fb->getRedirectLoginHelper();
		// 	try {
		// 		$_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
		// 		$access_token = (string) $helper->getAccessToken();
		// 	} catch(FacebookResponseException $e) {
		// 		// When Graph returns an error
		// 		echo 'Graph returned an error: ' . $e->getMessage();
		// 		exit;
		// 	} catch(FacebookSDKException $e) {
		// 		// When validation fails or other local issues
		// 		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		// 		exit;
		// 	}
		// }
		
		
		$api = Api::init($app_id, $app_secret, $access_token);
		$api->setLogger(new CurlLogger());
		
		// $fields = array(
		// 	'code',
		// );
		// $params = array(
		// );
		// echo json_encode((new AdsPixel($pixel_id))->getSelf(
		// 	$fields,
		// 	$params
		// )->exportAllData(), JSON_PRETTY_PRINT);
		
		$user_data = (new UserData())
			// It is recommended to send Client IP and User Agent for ServerSide API Events.
			->setClientIpAddress($_SERVER['REMOTE_ADDR'])
			->setClientUserAgent($_SERVER['HTTP_USER_AGENT'])
			->setEmail($email)
			->setExternalId($order_id);

		$custom_data = (new CustomData())
			->setCurrency($currency)
			->setValue($value);

		$event = (new Event())
			->setEventName('Purchase')
			->setEventTime(time())
			->setUserData($user_data)
			->setCustomData($custom_data);

		$events = array();
		array_push($events, $event);

		$request = (new EventRequest($pixel_id))
			->setEvents($events);
		$response = $request->execute();
	}

	private function missingTransaction($notif)
	{
		$userid = Parameter::where('key','missing_trans__user')->first()->value;
		$programid = Parameter::where('key','missing_trans__program')->first()->value;
		$categoryid = Parameter::where('key','missing_trans__zakat')->first()->value;
		
		$status 				= $notif->status_code;
		$status_message			= $notif->status_message;
		$transaction_id 		= $notif->transaction_id;  
		$order_id 				= $notif->order_id;
		$gross_amount 			= $notif->gross_amount;
		$payment_type 			= $notif->payment_type;
		$transaction_time		= $notif->transaction_time;
		$transaction_settlement = $notif->transaction_settlement;
		$transaction 			= $notif->transaction_status;
		$type 					= $notif->payment_type;
		$fraud 					= $notif->fraud_status;
		$signature_key 			= $notif->signature_key;

		$id = substr($order_id, 1);
		if (substr($order_id, 0,1) == 'P') {
			$donation_bu = DonasiBackup::with('detail.program.program_lang', 'user', 'guest')->find($id);

			$donasi = Donasi::create([
				'id'				=> ($donation_bu) ? $donation_bu->id : null,
				'user_id'           => ($donation_bu) ? $donation_bu->user_id : $userid,
				'guest_id'          => ($donation_bu) ? $donation_bu->guest_id : 0,
				'total_donasi'      => ($donation_bu) ? $donation_bu->total_donasi : $gross_amount,
				'metode_pembayaran' => ($donation_bu) ? $donation_bu->metode_pembayaran : $type,
				'status'			=> ($donation_bu) ? $donation_bu->status : $status,
				'batas_pembayaran'  => ($donation_bu) ? $donation_bu->batas_pembayaran : null,
				'snap_token'        => ($donation_bu) ? $donation_bu->snap_token : "-",
				'mid_order_id'      => ($donation_bu) ? $donation_bu->mid_order_id : "-",
				'device'			=> ($donation_bu) ? $donation_bu->device : "-"
			]);
			$donasi_detail = DonasiDetail::create([
				'id'			=> ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->id : null,
				'donasi_id'     => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->donasi_id : $donasi->id,
				'program_id'    => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->program_id : $programid,
				'akad_id'       => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->akad_id : 0,
				'agency_id'     => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->id_agency : 0,
				'nilai_donasi'  => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->nilai_donasi : $gross_amount,
				'komentar'      => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->komentar : '-',
				'anonim'        => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->anonim : 0,
				'verifikasi'    => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->verifikasi : 0,
			]);

		} else if (substr($order_id, 0,1) == 'Q') {
			$donation_bu = QurbanBackup::with('user', 'guest')->find($id);
			
			if ($donation_bu) {
				$qurban = Qurban::create([
					'id'				=> $donation_bu->id,
					'no_invoice'		=> $donation_bu->no_invoice,
					'category_id'		=> $donation_bu->category_id,
					'id_produk_qurban'  => $donation_bu->id_produk_qurban,
					'user_id'           => $donation_bu->user_id,
					'guest_id'			=> $donation_bu->guest_id,
					'member_id'         => $donation_bu->member_id,
					'nama_pequrban'		=> $donation_bu->nama_pequrban,
					'alamat_pequrban'   => $donation_bu->alamat_pequrban,
					'harga_qurban'		=> $donation_bu->harga_qurban,
					'sedekah_qurban'	=> $donation_bu->sedekah_qurban,
					'total_qurban'      => $donation_bu->total_qurban,
					'status'			=> $donation_bu->status,
					'metode_pembayaran' => $donation_bu->metode_pembayaran,
					'batas_pembayaran'  => $donation_bu->batas_pembayaran,
					'snap_token' 		=> $donation_bu->snap_token,
					'mid_order_id'      => $donation_bu->mid_order_id,
					'device' 			=> $donation_bu->device,
					'verifikasi'		=> $donation_bu->verifikasi
				]);
			}

		} else if (substr($order_id, 0,1) == 'Z') {
			$donation_bu = ZakatBackup::with('user', 'guest', 'detail.category')->find($id);

			$zakat = Zakat::create([
				'id'				=> ($donation_bu) ? $donation_bu->id : null,
				'user_id'           => ($donation_bu) ? $donation_bu->user_id : $userid,
				'guest_id'			=> ($donation_bu) ? $donation_bu->guest_id : 0,
				'member_id'         => ($donation_bu) ? $donation_bu->member_id : 0,
				'total_zakat'       => ($donation_bu) ? $donation_bu->total_zakat : $gross_amount,
				'metode_pembayaran' => ($donation_bu) ? $donation_bu->metode_pembayaran : $type,
				'batas_pembayaran'  => ($donation_bu) ? $donation_bu->batas_pembayaran : null,
				'status'			=> ($donation_bu) ? $donation_bu->status : $status,
				'mid_order_id'      => ($donation_bu) ? $donation_bu->mid_order_id : '-',
				'device'			=> ($donation_bu) ? $donation_bu->device : '-'
			]);
			$zakat_detail = ZakatDetail::create([
				'id'			=> ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->id : null,
				'zakat_id'      => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->zakat_id : $zakat->id,
				'program_id'    => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->program_id : null,
				'akad_id'       => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->akad_id : 0,
				'id_category'   => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->id_category : $categoryid,
				'nilai_zakat'   => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->nilai_zakat : $gross_amount,
				'komentar'      => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->komentar : '-',
				'anonim'        => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->anonim : 0,
				'verifikasi'    => ($donation_bu && $donation_bu->detail) ? $donation_bu->detail->verifikasi : 0,
			]);
		}
	}
}
