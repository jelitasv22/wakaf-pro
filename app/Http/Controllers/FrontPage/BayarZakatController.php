<?php

namespace App\Http\Controllers\Frontpage;

use Illuminate\Http\Request;
use App\Veritrans\Midtrans;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class BayarZakatController extends Controller 
{

    private $client;
    private $yayasan;
	private $language;
	private $theme;
	private $slider;
	private $statistik;
	private $program;
	private $posting;
	private $menu;
    private $color;
    private $qurban;
	private $profile;
    private $kategori_zakat;
    private $payment_vendors;
    public function __construct()
    {
        $language   = Session::get('language_id');
	    $client 	= new \GuzzleHttp\Client();
        $yayasan    = $client->request('GET', ENV('API_URL').'/api/ngo-yayasan/'.ENV('YAYASAN_KEY'));
	    $theme 		= $client->request('GET', ENV('API_URL').'/api/ngo-theme?yayasan='.ENV('YAYASAN_KEY'));
	    $slider 	= $client->request('GET', ENV('API_URL').'/api/ngo-slider?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
	    $program  	= $client->request('GET', ENV('API_URL').'/api/ngo-program?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
	    $posting    = $client->request('GET', ENV('API_URL').'/api/ngo-posting?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
		$menu		= $client->request('GET', ENV('API_URL').'/api/ngo-menu?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
		$color		= $client->request('GET', ENV('API_URL').'/api/ngo-theme/color?yayasan='.ENV('YAYASAN_KEY'));
		$qurban     = $client->request('GET', ENV('API_URL').'/api/ngo-qurban/pages?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
        $profile    = $client->request('GET', ENV('API_URL').'/api/ngo-pages/get-profile?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
        $payment_vendors = $client->request('GET', ENV('API_URL').'/api/payment_vendors/categories');

        $this->client    = $client; 
	    $this->language  = $language;
        $this->yayasan   = json_decode($yayasan->getBody()->getContents());
	    $this->theme 	 = json_decode($theme->getBody()->getContents());
	    $this->slider 	 = json_decode($slider->getBody()->getContents());
	    $this->program   = json_decode($program->getBody()->getContents());
	    $this->posting   = json_decode($posting->getBody()->getContents());
	    $this->menu      = json_decode($menu->getBody()->getContents());
	    $this->color     = json_decode($color->getBody()->getContents());
        $this->qurban    = json_decode($qurban->getBody()->getContents());
        $this->profile   = json_decode($profile->getBody()->getContents());
        $this->payment_vendors = json_decode($payment_vendors->getBody()->getContents());

//        dd($this->payment_vendors);

        $kategori_zakat = $client->request('GET', ENV('API_URL').'/api/zakat/category?lang='.$language.'&id_yayasan='.$this->yayasan->data->id);
        $this->kategori_zakat = json_decode($kategori_zakat->getBody()->getContents());

    }


    public function index(Request $request)
    {

        $language_ses = Session::get('language_id');
        $theme        = $this->theme;
        $menu         = $this->menu;
        $color        = $this->color;
        $program      = $this->program;
        $qurban       = $this->qurban;
        $posting      = $this->posting;
        $profile      = $this->profile;
        $yayasan      = $this->yayasan;
        $list_yayasan = $this->yayasan;
        $slider       = $this->slider;
        $data_list    = $this->qurban;
        $yayasan_list = $this->yayasan;
        $kategori_zakat = $this->kategori_zakat->data;




//        dd($kategori_zakat);

    	return view('front_page.'.$theme->theme->key.'.pages.zakat.index', compact(
    		'slider','program','posting','menu','color','language_ses','yayasan', 'kategori_zakat'
    	));
    }





    public function step(Request $request, $step){

        $nominalZakat = request()->input('nominal-zakat');
        $nominalZakat = (int) $nominalZakat;

        $jenisZakat   = request()->input('kategori');

        $language_ses = Session::get('language_id');
        $theme        = $this->theme;
        $menu         = $this->menu;
        $color        = $this->color;
        $program      = $this->program;
        $qurban       = $this->qurban;
        $posting      = $this->posting;
        $profile      = $this->profile;
        $yayasan      = $this->yayasan;
        $list_yayasan = $this->yayasan;
        $slider       = $this->slider;
        $data_list    = $this->qurban;
        $yayasan_list = $this->yayasan;
        $category_zakat = $this->qurban;
        $setup_zakat    = $this->qurban;
        $payment_vendors = $this->payment_vendors->data;

        if ($step == 1) {
            return view('front_page.'.$theme->theme->key.'.pages.zakat.index', compact(
                'slider','program','posting','menu','color','language_ses','yayasan'
            ));
        }elseif ($step == 2) {
                
            return view('front_page.'.$theme->theme->key.'.pages.zakat.step1', compact(
                'slider','program','posting','menu','color','language_ses','yayasan', 'nominalZakat', 'jenisZakat', 'payment_vendors'
            ));
        } else {
            // redirect 404
            return response()->status(404);
        }
    }


    public function processZakatTransaction(Request $request){
        $client = new \GuzzleHttp\Client();

        
        // check if session uid is exist
        if (Session::has('uid')) {
            $uid = Session::get('uid');
        } else {
            $uid = null;
        }
        
        $key_yayasan = ENV('YAYASAN_KEY');
        $jumlah_zakat = $request->nominal_zakat_field;
        $payment_type = $request->tipe_pembayaran;
        $vendor = $request->id_pembayaran;
        $nama_lengkap = $request->nama_lengkap_field;
        $email = $request->email_field;
        $no_hp = $request->phone_field;
        $device = "Web";
        $id_zakat = $request->id_zakat;
        $komentar = $request->komentar_field;
        $verify = $request->checkbox_verify;

        if ($verify == "on") {
            $verify = 1;
        } else {
            $verify = 0;
        }

        if ($komentar == null) {
            $komentar = "";
        }

        $payment_method = $request->method_payment;
        $no_rekening = $request->no_rek_payment;


        dd($uid, $key_yayasan, $jumlah_zakat, $payment_type,$vendor, $nama_lengkap, $email, $no_hp, $device, $id_zakat, $komentar, $verify, $payment_method, $no_rekening);
        
        $response = $client->request('POST', ENV('API_URL').'/api/snap-token-midtrans-zakat', [
            'form_params' => [
                'key_yayasan' => $key_yayasan,
                'jumlah_zakat' => $jumlah_zakat,
                'payment_type' => $payment_type,
                'user_id' => $uid,
                'no_handphone' => $no_hp,
                'email' => $email,
                'nama_lengkap' => $nama_lengkap,
                'device' => $device,
                'category_id' => $id_zakat,
                'komentar' => $komentar,
                'anonim' => 0,
                'verifikasi'=> $verify,
                'vendor' => $vendor,
                'payment_method' => $payment_method,
                'no_rekening' => $no_rekening

            ],
            "headers" => [
                // auth
                "Authorization"  => Session::get('token')
            ]
        ]);


        $response = json_decode($response->getBody()->getContents());

        

        dd($response);
    }
}


?>