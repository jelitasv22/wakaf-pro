<?php

namespace App\Http\Controllers\Frontpage;

use Illuminate\Http\Request;
use App\Veritrans\Midtrans;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Entities\Admin\modul\Guest;
use App\Entities\Admin\core\MenuFrontPage;
use App\Entities\Admin\core\MenuFrontPageLanguage;
use App\Entities\Admin\core\Theme;
use App\Entities\Admin\modul\Zakat;
use App\Entities\Admin\modul\ZakatDetail;

class ZakatController extends Controller
{
	public function __construct()
	{
		Midtrans::$serverKey = env('MIDTRANS_SERVER_KEY', '');
		Midtrans::$isProduction = env('MIDTRANS_PRODUCTION', false);

		if(\App::getLocale() == "id"){
			$id_lang = 1;
		}else{
			$id_lang = 2;
		}

		$this->language = $id_lang;

		$this->theme = Theme::where('status', 1)->first();

		$this->header = MenuFrontPage::join('menu_front_page_language', 'menu_front_page.id', '=', 'menu_front_page_language.id_menu_front_page')->join('category', 'menu_front_page.id_category', '=', 'category.id')->where('category.category', "Menu Header")->where('menu_front_page.id_sub_menu', 0)->where('menu_front_page_language.id_language', $id_lang)->orderBy('menu_front_page.sort_order', 'asc')->get();

		$this->footer = MenuFrontPage::join('menu_front_page_language', 'menu_front_page.id', '=', 'menu_front_page_language.id_menu_front_page')->join('category', 'menu_front_page.id_category', '=', 'category.id')->where('category.category', "Menu Footer")->where('menu_front_page.id_sub_menu', 0)->where('menu_front_page_language.id_language', $id_lang)->orderBy('menu_front_page.sort_order', 'asc')->get();
	
	}

	public function token() 
	{
		error_log('masuk ke snap token adri ajax');
		$midtrans = new Midtrans;
		$transaction_details = array(
			'order_id'       => uniqid(),
			'gross_amount'   => Input::get('jumlah_zakat')
		);
		
        // Populate items
		$items = [
			array(
				'id'            => 'zakat1',
				'price'         => Input::get('jumlah_zakat'),
				'quantity'      => 1,
				'name'          => Input::get('category_produk')
			)
		];

        // Populate customer's billing address
		$billing_address = array(
			'first_name'        => Input::get('nama'),
			'last_name'         => '',
			'address'           => '',
			'city'              => '',
			'postal_code'       => '',
			'phone'             => Input::get('no_telp'),
			'country_code'      => 'IDN'
		);

        // Populate customer's shipping address
		$shipping_address = array(
			'first_name'        => Input::get('nama'),
			'last_name'         => '',
			'address'           => '',
			'city'              => '',
			'postal_code'       => '',
			'phone'             => Input::get('no_telp'),
			'country_code'      => 'IDN'
		);

        // Populate customer's Info
		$customer_details = array(
			'first_name'        => Input::get('nama'),
			'last_name'         => '',
			'email'             => Input::get('email'),
			'phone'             => Input::get('no_telp'),
			'billing_address'   => $billing_address,
			'shipping_address'  => $shipping_address
		);

        // Data yang akan dikirim untuk request redirect_url.
		$transaction_data = array(
			'transaction_details'=> $transaction_details,
			'item_details'       => $items,
			'customer_details'   => $customer_details,
			'enabled_payments'	 => array('gopay')
		);

		try
		{
			$snap_token = $midtrans->getSnapToken($transaction_data);
			echo $snap_token;
		} 
		catch (Exception $e) 
		{   
			return $e->getMessage;
		}
	}

	public function finish(Request $request)
	{
		$result = $request->input('result_data');
		$result = json_decode($result);

		$zakat = Zakat::create([
			'user_id'           => $request->input('id'),
			'member_id'         => 0,
			'total_zakat'       => $request->input('donation'),
			'status'            => $result->transaction_status,
			'metode_pembayaran' => $result->payment_type,
			'batas_pembayaran'  => Null,
			'snap_token'        => $result->transaction_id,
			'mid_order_id'      => $result->order_id
		]);

		$zakat_detail = ZakatDetail::create([
			'zakat_id'      => $zakat->id,
			'program_id'    => Null,
			'akad_id'       => 0,
			'id_category'   => $request->input('category_id'),
			'nilai_zakat'   => $request->input('donation'),
			'komentar'      => ($request->input('komentar'))?$request->input('komentar'):'-',
			'anonim'        => ($request->input('anonim'))?$request->input('anonim'):0,
			'verifikasi'    => ($request->input('verifikasi'))?$request->input('verifikasi'):0,
		]);


		return redirect()->back();
	}

	public function setSuccess()
	{
		$header = $this->header;
		$footer = $this->footer;
		$language = $this->language;

		return view('front_page.'.$this->theme->key.'.pages.notif.success_zakat', compact('header','footer','language'));
	}

	public function setWait()
	{
		$header = $this->header;
		$header_body = $this->header_body;
		$footer = $this->footer;
		$footer_sub = $this->footer_sub;
		$language = $this->language;

		return view('front_page.'.$this->theme->key.'.pages.notif.wait_zakat', compact('header','header_body','footer','language', 'footer_sub'));
	}

	public function setPending()
	{
		$header = $this->header;
		$footer = $this->footer;
		$language = $this->language;

		return view('front_page.'.$this->theme->key.'.pages.notif.pending_zakat', compact('header','footer','language'));
	}

	public function setKonfirmasi()
	{
		if(\App::getLocale() == "id"){
			$id_lang = 1;
		}else{
			$id_lang = 2;
		}

		$header = $this->header;
		$footer = $this->footer;
		$language = $this->language;

		$bank = Bank::where('deleted_at', null)->get();

		if(session::has('id') == null){
			$data = Program::select('program.id as programid','programlanguage.judul','donasi.*','guest.nama_lengkap')
			->join('programlanguage', 'programlanguage.id_program', '=', 'program.id')
			->leftjoin('category', 'category.id', '=', 'program.id_category')
			->leftjoin('donasi_detail', 'donasi_detail.program_id', '=', 'program.id')
			->leftjoin('donasi', 'donasi.id', '=', 'donasi_detail.donasi_id')
			->leftjoin('guest', 'guest.id', '=', 'donasi.guest_id')
			->where('programlanguage.id_language', $id_lang)
			->orderBy('donasi.guest_id', 'DESC')
			->first();

		}else{
			$data = Program::select('program.id as programid','programlanguage.judul','donasi.*','users.nama_lengkap')
			->join('programlanguage', 'programlanguage.id_program', '=', 'program.id')
			->leftjoin('category', 'category.id', '=', 'program.id_category')
			->leftjoin('donasi_detail', 'donasi_detail.program_id', '=', 'program.id')
			->leftjoin('donasi', 'donasi.id', '=', 'donasi_detail.donasi_id')
			->leftjoin('users', 'users.id', '=', 'donasi.user_id')
			->where('programlanguage.id_language', $id_lang)
			->where('donasi.user_id', session::get('id'))
			->orderBy('donasi.guest_id', 'DESC')
			->first();
		}

		return view('front_page.'.$this->theme->key.'.pages.notif.konfirmasi', compact('header','footer','language','data','bank'));
	}

	public function setKonfirmasi2()
	{
		$header = $this->header;
		$footer = $this->footer;
		$language = $this->language;

		return view('front_page.'.$this->theme->key.'.pages.notif.notif_konfirmasi', compact('header','footer','language'));
	}
}
