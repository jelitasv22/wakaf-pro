<?php

namespace App\Http\Controllers\FrontPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class TabunganQurbanController extends Controller
{
    private $client;
    private $yayasan;
	private $language;
	private $theme;
	private $slider;
	private $statistik;
	private $program;
	private $posting;
	private $menu;
    private $color;
    private $qurban;
	private $profile;
    private $kategori_zakat;
    private $payment_vendors;
    private $user_profile;
    public function __construct()
    {
        $language   = Session::get('language_id');
	    $client 	= new \GuzzleHttp\Client();
        $yayasan    = $client->request('GET', ENV('API_URL').'/api/ngo-yayasan/'.ENV('YAYASAN_KEY'));
	    $theme 		= $client->request('GET', ENV('API_URL').'/api/ngo-theme?yayasan='.ENV('YAYASAN_KEY'));
	    $slider 	= $client->request('GET', ENV('API_URL').'/api/ngo-slider?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
	    $program  	= $client->request('GET', ENV('API_URL').'/api/ngo-program?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
	    $posting    = $client->request('GET', ENV('API_URL').'/api/ngo-posting?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
		$menu		= $client->request('GET', ENV('API_URL').'/api/ngo-menu?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
		$color		= $client->request('GET', ENV('API_URL').'/api/ngo-theme/color?yayasan='.ENV('YAYASAN_KEY'));
		$qurban     = $client->request('GET', ENV('API_URL').'/api/ngo-qurban/pages?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
        $profile    = $client->request('GET', ENV('API_URL').'/api/ngo-pages/get-profile?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
        $payment_vendors = $client->request('GET', ENV('API_URL').'/api/payment_vendors/categories');
        $user_profile = $client->get(ENV('API_URL') . "/api/user", [
            "headers" => [
                // auth
                "Authorization"  => Session::get('token')
                
            ]
        ]);

        $this->client    = $client; 
	    $this->language  = $language;
        $this->yayasan   = json_decode($yayasan->getBody()->getContents());
	    $this->theme 	 = json_decode($theme->getBody()->getContents());
	    $this->slider 	 = json_decode($slider->getBody()->getContents());
	    $this->program   = json_decode($program->getBody()->getContents());
	    $this->posting   = json_decode($posting->getBody()->getContents());
	    $this->menu      = json_decode($menu->getBody()->getContents());
	    $this->color     = json_decode($color->getBody()->getContents());
        $this->qurban    = json_decode($qurban->getBody()->getContents());
        $this->profile   = json_decode($profile->getBody()->getContents());
        $this->user_profile = json_decode($user_profile->getBody()->getContents());
        $this->payment_vendors = json_decode($payment_vendors->getBody()->getContents());

        $kategori_zakat = $client->request('GET', ENV('API_URL').'/api/zakat/category?lang='.$language.'&id_yayasan='.$this->yayasan->data->id);
        $this->kategori_zakat = json_decode($kategori_zakat->getBody()->getContents());

    }


    public function index(Request $request)
    {

        $language_ses = Session::get('language_id');
        $theme        = $this->theme;
        $menu         = $this->menu;
        $color        = $this->color;
        $program      = $this->program;
        $qurban       = $this->qurban;
        $posting      = $this->posting;
        $profile      = $this->profile;
        $yayasan      = $this->yayasan;
        $list_yayasan = $this->yayasan;
        $slider       = $this->slider;
        $data_list    = $this->qurban;
        $yayasan_list = $this->yayasan;
        $category_zakat = $this->qurban;
        $setup_zakat    = $this->qurban;
        $kategori_zakat = $this->kategori_zakat->data;

    	return view('front_page.'.$theme->theme->key.'.pages.tabungan-qurban.index', compact(
    		'slider','program','posting','menu','color','language_ses','yayasan', 'kategori_zakat'
    	));
    }

    public function realisasiQurban(Request $request){
        $language_ses = $this->language;
        $yayasan      = $this->yayasan;
        $theme        = $this->theme;
        $user_profile = $this->user_profile;
        $menu         = $this->menu;

        return view('front_page.'.$theme->theme->key.'.pages.tabungan-qurban.realisasi-qurban', compact(
            'language_ses','theme','menu','yayasan', 'user_profile',
        ));
    }
}
