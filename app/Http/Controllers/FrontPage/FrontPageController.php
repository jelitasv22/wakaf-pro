<?php

namespace App\Http\Controllers\FrontPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Redirect;

class FrontPageController extends Controller
{
	private $client;
    private $yayasan;
	private $language;
	private $theme;
	private $slider;
	private $statistik;
	private $program;
	private $posting;
	private $menu;
    private $color;
    private $qurban;
    private $profile;

	public function __Construct(Request $request)
	{
		$language   = Session::get('language_id');
	    $client 	= new \GuzzleHttp\Client();
        $yayasan    = $client->request('GET', ENV('API_URL').'/api/ngo-yayasan/'.ENV('YAYASAN_KEY'));
	    $theme 		= $client->request('GET', ENV('API_URL').'/api/ngo-theme?yayasan='.ENV('YAYASAN_KEY'));
	    $slider 	= $client->request('GET', ENV('API_URL').'/api/ngo-slider?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
	    $program  	= $client->request('GET', ENV('API_URL').'/api/ngo-program?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
	    $posting    = $client->request('GET', ENV('API_URL').'/api/ngo-posting?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
		$menu		= $client->request('GET', ENV('API_URL').'/api/ngo-menu?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
		$color		= $client->request('GET', ENV('API_URL').'/api/ngo-theme/color?yayasan='.ENV('YAYASAN_KEY'));
		$qurban     = $client->request('GET', ENV('API_URL').'/api/ngo-qurban/pages?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
        $profile    = $client->request('GET', ENV('API_URL').'/api/ngo-pages/get-profile?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));

        $this->client    = $client;
	    $this->language  = $language;
        $this->yayasan   = json_decode($yayasan->getBody()->getContents());
	    $this->theme 	 = json_decode($theme->getBody()->getContents());
	    $this->slider 	 = json_decode($slider->getBody()->getContents());
	    $this->program   = json_decode($program->getBody()->getContents());
	    $this->posting   = json_decode($posting->getBody()->getContents());
	    $this->menu      = json_decode($menu->getBody()->getContents());
	    $this->color     = json_decode($color->getBody()->getContents());
        $this->qurban    = json_decode($qurban->getBody()->getContents());
        $this->profile   = json_decode($profile->getBody()->getContents());
	}

	public function switch_language(Request $request, $id)
	{
		Session::put('language_id', $id);
		return redirect()->back();
	}

    public function login()
    {
        $language_ses = $this->language;
        $yayasan      = $this->yayasan;
    	$theme  	  = $this->theme;
    	$slider 	  = $this->slider;
    	$program      = $this->program;
    	$posting      = $this->posting;
    	$menu         = $this->menu;
    	$color        = $this->color;

        if (Session::get('token')) {
            return redirect()->route('dashboard.donatur');
        }

        return view('front_page.'.$theme->theme->key.'.login.index',  compact(
    		'slider','program','posting','menu','color','language_ses','yayasan'
    	));
    }

    public function register()
    {
        $language_ses = $this->language;
        $yayasan      = $this->yayasan;
    	$theme  	  = $this->theme;
    	$slider 	  = $this->slider;
    	$program      = $this->program;
    	$posting      = $this->posting;
    	$menu         = $this->menu;
    	$color        = $this->color;

        if (Session::get('token')) {
            return redirect()->route('dashboard.donatur');
        }

        return view('front_page.'.$theme->theme->key.'.register.index',  compact(
    		'slider','program','posting','menu','color','language_ses','yayasan'
    	));
    }

    public function index(Request $request)
    {
    	$language_ses = $this->language;
        $yayasan      = $this->yayasan;
    	$theme  	  = $this->theme;
    	$slider 	  = $this->slider;
    	$program      = $this->program;
    	$posting      = $this->posting;
    	$menu         = $this->menu;
    	$color        = $this->color;


    	return view('front_page.'.$theme->theme->key.'.main_layouts.index', compact(
    		'slider','program','posting','menu','color','language_ses','yayasan'
    	));
    }

    public function routing(Request $request)
    {
    	$language_ses = $this->language;
        $yayasan      = $this->yayasan;
    	$theme        = $this->theme;
    	$menu 		  = $this->menu;
    	$color 		  = $this->color;
    	$program      = $this->program;
        $posting      = $this->posting;
        $qurban       = $this->qurban;
        $profile      = $this->profile;
        $slider       = $this->slider;
        $statistik    = $this->statistik;

    	$endpoint = $request->getRequestUri();
    	$endpoint = explode('/',$endpoint);

    	foreach ($menu->all as $key => $url) {
            if($endpoint[1] == 'home'){
                return view('front_page.'.$theme->theme->key.'.main_layouts.index', compact(
                    'slider','statistik','program','posting','menu','color','language_ses','yayasan'
                ));
            }else if($url->url == $endpoint[1]){
                
                    
                $client = new \GuzzleHttp\Client();

                $yayasan_list = json_decode(
                    $client->get(env('API_URL').'/api/ngo-yayasan/'.env('YAYASAN_KEY'))
                            ->getBody()
                            ->getContents()
                )->data;

                $data_list = json_decode(
                $client->get(env('API_URL').'/api/konsultasi', [
                        "headers" => [
                            "Content-Type"  => "application/json",
                        ],
                        "json" => [
                            "id_yayasan" => $yayasan_list->id,
                            "group_by" => "yayasan.id",
                        ]
                    ])
                    ->getBody()
                    ->getContents()
                    );

                $list_yayasan = $data_list;

                $category_zakat =  json_decode(
                    $client->get(env('API_URL').'/api/zakat/category?id_yayasan='.$yayasan_list->id, [
                        "header" => [
                            "Content-Type" => "application/json",
                        ],
                        "json" => [
                            "id_yayasan" => $yayasan_list->id,
                        ]
                    ])
                    ->getBody()
                    ->getContents()
                );

                $setup_zakat = json_decode(
                    $client->get(env('API_URL').'/api/zakat/parameter', [
                        "header" => [
                            "Content-Type" => "application/json",
                        ],
                    ])
                    ->getBody()
                    ->getContents()
                );



    			return view('front_page.'.$theme->theme->key.'.pages.'.$endpoint[1].'.index', compact(
    				'language_ses','theme','menu','color','program','qurban','posting','profile','yayasan',
                    'list_yayasan','data_list','yayasan_list','category_zakat','setup_zakat'
    			));
    		}
    	}
    }

    public function detail_program($seo)
    {
    	$language_ses   = $this->language;
        $yayasan        = $this->yayasan;
    	$theme          = $this->theme;
    	$menu 			= $this->menu;
    	$client 		= $this->client->request('GET', ENV('API_URL').'/api/ngo-program/program/detail-program?seo='.$seo.'&lang='.$language_ses.'&yayasan='.ENV('YAYASAN_KEY'));

        $detail_program = json_decode($client->getBody()->getContents());

    	return view('front_page.'.$theme->theme->key.'.pages.program.detail', compact(
    		'language_ses','theme','menu','detail_program','yayasan'
    	));
    }

    public function detail_posting($seo)
    {
        $language_ses = $this->language;
        $yayasan      = $this->yayasan;
        $theme        = $this->theme;
        $menu         = $this->menu;
        $client       = $this->client->request('GET', ENV('API_URL').'/api/ngo-posting/posting/detail-posting?seo='.$seo.'&lang='.$language_ses.'&yayasan='.ENV('YAYASAN_KEY'));

        $detail_posting = json_decode($client->getBody()->getContents());

        return view('front_page.'.$theme->theme->key.'.pages.posting.detail', compact(
            'language_ses','theme','menu','detail_posting','yayasan'
        ));
    }

    public function donate_program($seo)
    {
        $language_ses = $this->language;
        $yayasan      = $this->yayasan;
        $theme        = $this->theme;
        $menu         = $this->menu;
        $client       = $this->client->request('GET', ENV('API_URL').'/api/ngo-program/program/detail-program?seo='.$seo.'&lang='.$language_ses.'&yayasan='.ENV('YAYASAN_KEY'));

        $detail_program = json_decode($client->getBody()->getContents());

        $program = $this->client->request('GET', ENV('API_URL').'/api/program/'.$detail_program->list->id_program);
        $program = json_decode($program->getBody()->getContents());
        $program = $program->data[0]->program->id;

        $donation_type = $this->client->request('GET', ENV('API_URL').'/api/program/donation-nominal/'.$program);
        $donation_type = json_decode($donation_type->getBody()->getContents());
        $donation_type = $donation_type->data;

        $nominals = null;
        if($donation_type == null) {
            $nominals = $this->client->request('GET', ENV('API_URL').'/api/program/nominal');
            $nominals = json_decode($nominals->getBody()->getContents());
            $nominals = $nominals->data;
        }

        return view('front_page.'.$theme->theme->key.'.pages.program.donate', compact(
            'language_ses','theme','menu','detail_program','yayasan', 'donation_type', 'nominals'
        ));
    }

    public function confirm_donasi(Request $request)
    {
        $no_invoice   = $request->no_invoice;
        $yayasan      = $this->yayasan;
        $language_ses = $this->language;
        $theme        = $this->theme;
        $menu         = $this->menu;
        $client       = $this->client->request('GET', ENV('API_URL').'/api/confirm-pay-donasi?no_invoice='.$no_invoice);

        $confirm = json_decode($client->getBody()->getContents());

        return view('front_page.'.$theme->theme->key.'.pages.program.confirm_invoice', compact(
            'language_ses','theme','menu','confirm','yayasan'
        ));
    }

    public function confirm_donasi_pay(Request $request)
    {
        $no_invoice = $request->no_invoice;
        $yayasan      = $this->yayasan;
        $language_ses = $this->language;
        $theme        = $this->theme;
        $menu         = $this->menu;
        $client       = $this->client->request('GET', ENV('API_URL').'/api/confirm-pay-donasi?no_invoice='.$no_invoice);

        $confirm = json_decode($client->getBody()->getContents());

        return view('front_page.'.$theme->theme->key.'.pages.program.confirm_pay', compact(
            'language_ses','theme','menu','confirm','yayasan'
        ));
    }

    public function purchase_qurban()
    {
        $language_ses = $this->language;
        $yayasan      = $this->yayasan;
        $theme        = $this->theme;
        $menu         = $this->menu;

        return view('front_page.'.$theme->theme->key.'.pages.qurban.purchase', compact(
            'language_ses','theme','menu','yayasan'
        ));
    }

    public function confirm_qurban(Request $request)
    {
        $no_invoice = $request->no_invoice;
        $yayasan      = $this->yayasan;
        $language_ses = $this->language;
        $theme        = $this->theme;
        $menu         = $this->menu;
        $client       = $this->client->request('GET', ENV('API_URL').'/api/confirm-pay-qurban?no_invoice='.$no_invoice);

        $confirm = json_decode($client->getBody()->getContents());

        return view('front_page.'.$theme->theme->key.'.pages.qurban.confirm_invoice', compact(
            'language_ses','theme','menu','confirm','yayasan'
        ));
    }

    public function confirm_qurban_pay(Request $request)
    {
        $no_invoice = $request->no_invoice;
        $yayasan      = $this->yayasan;
        $language_ses = $this->language;
        $theme        = $this->theme;
        $menu         = $this->menu;
        $client       = $this->client->request('GET', ENV('API_URL').'/api/confirm-pay-qurban?no_invoice='.$no_invoice);

        $confirm = json_decode($client->getBody()->getContents());

        return view('front_page.'.$theme->theme->key.'.pages.qurban.confirm_pay', compact(
            'language_ses','theme','menu','confirm','yayasan'
        ));
    }

    public function track_transaction(Request $request)
    {
        $no_invoice   = $request->no_invoice;
        $yayasan      = $this->yayasan;
        $language_ses = $this->language;
        $theme        = $this->theme;
        $menu         = $this->menu;
        $client       = $this->client->request('GET', ENV('API_URL').'/api/track/transaction?no_invoice='.$no_invoice);

        $list         = json_decode($client->getBody()->getContents());

        return view('front_page.track.index', compact(
            'language_ses','theme','menu','yayasan','list'
        ));
    }

    public function purchase_zakat(Request $request)
    {
        $yayasan      = $this->yayasan;
        $language_ses = $this->language;
        $theme        = $this->theme;
        $menu         = $this->menu;

        $category_id  = $request->category_id;
        $jumlah_zakat = $request->jumlah_zakat_purchase;

        if($jumlah_zakat == null){
            $jumlah_zakat = str_replace('Rp. ', '', $request->jumlah_zakat_purchase_detail);
            $jumlah_zakat = str_replace('.', '', $jumlah_zakat);
        }


        $category_zakat =  json_decode(
            $this->client->get(env('API_URL').'/api/zakat/category?id_yayasan='.$yayasan->data->id, [
                "header" => [
                    "Content-Type" => "application/json",
                ],
                "json" => [
                    "id_yayasan" => $yayasan->data->id,
                ]
            ])
            ->getBody()
            ->getContents()
        );

        return view('front_page.'.$theme->theme->key.'.pages.zakat.form_zakat', compact(
            'yayasan', 'language_ses', 'theme', 'menu', 'category_id', 'jumlah_zakat', 'category_zakat'
        ));
    }

    public function confirm_zakat(Request $request)
    {
        $no_invoice   = $request->no_invoice;
        $yayasan      = $this->yayasan;
        $language_ses = $this->language;
        $theme        = $this->theme;
        $menu         = $this->menu;
        $client       = $this->client->request('GET', ENV('API_URL').'/api/confirm-pay-zakat?no_invoice='.$no_invoice);

        $confirm = json_decode($client->getBody()->getContents());

        return view('front_page.'.$theme->theme->key.'.pages.zakat.confirm_invoice', compact(
            'language_ses','theme','menu','confirm','yayasan'
        ));
    }

    public function confirm_zakat_pay(Request $request)
    {
        $no_invoice   = $request->no_invoice;
        $yayasan      = $this->yayasan;
        $language_ses = $this->language;
        $theme        = $this->theme;
        $menu         = $this->menu;
        $client       = $this->client->request('GET', ENV('API_URL').'/api/confirm-pay-zakat?no_invoice='.$no_invoice);

        $confirm = json_decode($client->getBody()->getContents());

        return view('front_page.'.$theme->theme->key.'.pages.zakat.confirm_pay', compact(
            'language_ses','theme','menu','confirm','yayasan'
        ));
    }

    public function detail_zakat($seo)
    {
        $yayasan      = $this->yayasan;
        $language_ses = $this->language;
        $theme        = $this->theme;
        $menu         = $this->menu;

        $yayasan_list = json_decode(
            $this->client->get(env('API_URL').'/api/ngo-yayasan/'.env('YAYASAN_KEY'))
            ->getBody()
            ->getContents()
        )->data;

        $category_zakat =  json_decode(
            $this->client->get(env('API_URL').'/api/zakat/category?id_yayasan='.$yayasan_list->id, [
                "header" => [
                    "Content-Type" => "application/json",
                ],
                "json" => [
                    "id_yayasan" => $yayasan_list->id,
                ]
            ])
            ->getBody()
            ->getContents()
        );

        $setup_zakat = json_decode(
            $this->client->get(env('API_URL').'/api/zakat/parameter', [
                "header" => [
                    "Content-Type" => "application/json",
                ],
            ])
            ->getBody()
            ->getContents()
        );

        $hukum_zakat = json_decode(
            $this->client->get(env('API_URL').'/api/hukum-zakat/'.$seo.'?lang='.$language_ses, [
                "header" => [
                    "Content-Type" => "application/json",
                ],
            ])
            ->getBody()
            ->getContents()
        )->page;

        $ketentuan_zakat = json_decode(
            $this->client->get(env('API_URL').'/api/ketentuan-zakat/'.$seo.'?lang='.$language_ses, [
                "header" => [
                    "Content-Type" => "application/json",
                ],
            ])
            ->getBody()
            ->getContents()
        )->page;

        $cara_hitung_zakat = json_decode(
            $this->client->get(env('API_URL').'/api/cara-hitung-zakat/'.$seo.'?lang='.$language_ses, [
                "header" => "application/json",
            ])
            ->getBody()
            ->getContents()
        )->page;

        return view('front_page.'.$theme->theme->key.'.pages.zakat.detail', compact(
            'language_ses','theme','menu','yayasan','category_zakat','setup_zakat','seo','hukum_zakat',
            'ketentuan_zakat', 'cara_hitung_zakat'
        ));
    }

    function email_verify(Request $request)
    {
        $url = $request->url();
        $segments = explode('/', $url);
        $code = end($segments);

        $token = $code;

        return view('front_page.email.email_register_verify', compact('token'));
    }


    function verify_email_invoke(Request $request){
        
        $url = $request->url();
        $segments = explode('/', $url);
        $code = end($segments);

        $token = $code;

        $client = new Client();
        $response = $client->request('POST', ENV('API_URL').'/api/verify/'.$token, [
            "header" => [
                "Content-Type" => "application/json",
            ]
        ]);

        $response = json_decode($response->getBody()->getContents());

        if ($response->status == 200) {
            Session::put('success', $response->message);
            return redirect('/login');
        } else {
            Session::put('error', $response->message);
            return redirect('/login');
        }
    }

}
