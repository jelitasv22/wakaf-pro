<?php

namespace App\Http\Controllers\FrontPage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Entities\Admin\core\User;
use Socialite;
use DB;

class LoginGoogleController extends Controller
{
	public function index()
	{
		return Socialite::driver('google')->redirect();
	}

	public function callback()
	{
		try {
			$token    = md5(date('Y-m-d H:i:s')); 
			$user     = Socialite::driver('google')->user();
			$finduser = User::where('google_id', $user->id)->first();

			if($finduser){
				//cek jika account google sudah tersimpan ke database
				$this->create_token($finduser->id, $token);
				session::put('id', $finduser->id);
				session::put('name',$finduser->name);
				session::put('email',$finduser->email);
				session::put('foto', $finduser->foto);
				session::put('role_id', $finduser->role_id);
				session::put('login', $finduser);
				return redirect('/dashboard-donatur');

			}else{
				//insert jika account google belum ada di database
				$createuser = new User;
				$createuser->google_id = $user->id;
				$createuser->username = '';
				$createuser->password = '';
				$createuser->name     = $user->name;
				$createuser->email    = $user->email;
				$createuser->alamat   = '';
				$createuser->role_id  = 2;
				$createuser->foto     = $user->avatar;
				$createuser->is_created = $user->id;
				$createuser->email_verified = 1;
				$createuser->save();

				$this->create_token($createuser->id, $token);
				session::put('id', $createuser->id);
				session::put('name',$createuser->name);
				session::put('email',$createuser->email);
				session::put('foto', $createuser->foto);
				session::put('role_id', $createuser->role_id);
				session::put('login', $createuser);
				return redirect('/dashboard-donatur');
			}

		} catch (Exception $e) {
			dd($e->getMessage());
		}
	}

	public function create_token($id,$token){
		$plus7 = strtotime('+7 day');
		$expire_date = date('Y-m-d H:i:s', $plus7);

		DB::table('access_token')->insert([
			'id' 		 => $token,
			'user_id'    => $id,
			'created_at' => date('Y-m-d H:i:s'),
			'updated_at' => date('Y-m-d H:i:s'),
			'expired_at' => $expire_date
		]);
	}
}
