<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZakatDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zakat_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('zakat_id');
            $table->integer('program_id');
            $table->integer('akad_id');
            $table->double('nilai_zakat');
            $table->text('komentar');
            $table->integer('anonim');
            $table->integer('verifikasi');
            $table->nullableTimestamps('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zakat_details');
    }
}
