<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostingLanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posting_language', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('seo');
            $table->integer('id_posting');
            $table->integer('id_language');
            $table->text('judul');
            $table->text('content');
            $table->nullableTimestamps('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posting_language');
    }
}
