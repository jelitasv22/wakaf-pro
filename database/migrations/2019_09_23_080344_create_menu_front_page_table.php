<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuFrontPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_front_page', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_category');
            $table->integer('id_sub_menu');
            $table->string('jenis');
            $table->integer('sort_order');
            $table->string('url');
            $table->nullableTimestamps('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_front_page');
    }
}
