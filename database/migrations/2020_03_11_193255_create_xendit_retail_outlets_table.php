<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXenditRetailOutletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xendit_retail_outlets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_type');
            $table->bigInteger('transaction_id');
            $table->string('retail_payment_id');
            $table->string('owner_id');
            $table->string('external_id');
            $table->string('retail_outlet_name');
            $table->string('prefix');
            $table->string('name');
            $table->string('payment_code');
            $table->string('type');
            $table->string('status',50);
            $table->bigInteger('amount');
            $table->string('expiration_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xendit_retail_outlets');
    }
}
