<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonasiDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donasi_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('donasi_id');
            $table->integer('program_id');
            $table->integer('akad_id');
            $table->double('nilai_donasi');
            $table->text('komentar');
            $table->integer('anonim');
            $table->integer('verifikasi');
            $table->nullableTimestamps('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donasi_detail');
    }
}
