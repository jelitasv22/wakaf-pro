<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXenditVirtualAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xendit_virtual_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_type');
            $table->bigInteger('transaction_id');
            $table->string('va_id');
            $table->string('owner_id');
            $table->string('external_id');
            $table->string('payment_id')->nullable();
            $table->string('bank_code');
            $table->string('merchant_code');
            $table->string('name');
            $table->string('account_number');
            $table->bigInteger('amount')->nullable();
            $table->string('status');
            $table->string('callback_virtual_account_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xendit_virtual_accounts');
    }
}
