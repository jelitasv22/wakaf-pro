<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXenditCreditCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xendit_credit_cards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_type');
            $table->bigInteger('transaction_id');
            $table->string('token_id');
            $table->string('authentication_id');
            $table->string('charge_id')->nullable();
            $table->string('business_id')->nullable();
            $table->string('external_id')->nullable();
            $table->string('merchant_id')->nullable();
            $table->string('bank_reconciliation_id')->nullable();
            $table->string('masked_card_number');
            $table->string('status');
            $table->string('payer_authentication_url');
            $table->string('amount')->nullable();
            $table->string('currency',4)->nullable();
            $table->string('merchant_reference_code')->nullable();
            $table->string('card_type')->nullable();
            $table->string('charge_type')->nullable();
            $table->string('card_brand')->nullable();
            $table->string('eci')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xendit_credit_cards');
    }
}
