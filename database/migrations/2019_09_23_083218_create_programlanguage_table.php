<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramlanguageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programlanguage', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_program');
            $table->string('seo');
            $table->integer('id_language');
            $table->varchar('judul');
            $table->text('deskripsi');
            $table->nullableTimestamps('deleted_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programlanguage');
    }
}
