<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXenditCreditCardRefundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xendit_credit_card_refunds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('refund_id');
            $table->string('external_id');
            $table->string('credit_card_charge_id');
            $table->string('xendit_user_id');
            $table->bigInteger('amount');
            $table->string('currency',4);
            $table->string('status');
            $table->integer('fee_refund_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xendit_credit_card_refunds');
    }
}
