<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXenditEWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xendit_e_wallets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('transaction_type');
            $table->bigInteger('transaction_id');
            $table->string('business_id')->nullable();
            $table->string('external_id');
            $table->bigInteger('amount');
            $table->string('phone')->nullable();
            $table->string('ewallet_type');
            $table->string('status')->nullable();
            $table->string('checkout_url')->nullable();
            $table->string('callback_authentication_token')->nullable();
            $table->string('failure_code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xendit_e_wallets');
    }
}
