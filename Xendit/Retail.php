<?php

/**
 * Retail.php
 * php version 7.2.0
 *
 * @category Class
 * @package  Xendit
 * @author   Ellen <ellen@xendit.co>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     https://api.xendit.co
 */

namespace Xendit;

/**
 * Class Retail
 *
 * @category Class
 * @package  Xendit
 * @author   Ellen <ellen@xendit.co>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     https://api.xendit.co
 */
class Retail
{
    use ApiOperations\Request;
    use ApiOperations\Create;
    use ApiOperations\Retrieve;
    use ApiOperations\Update;

    /**
     * Instantiate relative URL
     *
     * @return string
     */
    public static function classUrl()
    {
        return "/fixed_payment_code";
    }

    /**
     * Instantiate required params for Create
     *
     * @return array
     */
    public static function createReqParams()
    {
        return ['external_id', 'retail_outlet_name', 'name', 'expected_amount'];
    }

    /**
     * Instantiate required params for Update
     *
     * @return array
     */
    public static function updateReqParams()
    {
        return ['expected_amount', 'name'];
    }

    /* YOU MUST ADD THIS FUNCTION TO YOUR VENDOR
        path : vendor/xendit/xendit-php/src/Retail.php
    */
    public static function createPayment($params = [])
    {
        $url = self::classUrl();

        self::validateParams($params, self::createReqParams());

        return static::_request('POST', $url, $params);
    }

    public static function updatePayment($retail_payment_id, $params = [])
    {
        $url = self::classUrl() . "/" . $retail_payment_id;

        self::validateParams($params, self::updateReqParams());

        return static::_request('PATCH', $url, $params);
    }
}
