<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// #front page route
// use App\Http\Controllers\FrontPage\BulkDriver;
// use App\Entities\Admin\core\MenuFrontPage;
// use App\Entities\Admin\core\Parameter;
$language   = \Session::get('language_id');
$client 	= new \GuzzleHttp\Client();
$menu		= $client->request('GET', ENV('API_URL').'/api/ngo-menu?lang='.$language.'&yayasan='.ENV('YAYASAN_KEY'));
$url        = json_decode($menu->getBody()->getContents());

#NGO ROUTE

Route::get('/logout', 'FrontPage\DashboardDonaturController@Logout')->name('dashboard.logout');
// Route::get('/', 'FrontPage\FrontPageController@index')->name('home');

Route::get('/about', function () {
    return view('Template.Pages.About-us');
});
Route::get('/program_wakaf', function () {
    return view('Template.Pages.program');
});
Route::get('/detail-program', function () {
    return view('Template.Pages.detail-program');
});
Route::get('/donasi', function () {
    return view('Template.Pages.donasi');
});
Route::get('/news', function () {
    return view('Template.Pages.berita');
});

Route::get('/artikel', function () {
    return view('Template.Pages.artikel');
});
Route::get('/kontak', function () {
    return view('Template.Pages.contact');
});


Route::get('/', function () {
    return view('Template.Pages.home');
})->name('home');

foreach($url->all as $url){
	Route::get('/'.$url->url, 'FrontPage\FrontPageController@routing')->name('front_page.'.$url->url);
}

Route::middleware(['auth.session'])->prefix('/dashboard-donatur')->group(function () {
    Route::get('/', 'FrontPage\DashboardDonaturController@index')->name('dashboard.donatur');
    Route::post('/update-profile', 'FrontPage\DashboardDonaturController@updateProfile')->name('dashboard.update_profile');
    Route::post('/update-password', 'FrontPage\DashboardDonaturController@UpdatePassword')->name('dashboard.update_password');

});


Route::group(['prefix'=> 'bayar-zakat'], function(){
	Route::get('/', 'FrontPage\BayarZakatController@index')->name('bayar_zakat.index');
	Route::get('/step/{position}', 'FrontPage\BayarZakatController@step')->name('bayar-zakat.step');
	// payment
	// Route::post('/step/{position}', 'FrontPage\BayarZakatController@processZakatTransaction')->name('bayar-zakat.purchase_zakat');
});

Route::group(['prefix' => 'language'], function(){
	Route::get('/switch/{id?}', 'FrontPage\FrontPageController@switch_language')->name('language.switch');
});

Route::group(['prefix' => 'login'], function(){
	Route::get('/', 'FrontPage\FrontPageController@login')->name('login.index');
	Route::post('/', 'Auth\LoginController@Login')->name('login.do_login');
});
Route::group(['prefix' => 'register'], function(){
	Route::get('/', 'FrontPage\FrontPageController@register')->name('register.index');
	Route::post('/', 'Auth\RegisterController@register')->name('register.do_register');
});

Route::group(['prefix' => 'program'], function(){
	Route::get('/detail-program/{seo}', 'FrontPage\FrontPageController@detail_program')->name('program.detailprogram');
	Route::get('/donate/{seo}', 'FrontPage\FrontPageController@donate_program')->name('program.donate');
	Route::get('/confirm', 'FrontPage\FrontPageController@confirm_donasi')->name('program.confirm');
	Route::get('/confirm-pay', 'FrontPage\FrontPageController@confirm_donasi_pay')->name('program.confirm_pay');
});

Route::group(['prefix' => 'posting'], function(){
	Route::get('/detail-posting/{seo}', 'FrontPage\FrontPageController@detail_posting')->name('posting.detailposting');
});

Route::group(['prefix' => 'qurban'], function(){
	Route::get('/purchase', 'FrontPage\FrontPageController@purchase_qurban')->name('qurban.purchase');
	Route::get('/confirm', 'FrontPage\FrontPageController@confirm_qurban')->name('qurban.confirm');
	Route::get('/confirm-pay', 'FrontPage\FrontPageController@confirm_qurban_pay')->name('qurban.confirm_pay');
});

Route::get('track/transaction/{no_invoice?}', 'FrontPage\FrontPageController@track_transaction')->name('track.transaction');
Route::post('/send_konsultasi', 'FrontPage\FrontPageController@send')->name('send');

Route::group(['prefix' => 'zakat'], function(){
	Route::post('/purchase', 'FrontPage\FrontPageController@purchase_zakat')->name('zakat.purchase');
	Route::get('/confirm', 'FrontPage\FrontPageController@confirm_zakat')->name('zakat.confirm');
	Route::get('/confirm-pay', 'FrontPage\FrontPageController@confirm_zakat_pay')->name('zakat.confirm_pay');

	Route::get('/detail/{seo}', 'FrontPage\FrontPageController@detail_zakat');
});

Route::middleware(['auth.session'])->prefix('/tabungan-qurban')->group(function(){
	Route::get('/tabung', 'FrontPage\TabunganQurbanController@index')->name('tabungan-qurban.purchase');
	Route::get('/realisasi', 'FrontPage\TabunganQurbanController@realisasiQurban')->name('tabungan-qurban.realisasi');
});

// Route::get('/konsultasi', 'FrontPage\FrontPageController@Konsultasi')->name('index.konsultasi');

#google login
// Route::get('/login/google', 'FrontPage\LoginGoogleController@index')->name('login.google');
// Route::get('/login/google/callback', 'FrontPage\LoginGoogleController@callback')->name('login.callback');

#get kategori ajax midtrans zakat
// Route::get('/get_data_kategori/{id}', 'FrontPage\FrontPageController@get_kategori')->name('kategorigetdata');

#login and register
// Route::get('/masuk', 'FrontPage\FrontPageController@login')->name('front_page.login');
// Route::post('/proses-masuk', 'FrontPage\FrontPageController@pro_login')->name('front_page.pro_login');
// Route::get('/daftar', 'FrontPage\FrontPageController@register')->name('front_page.register');
// Route::post('/proses-daftar', 'FrontPage\FrontPageController@pro_register')->name('front_page.pro_register');
// Route::get('/daftar/cek_email/{email}', 'FrontPage\FrontPageController@cek_email')->name('front_page.cek_email');
Route::get('/daftar/email-verify/{tok}/', 'FrontPage\FrontPageController@email_verify')->name('front_page.email_verify');
Route::get('/daftar/email-verify-invoke/{tok}', 'FrontPage\FrontPageController@verify_email_invoke')->name('front_page.invoke_email_verify');

// Route::get('/dashboard-donatur/cek_password/{password}', 'FrontPage\FrontPageController@cek_password')->name('front_page.cek_password');

// Route::get('/forgot-password', 'FrontPage\FrontPageController@forgot_password')->name('front_page.forgot_password');
// Route::post('/forgot-password-post', 'FrontPage\FrontPageController@forgot_password_post')->name('front_page.forgot_password_post');
// Route::get('/forgot-password/{token}', 'FrontPage\FrontPageController@verify_forgot_password')->name('front_page.verify_forgot_password');
// Route::post('/post-ganti-password/{id}', 'FrontPage\FrontPageController@post_ganti_password')->name('front_page.post_reset_password');

// #front page
// Route::get('/home', 'FrontPage\FrontPageController@index')->name('front_page.index');
// Route::get('/dashboard-donatur', 'FrontPage\FrontPageController@dashboard_donatur')->name('front_page.dash_donatur');
// Route::get('/dashboard-donatur/post_ganti_password/{id}', 'FrontPage\FrontPageController@post_ganti_password')->name('front_end.post_reset_password');
// Route::post('/dashboard-donatur/post_ganti_profile/{id}', 'FrontPage\FrontPageController@post_ganti_profile')->name('front_end.post_edit_profile');
// Route::get('/dashboard-donatur/post_ganti_profile/{id}', 'FrontPage\FrontPageController@post_ganti_profile')->name('front_end.post_edit_profile');

// // foreach($url as $url){
// // 	Route::get('/'.$url->url, 'FrontPage\FrontPageController@routing')->name('front_page.'.$url->url);
// // }
// Route::get('/logout', 'FrontPage\FrontPageController@logout')->name('front_page.logout');

// #change lang
// Route::post('/switch-language', 'FrontPage\FrontPageController@switch_language')->name('front_page.switch_language');

// #program dan donasi
// Route::get('donateform/{id}/{agency?}', 'FrontPage\FrontPageController@donasi_program')->name('program.donasi');
// Route::get('detailprogram/{id}/{agency?}', 'FrontPage\FrontPageController@detailprogram')->name('program.detailprogram');
// Route::get('/get-invoice/{id}', 'FrontPage\DonasiController@get_invoice')->name('donasi.get_invoice');

// #qurban
// Route::get('/form-qurban', 'FrontPage\FrontPageController@donasi_qurban')->name('donasi.qurban');

// #update
// Route::get('update/{id}', 'FrontPage\FrontPageController@update_detail')->name('update.detail');

// #search program
// Route::get('/programs/pencarian', 'FrontPage\FrontPageController@cari')->name('program.cari');
// Route::get('/programs/pencarian/{seo}', 'FrontPage\FrontPageController@url_program_search');

// #zakat
// Route::group(['prefix' => 'zakat'], function(){
// 	Route::get('/form-zakat', 'FrontPage\FrontPageController@form_zakat')->name('zakat.form');
// 	Route::get('/kalkulator', 'FrontPage\FrontPageController@kalkulator')->name('zakat.kalkulator');
// 	Route::get('/snaptokens', 'FrontPage\ZakatController@token')->name('zakat.snaptokens');
// 	Route::post('/snapfinishs', 'FrontPage\ZakatController@finish')->name('zakat.snapfinishs');
// 	Route::post('/zakat-post-penghasilan', 'FrontPage\FrontPageController@zakat_penghasilan_store')->name('zakat.insert_penghasilan');
// 	Route::post('/zakat-post-harta', 'FrontPage\FrontPageController@zakat_harta_store')->name('zakat.insert_harta');
// 	Route::post('/zakat-post-perniagaan', 'FrontPage\FrontPageController@zakat_perniagaan_store')->name('zakat.insert_perniagaan');
// 	Route::post('/zakat-post-fitrah', 'FrontPage\FrontPageController@zakat_fitrah_store')->name('zakat.insert_fitrah');
// });

// #kisah sukses
// Route::get('/kisah-sukses/{id}', 'FrontPage\FrontPageController@kisah_sukses')->name('home.kisah');

// #notif midtrans
// Route::group(['prefix' => 'notif'], function(){
// 	Route::get('/notif-success', 'FrontPage\DonasiController@setSuccess')->name('notif.success');
// 	Route::get('/notif-pending', 'FrontPage\DonasiController@setPending')->name('notif.pending');
// 	Route::get('/notif-error', 'FrontPage\DonasiController@setError')->name('notif.error');
	Route::get('/notif-wait', 'FrontPage\DonasiController@setWait')->name('notif.wait');
// 	Route::get('/notif-konfirmasi/{token}', 'FrontPage\DonasiController@setKonfirmasi')->name('notif.konfirmasi');
// 	Route::get('/notifikasi-konfirmasi', 'FrontPage\DonasiController@setKonfirmasi2')->name('notif.konfirmasi2');
// });

// Route::group(['prefix' => 'notif-zakat'], function(){
// 	Route::get('/notif-success', 'FrontPage\ZakatController@setSuccess')->name('notif-zakat.success');
// 	Route::get('/notif-pending', 'FrontPage\ZakatController@setPending')->name('notif-zakat.pending');
// 	Route::get('/notif-error', 'FrontPage\ZakatController@setError')->name('notif-zakat.error');
// 	Route::get('/notif-wait', 'FrontPage\DonasiController@setWaitZakat')->name('notif-zakat.wait');
// 	Route::get('/notif-konfirmasi', 'FrontPage\ZakatController@setKonfirmasi')->name('notif-zakat.konfirmasi');
// 	Route::get('/notifikasi-konfirmasi', 'FrontPage\ZakatController@setKonfirmasi2')->name('notif-zakat.konfirmasi2');
// });

// Route::group(['prefix' => 'notif-qurban'], function(){
// 	Route::get('/notif-wait', 'FrontPage\DonasiController@setWaitQurban')->name('notif-qurban.wait');
// });

#redirect
// Route::get('/notif-register', 'FrontPage\FrontPageController@notif_register')->name('notif.register');

// #footer
// $parameter = Parameter::Where('key', 'category_menu')->first();
// $url_footer = MenuFrontPage::where('id_category', $parameter->value)->where('id_sub_menu', 0)->get();
// $urls_dropdown = MenuFrontPage::where('id_category', $parameter->value)->where('id_sub_menu', '!=', 0)->get();

// foreach ($url_footer as $key => $footer) {
// 	$val = str_replace(' ', '-', $footer->url);
// 	Route::get('/'.$val, 'FrontPage\FrontPageController@routing_footer')->name('front_page.'.$val);
// }

// foreach ($urls_dropdown as $key => $footer_sub) {
// 	$val = str_replace(' ', '-', $footer_sub->url);
// 	Route::get('/'.$val, 'FrontPage\FrontPageController@routing_footer')->name('front_page.'.$val);
// }

#detail transfer
// Route::get('/konfirmasi-pembayaran/{id}', 'FrontPage\DonasiController@konfirmasi_pembayaran')->name('konfirmasi.pembayaran');
// Route::post('/approve-pay', 'FrontPage\DonasiController@konfirmasi_pay')->name('konfirmasi.pay');
#end frontpage route

#error 404 Not Founds
// Route::get('/error404', 'Admin\core\LoginController@error404')->name('error404');

Route::post('/notification/handler', 'FrontPage\DonasiController@notificationHandler')->name('notification.handler');

// Route::post('/snaptoken', 'FrontPage\DonasiController@token')->name('donasi.snaptoken');
// Route::post('/snapfinish', 'FrontPage\DonasiController@finish')->name('donasi.snapfinish');
Route::post('/update/status-donasi', 'FrontPage\DonasiController@updateStatus')->name('donasi.updateStatus');
// Route::post('/delete/donasi', 'FrontPage\DonasiController@deleteDonasi')->name('donasi.deleteDonasi');

// Route::post('/snaptoken-zakat', 'FrontPage\DonasiController@token_zakat')->name('donasi.snaptokenZakat');
// Route::post('/update/status-zakat', 'FrontPage\DonasiController@updateStatusZakat')->name('donasi.updateStatusZakat');
// Route::post('/delete/zakat', 'FrontPage\DonasiController@deleteZakat')->name('donasi.deleteZakat');

// Route::get('/harga-produk', 'FrontPage\FrontPageController@harga_produk')->name('donasi.harga_hewan_qurban');
// Route::post('/snaptoken-qurban', 'FrontPage\DonasiController@token_qurban')->name('donasi.snaptokenQurban');
Route::post('/update/status-qurban', 'FrontPage\DonasiController@updateStatusQurban')->name('donasi.updateStatusQurban');
// Route::post('/delete/qurban', 'FrontPage\DonasiController@deleteQurban')->name('donasi.deleteQurban');
