<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Image
Route::get('program/image/{id}', 'Api\ImageController@getFotoProgram');
Route::get('dashboard/getSlider/{id}', 'Api\ImageController@getSliderFoto');
Route::get('/user/image/{id}', 'Api\ImageController@getUserFoto');
Route::get('kepengurusan/foto/{id}', 'Api\ImageController@fotoKepengurusan');
Route::get('posting/image/{id}', 'Api\ImageController@getImagePosting');
Route::get('program/icon/{id_relasi}', 'Api\ImageController@getIcon');
Route::get('/metode/icon/{id}', 'Api\ImageController@getFotoMetodePembayaran');

Route::post('/user/edit/foto', 'Api\ImageController@editFoto');

// Xendit experiment
Route::group(['prefix' => 'credit_card'], function(){
    Route::post('/token','Api\XenditExperiment@cc_token');
    Route::post('/charge/{xendit_cc_id}','Api\XenditExperiment@cc_charge');
    Route::get('/refund/{xendit_cc_id}','Api\XenditExperiment@cc_refund');
    Route::post('/refund_callback','Api\XenditExperiment@cc_refund_callback');
});
Route::group(['prefix' => 'ewallet'], function(){
    Route::post('/payment','Api\XenditExperiment@ew_payment');
    Route::post('/callback','Api\XenditExperiment@ew_callback');
});
Route::group(['prefix' => 'retail_outlet'], function(){
    Route::post('/payment','Api\XenditExperiment@ro_create_payment');
    Route::post('/payment/{payment_id}','Api\XenditExperiment@ro_update_payment');
    Route::post('/callback','Api\XenditExperiment@ro_callback');
});
Route::group(['prefix' => 'virtual_account'], function(){
    Route::get('/banks','Api\XenditExperiment@va_banks');
    Route::post('/payment','Api\XenditExperiment@va_create_payment');
    Route::post('/payment/{va_id}/update','Api\XenditExperiment@va_update_payment');
    Route::post('/callback','Api\XenditExperiment@va_callback');
});

// WABiz experiment
Route::group(['prefix' => 'wabiz'], function(){
    Route::post('/check-contact','Api\WABizController@checkContact');
    Route::post('/send-text-message','Api\WABizController@sendTextMessage');
    Route::post('/send-template-message','Api\WABizController@sendTemplateMessage');
});

/* Log Error */
Route::group(['prefix' => 'logError'], function(){
    Route::get('/', 'Api\LogErrorController@log_error');
    Route::get('/show/{filename}', 'Api\LogErrorController@log_error_show');
    Route::get('/download/{filename}', 'Api\LogErrorController@log_error_download');
});