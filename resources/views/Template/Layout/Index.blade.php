<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Wakaf Pro || Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/logo/favicon.png">
    <link rel="stylesheet" href="{{ asset('asset_template/css/bootstrap.min.css') }}">
    <!-- CSS here -->
    
    <link rel="stylesheet" href="{{ asset('asset_template/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/custom-animation.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/swiper-bundle.css')}}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/meanmenu.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/font-awesome-pro.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/spacing.css') }}">
    <link rel="stylesheet" href="{{ asset('asset_template/css/style.css') }}">
</head>

<body>

    <!-- preloader -->
    <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div>
    <!-- preloader end  -->

    <!-- back-to-top-start  -->
    <button class="scroll-top scroll-to-target" data-target="html">
        <i class="far fa-angle-double-up"></i>
    </button>
    <!-- back-to-top-end  -->

    <!-- search popup start -->
    <div class="search__popup">
        <div class="container">
        <div class="row">
            <div class="col-xxl-12">
                <div class="search__wrapper">
                    <div class="search__top d-flex justify-content-between align-items-center">
                    <div class="search__logo">
                        <a href="index.html">
                            <img src="assets/img/logo/footer-1.png" alt="">
                        </a>
                    </div>
                    <div class="search__close">
                        <button type="button" class="search__close-btn search-close-btn">
                            <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M17 1L1 17" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M1 1L17 17" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>                                 
                        </button>
                    </div>
                    </div>
                    <div class="search__form">
                    <form action="#">
                        <div class="search__input">
                            <input class="search-input-field" type="text" placeholder="Type here to search...">
                            <span class="search-focus-border"></span>
                            <button type="submit">
                                <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.55 18.1C14.272 18.1 18.1 14.272 18.1 9.55C18.1 4.82797 14.272 1 9.55 1C4.82797 1 1 4.82797 1 9.55C1 14.272 4.82797 18.1 9.55 18.1Z" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M19.0002 19.0002L17.2002 17.2002" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg> 
                            </button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <!-- search popup end -->

    <!-- tp-offcanvus-area-start -->

         @include('Template.Layout.navbar')

    <main>
        @yield('content')
  

    




    </main>
    @include('Template.Layout.footer')


    <!-- JS here -->
    <script src="{{ asset('asset_template/js/jquery.js') }}"></script>
    <script src="{{ asset('asset_template/js/waypoints.js') }}"></script>
    <script src="{{ asset('asset_template/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('asset_template/js/slick.js') }}"></script>
    <script src="{{ asset('asset_template/js/magnific-popup.js') }}"></script>
    <script src="{{ asset('asset_template/js/purecounter.js') }}"></script>
    <script src="{{ asset('asset_template/js/wow.js') }}"></script>
    <script src="{{ asset('asset_template/js/nice-select.js') }}"></script>
    <script src="{{ asset('asset_template/js/swiper-bundle.js') }}"></script>
    <script src="{{ asset('asset_template/js/isotope-pkgd.js') }}"></script>
    <script src="{{ asset('asset_template/js/imagesloaded-pkgd.js') }}"></script>
    <script src="{{ asset('asset_template/js/ajax-form.js') }}"></script>
    <script src="{{ asset('asset_template/js/main.js') }}"></script>



</body>

</html>