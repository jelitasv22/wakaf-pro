<footer>
        <!-- footer-area-start -->
        <div class="tp-footer__area">
            <div class="tp-footer__bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 mb-45 wow tpfadeUp" data-wow-duration=".9s"
                        data-wow-delay=".3s">
                            <div class="tp-footer__widget footer-2-col-2">
                                <div class="tp-footer__logo">
                                    <a href="/home">
                                        <img src="{{ asset('asset_template/img/logo/wapro-hitam.svg') }}" alt="">
                                    </a>
                                </div>
                                <div class="tp-footer__text px-5">
                                    <p>Yayasan kemanusiaan yang berfokus pada pengumpulan dan distribusi dana bantuan Indonesia yang sedang mengalami kesulitan bencana, konflik dan kemiskinan.Karena kita adalah saudara, kami mengajak masyarakat Indonesia untuk memberikan kontribusi terbaik mereka.</p>
                                </div>
                                {{-- <div class="tp-footer__contact-list">
                                    <div class="tp-footer__contact-item pb-20 d-flex about-items-center">
                                        <div class="tp-footer__icon">
                                            <i class="flaticon-mail"></i>
                                        </div>
                                        <div class="tp-footer__text">
                                             <a href="mailto:poorexcharity@gmail.com">poorexcharity@gmail.com</a>
                                        </div>
                                    </div>
                                    <div class="tp-footer__contact-item d-flex about-items-center">
                                        <div class="tp-footer__icon">
                                            <i aria-hidden="true" class="flaticon-phone"></i>
                                        </div>
                                        <div class="tp-footer__text">
                                             <a href="tel:+990988764578">+99 (098) 876 4578</a>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 mb-45 wow tpfadeUp" data-wow-duration=".9s"
                        data-wow-delay=".5s">
                            <div class="tp-footer__widget footer-2-col-2">
                                <h4 class="tp-footer__widget-title-2">menu</h4>
                                <div class="tp-footer__list">
                                    <ul>
                                        <li><a href="/about">About</a></li>
                                        <li><a href="/program_wakaf">Program     </a></li>
                                        <li><a href="/news">Berita</a></li>
                                        <li><a href="/artikel">Artikel</a></li>
                                        <li><a href="/kontak">Kontak</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4 mb-45 wow tpfadeUp" data-wow-duration=".9s"
                        data-wow-delay=".9s"
                            <div class="tp-footer__widget footer-2-col-4">
                                <div class="tp-footer__donate-box">
                                    <h4 class="tp-footer__donate-title-sm">Mari Bekerja sama demi <br> dunia yang lebih indah, Bergabung sekarang!</h4>
                                    <a class="tp-btn theme-2-bg" href="/donasi">Donasi Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer-area-end -->

        <!-- copyright-area-start -->
        <div class="tp-copyright__area tp-copyright__bg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="tp-copyright__text text-center text-sm-start">
                            <span>© Copyright 2023 by proWakaf.com</span>
                        </div>
                    </div>
                    {{-- <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="tp-copyright__social text-center text-sm-end">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                            <a href="#"><i class="fab fa-pinterest-p"></i></a>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        <!-- copyright-area-end -->

    </footer>