<header class="tp-header-height">
        <div id="header-sticky" class="tp-header-3__area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-2 col-lg-6 col-md-4 col-7">
                        <div class="tp-header-3__logo">
                            <a href="/">
                                <img src="{{ asset('asset_template/img/logo/wakafPro.png') }}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-7 d-none d-xl-block">
                        <div class="tp-header-3__main-menu">
                            <nav class="tp-main-menu-content">
                                <ul>
                                    <li class="has-dropdown"><a href="/about">About</a></li>
                                    <li class="has-dropdown"><a href="/program_wakaf">Program</a>
                                        <ul class="submenu tp-submenu">
                                            <li><a href="/detail-program">Detail Program</a></li>
                                        </ul>
                                    </li>
                                    <li class="has-dropdown">
                                        <a href="/news">Berita</a>
                                    </li>
                                    <li class="has-dropdown">
                                        <a href="/artikel">Artikel</a>
                                    </li>
                                    <li><a href="/kontak">Kontak</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-8 col-5">
                        <div class="tp-header-3__right-box">
                            <div class="tp-header-3__right-action text-end">
                                <ul class="d-flex align-items-center justify-content-end">
                                    <li>
                                        <div class="tp-header-3__icon-box d-none d-md-block">
                                            <button class="search-open-btn"><i class="flaticon-loupe"></i></button>
                                        </div>
                                    </li>                                    
                                    <li>
                                        <div class="tp-header-3__btn d-none d-md-block">
                                            <a class="tp-btn" href="donation-sidebar.html">Donate Now</a>
                                        </div>
                                    </li>  
                                    <li>
                                        <div class="tp-header-3__bar d-xl-none">
                                            <button class="tp-menu-bar"><i class="fa-solid fa-bars-staggered"></i></button>
                                        </div>
                                    </li>                                  
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>