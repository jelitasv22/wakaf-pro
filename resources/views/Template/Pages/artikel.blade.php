@extends('Template.Layout.Index')
@section('content')
<main>

<!-- breadcrumb-area-start -->
<div class="tp-breadcrumb__area p-relative fix tp-breadcrumb-height" data-background="{{ asset('asset_template/img/about/Main.png') }}">
    <div class="tp-breadcrumb__shape-1 z-index-5">
        <img src="assets/img/breadcrumb/breadcrumb-shape-1.png" alt="">
    </div>
    <div class="tp-breadcrumb__shape-2 z-index-5">
        <img src="assets/img/breadcrumb/breadcrumb-shape-2.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="tp-breadcrumb__content z-index-5">
                    <div class="tp-breadcrumb__list">
                       <span><a href="/">home</a></span> 
                       <span class="dvdr"><i class="fa-sharp fa-solid fa-slash-forward"></i></span>
                       <span>Artikel</span>
                    </div>
                    <h3 class="tp-breadcrumb__title">Artikel</h3>
                 </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area-end -->

<!-- event-area-atart -->
<div class="tp-event-details__area pt-120 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8">
                <div class="tp-event-details__left-box">
                    <div class="tp-event-details__thumb p-relative pb-35">
                        <img src="{{ asset('asset_template/img/artikel/Banner.png') }}" alt="">
                        <div class="tp-event-details__thumb-text d-none d-md-block">
                            <span>08 April</span>
                        </div>
                    </div>
                    <h4 class="tp-event-details__title">Donasi kita, untuk cita anak miskin Indonesia terwujud</h4>
                    <div class="tp-event-details__text pb-35">
                        <p>Di tengah hiruk pikuk kehidupan kota, ada secercah cahaya yang menyinari kisah-kisah penuh
                             haru dan harapan yang tersembunyi dari pandangan kita. Sebuah sudut kota,
                              di mana langkah seorang anak sekolah menggema sebagai simbol kemungkinan,
                               karena seragam yang dipakainya adalah hasil dari kebaikan dan uluran tangan
                                yang datang dari kita semua. Tepi jalan ditempati oleh ibu-ibu yang saling 
                                bersatu, menggenggam erat secarik harapan yang dulu hanya ada dalam mimpinya, 
                                kini mulai terwujud dalam kenyataan yang membahagiakan.
                            Donasi yang kita salurkan,
                             jauh lebih daripada sekedar memberikan materi. 
                             Ia mencerminkan sebuah sentuhan empati, perhatian tulus
                             , dan keinginan sungguh-sungguh untuk membawa perubahan positif ke dalam kehidupan orang lain. Setiap sumbangan yang kita berikan membuka pintu berbagai peluang: peluang pendidikan, peluang perubahan, dan yang paling penting, peluang harapan yang mampu mengubah takdir.
                            
                            Saat kita berdonasi, kita sebenarnya memberikan lebih dari sekadar barang atau uang. Kita memberikan kesempatan kepada anak-anak untuk memperoleh pendidikan, bermimpi, dan mewujudkan cita-cita mereka. Kita juga memberikan perlindungan bagi mereka yang tenggelam dalam keterbatasan, dan yang tak kalah pentingnya, kita memberikan keyakinan bahwa di dunia ini, masih banyak orang baik yang siap menyokong dan membantu.
                            
                            Tiap detik berlalu, melahirkan kisah kebaikan yang terus berputar. Sekolah-sekolah berdiri, komunitas-komunitas berkembang, dan generasi penerus bangsa tumbuh dengan penuh harapan. Semua ini bukan sekadar khayalan, melainkan hasil nyata dari kepedulian kita yang tulus.
                            
                            Namun, perjalanan ini masih jauh dari selesai. Banyak anak-anak yang masih menanti kebaikan dari uluran tangan kita. Potensi-potensi yang belum tergali pun masih begitu besar. Oleh karena itu, mari kita terus berbagi dan berdonasi. Dengan setiap donasi yang kita sumbangkan, kita membawa mereka lebih dekat kepada impian dan cita-cita yang selama ini mereka pegang erat di dalam hati mereka. Bersama-sama, mari kita terus membangun masa depan yang lebih baik, satu langkah kebaikan pada satu langkah berikutnya.</p>
                    </div>
                    @include('Template.Component.data-donation')
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="tp-event-details__right-box"> 
                    <div class="sidebar__widget mb-30">
                        <h3 class="sidebar__widget-title">Berita Lainnya</h3>
                        <div class="sidebar__widget-content">
                            <div class="sidebar__post">
                                <div class="rc__post mb-10 d-flex align-items-center">
                                    <div class="rc__post-thumb mr-20">
                                        <a href="/artikel"><img
                                                src="{{ asset('asset_template/img/artikel/Artikel1.png') }}" alt=""></a>
                                    </div>
                                    <div class="rc__post-content">
                                        <div class="rc__meta">
                                            <span><i class="flaticon-comment"></i>
                                                02 Comments</span>
                                        </div>
                                        <h3 class="rc__post-title">
                                            <a href="/artikel">We Should Donate <br>For Indian’s
                                                Poor</a>
                                        </h3>
                                    </div>
                                </div>
                                <div class="rc__post mb-10 d-flex align-items-center">
                                    <div class="rc__post-thumb mr-20">
                                        <a href="/artikel"><img
                                                src="{{ asset('asset_template/img/artikel/Artikel1.png') }}" alt=""></a>
                                    </div>
                                    <div class="rc__post-content">
                                        <div class="rc__meta">
                                            <span><i class="flaticon-comment"></i>
                                                02 Comments</span>
                                        </div>
                                        <h3 class="rc__post-title">
                                            <a href="/artikel">Quick Fundraising For <br>African
                                                Child</a>
                                        </h3>
                                    </div>
                                </div>
                                <div class="rc__post d-flex align-items-center">
                                    <div class="rc__post-thumb mr-20">
                                        <a href="/artikel"><img
                                                src="{{ asset('asset_template/img/artikel/Artikel1.png') }}" alt=""></a>
                                    </div>
                                    <div class="rc__post-content">
                                        <div class="rc__meta">
                                            <span><i class="flaticon-comment"></i>
                                                02 Comments</span>
                                        </div>
                                        <h3 class="rc__post-title">
                                            <a href="blog-details.html">Bring The People For <br>Raise Hands</a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- event-area-end -->




</main>
@endsection