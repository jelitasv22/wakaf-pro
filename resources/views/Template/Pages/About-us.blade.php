@extends('Template.Layout.Index')
@section('content')
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Poorex - Nonprofit Charity HTML Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>

    <!-- preloader -->
   
    <!-- preloader end  -->


    <!-- search popup start -->
    <!-- search popup end -->

    <main>

        <!-- breadcrumb-area-start -->
        <   
        <!-- breadcrumb-area-end -->

        <!-- about-area-start -->
        <div class="tp-about__area tp-about__space">
            <div class="container">
                <div class="row align-items-xl-start align-items-center">
                    <div class="col-xl-5 col-lg-5 wow tpfadeLeft" data-wow-duration=".9s"
                    data-wow-delay=".3s">
                        <div class="tp-about-us__main-thumb p-relative">
                            <img src="{{ asset('asset_template/img/about/poorkids.jpg.png') }}" alt="">
                            <div class="tp-about-us__thumb-sm">
                                <img src="{{ asset('asset_template/img/about/about-1.png') }}" alt="">
                            </div>
                            <div class="tp-about-us__thumb-border"></div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 wow tpfadeRight" data-wow-duration=".9s"
                    data-wow-delay=".7s">
                        <div class="tp-about-2__right-box">
                            <div class="tp-about-2__section-title pb-25">
                                <span class="tp-section-subtitle-2">ABOUT US</span>
                                <h4 class="tp-section-title">Investasi Cinta <br> untuk Umat</h4>
                            </div>
                            <div class="tp-about-2__text">
                                <p>Kami adalah lembaga amal yang berdedikasi untuk membawa keberkahan melalui wakaf. Dengan hati yang penuh cinta, kami menghubungkan dermawan dengan mereka yang membutuhkan bantuan. Fokus kami meliputi pendidikan, kesehatan, ekonomi, dan bantuan sosial. Bersama, mari ciptakan perubahan positif. Bergabunglah dengan kami untuk menjadi bagian dari kebaikan yang tak terhingga.
                                </p>
                            </div>
                            <div class="tp-about-2__bottom d-flex">
                                <div class="tp-about-2__btn">
                                    <a class="tp-btn" href="/donasi">Donasi Sekarang</a>
                                </div>
        
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- about-area-end -->
        <!-- cta-area-start -->
        <div class="tp-cta-2__area tp-cta-2__pt">
            <div class="tp-cta-2__bg p-relative fix" data-background="{{ asset('asset_template/img/about/About-banner.png') }}">
                <div class="tp-cta-2__shape-1 d-none d-xl-block">
                    <img src="assets/img/cta/cta-shape-1-1.png" alt="">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="tp-cta-2__content text-center">
                                <span class="tp-cta-2__subtitle">let's start something for them</span>
                                <h4 class="tp-cta-2__title">No One Has Ever Become <br>Poor From Giving</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- cta-area-end -->

        <!-- testimonial-area-start -->
        {{-- <div class="tp-testimonial__area">
            <div class="tp-testimonial__bg" data-background="assets/img/testimonial/testi-bg-1.jpg">
                <div class="container-fluid g-0">
                    <div class="row g-0">
                        <div class="col-xl-5 col-lg-6 col-md-6">
                            <div class="tp-testimonial__left-side">
                                <div class="tp-testimonial__section-title pb-20">
                                    <span class="tp-section-subtitle">OUR TESTIMONIALS</span>
                                    <h4 class="tp-section-title">What Our Client <br>Tell About?</h4>
                                </div>
                                <div class="tp-testimonial__left-text">
                                    <p>Charity is the act of exte kindneuncondit ionalwhich is <br> a conscious act but
                                        the decision is made by the heart, <br> without expecting act but the decisio
                                    </p>
                                </div>
                                <a class="tp-btn" href="team-details.html">All Testimonials</a>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-6 col-md-6">
                            <div class="tp-testimonial__wrapper">
                                <div class="swiper-container tp-testimonial__active">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="tp-testimonial__item-box">
                                                <div class="tp-testimonial__item">
                                                    <div class="tp-testimonial__thumb">
                                                        <img src="assets/img/testimonial/author-1-1.png" alt="">
                                                    </div>
                                                    <div class="tp-testimonial__content p-relative">
                                                        <div class="tp-testimonial__text">
                                                            <div class="tp-testimonial__star">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                            </div>
                                                            <p>We can help with both your work and personal life tasks,
                                                                letting
                                                                you focus on what’s most important. simply are many
                                                                variations
                                                                of passages of orem Ipsum</p>
                                                        </div>
                                                        <div class="tp-testimonial__quote">
                                                            <span><i class="flaticon-double-quotes"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tp-testimonial__author-info">
                                                    <h4 class="tp-testimonial__author-name">Paul Pitterson</h4>
                                                    <span>Volunteers</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tp-testimonial__item-box">
                                                <div class="tp-testimonial__item">
                                                    <div class="tp-testimonial__thumb">
                                                        <img src="assets/img/testimonial/author-1-3.png" alt="">
                                                    </div>
                                                    <div class="tp-testimonial__content p-relative">
                                                        <div class="tp-testimonial__text">
                                                            <div class="tp-testimonial__star">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                            </div>
                                                            <p>We can help with both your work and personal life tasks,
                                                                letting
                                                                you focus on what’s most important. simply are many
                                                                variations
                                                                of passages of orem Ipsum</p>
                                                        </div>
                                                        <div class="tp-testimonial__quote">
                                                            <span><i class="flaticon-double-quotes"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tp-testimonial__author-info">
                                                    <h4 class="tp-testimonial__author-name">Lucas Oligan</h4>
                                                    <span>Designer</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tp-testimonial__item-box">
                                                <div class="tp-testimonial__item">
                                                    <div class="tp-testimonial__thumb">
                                                        <img src="assets/img/testimonial/author-1-1.png" alt="">
                                                    </div>
                                                    <div class="tp-testimonial__content p-relative">
                                                        <div class="tp-testimonial__text">
                                                            <div class="tp-testimonial__star">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                            </div>
                                                            <p>We can help with both your work and personal life tasks,
                                                                letting
                                                                you focus on what’s most important. simply are many
                                                                variations
                                                                of passages of orem Ipsum</p>
                                                        </div>
                                                        <div class="tp-testimonial__quote">
                                                            <span><i class="flaticon-double-quotes"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tp-testimonial__author-info">
                                                    <h4 class="tp-testimonial__author-name">Devid Roko</h4>
                                                    <span>Volunteers</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tp-testimonial__item-box">
                                                <div class="tp-testimonial__item">
                                                    <div class="tp-testimonial__thumb">
                                                        <img src="assets/img/testimonial/author-1-2.png" alt="">
                                                    </div>
                                                    <div class="tp-testimonial__content p-relative">
                                                        <div class="tp-testimonial__text">
                                                            <div class="tp-testimonial__star">
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                            </div>
                                                            <p>We can help with both your work and personal life tasks,
                                                                letting
                                                                you focus on what’s most important. simply are many
                                                                variations
                                                                of passages of orem Ipsum</p>
                                                        </div>
                                                        <div class="tp-testimonial__quote">
                                                            <span><i class="flaticon-double-quotes"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tp-testimonial__author-info">
                                                    <h4 class="tp-testimonial__author-name">Gimmy Alia</h4>
                                                    <span>Donor of charity</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- testimonial-area-end -->

        <!-- team-area-start -->
        {{-- <div class="tp-team-2__area pt-115 pb-65">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="tp-team-2__section-title pb-50 text-center">
                            <span class="tp-section-subtitle-2">our TEAM</span>
                            <h4 class="tp-section-title">Meet With Our Expert</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 wow tpfadeUp" data-wow-duration=".9s"
                    data-wow-delay=".3s">
                        <div class="tp-team-2__wrapper">
                            <div class="tp-team-2__item text-center">
                                <div class="tp-team-2__thumb">
                                    <img src="assets/img/team/team-1-1.jpg" alt="">
                                </div>
                                <div class="tp-team-2__content">
                                    <div class="tp-team-2__author-info">
                                        <a href="team-details.html"><h4 class="tp-team-2__author-name">Kaira Beasley</h4></a>
                                        <span>Volunteer</span>
                                    </div>
                                    <div class="tp-team-2__social-box">
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-instagram"></i></a>
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 wow tpfadeUp" data-wow-duration=".9s"
                    data-wow-delay=".5s">
                        <div class="tp-team-2__wrapper">
                            <div class="tp-team-2__item text-center">
                                <div class="tp-team-2__thumb">
                                    <img src="assets/img/team/team-1-2.jpg" alt="">
                                </div>
                                <div class="tp-team-2__content">
                                    <div class="tp-team-2__author-info">
                                        <a href="team-details.html"><h4 class="tp-team-2__author-name">Charlotte Ava</h4></a>
                                        <span>Volunteer</span>
                                    </div>
                                    <div class="tp-team-2__social-box">
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-instagram"></i></a>
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30 wow tpfadeUp" data-wow-duration=".9s"
                    data-wow-delay=".7s">
                        <div class="tp-team-2__wrapper">
                            <div class="tp-team-2__item text-center">
                                <div class="tp-team-2__thumb">
                                    <img src="assets/img/team/team-1-3.jpg" alt="">
                                </div>
                                <div class="tp-team-2__content">
                                    <div class="tp-team-2__author-info">
                                        <a href="team-details.html"><h4 class="tp-team-2__author-name">Alessio Done</h4></a>
                                        <span>Volunteer</span>
                                    </div>
                                    <div class="tp-team-2__social-box">
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-instagram"></i></a>
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-pinterest-p"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- team-area-end -->

        <!-- cta-area-start -->
        <!-- cta-area-end -->

    </main>
</body>

</html>   
@endsection
