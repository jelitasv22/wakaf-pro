@extends('Template.Layout.Index')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Form Donasi</title>
  <!-- Bootstrap CSS -->

</head>
<body>

<main>
    <div class="tp-breadcrumb__area p-relative fix tp-breadcrumb-height" data-background="assets/img/about/Main.png">
            <div class="tp-breadcrumb__shape-1 z-index-5">
                <img src="assets/img/breadcrumb/breadcrumb-shape-1.png" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="tp-breadcrumb__content z-index-5">
                            <div class="tp-breadcrumb__list">
                               <span><a href="/">home</a></span> 
                               <span class="dvdr"><i class="fa-sharp fa-solid fa-slash-forward"></i></span>
                               <span>Donasi</span>
                            </div>
                            <h3 class="tp-breadcrumb__title"> Donasi Sekarang</h3>
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tp-event-details__area pt-120 pb-120">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="tp-event-details__left-side">
                        
                        <h4 class="tp-event-details__title mb-45">Form Donasi</h4>
                        <div class="tp-donation-details__notice-box grey-bg">
                            <div class="tp-donation-details__notice-shape d-none d-md-block">
                                <img src="assets/img/donate/notic.png" alt="">
                            </div>
                            <span><b>perhatian:</b>Data dibawah merupakan data yang wajib diisi</span>
                        </div>
                        <div class="tp-donate__button-box">
                            <button>Rp 10.000</button>
                            <button>Rp 50.000</button>
                            <button>Rp 100.000</button>
                            <button>Rp 150.000</button>
                            <button>Rp 200.000</button>
                            <button>Rp 250.000</button>
                            <button>Rp 1.000.000</button>
                        </div>
                        <div class="tp-donation-details__input-box pb-25">
                            <div class="row">
                                <div class="col-xl-2 col-lg-2 col-md-2 mb-30">
                                    <div class="tp-donation-details__text-box">
                                        <span>Kustom</span>
                                     </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 mb-30">
                                    <div class="tp-donation-details__input">
                                        <input type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tp-donation-details__form-box">
                            <h4 class="tp-donation-details__form-title pb-45">Informasi Personal</h4>
                            <form action="#">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 mb-30">
                                        <div class="tp-donation-details__input">
                                            <label>Nama Lengkap</label>
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 mb-30">
                                        <div class="tp-donation-details__input">
                                            <label>Telepon</label>
                                            <input type="Number">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 mb-30">
                                        <div class="tp-donation-details__input">
                                            <label>Email</label>
                                            <input type="email">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 mb-30">
                                        <div class="tp-donation-details__input">
                                            <label>Komentarmu</label>
                                            <input type="text">
                                        </div>
                                    </div>
                                    <div class="postbox__comment-agree-2 pb-50">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                            <label class="form-check-label text-theme" for="flexCheckDefault">
                                                kami menyetujui syarat dan ketentuan
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</main>

@endsection