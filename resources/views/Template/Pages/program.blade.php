@extends('Template.Layout.Index')
@section('content')
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>proWakaf || Program</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>

<body>
    <main>

        <!-- breadcrumb-area-start -->
        <div class="tp-breadcrumb__area p-relative fix tp-breadcrumb-height" data-background="{{ asset('asset_template/img/about/Main.png') }}">

            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="tp-breadcrumb__content z-index-5">
                            <div class="tp-breadcrumb__list">
                               <span><a href="/home">home</a></span> 
                               <span class="dvdr"><i class="fa-sharp fa-solid fa-slash-forward"></i></span>
                               <span>Program</span>
                            </div>
                            <h3 class="tp-breadcrumb__title">Program</h3>
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb-area-end -->


        <!-- donate-area-end -->
        <div class="tp-donate-2__area pt-115 pb-75">
            <div class="container">
                <div class="row gx-50">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-40 wow tpfadeUp" data-wow-duration=".9s" data-wow-delay=".3s">
                        <div class="tp-donate-2__item">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="tp-donate-2__thumb">
                                        <img src="{{ asset('asset_template/img/program/konten1.png ') }}" alt="">
                                        <div class="tp-donate-2__thumb-text">
                                        <a href="/donasi"><i class="flaticon-tag"></i>Donasi</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="tp-donate-2__content">
                                        <div class="tp-donate-2__text">
                                            <a href="/detail-program ">
                                                <h5 class="tp-donate-2__title">Pengumpulan Wakaf Uang melalui Lembaga Keuangan Syariah</h5>
                                            </a>
                                            <p>Under his leadership, we believe <br> strengthen our position</p>
                                        </div> 
                                        <div class="tp-donate-2-progress">
                                            <div class="tp-donate-2-progress-item fix">
                                                <span class="progress-count">42%</span>
                                                <div class="progress">
                                                    <div class="progress-bar wow slideInLeft" data-wow-duration="1s"
                                                        data-wow-delay=".3s" role="progressbar" data-width="42%"
                                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"
                                                        style="width: 58%; visibility: visible; animation-duration: 1s; animation-delay: 0.3s; animation-name: slideInLeft;">
                                                    </div>
                                                </div>
                                                <div class="progress-goals">
                                                    <span>Raised <b> $4,407</b></span>
                                                    <span>Goal <b> $10.000</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tp-donate-2__button">
                                            <a class="tp-grey-btn" href="/donasi">Donasi sekarang</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 mb-40 wow tpfadeUp" data-wow-duration=".9s" data-wow-delay=".3s">
                        <div class="tp-donate-2__item">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="tp-donate-2__thumb">
                                        <img src="{{ asset('asset_template/img/program/konten1.png ') }}" alt="">
                                        <div class="tp-donate-2__thumb-text">
                                        <a href="donation-details.html"><i class="flaticon-tag"></i>Donasi</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="tp-donate-2__content">
                                        <div class="tp-donate-2__text">
                                            <a href="/detail-program ">
                                                <h5 class="tp-donate-2__title">Penanaman Pohon Karet berupa Kebun Wakaf Produktif</h5>
                                            </a>
                                            <p>Under his leadership, we believe <br> strengthen our position</p>
                                        </div> 
                                        <div class="tp-donate-2-progress">
                                            <div class="tp-donate-2-progress-item fix">
                                                <span class="progress-count">42%</span>
                                                <div class="progress">
                                                    <div class="progress-bar wow slideInLeft" data-wow-duration="1s"
                                                        data-wow-delay=".3s" role="progressbar" data-width="42%"
                                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"
                                                        style="width: 58%; visibility: visible; animation-duration: 1s; animation-delay: 0.3s; animation-name: slideInLeft;">
                                                    </div>
                                                </div>
                                                @include('Template.Component.progres-goals')
                                            </div>
                                        </div>
                                        <div class="tp-donate-2__button">
                                            <a class="tp-grey-btn" href="/donasi">Donasi sekarang</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="basic-pagination text-center pt-30">
                            <nav>
                               <ul>
                                  <li>
                                     <a href="/program_wakaf">1</a>
                                  </li>
                                  <li>
                                     <a href="/program_wakaf">2</a>
                                  </li>
                                  <li>
                                     <a href="/program_wakaf">3</a>
                                  </li>
                                  <li>
                                     <a href="/program_Wakaf">
                                        <span class="current"><i class="flaticon-arrow-right"></i></span>
                                     </a>
                                  </li>
                               </ul>
                            </nav>
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- donate-area-end -->


    </main>
</body>

</html>
@endsection
