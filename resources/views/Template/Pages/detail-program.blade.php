@extends('Template.Layout.Index')
@section('content')
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">




<body>
    <main>

        <!-- breadcrumb-area-start -->
        <div class="tp-breadcrumb__area p-relative fix tp-breadcrumb-height" data-background="{{ asset('asset_template/img/about/Main.png') }}">
            <div class="tp-breadcrumb__shape-1 z-index-5">
                <img src="assets/img/breadcrumb/breadcrumb-shape-1.png" alt="">
            </div>
            <div class="tp-breadcrumb__shape-2 z-index-5">
                <img src="assets/img/breadcrumb/breadcrumb-shape-2.png" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="tp-breadcrumb__content z-index-5">
                            <div class="tp-breadcrumb__list">
                               <span><a href="index.html">home</a></span> 
                               <span class="dvdr"><i class="fa-sharp fa-solid fa-slash-forward"></i></span>
                               <span>Detail donasi</span>
                            </div>
                            <h3 class="tp-breadcrumb__title">Detail Donasi</h3>
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb-area-end -->

        <!-- event-area-atart -->
        <div class="tp-event-details__area pt-120 pb-120">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="tp-event-details__left-box">
                            <div class="tp-event-details__thumb p-relative pb-35">
                                <img src="{{ asset('asset_template/img/program/Detail-program.png') }}" alt="">
                            </div>
                            <h4 class="tp-event-details__title">Pengumpulan wakaf uang melalui lembaga keuangan syariah</h4>
                            <div class="tp-event-details__text pb-35">
                                <p>Curabitur luctus euismod metus, eu pellentesque mauris tempus sit amet. Proin ante
                                    posuerelacus auctor, elementum tempor tellus. Integer mattis justo eu enim tempus
                                    lacinia. Fusce vitae enim diam. Ut commodo viverra magna non egestas. Integer
                                    sodales massa at odio tristique volutpat. Proin posuere odio maximus, eleifend felis
                                    sed, ultrices turpis. Proin ultricies sodales nisl vel euismod. Praesent vestibulum
                                    sem lorem, eget fermentum justo iaculis et. Integer tellus dolor, venenatis vita
                                    tortor et,accumsan laoreet sem. Sed laoreet rutrum ex, et efficitur </p>
                            </div>
                            @include('Template.Component.data-donation')
                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <!-- event-area-end -->




    </main>

@endsection

</body>

</html>