@extends('Template.Layout.Index')
@section('content')
<main>

<!-- breadcrumb-area-start -->
<div class="tp-breadcrumb__area p-relative fix tp-breadcrumb-height" data-background="{{ asset('asset_template/img/about/Main.png') }}">
    <div class="tp-breadcrumb__shape-1 z-index-5">
        <img src="assets/img/breadcrumb/breadcrumb-shape-1.png" alt="">
    </div>
    <div class="tp-breadcrumb__shape-2 z-index-5">
        <img src="assets/img/breadcrumb/breadcrumb-shape-2.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="tp-breadcrumb__content z-index-5">
                    <div class="tp-breadcrumb__list">
                       <span><a href="/">home</a></span> 
                       <span class="dvdr"><i class="fa-sharp fa-solid fa-slash-forward"></i></span>
                       <span>Artikel</span>
                    </div>
                    <h3 class="tp-breadcrumb__title">Artikel</h3>
                 </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area-end -->

<!-- event-area-atart -->
<div class="tp-event-details__area pt-120 pb-120">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8">
                <div class="tp-event-details__left-box">
                    <div class="tp-event-details__thumb p-relative pb-35">
                        <img src="{{ asset('asset_template/img/artikel/Banner.png') }}" alt="">
                        <div class="tp-event-details__thumb-text d-none d-md-block">
                            <span>08 April</span>
                        </div>
                    </div>
                    <h4 class="tp-event-details__title">Spirit Berqurban dan Manfaatnya bagi Sesama</h4>
                    <div class="tp-event-details__text pb-35">
                        <p>Dalam setiap detik kehidupan, tradisi berqurban membawa aroma keharmonisan dan kepedulian yang mendalam. Tidak sekadar ritual ibadah, berqurban merupakan simbol tulusnya rasa syukur kita kepada Sang Pencipta dan dedikasi untuk menghidupkan semangat berbagi dengan sesama.

                            Setiap tahun, saat bulan Dzulhijjah menjelang, kita disajikan dengan momentum emas untuk menunjukkan kecintaan kita kepada Allah SWT. Namun, di balik ritual yang khusyuk itu, terdapat kisah-kisah inspiratif dari setiap individu yang berqurban. Mereka yang dengan ikhlas menyisihkan sebagian rezeki untuk memastikan bahwa tidak ada saudara mereka yang merasa kelaparan atau terpinggirkan.
                            
                            Ketika keluarga-keluarga berkumpul untuk melaksanakan berqurban, mereka tidak hanya mengisi perut dengan daging yang nikmat. Mereka, dengan rasa syukur yang mendalam, memastikan bahwa setiap potongan daging tersebut akan mencapai tangan-tangan yang membutuhkan. Dalam setiap irisannya, terdapat doa-doa dan harapan untuk keberkahan bagi mereka yang menerimanya.
                            
                            Namun, manfaat berqurban tidak berhenti sampai di situ. Setiap proses berqurban mengajarkan kita tentang nilai-nilai kebersamaan, empati, dan solidaritas. Ia mengingatkan kita bahwa di luar sana, ada banyak saudara kita yang membutuhkan bantuan kita. Semangat berqurban mengajak kita untuk tidak hanya berbicara, tetapi juga beraksi, bergerak tangan mempererat tali kasih, dan menghangatkan hati yang sedang merasa dingin.
                            
                            Di era digital ini, semangat berqurban dapat menembus batas-batas geografis. Melalui platform donasi online, kita dapat menyebarkan kebaikan lebih luas lagi, mengajak banyak orang untuk berpartisipasi dalam misi mulia ini. Mari kita jadikan momentum berqurban sebagai ladang kebaikan yang tak berbatas, di mana setiap donasi yang kita sumbangkan akan menjadi benih kebahagiaan bagi banyak jiwa yang membutuhkan. </p>
                    </div>
                    @include('Template.Component.data-donation')
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="tp-event-details__right-box"> 
                    <div class="sidebar__widget mb-30">
                        <h3 class="sidebar__widget-title">Berita Lainnya</h3>
                        <div class="sidebar__widget-content">
                            <div class="sidebar__post">
                                <div class="rc__post mb-10 d-flex align-items-center">
                                    <div class="rc__post-thumb mr-20">
                                        <a href="/artikel"><img
                                                src="{{ asset('asset_template/img/artikel/Artikel1.png') }}" alt=""></a>
                                    </div>
                                    <div class="rc__post-content">
                                        <div class="rc__meta">
                                            <span><i class="flaticon-comment"></i>
                                                02 Comments</span>
                                        </div>
                                        <h3 class="rc__post-title">
                                            <a href="/artikel">We Should Donate <br>For Indian’s
                                                Poor</a>
                                        </h3>
                                    </div>
                                </div>
                                <div class="rc__post mb-10 d-flex align-items-center">
                                    <div class="rc__post-thumb mr-20">
                                        <a href="/artikel"><img
                                                src="{{ asset('asset_template/img/artikel/Artikel1.png') }}" alt=""></a>
                                    </div>
                                    <div class="rc__post-content">
                                        <div class="rc__meta">
                                            <span><i class="flaticon-comment"></i>
                                                02 Comments</span>
                                        </div>
                                        <h3 class="rc__post-title">
                                            <a href="/artikel">Quick Fundraising For <br>African
                                                Child</a>
                                        </h3>
                                    </div>
                                </div>
                                <div class="rc__post d-flex align-items-center">
                                    <div class="rc__post-thumb mr-20">
                                        <a href="/artikel"><img
                                                src="{{ asset('asset_template/img/artikel/Artikel1.png') }}" alt=""></a>
                                    </div>
                                    <div class="rc__post-content">
                                        <div class="rc__meta">
                                            <span><i class="flaticon-comment"></i>
                                                02 Comments</span>
                                        </div>
                                        <h3 class="rc__post-title">
                                            <a href="blog-details.html">Bring The People For <br>Raise Hands</a>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- event-area-end -->




</main>
@endsection