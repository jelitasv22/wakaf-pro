@extends('Template.Layout.Index')
@section('content')
    <main>

        <!-- breadcrumb-area-start -->
        <div class="tp-breadcrumb__area p-relative fix tp-breadcrumb-height" data-background="{{ asset('asset_template/img/about/Main.png') }}">
            <div class="tp-breadcrumb__shape-1 z-index-5">
                <img src="assets/img/breadcrumb/breadcrumb-shape-1.png" alt="">
            </div>
            <div class="tp-breadcrumb__shape-2 z-index-5">
                <img src="assets/img/breadcrumb/breadcrumb-shape-2.png" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="tp-breadcrumb__content z-index-5">
                            <div class="tp-breadcrumb__list">
                               <span><a href="index.html">home</a></span> 
                               <span class="dvdr"><i class="fa-sharp fa-solid fa-slash-forward"></i></span>
                               <span>kontak</span>
                            </div>
                            <h3 class="tp-breadcrumb__title">kontak</h3>
                         </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb-area-end -->


        <!-- form-area-start -->
        <div class="tp-contact-form__area tp-contact-form__space">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-5 wow tpfadeLeft" data-wow-duration=".9s"
                    data-wow-delay=".3s">
                        <div class="tp-contact-form__left-box">
                            <span class="tp-contact-form__subtitle">Konsultasi dengan proWakaf</span>
                            <h4 class="tp-section-title pb-20">Just have a quick <br>any questions?</h4>
                            <p>Dalam setiap tindakan kebaikan yang dilakukan, amal membantu membentuk dunia yang lebih baik dan lebih peduli untuk kita semua.</p>
                            <div class="tp-contact-form__social-box">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-instagram"></i></a>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                                <a href="#"><i class="fab fa-pinterest-p"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7 wow tpfadeRight" data-wow-duration=".9s"
                    data-wow-delay=".7s">
                        <div class="tp-contact-form__form-box">
                            <form action="#">
                                <div class="row">
                                    <div class="col-xl-12 mb-30">
                                        <div class="tp-contact-form__input-box">
                                            <input type="text" placeholder="Nama Lengkap">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 mb-30">
                                        <div class="tp-contact-form__input-box">
                                            <input type="email" placeholder="Masukkan Email">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 mb-30">
                                        <div class="tp-contact-form__input-box">
                                            <input type="text" placeholder="Nomor Hp">
                                        </div>
                                    </div>
                                    <div class="col-xl-6 mb-30">
                                        <div class="tp-contact-form__input-box">
                                            <input type="text" placeholder="Subjek">
                                        </div>
                                    </div>
                                    <div class="col-xl-12 mb-30">
                                        <div class="tp-contact-form__textarea-box">
                                            <textarea placeholder="Tulis pesanmu"></textarea>
                                        </div>
                                    </div>
                                </div>                                
                            </form>
                            <div class="tp-contact-form__button">
                                <button class="tp-btn">Kirim</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- form-area-end -->

        <!-- contact-area-start -->
        <div class="tp-contact__area">
            <div class="container">
                <div class="tp-contact__bg">
                    <div class="tp-contact__wrapper d-flex align-items-center justify-content-between">
                        <div class="tp-contact__item d-flex align-items-center">
                            <div class="tp-contact__icon">
                              <span><i class="flaticon-phone"></i></span>
                            </div>
                            <div class="tp-contact__text">
                                <a href="tel:+92307776-0800">+ 92 (307) 776-0800</a>
                                <a href="tel:+008898768">+ 00 (8898) 768</a>
                            </div>
                        </div>                        
                        <div class="tp-contact__item d-flex align-items-center">
                            <div class="tp-contact__icon">
                              <span><i class="flaticon-email"></i></span>
                            </div>
                            <div class="tp-contact__text">
                                <a href="mailto:poorexcharityhelp@gmail.com">poorexcharityhelp@gmail.com</a>
                                <a href="mailto:infocompany@gmail.com">infocompany@gmail.com</a>
                            </div>
                        </div>
                        <div class="tp-contact__item d-flex align-items-center">
                            <div class="tp-contact__icon">
                              <span><i class="flaticon-location"></i></span>
                            </div>
                            <div class="tp-contact__text">
                                <a href="#">55 Hereford catdal street line <br> New york, USA</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- contact-area-end -->

        <!-- location-area-start -->
        <div class="tp-location__area">
            <div class="container-fluid g-0">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="tp-location__info-box">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d2803502.0308645153!2d89.16338837780354!3d23.65743155768624!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1684146049617!5m2!1sen!2sbd" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- location-area-end -->


    </main>

@endsection