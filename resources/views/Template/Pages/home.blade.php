@extends('Template.Layout.Index')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
</head>
<body>
    @include('Template.Component.hero-banner')
         <!-- brand-area-start -->
         @include('Template.Component.Brand-area')
        <!-- brand-area-end -->
           <!-- about-area-start -->
           @include('Template.Component.about')
           <!-- about-area-end -->
                 <!-- mission-area-start -->
           @include('Template.Component.category') 
        <!-- mission-area-end -->
        @include('Template.Component.gallery')
        <!-- blog-area-start -->
        @include('Template.Component.news')
        <!-- blog-area-end -->
        @include('Template.Component.artikel')

     
</body>
</html>
@endsection
