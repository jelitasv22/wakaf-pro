<div class=" tp-slider-2__area tp-slider-2__mb p-relative">
<div class="tp-slider-2__grey-bg"></div>
<div class="container">
    <div class="row">
        <div class="col-xl-7 col-lg-6">
            <div class="tp-slider-2__content">
                <div class="tp-slider-2__section-box pb-10">
                    {{-- <span class="tp-slider-2-subtitle wow tpfadeUp" data-wow-duration=".9s"
                    data-wow-delay=".3s">START donate on our charity</span> --}}
                    <h4 class="tp-slider-2-title wow tpfadeUp" data-wow-duration=".9s"
                    data-wow-delay=".5s">Wakaf <b>Produktif</b>  <br>Berdayakan Umat<br></h4>
                </div>
                <div class="tp-slider-2__content-text">
                    <p class="wow tpfadeUp" data-wow-duration=".9s"
                    data-wow-delay=".7s">Bantu kami membangun masa depan yang menghargai setiap kehidupan manusia. Bergabunglah dengan proyek kesejahteraan kami di bidang pendidikan, kesehatan, dan penghidupan.</p>
                    <a class="tp-btn wow tpfadeUp" data-wow-duration=".9s"
                    data-wow-delay=".9s" href="/donasi">Donasi Sekarang</a>
                </div>
            </div>
        </div>
        <div class="col-xl-5 col-lg-6">
            <div class="tp-slider-2__main-thumb">
                <img src="{{ asset('asset_template/img/logo/poor-kids.svg') }}" alt="">
            </div>
        </div>
    </div>
</div>
</div>