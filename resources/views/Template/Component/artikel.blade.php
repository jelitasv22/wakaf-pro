<div class="tp-blog-2__area tp-blog-2__space">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="tp-blog-2__section-title pb-50 text-center">
                    <span class="tp-section-subtitle">Artikel kami</span>
                    <h4 class="tp-section-title">Artikel Terbaru</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 mb-30 wow tpfadeUp" data-wow-duration=".9s"
            data-wow-delay=".7s">
                <div class="tp-blog-2__item">
                    <div class="tp-blog-2__thumb p-relative">
                        <img src="{{ asset('asset_template/img/artikel/artikel-home.png') }}" alt="">
                        <div class="tp-blog-2__icon">
                            <a class="popup-image" href="{{ asset('asset_template/img/artikel/artikel-home.png') }}"><i class="fa-sharp fa-solid fa-plus"></i></a>
                        </div>
                    </div>
                    <div class="tp-blog-2__content">
                        <div class="tp-blog-2__tag">
                            <span><i class="flaticon-tag"></i>Donasi</span>
                        </div>
                        <a href="blog-details.html"><h4 class="tp-blog-2__title-sm">Donasi Kita, Untuk Cita Anak Miskin Indonesia Terwujud</h4></a>
                        <span class="tp-blog-2__meta">OCTOBER 24, 2022</span>
                        <a href="/artikel">
                            <div class="tp-blog-2__link text-center">
                                <span>Baca Selengkapnya<i class="flaticon-arrow-right"></i></span>
                            </div>
                        </a> 
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 mb-30 wow tpfadeUp" data-wow-duration=".9s"
            data-wow-delay=".7s">
                <div class="tp-blog-2__item">
                    <div class="tp-blog-2__thumb p-relative">
                        <img src="{{ asset('asset_template/img/artikel/Banner.png') }}" alt="">
                        <div class="tp-blog-2__icon">
                            <a class="popup-image" href="{{ asset('asset_template/img/artikel/Banner.png') }}"><i class="fa-sharp fa-solid fa-plus"></i></a>
                        </div>
                    </div>
                    <div class="tp-blog-2__content">
                        <div class="tp-blog-2__tag">
                            <span><i class="flaticon-tag"></i>Zakat</span>
                        </div>
                        <a href="blog-details.html"><h4 class="tp-blog-2__title-sm">Makna Zakat: Zakat Itu lebih Dari Sekadar Kewajiban</h4></a>
                        <span class="tp-blog-2__meta">OCTOBER 24, 2022</span>
                        <a href="/artikel">
                            <div class="tp-blog-2__link text-center">
                                <span>Baca Selengkapnya<i class="flaticon-arrow-right"></i></span>
                            </div>
                        </a> 
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 mb-30 wow tpfadeUp" data-wow-duration=".9s"
            data-wow-delay=".7s">
                <div class="tp-blog-2__item">
                    <div class="tp-blog-2__thumb p-relative">
                        <img src="{{ asset('asset_template/img/artikel/pendidikan.png') }}" alt="">
                        <div class="tp-blog-2__icon">
                            <a class="popup-image" href="{{ asset('asset_template/img/artikel/pendidikan.png') }}"><i class="fa-sharp fa-solid fa-plus"></i></a>
                        </div>
                    </div>
                    <div class="tp-blog-2__content">
                        <div class="tp-blog-2__tag">
                            <span><i class="flaticon-tag"></i>Pendidikan</span>
                        </div>
                        <a href="blog-details.html"><h4 class="tp-blog-2__title-sm">Infaq Pendidikan: Investasi Masa Depan Generasi Bangsa</h4></a>
                        <span class="tp-blog-2__meta">OCTOBER 24, 2022</span>
                        <a href="/artikel">
                            <div class="tp-blog-2__link text-center">
                                <span>Baca Selengkapnya<i class="flaticon-arrow-right"></i></span>
                            </div>
                        </a> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>