<div class="tp-mission-2__area tp-mission-2__space p-relative fix z-index grey-bg">
    <div class="tp-mission-2__shape">
        <img src="assets/img/mission/mission-shape-1.png" alt="">
    </div>
    <div class="tp-mission-2__plr">
        <div class="container-fluid g-0">
            <div class="row">
                <div class="col-xl-5 col-lg-7 col-md-7">
                    <div class="tp-mission-2__left-box">
                        <div class="tp-mission-2__section-box pb-10">
                         
                            <h4 class="tp-section-title">Bersama kami  <br>Membantu sesama </h4>
                        </div>
                        <div class="tp-mission-2__left-text">
                            <p>Charity is the act of extending love and kindness to <br>others unconditional which
                             is a conscious act.</p>
                            <a class="tp-btn" href="/donasi">Donasi sekarang</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-5 col-md-5">
                    <div class="tp-mission-2__wrapper">
                        <div class="swiper-container tp-mission-2__active">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="tp-mission-2__item">
                                        <div class="tp-mission-2__single-bg"></div>
                                        <div class="tp-mission-2__icon">
                                            <span><svg xmlns="http://www.w3.org/2000/svg" width="71" height="71" viewBox="0 0 71 71" fill="none">
                                                <path d="M47.3333 18.5784C47.333 20.3584 46.629 22.0662 45.3749 23.3294C42.4875 26.2404 39.686 29.2757 36.6892 32.0802C36.3546 32.385 35.9158 32.5497 35.4633 32.5404C35.0108 32.531 34.5791 32.3483 34.2574 32.0299L25.625 23.3324C24.3702 22.0684 23.666 20.3595 23.666 18.5784C23.666 16.7972 24.3702 15.0884 25.625 13.8243C26.2512 13.1935 26.9959 12.6929 27.8164 12.3513C28.6369 12.0097 29.5169 11.8338 30.4057 11.8338C31.2945 11.8338 32.1745 12.0097 32.9949 12.3513C33.8154 12.6929 34.5602 13.1935 35.1864 13.8243L35.4999 14.1409L35.8135 13.8243C36.7534 12.8743 37.9547 12.2254 39.2645 11.9603C40.5743 11.6953 41.9334 11.8259 43.1687 12.3357C44.404 12.8455 45.4597 13.7114 46.2013 14.8231C46.9429 15.9348 47.337 17.242 47.3333 18.5784Z" stroke="#FE7F4C" stroke-width="3" stroke-linejoin="round"/>
                                                <path d="M53.25 59.1667L64.5627 47.854C64.8956 47.5214 65.0829 47.0703 65.0833 46.5997V31.0625C65.0833 29.8856 64.6158 28.7569 63.7836 27.9247C62.9514 27.0925 61.8227 26.625 60.6458 26.625C59.4689 26.625 58.3402 27.0925 57.508 27.9247C56.6759 28.7569 56.2083 29.8856 56.2083 31.0625V44.375" stroke="#FE7F4C" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                                                <path d="M53.25 47.3333L55.7882 44.7951C55.9216 44.6619 56.0273 44.5037 56.0994 44.3296C56.1715 44.1555 56.2085 43.9688 56.2083 43.7804C56.2079 43.5143 56.1335 43.2537 55.9935 43.0275C55.8535 42.8013 55.6534 42.6184 55.4155 42.4994L54.1049 41.8456C52.9946 41.2905 51.7378 41.0986 50.5125 41.2972C49.2871 41.4957 48.1552 42.0747 47.2771 42.952L44.6294 45.5997C43.5197 46.7091 42.8961 48.2138 42.8958 49.7828V59.1667M17.75 59.1667L6.43729 47.854C6.10432 47.5214 5.91704 47.0703 5.91663 46.5997V31.0625C5.91663 29.8856 6.38415 28.7569 7.21634 27.9247C8.04853 27.0925 9.17723 26.625 10.3541 26.625C11.531 26.625 12.6597 27.0925 13.4919 27.9247C14.3241 28.7569 14.7916 29.8856 14.7916 31.0625V44.375" stroke="#FE7F4C" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                                                <path d="M17.75 47.3333L15.2117 44.7951C15.0785 44.6618 14.9728 44.5036 14.9007 44.3295C14.8287 44.1554 14.7916 43.9688 14.7916 43.7803C14.7916 43.239 15.0993 42.7449 15.5845 42.4994L16.895 41.8456C18.0053 41.2904 19.2621 41.0986 20.4874 41.2971C21.7128 41.4957 22.8447 42.0746 23.7228 42.952L26.3705 45.5997C27.4802 46.709 28.1038 48.2137 28.1041 49.7828V59.1666" stroke="#FE7F4C" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
                                              </svg></span>
                                        </div>
                                        <div class="tp-mission-2__text">
                                            <a href="/detail-program"><h4 class="tp-mission-2__title-sm">Zakat</h4></a>
                                            <p>     Dengan berzakat, kita berinvestasi dalam keberkahan dan kesejahteraan bersama.</p>
                                        </div>
                                        <div class="tp-mission-2__shape-box">
                                            <span class="tp-mission-2__arrow"><i class="fa-light fa-arrow-right-long"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="tp-mission-2__item">
                                        <div class="tp-mission-2__single-bg"></div>
                                        <div class="tp-mission-2__icon">
                                            <span><svg xmlns="http://www.w3.org/2000/svg" width="60" height="60" viewBox="0 0 60 60" fill="none">
                                                <path d="M30 5.625C40.3387 5.625 48.75 14.0363 48.75 24.375C48.75 34.7137 40.3387 43.125 30 43.125C19.6613 43.125 11.25 34.7137 11.25 24.375C11.25 14.0363 19.6613 5.625 30 5.625ZM30 9.375C21.7294 9.375 15 16.1044 15 24.375C15 32.6456 21.7294 39.375 30 39.375C38.2706 39.375 45 32.6456 45 24.375C45 16.1044 38.2706 9.375 30 9.375ZM31.875 13.125V15.3562C33.227 15.8478 34.3464 16.8268 35.0138 18.1013C35.5949 19.2232 35.7698 20.5118 35.5087 21.7481C35.278 22.8238 34.7431 23.8104 33.9675 24.5906C33.4556 25.1025 32.8463 25.5169 32.175 25.8038C31.5037 26.0906 30.7687 26.25 30 26.25C29.7337 26.25 29.4825 26.3006 29.2575 26.3925C29.0348 26.4831 28.8324 26.6174 28.6624 26.7874C28.4924 26.9574 28.3581 27.1598 28.2675 27.3825C28.1756 27.6075 28.125 27.8587 28.125 28.125C28.1168 28.4943 28.2206 28.8575 28.4228 29.1667C28.6249 29.476 28.9159 29.7168 29.2575 29.8575C29.4825 29.9494 29.7337 30 30 30C30.2681 30 30.5175 29.9494 30.7425 29.8575C30.965 29.7666 31.1671 29.6321 31.3369 29.4619C31.5107 29.2858 31.6477 29.0769 31.7401 28.8474C31.8325 28.6179 31.8783 28.3724 31.875 28.125H35.625C35.6248 29.5839 35.0471 30.9833 34.0181 32.0175C33.416 32.6316 32.6836 33.1026 31.875 33.3956V35.625H28.125V33.3937C27.1796 33.0549 26.3411 32.4709 25.6954 31.7017C25.0497 30.9324 24.6199 30.0055 24.45 29.0156C24.2184 27.6096 24.5396 26.1684 25.3462 24.9937C25.7532 24.3947 26.2697 23.8782 26.8687 23.4712C27.4759 23.0592 28.1573 22.7689 28.875 22.6163C29.245 22.539 29.622 22.5001 30 22.5C30.2681 22.5 30.5175 22.4494 30.7425 22.3575C30.9652 22.2669 31.1676 22.1326 31.3376 21.9626C31.5076 21.7926 31.6419 21.5902 31.7325 21.3675C31.8278 21.1316 31.8762 20.8794 31.875 20.625C31.875 20.3569 31.8244 20.1075 31.7325 19.8825C31.6419 19.6598 31.5076 19.4574 31.3376 19.2874C31.1676 19.1174 30.9652 18.9831 30.7425 18.8925C30.5175 18.8006 30.2663 18.75 30 18.75C28.9312 18.75 28.125 19.5562 28.125 20.625H24.375C24.3745 20.0364 24.4682 19.4515 24.6525 18.8925C24.9285 18.0801 25.384 17.3403 25.9852 16.7281C26.5863 16.1159 27.3177 15.647 28.125 15.3562V13.125H31.875ZM56.25 39.375V54.375H52.5V43.125H42.4125C44.0024 42.0619 45.4514 40.8019 46.725 39.375H56.25ZM13.275 39.375C14.5486 40.8019 15.9976 42.0619 17.5875 43.125H7.5V54.375H3.75V39.375H13.275ZM48.75 46.875V50.625H11.25V46.875H48.75Z" fill="#FE7F4C"/>
                                              </svg></i></span>
                                        </div>
                                        <div class="tp-mission-2__text">
                                            <a href="donation-details.html"><h4 class="tp-mission-2__title-sm">Infaq </h4></a>
                                            <p>Infaq mengalirkan rezeki, membangun jembatan kemanusiaan & berbagi harapan untuk orang diluar sana.</p>
                                        </div>
                                        <div class="tp-mission-2__shape-box">
                                            <span class="tp-mission-2__arrow"><i class="fa-light fa-arrow-right-long"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="tp-mission-2__item">
                                        <div class="tp-mission-2__single-bg"></div>
                                        <div class="tp-mission-2__icon">
                                            <span><svg xmlns="http://www.w3.org/2000/svg" width="53" height="52" viewBox="0 0 53 52" fill="none">
                                                <path d="M37.8571 0C30.8914 0 25.2381 5.68195 25.2381 12.6829C25.2381 19.6839 30.8914 25.3659 37.8571 25.3659C44.8229 25.3659 50.4762 19.6839 50.4762 12.6829C50.4762 5.68195 44.8229 0 37.8571 0ZM37.8571 20.2927C33.6676 20.2927 30.2857 16.8937 30.2857 12.6829C30.2857 8.47219 33.6676 5.07317 37.8571 5.07317C42.0467 5.07317 45.4286 8.47219 45.4286 12.6829C45.4286 16.8937 42.0467 20.2927 37.8571 20.2927ZM45.4286 35.5122H40.381C40.381 32.4683 38.4881 29.7288 35.6614 28.6634L20.1148 22.8293H0V50.7317H15.1429V47.079L32.8095 52L53 45.6585V43.122C53 38.9112 49.6181 35.5122 45.4286 35.5122ZM10.0952 45.6585H5.04762V27.9024H10.0952V45.6585ZM32.7338 46.6985L15.1429 41.8537V27.9024H19.2062L33.8948 33.4068C34.7529 33.7366 35.3333 34.5737 35.3333 35.5122C35.3333 35.5122 30.2857 35.3854 29.5286 35.1317L23.5219 33.1278L21.9319 37.9473L27.9386 39.9512C29.2257 40.3824 30.5633 40.5854 31.9262 40.5854H45.4286C46.4129 40.5854 47.2962 41.1941 47.7 42.0312L32.7338 46.6985Z" fill="#FE7F4C"/>
                                              </svg></span>
                                        </div>
                                        <div class="tp-mission-2__text">
                                            <a href="donation-details.html"><h4 class="tp-mission-2__title-sm">
                                                Wakaf
                                            </h4></a>
                                            <p>Melalui wakaf, kita menanamkan jejak kebaikan yang tak terhingga untuk seseorang diluar sana yang mengalami kesulitan.</p>
                                        </div>
                                        <div class="tp-mission-2__shape-box">
                                            <span class="tp-mission-2__arrow"><i class="fa-light fa-arrow-right-long"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="tp-mission-2__item">
                                        <div class="tp-mission-2__single-bg" data-background="assets/img/mission/mission-bg.png"></div>
                                        <div class="tp-mission-2__icon">
                                            <span><i class="flaticon-book"></i></span>
                                        </div>
                                        <div class="tp-mission-2__text">
                                            <a href="donation-details.html"><h4 class="tp-mission-2__title-sm">Kids Education</h4></a>
                                            <p>Consectetur adipiscing elsed <br> do eiusmod te incididunt ut <br> labore et dolore</p>
                                        </div>
                                        <div class="tp-mission-2__shape-box">
                                            <span class="tp-mission-2__arrow"><i class="fa-light fa-arrow-right-long"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>