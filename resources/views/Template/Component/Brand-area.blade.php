<div class="tp-brand-2__area">
            <div class="container">
                <div class="tp-brand-2__border">
                    <div class="row">
                        <div class="col-12">
                            <div class="tp-brand-2__wrapper">
                                <div class="swiper-container tp-brand-2__active">
                                    <div class="swiper-wrapper"> 
                                        <div class="swiper-slide">
                                            <div class="tp-brand-2__item text-center">
                                                <img src="{{ asset('asset_template/img/logo/client/Unpad.svg') }}" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tp-brand-2__item text-center">
                                                <img src="{{ asset('asset_template/img/logo/client/BARA.svg') }}" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tp-brand-2__item text-center">
                                                <img src="{{ asset('asset_template/img/logo/client/NGO360.svg') }}" alt="">
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tp-brand-2__item text-center">
                                                <img src="{{ asset('asset_template/img/logo/client/Finpos.svg') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>