<div class="tp-about-2__area fix p-relative pt-120 pb-120">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-5 col-lg-5 wow tpfadeLeft" data-wow-duration=".9s"
            data-wow-delay=".3s">
                <div class="tp-about-2__main-thumb p-relative text-center text-lg-end">
                    <img src="{{ asset('asset_template/img/about/poorkids.jpg.png') }}" alt="">
                    <div class="tp-about-2__thumb-sm">
                        <img src="{{ asset('asset_template/img/about/about-1.png') }}" alt="">
                    </div>
                    <div class="tp-about-2__thumb-border"></div>
                </div>
            </div>
            <div class="col-xl-7 col-lg-7 wow tpfadeRight" data-wow-duration=".9s"
            data-wow-delay=".9s">
                <div class="tp-about-2__right-box">
                    <div class="tp-about-2__section-title pb-25">
                        <span class="tp-section-subtitle-2">ABOUT US</span>
                        <h4 class="tp-section-title">Investasi Cinta <br> untuk Umat</h4>
                    </div>
                    <div class="tp-about-2__text">
                        <p>Kami adalah lembaga amal yang berdedikasi untuk membawa keberkahan melalui wakaf. Dengan hati yang penuh cinta, kami menghubungkan dermawan dengan mereka yang membutuhkan bantuan. Fokus kami meliputi pendidikan, kesehatan, ekonomi, dan bantuan sosial. Bersama, mari ciptakan perubahan positif. Bergabunglah dengan kami untuk menjadi bagian dari kebaikan yang tak terhingga.
                        </p>
                    </div>
                    <div class="tp-about-2__bottom d-flex">
                        <div class="tp-about-2__btn">
                            <a class="tp-btn" href="/donasi">Donasi Sekarang</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>