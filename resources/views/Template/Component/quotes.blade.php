        <!-- cta-area-start -->
        <div class="tp-cta__area">
            <div class="tp-cta__bg" data-background="assets/img/cta/cta-bg.jpg">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-xl-7 col-lg-7 col-md-7">
                            <div class="tp-cta__content d-flex align-items-center">
                                <div class="tp-cta__icon d-none d-sm-block">
                                    <img src="assets/img/cta/cta-icon-1.png" alt="">
                                </div>
                                <h4 class="tp-cta__title-sm">Join your hand with us for a <br> better life and future
                                </h4>
                            </div>
                        </div>
                        <div class="col-xl-5 col-lg-5 col-md-5">
                            <div class="tp-cta__button text-md-end text-start">
                                <a class="tp-btn" href="/donasi">Donasi sekarang</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- cta-area-end -->