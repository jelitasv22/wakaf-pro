<div
                                class="tp-donation-details__progress-box  grey-bg d-flex align-items-center justify-content-between mb-50">
                                <div class="tp-donation-details__progress">
                                    <div class="tp-donation-details__progress-item fix">
                                        <span class="progress-count">80%</span>
                                        <div class="progress">
                                            <div class="progress-bar wow slideInLeft" data-wow-duration="1s"
                                                data-wow-delay=".3s" role="progressbar" data-width="80%"
                                                aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"
                                                style="width: 58%; visibility: visible; animation-duration: 1s; animation-delay: 0.3s; animation-name: slideInLeft;">
                                            </div>
                                        </div>
                                        <div class="progress-goals">
                                            <span>Terkumpul <b> Rp 15.000.000</b></span>
                                            <span>Target <b> Rp 25.000.000</b></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tp-donation-details__button d-none d-xl-block">
                                    <a class="tp-btn" href="/donasi">Donasi Sekarang</a>
                                </div>
                            </div>