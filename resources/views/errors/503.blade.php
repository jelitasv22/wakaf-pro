<!doctype html>
<title>Site Suspended</title>
<head>
    <style>
        body { 
            text-align: left; 
            padding: 150px; 
            background-image : url({{url('image/suspend.jpeg')}});
            background-repeat : no-repeat;
        }
        h1 { font-size: 50px; }
        body { font: 20px Helvetica, sans-serif; color: #fff; }
        article { display: block; text-align: left; width: 650px; margin: 0; }
        a { color: #dc8100; text-decoration: none; }
        a:hover { color: #333; text-decoration: none; }
        img {
            height : 100px;
            margin : 0;
        }
    </style>
</head>

<body>
    <article>
        <img src="https://cms.ngo360.id/admin/assets/media/img/logo_NGO.png" alt="NGO360">
        <h1>Your site is being suspended!</h1>
        <div>
            <p>Mohon maaf atas ketidak nyamanannya, tapi website anda sedang dalam kondisi <i>suspend</i>. Mohon hubungi admin untuk menindaklanjuti!</p>
            <p>&mdash; NGO360 Team</p>
        </div>
    </article>
</body>