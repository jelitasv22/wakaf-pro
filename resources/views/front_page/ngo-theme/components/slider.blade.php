<div class="slider_wrap slider_fullwide slider_engine_revo slider_alias_main">
    <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
        <!-- START REVOLUTION SLIDER 5.2.5.4 fullwidth mode -->
        <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" data-version="5.2.5.4">
            <ul id="slide">
                <?php foreach ($slider->list as $key => $value) { ?>
                    <li data-index="rs-{{$key}}" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <img src="{{ENV('BACKEND_URL')}}/admin/assets/media/slider/{{$value->gambar}}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                       
                     
                        <p class="title-slider">{{$value->judul}}</p>
                        <a href="{{$value->link}}" class="slider-btn">{{$value->title_button}}</a>
                    </li>
                <?php } ?>
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
        <!-- END REVOLUTION SLIDER -->
    </div>
</div>