<div class="block_1474464398176" style="padding-bottom: 100px">
    <div class="column_container">
        <div class="column-inner">
            <div class="wrapper">
                <div class="h43"></div>
                <div class="h08"></div>
                <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_3 padding_button margin_bottom_huge" style="margin-bottom: 0px !important;background-color: #fff;
                padding-top: 20px;">
                    <div class="column-1_2 sc_column_item sc_column_item_1 odd first">
                        <div id="sc_skills_diagram_920271764" class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills">
                            <div class="columns_wrap sc_skills_columns sc_skills_columns_1">
                                <div class="sc_skills_column column-1_1">
                                    <div class="sc_skills_item sc_skills_style_1 odd first">
                                        <div class="sc_skills_count">
                                            <div style="text-align: center; font-size: 25px;" id="donasi_terkumpul_statistik"></div>
                                        </div>
                                        <div class="sc_skills_info">
                                            <div class="sc_skills_label" id="title-statistik">Donasi Terkumpul</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="h22"></div>
                    </div>
                    <div class="column-1_2 sc_column_item sc_column_item_3 odd">
                        <div id="sc_skills_diagram_1081405868" class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills">
                            <div class="columns_wrap sc_skills_columns sc_skills_columns_1">
                                <div class="sc_skills_column column-1_1">
                                    <div class="sc_skills_item sc_skills_style_1 odd first">
                                        <div class="sc_skills_count">
                                            <div style="text-align: center; font-size: 25px;" id="donasi_terkumpul_statistik2"></div>
                                        </div>
                                        <div class="sc_skills_info">
                                            <div class="sc_skills_label" id="title-statistik">Donatur Tergabung</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="h22"></div>
                    </div>
                </div>
                <div class="h08"></div>
            </div>
        </div>
    </div>
</div>