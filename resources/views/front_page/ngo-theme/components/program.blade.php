<div class="block">
    <div class="column_container">
        <div class="column-inner">
            <div class="wrapper">
                <div class="h55"></div>
                <div id="sc_donations_108588160" class="sc_donations sc_donations_style_excerpt">
                    <h2 class="sc_donations_title sc_item_title">Program</h2>

                    <div class="sc_donations_columns_wrap" id="program_list_home">
                        <center id="loading-program">Loading Program . . . </center>
                    </div>

                    <div id="lainnya" class="sc_donations_button sc_item_button">
                        <a href="{{ url('program') }}" class="sc_button sc_button_square sc_button_style_filled ">Lainnya</a>
                    </div>
                </div>
                <div class="h28"></div>
            </div>
        </div>
    </div>
</div>