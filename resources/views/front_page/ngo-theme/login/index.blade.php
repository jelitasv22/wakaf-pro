<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
	<title>Zakat Agro Bina Alam Mandiri</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">
	<link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
	<link rel="stylesheet" href="{{ asset('themes/pagination/dist/pagination.php')}}" type="text/css">
	@include('front_page.ngo-theme.main_layouts.css')
</head>
<style>
	.button-zakat {
		width: 100%;
		margin-top: 10px;
		padding: 5px;
		text-align: center;
		font-size: 14px;
		background: white;
		border: 1px solid #ddd;
		border-radius: 5px;
		display: inline-block;
		cursor: pointer;
		font-family: Arial;
		font-weight: bold;
		text-decoration: none;
		color: #fff !important;
	}

	.button-zakat:hover {
		color: white !important;
		background-color: #fbce1b;
	}
</style>

<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
	<a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
	<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
	<div class="body_wrap">
		<div class="page_wrap">
			<div class="top_panel_fixed_wrap"></div>
			@include('front_page.ngo-theme.main_layouts.header')
			@include('front_page.ngo-theme.components.messages')

			<div class="container mt-5">
				<div class="row justify-content-center align-items-center text-center p-2">
					<div class="m-1 col-sm-8 col-md-6 col-lg-4 shadow-sm p-3 mb-5 bg-white border rounded">
						<div class="pt-5 pb-5">
							<div class="d-flex justify-content-center">
								<img class="rounded" src="https://freelogovector.net/wp-content/uploads/logo-images-13/microsoft-cortana-logo-vector-73233.png" alt="" width="70px" height="70px">
							</div>
							<p class="text-uppercase mt-3">Login</p>
							<form class="form" action="{{Route('login.do_login')}}" method="POST">
								{{csrf_field()}}
								<div class="form-group">
									<input type="text" name="username" class="form-control" id="username" required placeholder="Enter Username">
								</div>
								<div class="form-group">
									<input type="password" name="password" class="form-control" id="password" required placeholder="Password">
								</div>
								<button class="btn btn-lg btn-block btn-primary mt-4" type="submit">Login</button>
								<div class="text-right mt-2">
									<a href="#">Forgot Password?</a>
								</div>
							</form>
						</div>
						<div class="text-center mt-2">
							<a href="{{Route('register.index')}}">Create an account?</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@include('front_page.ngo-theme.components.footer')

	</div>
	<!-- /.page_wrap -->
	</div>
	<!-- /.body_wrap -->
	<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
	<div class="custom_html_section"></div>
	@include('front_page.ngo-theme.main_layouts.js')

</body>

</html>