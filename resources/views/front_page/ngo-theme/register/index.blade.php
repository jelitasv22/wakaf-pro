<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
  <title>Zakat Agro Bina Alam Mandiri</title>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="format-detection" content="telephone=no">
  <link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
  <link rel="stylesheet" href="{{ asset('themes/pagination/dist/pagination.php')}}" type="text/css">
  @include('front_page.ngo-theme.main_layouts.css')
</head>
<style>

</style>

<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
  <a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
  <a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
  <div class="body_wrap">
    <div class="page_wrap">
      <div class="top_panel_fixed_wrap"></div>
      @include('front_page.ngo-theme.main_layouts.header')
			@include('front_page.ngo-theme.components.messages')
      <div class="container mt-5">
        <div class="row d-flex justify-content-center align-items-center h-100">
          <div class="col-lg-8 col-xl-6">
            <div class="card rounded-3">
              <div class="card-body p-4 p-md-5">
                <h5 class="mb-4 text-center">Registrasi Akun</h5>
                <form action="{{Route('register.do_register')}}" method="post">
								{{csrf_field()}}
                  
                  <div class="form-group">
                    <label for="username">Username:</label>
                    <input type="text" class="form-control" name="username" id="username" placeholder="Masukkan username" required>
                  </div>
                  <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Masukkan password" required>
                  </div>
                  <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Masukkan email" required>
                  </div>
                  <div class="form-group">
                    <label for="fullname">Nama Lengkap:</label>
                    <input type="text" class="form-control" name="name" id="fullname" placeholder="Masukkan nama lengkap" required>
                  </div>
                  <div class="form-group">
                    <label for="address">Alamat:</label>
                    <textarea class="form-control" id="address" name="alamat"  placeholder="Masukkan alamat" required></textarea>
                  </div>
                  <div class="form-group">
                    <label for="phone">Nomor Telepon:</label>
                    <input type="tel" class="form-control" id="phone" name="no_telp" placeholder="Masukkan nomor telepon" required>
                  </div>
                  <div class="form-group">
                    <label for="bio">Biodata:</label>
                    <textarea class="form-control" id="bio" name="bio" placeholder="Masukkan biodata" required></textarea>
                  </div>

                  <div class="input-group mb-3 mt-3 ml-4" id="">
                    <input type="checkbox" name="checkbox_verify" class="form-check-input" required id="checkbox-verify">
                    <label class="form-check-label ml-2" for="">Saya telah membaca dan menyetujui <a href="">Syarat dan Ketentuan</a> yang berlaku</label>
                  </div>

                  <div class="form-group">
                    <button type="submit" class="btn btn-success col-12 mb-1">Register Sekarang</button>
                  </div>

                  <div class="form-group">
                    <p class="text-center">Sudah punya akun? <a href="{{ route('login.index') }}">Login</a></p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('front_page.ngo-theme.components.footer')
  </div>
  <!-- /.page_wrap -->
  </div>
  <!-- /.body_wrap -->
  <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
  <div class="custom_html_section"></div>
  @include('front_page.ngo-theme.main_layouts.js')
  @include('front_page.ngo-theme.pages.zakat.js')
</body>

</html>