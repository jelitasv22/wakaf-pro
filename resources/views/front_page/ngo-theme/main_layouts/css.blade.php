<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
<link rel='dns-prefetch' href='http://s.w.org/' />
<link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700%7COpen+Sans:300,300i,400,400i,600,600i,700,700i,800,800i%7CRaleway:100,200,300,400,500,600,700,800,900&amp;subset=latin-ext" rel="stylesheet">
<link property="stylesheet" rel='stylesheet' id='charity_is_hope-font-WCManoNegraBta-style-css' href="{{asset('themes/ngo-theme/css/font-face/WCManoNegraBta/stylesheet.css')}}" type='text/css' media='all' /><link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/icons.min.css')}}">
<link property="stylesheet" rel='stylesheet' id='charity_is_hope-fontello-style-css' href="{{asset('themes/ngo-theme/css/fontello/css/fontello.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='rs-plugin-settings-css' href="{{asset('themes/ngo-theme/js/vendor/plugins/revslider/public/assets/css/settings.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='essential-grid-plugin-settings-css' href="{{asset('themes/ngo-theme/js/vendor/plugins/essential-grid/public/assets/css/settings.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='charity_is_hope-main-style-css' href="{{asset('themes/ngo-theme/css/style.css')}}" type='text/css' media='all' />
<link rel='stylesheet' type='text/css' media="screen" href="{{asset('themes/ngo-theme/css/global.css')}}"/>
<link rel="stylesheet" href="{{asset('themes/ngo-theme/css/bootstrap.min.css')}}">
<link property="stylesheet" rel='stylesheet' id='charity_is_hope-animation-style-css' href="{{asset('themes/ngo-theme/js/vendor/fw/css/core.animation.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='charity_is_hope-shortcodes-style-css' href="{{asset('themes/ngo-theme/js/vendor/shortcodes/theme.shortcodes.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='charity_is_hope-plugin.tribe-events-style-css' href="{{asset('themes/ngo-theme/css/plugin.tribe-events.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='charity_is_hope-plugin.donations-style-css' href="{{asset('themes/ngo-theme/css/plugin.donations.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='charity_is_hope-responsive-style-css' href="{{asset('themes/ngo-theme/css/responsive.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='mediaelement-css' href="{{asset('themes/ngo-theme/js/vendor/mediaelement/mediaelementplayer.min.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='trx-donations-style-css'  href="{{asset('themes/ngo-theme/js/vendor/plugins/trx_donations/trx_donations.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='charity_is_hope-messages-style-css' href="{{asset('themes/ngo-theme/js/vendor/fw/js/core.messages/core.messages.css')}}" type='text/css' media='all' />
<link property="stylesheet" rel='stylesheet' id='charity_is_hope-swiperslider-style-css' href="{{asset('themes/ngo-theme/js/vendor/fw/js/swiper/swiper.css')}}" type='text/css' media='all' />
<link rel="stylesheet" href="{{ asset('themes/pagination/dist/pagination.css')}}" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/payment/payment.css') }}">

<link href="{{asset('admin/assets/vendors/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css')}}" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" type="text/css" href="{{asset('themes/ngo-theme/logo-NGO1.png')}}">
<style type="text/css">
	<?php
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, ENV('API_URL').'/api/ngo-theme/color?yayasan='.ENV('YAYASAN_KEY'));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$output = curl_exec($ch);
	curl_close($ch);
	$color = json_decode($output);
	?>

	.swal2-popup.swal2-toast.swal2-show {
		background-color: #16af2b;
	}

	.swal2-popup.swal2-toast .swal2-title {
		color: white;
	}
</style>
@yield('css')
