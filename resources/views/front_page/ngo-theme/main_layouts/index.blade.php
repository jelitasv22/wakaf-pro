<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
	<title>Agro Bina Alam Mandiri</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no"><link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
	@include('front_page.ngo-theme.main_layouts.css')

</head>
<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
	<a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
	<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
	<div class="body_wrap">
		<div class="page_wrap" >
			<div class="top_panel_fixed_wrap"></div>
			
		    @include('front_page.ngo-theme.main_layouts.header')

		    @include('front_page.ngo-theme.components.slider')

			<div class="page_content_wrap page_paddings_no">
				<div class="content_wrap">
					<div class="content">
						<div class="itemscope post_item post_item_single post_featured_default post_format_standard post-2 page type-page status-publish hentry" itemscope itemtype="http://schema.org/Article">
							<div class="post_content" itemprop="articleBody">
                                {{-- statistik home --}}
		                        @include('front_page.ngo-theme.components.statistik_home')
                                {{-- statistik home --}}

                                {{-- program --}}
		                        @include('front_page.ngo-theme.components.program')
                                {{-- program --}}

								@include('front_page.ngo-theme.components.posting')

							</div>
						</div>
					</div>
				</div>
			</div>
			@include('front_page.ngo-theme.components.footer')
		</div>
		<!-- /.page_wrap -->
	</div>
	<!-- /.body_wrap -->
	<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

	<?php 
		$numberone = "/^0/"; 
		$noWa = $yayasan->data->no_wa; 
		$ganti = "62";  
		$result = preg_replace($numberone, $ganti, $noWa);

	?>
	<a href="https://api.whatsapp.com/send?phone={{$result}}&text=Hallo" class="float" target="_blank">
		<i class="fa fa-whatsapp my-float"></i>
	</a>
    <div class="custom_html_section"></div>
	@include('front_page.ngo-theme.main_layouts.js')

</body>
</html>
