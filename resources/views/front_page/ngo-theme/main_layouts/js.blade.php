<script type='text/javascript' src="{{asset('themes/ngo-theme/js/vendor/jquery.js')}}"></script>
<script src="{{asset('themes/ngo-theme/js/vendor/bootstrap.min.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/vendor/jquery-migrate.min.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/vendor/fw/js/superfish.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/vendor/fw/js/core.utils.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/vendor/fw/js/core.init.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/vendor/fw/js/swiper/swiper.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/vendor/shortcodes/theme.shortcodes.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/vendor/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/ngo-theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.video.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/ngo-theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/ngo-theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/ngo-theme/js/vendor/plugins/revslider/public/assets/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/vendor/plugins/isotope/dist/isotope.pkgd.min.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/vendor/plugins/essential-grid/public/assets/js/jquery.themepunch.tools.min.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/custom/theme.init.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/custom/global.js')}}"></script>
<script type='text/javascript' src="{{asset('themes/ngo-theme/js/custom/rev_slider_1_1.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/pagination/dist/pagination.min.js')}}"></script>
<script src="{{asset('admin/assets/vendors/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function() {

		function tanggal_indonesia($tgl) {
			var date = new Date($tgl);

			var tahun = date.getFullYear();
			var bulan = date.getMonth();
			var tanggal = date.getDate();
			var hari = date.getDay();
			var jam = date.getHours();
			var menit = date.getMinutes();
			var detik = date.getSeconds();
			switch(hari) {
				case 0: hari = "Minggu"; break;
				case 1: hari = "Senin"; break;
				case 2: hari = "Selasa"; break;
				case 3: hari = "Rabu"; break;
				case 4: hari = "Kamis"; break;
				case 5: hari = "Jum'at"; break;
				case 6: hari = "Sabtu"; break;
			}
			switch(bulan) {
				case 0: bulan = "Januari"; break;
				case 1: bulan = "Februari"; break;
				case 2: bulan = "Maret"; break;
				case 3: bulan = "April"; break;
				case 4: bulan = "Mei"; break;
				case 5: bulan = "Juni"; break;
				case 6: bulan = "Juli"; break;
				case 7: bulan = "Agustus"; break;
				case 8: bulan = "September"; break;
				case 9: bulan = "Oktober"; break;
				case 10: bulan = "November"; break;
				case 11: bulan = "Desember"; break;
			}
			var tampilTanggal = hari + ", " + tanggal + " " + bulan + " " + tahun;
			var tampilWaktu = "Jam: " + jam + ":" + menit + ":" + detik;

			return tampilTanggal;
		}

		$.ajax({
			url: '{{ENV("API_URL")}}/api/ngo-statistik?yayasan={{ENV("YAYASAN_KEY")}}',
			type: 'GET',
			dataType: 'Json',
		})
		.done(function(res) {
			if(res.donasi.total_donasi == null){
				$('#donasi_terkumpul_statistik').html('Rp. 0')
			}else{
				$('#donasi_terkumpul_statistik').html('Rp. '+res.donasi.total_donasi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
			}

			$('#donasi_terkumpul_statistik2').html(res.donatur)
		});

		$.ajax({
			url: '{{ENV("API_URL")}}/api/ngo-program?lang={{$language_ses}}&yayasan={{ENV("YAYASAN_KEY")}}',
			type: 'GET',
			dataType: 'Json',
		})
		.done(function(res) {
			$.each(res.list, function(index, val) {
				$('#loading-program').html('')
				if(val.dana_target == null && val.total != 0){
					hasil = 100;
				}else if(val.dana_target != null && val.total != 0){
					persen = val.dana_target/100;
					hasil  = val.total/persen;
				}else{
					hasil = 0;
				}

				if(val.total == null){
					total = 0;
				}else{
					total = val.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
				}

				if(val.dana_target == null){
					target = 0;
				}else{
					target = val.dana_target.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
				}

				$('#program_list_home').append('<div class="post_item_excerpt post_type_donation sc_donations_column-1_3">'+
					'<div class="post_featured">'+
						'<img width="570" height="320" src="{{ENV("BACKEND_URL")}}/admin/assets/media/foto-program/'+val.foto+'" class="attachment-thumb_med size-thumb_med" alt="" />'+
					'</div>'+
					'<div class="post_body">'+
						'<div class="post_header entry-header">'+
							'<h4 class="entry-title"><a href="{{ url("program/detail-program") }}/'+val.seo+'" rel="bookmark">'+val.judul+'</a></h4>'+
							'<p>'+tanggal_indonesia(val.tanggal)+'</p>'+
						'</div>'+
						'<div class="post_content entry-content">'+
							'<p>'+val.resume+'</p>'+
							'<div class="post_info_donations">'+
							'<div class="top">'+
								'<span class="post_info_item post_raised"><span class="post_counters_label">Terkumpul</span></span>'+
								'<span class="post_info_item post_goal"><span class="post_counters_label">Target</span></span>'+
							'</div>'+
							'<div class="middle">'+
								'<span style="width: '+hasil+'%;"></span>'+
							'</div>'+
							'<div class="bottom">'+
								'<span class="post_counters_number_raised">'+
								total+
								'</span>'+
								'<span class="post_counters_number_goal">'+
								target+
								'</span>'+
							'</div>'+
						'</div>'+
						'<a href="{{ url("program/donate") }}/'+val.seo+'" class="post_readmore" style="padding: 10px; border-radius: 5px; margin-top: 10px; width: 93%;">'+
						'<span class="post_readmore_label">Donasi Sekarang</span>'+
						'</a>'+
						'</div>'+
					'</div>'+
				'</div>');
			});
		});

		$.ajax({
			url: '{{ENV("API_URL")}}/api/ngo-posting?lang={{$language_ses}}&yayasan={{ENV("YAYASAN_KEY")}}',
			type: 'GET',
			dataType: 'Json',
		})
		.done(function(res) {
			var parser = new DOMParser();
			$.each(res.list, function(index, val) {
			$('#loading-posting').html('')

			$('#posting_list_home').append('<div class="post_item_excerpt post_type_donation sc_donations_column-1_3">'+
					'<div class="post_featured">'+
						'<img width="570" height="320" src="{{ENV("BACKEND_URL")}}/admin/assets/media/posting/'+val.image+'" class="attachment-thumb_med size-thumb_med" alt="" />'+
					'</div>'+
					'<div class="post_body">'+
						'<div class="post_header entry-header">'+
							'<h4 class="entry-title"><a href="{{ url("posting/detail-posting") }}/'+val.seo+'" rel="bookmark">'+val.judul+'</a></h4>'+
							'<p>'+tanggal_indonesia(val.created_at)+'</p>'+
						'</div>'+
						'<div class="post_content entry-content" style="overflow: hidden;">'+
							'<p>'+val.content.substr(0, 300)+'...</p>'+
						'</div>'+
					'</div>'+
				'</div>');
			});
		});
	});
</script>
