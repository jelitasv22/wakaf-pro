
<header class="top_panel_wrap top_panel_style_1 scheme_original">
	<div class="top_panel_wrap_inner top_panel_inner_style_1 top_panel_position_above">
		<div class="top_panel_middle" style="background-color: #404040;">
			<div class="content_wrap">
				<div class="contact_logo">
					<div class="logo">
						<a href="{{ url('/') }}"><img src="{{ENV('BACKEND_URL')}}/admin/assets/media/logo_yayasan/{{$yayasan->data->logo_primary}}" style="width: 60px; height: 60px;" class="logo_main" alt="" width="118" height="69"></a>
					</div>
				</div>
				<div class="contact_button">
					<!-- <a class="first_button" href="{{ Route('login.index') }}">Login</a> -->
					<a class="second_button" href="{{ Route('qurban.purchase') }}">Tunaikan Qurban</a> 

					<?php if (!Session::has('token')) { ?>
						<a class="second_button" href="{{ Route('login.index') }}">Login</a> 
					<?php }?>

				</div>
				<div class="contact_socials">
					<div class="sc_socials sc_socials_type_icons sc_socials_shape_round sc_socials_size_tiny">
						<div class="sc_socials_item">
							<?php if($language_ses == 'id' || $language_ses == null){ ?>
								<a href="{{ Route('language.switch', 'id') }}">
									<div class="flag-language" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAG1BMVEXIDy7////h4eHGABzkpqzHACDVk5ni5ubKysoGtXH1AAAA6UlEQVR4nO3Quw0CQRTAwMce9+m/YuKVUyQIZiqwPAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPAl58HunOvN7pr7xe6e9euEv7M8CU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/Kk1jyL3fMB/IZrLET5hIcAAAAASUVORK5CYII='); width: 100%; height: 100%;"></div>
								</a>
							<?php }else{ ?>
								<a href="{{ Route('language.switch', 'id') }}">
									<div class="flag-language flag-off" style="background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAC3CAMAAAAGjUrGAAAAG1BMVEXIDy7////h4eHGABzkpqzHACDVk5ni5ubKysoGtXH1AAAA6UlEQVR4nO3Quw0CQRTAwMce9+m/YuKVUyQIZiqwPAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPAl58HunOvN7pr7xe6e9euEv7M8CU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/KkPClPypPypDwpT8qT8qQ8KU/Kk/Kk1jyL3fMB/IZrLET5hIcAAAAASUVORK5CYII='); width: 100%; height: 100%;"></div>
								</a>
							<?php } ?>
						</div>
						<div class="sc_socials_item">
							<?php if($language_ses == 'en'){ ?>
								<a href="{{ Route('language.switch', 'en') }}">
									<div class="flag-language" style="background-image: url('https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/1200px-Flag_of_the_United_Kingdom.svg.png'); width: 100%; height: 100%;"></div>
								</a>
							<?php } else { ?>
								<a href="{{ Route('language.switch', 'en') }}">
									<div class="flag-language flag-off" style="background-image: url('https://upload.wikimedia.org/wikipedia/en/thumb/a/ae/Flag_of_the_United_Kingdom.svg/1200px-Flag_of_the_United_Kingdom.svg.png'); width: 100%; height: 100%;"></div>
								</a>
							<?php } ?>
						</div>
						<!-- <div class="sc_socials_item">
							<a href="https://www.instagram.com/karyacahayainsani" target="blank">
								<div class="flag-language" style="background-image: url('{{asset('themes/ngo-theme/instagram1.jpg')}}'); width: 100%; height: 100%;"></div>
							</a>
						</div> -->
					</div>
				</div>
			</div>
		</div>
		<div class="top_panel_bottom">
			<div class="content_wrap clearfix">
				<nav class="menu_main_nav_area menu_hover_fade">
					<ul id="menu_main" class="menu_main_nav">
						<?php $uri = explode('/', $_SERVER['REQUEST_URI']); ?>
						<?php foreach ($menu->list as $key => $header) { ?>
							<?php 
							if($uri[1] == $header->url){
								$active = 'current-menu-item';
							}else{
								$active = '';
							}
							?>
							<li class="menu-item {{$active}}">
								<a href="{{ url($header->url) }}">
									<span>{{$header->judul_menu}} </span>
								</a>
							</li>
							<?php 
							if($header->judul_menu == "#"){
								foreach ($menu->body as $body) { ?>
									<?php 
									if($uri[1] == $body->url){
										$active_sub = 'current-menu-item';
									}else{
										$active_sub = '';
									}
									?>
									<li class="menu-item menu-item-home menu-item-has-children {{$active_sub}}">
										<a href="{{ url($header->url) }}"><span>{{$header->judul_menu}}</span></a>
										<ul class="sub-menu">
											<?php if($header->id == $body->id_sub_menu){?>
												<li class="menu-item {{$active_sub}}">
													<a href="{{ url($body->url) }}"><span>{{$body->judul_menu}}</span></a>
												</li>
											<?php } ?>
										</ul>
									</li>
								<?php }} ?>
							<?php } ?>
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</header>
	<div class="header_mobile" style="background-color: #404040;">
		<div class="content_wrap">
			<div class="menu_button icon-menu"></div>
			<div class="logo">
				<a href="{{ url('/') }}"><img src="{{ENV('BACKEND_URL')}}/admin/assets/media/logo_yayasan/{{$yayasan->data->logo_primary}}" style="width: 60px; height: 60px;" class="logo_main" alt="" width="118" height="69"></a>
			</div>
		</div>
		<div class="side_wrap">
			<div class="close">Close</div>
			<div class="panel_top">
				<nav class="menu_main_nav_area">
					<ul id="menu_mobile" class="menu_main_nav">
						<?php $uri = explode('/', $_SERVER['REQUEST_URI']); ?>
						<?php foreach ($menu->list as $key => $header) { ?>
							<?php 
							if($uri[1] == $header->url){
								$active = 'current-menu-item';
							}else{
								$active = '';
							}
							?>
							<li class="menu-item {{$active}}">
								<a href="{{ url($header->url) }}">
									<span>{{$header->judul_menu}}</span>
								</a>
							</li>
							<?php 
							if($header->judul_menu == "#"){
								foreach ($menu->body as $body) { ?>
									<?php 
									if($uri[1] == $body->url){
										$active_sub = 'current-menu-item';
									}else{
										$active_sub = '';
									}
									?>
									<li class="menu-item menu-item-home menu-item-has-children {{$active_sub}}">
										<a href="{{ url($header->url) }}"><span>{{$header->judul_menu}}</span></a>
										<ul class="sub-menu">
											<?php if($header->id == $body->id_sub_menu){?>
												<li class="menu-item {{$active_sub}}">
													<a href="{{ url($body->url) }}"><span>{{$body->judul_menu}}</span></a>
												</li>
											<?php } ?>
										</ul>
									</li>
								<?php }} ?>
							<?php } ?>
						</ul>
					</nav>
				</div>
			</div>
			<div class="mask"></div>
		</div>