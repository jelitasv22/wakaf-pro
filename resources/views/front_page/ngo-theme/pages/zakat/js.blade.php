<script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}" data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {



		function tanggal_indonesia($tgl) {
			var date = new Date($tgl);

			var tahun = date.getFullYear();
			var bulan = date.getMonth();
			var tanggal = date.getDate();
			var hari = date.getDay();
			var jam = date.getHours();
			var menit = date.getMinutes();
			var detik = date.getSeconds();
			switch (hari) {
				case 0:
					hari = "Minggu";
					break;
				case 1:
					hari = "Senin";
					break;
				case 2:
					hari = "Selasa";
					break;
				case 3:
					hari = "Rabu";
					break;
				case 4:
					hari = "Kamis";
					break;
				case 5:
					hari = "Jum'at";
					break;
				case 6:
					hari = "Sabtu";
					break;
			}
			switch (bulan) {
				case 0:
					bulan = "Januari";
					break;
				case 1:
					bulan = "Februari";
					break;
				case 2:
					bulan = "Maret";
					break;
				case 3:
					bulan = "April";
					break;
				case 4:
					bulan = "Mei";
					break;
				case 5:
					bulan = "Juni";
					break;
				case 6:
					bulan = "Juli";
					break;
				case 7:
					bulan = "Agustus";
					break;
				case 8:
					bulan = "September";
					break;
				case 9:
					bulan = "Oktober";
					break;
				case 10:
					bulan = "November";
					break;
				case 11:
					bulan = "Desember";
					break;
			}
			var tampilTanggal = hari + ", " + tanggal + " " + bulan + " " + tahun;
			var tampilWaktu = "Jam: " + jam + ":" + menit + ":" + detik;

			return tampilTanggal;
		}

		//		getPaymentMethod();

		var rupiah = document.getElementById('nominal');
		if (rupiah)
			rupiah.addEventListener('keyup', function(e) {
				rupiah.value = formatRupiah(this.value, '');
			});

		function formatRupiah(angka, prefix) {
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
				split = number_string.split(','),
				sisa = split[0].length % 3,
				rupiah = split[0].substr(0, sisa),
				ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			if (ribuan) {
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		}


		var selectedZakat = "";

		function hideAllMenu() {

			$("#zakat-fitrah-page").hide();
			$("#zakat-maal-page").hide();
			$("#zakat-emas-page").hide();
			$("#zakat-perak-page").hide();
			$("#zakat-penghasilan-page").hide();
			$("#calc-bayar-zakat-btn").hide();
		}

		hideAllMenu();
		var hargaEmas = 0;
		var hargaPerak = 0;


		// Nishab
		var nishabEmas = 85;
		var nishabPerak = 595;
		var nishabPenghasilan = 0;
		var nishabFitrah = 45000;

		$.ajax({
			url: "https://api.ngo360.id/api/zakat/parameter",
			type: "GET",
			dataType: "JSON",
			success: function(data) {
				hargaEmas = data.harga_emas.value;
				hargaPerak = data.harga_perak.value;

				$("#gold-value").val(hargaEmas);
				$("#silver-value").val(hargaPerak);
			}
		});

		$('#kategori-zakat').change(function() {


			var availableMenu = ["113", "114", "115", "116"] // id zakat penghasilan, fitrah, emas, perak
			var selectedValue = $(this).val();
			switch (selectedValue) {
				case availableMenu[0]:
					hideAllMenu();
					$("#zakat-penghasilan-page").show();
					$("#calc-bayar-zakat-btn").show();
					console.log("penghasilan");
					$("#id-zakat-penghasilan").val("113"); // id zakat penghasilan
					selectedZakat = "113"
					break;
				case availableMenu[1]:

					hideAllMenu();
					$("#zakat-fitrah-page").show();
					console.log("fitrah");
					$("#calc-bayar-zakat-btn").show();
					$("#id-zakat-fitrah").val("114"); // id zakat fitrah
					selectedZakat = "114"
					break;
				case availableMenu[2]:

					hideAllMenu();
					$("#zakat-emas-page").show();
					$("#calc-bayar-zakat-btn").show();
					console.log("emas");
					$("#id-zakat-emas").val("115"); // id zakat emas
					selectedZakat = "115"
					break;
				case availableMenu[3]:

					hideAllMenu();
					$("#zakat-perak-page").show();
					$("#calc-bayar-zakat-btn").show();
					$("#id-zakat-perak").val("116"); // id zakat perak
					selectedZakat = "116"
					break;
				default:
					hideAllMenu();
					break;
			}
		});



		$("#selected-method").hide();
		$("#select-payment-method").on('click', function() {

			//		console.log(vendor_id)
			getPaymentMethod();
			$("#metode-pembayaran").addClass('payment-method-active');
			$("#metode-pembayaran").removeClass('payment-method-hide');
		});

		$("#btn-payment-method2").on('click', function() {
			$("#metode-pembayaran").addClass('payment-method-active');
			$("#metode-pembayaran").removeClass('payment-method-hide');
		});

		$("#mthd-close").off('click').on('click', function() {
			$("#metode-pembayaran").addClass('payment-method-hide');
			$("#metode-pembayaran").removeClass('payment-method-active');
		});

		$("#simple-pop-close").off('click').on('click', function() {
			$("#notif-pembayaran").addClass('payment-method-hide');
			$("#notif-pembayaran").removeClass('payment-method-active');

			$('#program_id').val('');
			$('#nominal').val('');
			$('#nama_lengkap').val('');
			$('#no_handphone').val('');
			$('#email').val('');
			$("#payment_vendor").val('');
			$("#payment_type").val('');
			$("#payment_method").val('');
			$('#selected-method-img').val('')
			$('#selected-method-label').val('')
			$('#no_rekening_show').val('');
		});


		// --------------------------------- ZAKAT FITRAH --------------------------------- //
		// Note : Sorry yang nanti develop ini, bad code? sudah pasti, so i hope you can make it better than this

		var rice_price_fitrah = 0;

		$('input[name="opt-rice-price-fitrah"]').change(function() {
			rice_price_fitrah = $('input[name="opt-rice-price-fitrah"]:checked').val();
			$('#jiwa-field-fitrah').trigger('input');
		});

		$('#zakat-fitrah-page').on('submit', function(e) {
			//e.preventDefault();

			if ($('#total-field-fitrah').val() < 45000) {
				Swal.fire({
					icon: 'error',
					title: 'Oops...',
					text: 'Nominal zakat fitrah minimal Rp 45.000',
				});
				return false;
			}

			$('#opt-rice-price-fitrah-1').removeAttr('name');
			$('#opt-rice-price-fitrah-2').removeAttr('name');
		});

		$('#jiwa-field-fitrah').on('input', function() {
			var jiwa = $(this).val();

			// 1 jiwa = 2,5 kg
			var zakat = rice_price_fitrah * 2.5;
			var total = jiwa * (rice_price_fitrah * 2.5);



			$('#jumlah-zakat-field-fitrah').val(zakat);
			$('#total-field-fitrah').val(total);
		});

		// --------------------------------- END ZAKAT FITRAH --------------------------------- //

		// --------------------------------- ZAKAT MAAL --------------------------------- //


		// zakat mal
		// Menghitung total harta saat input berubah
		$('input[name^="jumlah_"], #uang-tunai, #properti, #kendaraan, #saham, #harta-lainnya, #kewajiban-hutang').on('input', function() {
			var totalHarta = 0;
			$('input[name^="jumlah_"], #uang-tunai, #properti, #kendaraan, #saham, #harta-lainnya').each(function() {
				totalHarta += parseFloat($(this).val()) || 0;
			});
			$('#total-harga').val(totalHarta);

			var nisabZakatHarta = 85 * 999999; // Misalkan harga emas 1: gram = Rp 999,999
			$('#nisab-zakat-harta').val(nisabZakatHarta);

			var hartaDizakatkan = totalHarta - parseFloat($('#kewajiban-hutang').val()) || 0;
			$('#harta-dizakatkan').val(hartaDizakatkan);

			if (hartaDizakatkan >= nisabZakatHarta) {
				var zakatMaal = hartaDizakatkan * 0.025; // 2.5% dari harta yang dizakatkan
				// Tampilkan nilai zakat pada elemen HTML yang sesuai
			} else {
				// Tampilkan nilai zakat 0 pada elemen HTML yang sesuai
			}
		});

		// --------------------------------- END ZAKAT MAAL --------------------------------- //

		// --------------------------------- ZAKAT PENGHASILAN --------------------------------- //

		// Menghitung jumlah zakat saat input berubah
		$('input[name="opt-rice-price-penghasilan"]').change(function() {
			var selectedValue = $('input[name="opt-rice-price-penghasilan"]:checked').val();
			var nishab = selectedValue * 522;
			$('#harga-beras').val(selectedValue);
			$('#nishab').val(nishab);

			nishabPenghasilan = nishab;
		});



		$('#penghasilan-perbulan, #penghasilan-tambahan, #pengeluaran-perbulan,input[name="opt-rice-price-penghasilan"]:checked, #harga-beras, #jumlah-bulan').on('keyup', function() {
			var penghasilanPerbulan = parseFloat($('#penghasilan-perbulan').val() || 0);
			var penghasilanTambahan = parseFloat($('#penghasilan-tambahan').val() || 0);
			var pengeluaranPerbulan = parseFloat($('#pengeluaran-perbulan').val() || 0);
			var ricePrice = parseFloat($('input[name="opt-rice-price-penghasilan"]:checked').val());
			var hargaBeras = parseFloat($('#harga-beras').val());
			var jumlahBulan = parseFloat($('#jumlah-bulan').val());

			var totalPenghasilan = penghasilanPerbulan + penghasilanTambahan;
			var penghasilanSetelahPengeluaran = totalPenghasilan - pengeluaranPerbulan;

			var zakatAmount = 0;

			zakatAmount = (penghasilanSetelahPengeluaran * 0.025 * jumlahBulan).toFixed(2);

			$('#penghasilan-setelah-pengeluaran').val(penghasilanSetelahPengeluaran);
			$('#zakat-hasil-perhitungan').val(zakatAmount);
		});

		$('#zakat-penghasilan-page').on('submit', function(e) {

			$('#rice-price-1').removeAttr('name');
			$('#rice-price-2').removeAttr('name');
		});




		// --------------------------------- END ZAKAT PENGHASILAN --------------------------------- //

		// --------------------------------- ZAKAT EMAS --------------------------------- //

		$('#gold-weight').on('input', function() {
			var goldWeight = parseFloat($(this).val() || 0);
			var goldPrice = hargaEmas;
			var zakatAmount = 0;

			zakatAmount = (goldWeight * goldPrice * 0.025).toFixed(2);
			$('#gold-amount').val(zakatAmount);
		});

		// --------------------------------- END ZAKAT EMAS --------------------------------- //

		// --------------------------------- ZAKAT PERAK --------------------------------- //

		$('#silver-weight').on('input', function() {
			var silverWeight = parseFloat($(this).val() || 0);
			var silverPrice = hargaPerak;
			var zakatAmount = 0;
			zakatAmount = (silverWeight * silverPrice * 0.025).toFixed(2);
			$('#silver-amount').val(zakatAmount);
		});

		// --------------------------------- END ZAKAT PERAK --------------------------------- //

		// idk what i'm doing, just think for understand my bad code
		$("#calc-bayar-zakat-btn").on('click', function() {
			if (selectedZakat == "113") {
				if ($('#zakat-hasil-perhitungan').val() == 0) {
					return false;
				}
				if ($('#zakat-hasil-perhitungan').val() < nishabPenghasilan) {
					Swal.fire({
						title: 'Mohon Maaf',
						text: "Anda belum wajib membayar zakat penghasilan, apakah anda ingin melanjutkan untuk sedekah?",
						showCancelButton: true,
						confirmButtonText: 'Ya',
					}).then((result) => {
						if (result.value) {
							$('#zakat-penghasilan-page').submit();
						}
					})
				} else {
					$('#zakat-penghasilan-page').submit();
				}
			}

			if (selectedZakat == "114") {

				if ($('#total-field-fitrah').val() == 0) {
					return false;
				}
				$('#zakat-fitrah-page').submit();

			}

			if (selectedZakat == "115") {

				if ($('#gold-amount').val() == 0) {
					return false;
				}
				if ($('#gold-weight').val() < 85) {
					Swal.fire({
						title: 'Mohon Maaf',
						text: "Anda belum wajib membayar zakat emas, apakah anda ingin melanjutkan untuk sedekah?",
						showCancelButton: true,
						confirmButtonText: 'Ya',
						cancelButtonText: 'Tidak',
					}).then((result) => {
						console.log(result.value);
						if (result.value) {
							$('#zakat-emas-page').submit();
						} 
					})
				} else {
					$('#zakat-emas-page').submit();
				}
			}

			if (selectedZakat == "116") {
				if ($('#silver-amount').val() == 0) {
					return false;
				}

				if ($('#silver-weight').val() < 595) {
					Swal.fire({
						title: 'Mohon Maaf',
						text: "Anda belum wajib membayar zakat perak, apakah anda ingin melanjutkan untuk sedekah?",
						showCancelButton: true,
						confirmButtonText: 'Ya',
					}).then((result) => {
						if (result.value) {
							$('#zakat-perak-page').submit();
						}
					})
				} else {
					$('#zakat-perak-page').submit();
				}
			}

		});


		$("#purchase-zakat").on('click', function(event) {
			event.preventDefault();
			var _nominal_zakat = $('#nominal-zakat-field').val();
			var _id_zakat = $('#id-zakat').val();
			var _payment_category = $('#payment_category').val();
			var _vendor = $('#id-pembayaran').val();
			var _tipe_pembayaran = $('#tipe-pembayaran').val();
			var _method_pembayaran = $('#method-payment').val();
			var _no_rekening = $('#no-rek-payment').val();
			var _nama_lengkap = $('#nama-lengkap-field').val();
			var _no_handphone = $('#phone-field').val();
			var _email = $('#email-field').val();
			var _komentar = $('#komentar-field').val();
			var _verify = $('#checkbox-verify').val();
			var _uid = null;
			var _session = null;

			if ('{{Session::has("uid")}}') {
				_uid = '{{Session::get("uid")}}';
			}

			if ('{{Session::has("token")}}') {
				_session = '{{Session::get("token")}}';
			}

			if (_nominal_zakat == "" || _id_zakat == "" || _payment_category == "" || _vendor == "" || _tipe_pembayaran == "" || _method_pembayaran == "" || _no_rekening == "" || _nama_lengkap == "" || _no_handphone == "" || _email == "" || _verify == "") {
				return false;
			}



			$.ajax({
				url: '{{ENV("API_URL")}}' + '/api/snap-token-midtrans-zakat',
				type: 'POST',
				data: {
					key_yayasan: '{{ENV("YAYASAN_KEY")}}',
					jumlah_zakat: _nominal_zakat,
					payment_type: _tipe_pembayaran,
					user_id: _uid,
					no_handphone: _no_handphone,
					email: _email,
					nama_lengkap: _nama_lengkap,
					device: "Web",
					category_id: _id_zakat,
					komentar: _komentar,
					anonim: 0,
					verifikasi: _verify,
					vendor: _vendor,
					payment_method: _method_pembayaran,
					no_rekening: _no_rekening
				},
				headers: {
					'Authorization': _session
				},
				success: function(res) {
					console.log(res);
					if (_tipe_pembayaran == "manual_transfer") {
						$("#notif-pembayaran").removeClass('payment-method-hide');
						$("#notif-pembayaran").addClass('payment-method-active');

						let modal_title = $("#modal_title")
						let modal_body = $("#modal_body")

						let type = $("#payment_type").val()
						let method = $("#payment_method").val()

						let nominal = res.moota.nominal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")

						$("#title_popup").html('Transfer ' + res.moota.nama_bank)
						$("#moota-logo").attr("src", $("#selected-method-img").attr("src"))
						$("#no_rekening_show").html(res.moota.no_rekening)

						$('#intruksi-pembayaran').html('<center><br />' +
							'<span><b>Intruksi Pembayaran</b></span><br>' +
							'<span>Nominal yang harus anda bayar :</span><br>' +
							'<span style="font-size: 18px;"><b>Rp. ' + formatRupiah(res.moota.nominal.toString(),'Rp') + '</b></span><br>' +
							'<span>*Harap selesaikan pembayaran sesuai dengan nominal yang tertera untuk memudahkan pengecekan!</span><br>' +
							'<span>*Sesuaikan nominal sampai 3 digit terakhir! (Kelebihan dari nominal akan didonasikan juga)</span><br>' +
							'<span>*Lakukan pembayaran maksimal 1x24 jam!</span><br>' +
							'<span>*Silahkan akses link berikut untuk melihat perkembangan donasi anda! (Link dikirim juga ke email anda)</span><br>' +
							'<span>*Tekan tombol dibawah juka sudah melakukan pembayaran!</span><br><br />' +
							'<span>' +
							'<a href="javascript:0;" class="post_readmore" onclick="confirm_pay(`' + res.moota.no_invoice + '`,`' + res.moota.vendor + '`,`' + res.moota.nominal + '`,`' + res.moota.moota_bank + '`)">' +
							'<button style="padding: 5px; width: 100%; border-radius: 5px;" >' +
							'<span class="post_readmore_label">Saya Sudah Melakukan Transfer</span>' +
							'</button>' +
							'</a>' +
							'</span>' +
							'</center>');
					} else {
						snap.pay(res.snap_token, {
							onSuccess: function(result) {
								console.log(result);
								$('#snaptoken').val('');
								$.ajax({
									url: '{{ route("donasi.updateStatusQurban") }}',
									type: 'POST',
									dataType: 'json',
									data: {
										"_token": "{{ csrf_token() }}",
										"id": result.order_id,
										"status": result.transaction_status,
										"payment_method": result.payment_method,
										"snap_token": data.snap_token
									},
									success: function(data) {
										hideLoading();
										console.log(data)
										window.location.href = "{{Route('notif.wait')}}";
									}
								});
							},
							onPending: function(result) {
								console.log(result);
								$('#snaptoken').val('');
								window.location.href = "{{Route('notif.wait')}}";
							},
							onError: function(result) {
								console.log(result);
								$('#snaptoken').val('');
								window.location.href = "{{Route('notif.wait')}}";
							},
							onClose: function() {

							}
						});
					}

				},
				error: function(xhr, status, error) {
					messageAlert(error)
					// Handle any errors that occur during the request
				}
			});


		});


		function getPaymentMethod() {
			var popup = $("#payment-popup-body");
			popup.html("")
			$.ajax({
				Type: 'GET',
				dataType: 'json',
				url: '{{ENV("API_URL")}}/api/payment_vendors/categories',
				success: function(response) {

					response.data.forEach(element => {
						$.ajax({
							Type: 'GET',
							dataType: 'json',
							url: '{{ENV("API_URL")}}/api/payment_vendors/' + element.id + '?yayasan={{ENV("YAYASAN_KEY")}}',
							success: function(response2) {
								$show_title = false;
								try {
									response2.data.forEach(element2 => {
										if (element2.bank_rekening_moota == null) {
											$show_title = true
											// throw BreakException
										} else {
											if (element2.bank_rekening_moota == null) {
												$show_title = true
												// throw BreakException
											} else {
												var used_for = element2.bank_rekening_moota.used_for.split(',');
												for (var i = 0; i < used_for.length; i++) {
													if (used_for[i].includes('Donasi')) {
														$show_title = true
													} else if (used_for[i].includes('Qurban')) {
														$show_title = true
													} else if (used_for[i].includes('Zakat')) {
														$show_title = true
													} else {
														// throw BreakException
													}
												}
											}
										}
									});
								} catch (e) {}

								if (response2.data.length > 0 && $show_title)
									popup.append('<p class="category-title">' + element.category + '</p>');

								response2.data.forEach(element2 => {
									console.log(element2)
									var used_for = ''
									if (element2.bank_rekening_moota) {
										used_for = element2.bank_rekening_moota.used_for.split(',');
									}
									if (element2.bank_rekening_moota == null) {
										$show_title = true
										// throw BreakException
									} else {
										for (var i = 0; i < used_for.length; i++) {
											if (used_for[i].includes('Donasi')) {
												$show_title = true
											} else if (used_for[i].includes('Qurban')) {
												$show_title = true
											} else if (used_for[i].includes('Zakat')) {
												$show_title = true
											} else {
												// throw BreakException
											}
										}
									}

									if (element2.bank_rekening_moota == null || (element2.bank_rekening_moota && $show_title)) {
										let vendor = element2.vendor
										let type = element2.payment_type
										let method = (vendor == "midtrans") ? element2.midtrans_code : element2.xendit_code

										if (method == null) method = element2.moota_bank_id

										let category = ""
										if (element2.payment_type == "virtual_account") category = "Virtual Account "
										if (element2.payment_type == "manual_transfer") category = "Transfer "
										let label = category + element2.payment_name

										let no_rek = ''
										if (element2.bank_rekening_moota) {
											no_rek = element2.bank_rekening_moota.no_rekening;
										}
										let img_src = "{{ENV('BACKEND_URL')}}/api/metode/icon/" + element2.id;
										popup.append('<div class="item-method" onclick="choose_method(`' + vendor + '`,`' + type + '`,`' + method + '`,`' + label + '`,`' + img_src + '`,`' + no_rek + '`)">' +
											'<img class="logo-payment" src=' + img_src + ' alt="">' +
											'<p class="title-payment">' + element2.payment_name + '</p>' +
											'</div>');
									}
								});
							}
						});

					});
				}
			});
		}

	});

	function choose_method(vendor, type, method, label, img_src, no_rek) {



		$("#mthd-close").click()
		$("#payment-method-image").attr("src", img_src)
		$("#payment-method-text").html(label)
		$("#id-pembayaran").val(vendor)
		$("#tipe-pembayaran").val(type)
		$("#method-payment").val(method)
		$("#no-rek-payment").val(no_rek)




		console.log($("#id-pembayaran").val())
		console.log($("#tipe-pembayaran").val())
		console.log($("#method-payment").val())
		console.log($("#no-rek-payment").val())

	}

	function confirm_pay(id, vendor, nominal, moota_bank_id) {
		if (vendor == "moota") {
			$.post('{{ENV("API_URL")}}/api/moota/mutation/by-amount', {
					_method: 'POST',
					bank_id: moota_bank_id,
					amount: nominal,
					type: "Zakat"
				},
				function(data, status) {
					console.log(data)
					messageAlert(data.message);
					$('#nominal-zakat-field').val();
					$('#id-zakat').val();
					$('#payment_category').val();
					$('#id-pembayaran').val();
					$('#tipe-pembayaran').val();
					$('#method-payment').val();
					$('#no-rek-payment').val();
					$('#nama-lengkap-field').val();
					$('#phone-field').val();
					$('#email-field').val();
					$('#komentar-field').val();
					$('#checkbox-verify').val();


					window.location.href = "{{ url('dashboard-donatur?menu=histori-zakat') }}"
				}
			);
		} else {

			$.ajax({
					url: '{{ENV("API_URL")}}/api/confirm-pay-qurban',
					type: 'GET',
					dataType: 'Json',
					data: {
						no_invoice: id
					}
				})
				.done(function(res) {
					messageAlert('Silahkan Lakukan Konfirmasi Pembayaran !');
					$("#notif-pembayaran").removeClass('payment-method-active');

					//window.location.href = "{{ url('qurban/confirm?no_invoice=') }}" + res.confirm.no_invoice
				});

		}
	}


	function copyToClip(element_id) {
		var temp = $("<input>");
		$("body").append(temp);
		temp.val($("#" + element_id).text()).select();
		document.execCommand("copy");
		temp.remove();
	}

	function messageAlert(response) {
		const Toast = Swal.mixin({
			toast: true,
			customClass: 'swal-wide',
			position: 'bottom-end',
			showConfirmButton: false,
			timer: 5000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		Toast.fire({
			title: response
		});
	}
</script>