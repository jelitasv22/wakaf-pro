<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <title>Zakat Agro Bina Alam Mandiri</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
    <link rel="stylesheet" href="{{ asset('themes/pagination/dist/pagination.php')}}" type="text/css">
    @include('front_page.ngo-theme.main_layouts.css')
</head>
<style>
    .button-zakat {
        width: 100%;
        margin-top: 10px;
        padding: 5px;
        text-align: center;
        font-size: 14px;
        background: white;
        border: 1px solid #ddd;
        border-radius: 5px;
        display: inline-block;
        cursor: pointer;
        font-family: Arial;
        font-weight: bold;
        text-decoration: none;
        color: #fff !important;
    }

    .button-zakat:hover {
        color: white !important;
        background-color: #fbce1b;
    }
</style>

<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
    <a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
    <a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            @include('front_page.ngo-theme.main_layouts.header')

            <div class="page_content_wrap page_paddings_no">
                <div class="content_wrap">
                    <div class="container">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-sm-12 col-lg-8 mt-3">
                                <div class="card">
                                    <div class="card-body bg-white">
                                        <form action="{{ route('bayar-zakat.step', 2) }}" method="get" onsubmit="validateForm(event)">

                                            <h2 class="sc_donations_title sc_item_title" style="font-size: 25px; margin-top: 15px;">Bayar Zakat</h2>
                                            <div class="">
                                                <br>
                                                <label class="sc_donations_form_label" for="">Kategori Zakat</label>
                                                <select name="kategori" id="bayar-zakat-category" class="form-select" style="width: 100%;">
                                                    @foreach($kategori_zakat as $category)
                                                    <option value="{{ $category->id }}">{{ $category->category }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="row d-none">
                                                <div class="col-sm-12 col-lg-6 mb-3">
                                                    <button type="button" class="button-zakat btn btn-block" onclick="suggestButton('100000')">Rp 100.000</button>
                                                </div>
                                                <div class="col-sm-12 col-lg-6 mb-3">
                                                    <button type="button" class="button-zakat btn btn-block" onclick="suggestButton('200000')">Rp 200.000</button>
                                                </div>
                                                <div class="col-sm-12 col-lg-6 mb-3">
                                                    <button type="button" class="button-zakat btn btn-block" onclick="suggestButton('500000')">Rp 500.000</button>
                                                </div>
                                                <div class="col-sm-12 col-lg-6 mb-3">
                                                    <button type="button" class="button-zakat btn btn-block" onclick="suggestButton('1000000')">Rp 1.000.000</button>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="mb-3">
                                                    <label class="sc_donations_form_label" for="">Jumlah Nominal Zakat</label>
                                                    <input type="number" required id="nominal-zakat" name="nominal-zakat" class="form-control border" placeholder="0">
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-end">
                                                <button type="submit" class="button-zakat btn btn-block">Bayar Zakat</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-lg-8 mt-3">
                            <div class="card">
                                <div class="card-header bg-white ">
                                    <h2 class="text-center" style="font-size: 25px; margin-top: 15px;">Kalkulator Zakat</h2>
                                    <div class="mb-3">
                                        <label class="form-label">Kategori Zakat</label>
                                        <select id="kategori-zakat" class="form-select" style="width: 100%;" aria-label="Kategori Zakat">
                                            <option selected>Pilih Kategori Zakat</option>
                                            @foreach($kategori_zakat as $category)
                                            <option value="{{ $category->id }}">{{ $category->category }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <form name="zakat-fitrah-page" id="zakat-fitrah-page" action="{{ route('bayar-zakat.step', 2) }}" method="get">

                                        <div>
                                            <label class="form-label mt-3">Pilih Harga Beras</label>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" name="opt-rice-price-fitrah" id="opt-rice-price-fitrah-1" value="13000">Rp. 13.000
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" name="opt-rice-price-fitrah" id="opt-rice-price-fitrah-2" value="15000">Rp. 15.000
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col">
                                                    <div class="mb-3" hidden id="">
                                                        <label class="form-label">ID</label>
                                                        <input type="text" id="id-zakat-fitrah" name="kategori" class="form-control" placeholder="0">
                                                    </div>
                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Jumlah Zakat</label>
                                                        <input type="text" id="jumlah-zakat-field-fitrah" required disabled class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Jumlah Jiwa</label>
                                                        <input type="number" value="1" min="1" id="jiwa-field-fitrah" required class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Total Keseluruhan</label>
                                                        <input type="number" min="1" name="nominal-zakat" id="total-field-fitrah" readonly class="form-control" placeholder="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </form>
                                    <form name="zakat-maal-page" id="zakat-maal-page" action="{{ route('bayar-zakat.step', 2) }}" method="get">
                                        <div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Jumlah Emas (Gram)</label>
                                                        <input type="number" id="jumlah-zakat-field-fitrah" name="jumlah_emas" required class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Jumlah Perak (Gram)</label>
                                                        <input type="number" id="jumlah-perak" name="jumlah_perak" required class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Uang Tunai, Tabungan, Deposito, atau Sejenisnya</label>
                                                        <input type="number" id="uang-tunai" name="uang_tunai" class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Properti (Tidak termasuk yang ditinggali)</label>
                                                        <input type="number" id="properti" name="properti" class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Kendaraan (Tidak termasuk yang dipakai untuk keperluan keluarga)</label>
                                                        <input type="number" id="kendaraan" name="kendaraan" class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Saham, Sukuk, dan Surat berharga lainnya</label>
                                                        <input type="number" id="saham" name="saham" class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Harta lainnya (Koleksi seni, Barang antik, Barang berharga, Dll)</label>
                                                        <input type="number" id="harta-lainnya" name="harta_lainnya" class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Total Harga</label>
                                                        <input type="number" id="total-harga" disabled class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Kewajiban Hutang yang jatuh tempo</label>
                                                        <input type="number" id="kewajiban-hutang" name="kewajiban_hutang" class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Harta yang dizakatkan</label>
                                                        <input type="number" id="harta-dizakatkan" disabled class="form-control" placeholder="0">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Nisab zakat harta (Setara dengan 85 Gram Emas x Harga Emas)</label>
                                                        <input type="number" id="nisab-zakat-harta" disabled class="form-control" placeholder="0">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <form name="zakat-emas-page" id="zakat-emas-page" action="{{ route('bayar-zakat.step', 2) }}" method="get">
                                        <div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="mb-3" hidden id="">
                                                        <label class="form-label">ID</label>
                                                        <input type="number" id="id-zakat-emas" name="kategori" class="form-control" placeholder="0">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="weight">Berat Emas (gram)</label>
                                                        <input type="number" class="form-control" id="gold-weight" placeholder="Masukkan berat emas dalam gram" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="value">Harga Emas per gram (Rp)</label>
                                                        <input type="number" class="form-control" readonly id="gold-value" placeholder="Masukkan harga emas per gram" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="zakatAmount">Jumlah Zakat yang Harus Dikeluarkan (Rp)</label>
                                                        <input type="number" value="0" required class="form-control" name="nominal-zakat" id="gold-amount" readonly>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </form>


                                    <form name="zakat-perak-page" id="zakat-perak-page" action="{{ route('bayar-zakat.step', 2) }}" method="get">
                                        <div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="mb-3" hidden id="">
                                                        <label class="form-label">ID</label>
                                                        <input type="text" id="id-zakat-perak" name="kategori" class="form-control" placeholder="0">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="weight">Berat Perak (gram)</label>
                                                        <input type="number" class="form-control" id="silver-weight" placeholder="Masukkan berat emas dalam gram" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="value">Harga Perak per gram (Rp)</label>
                                                        <input type="number" class="form-control" id="silver-value" readonly placeholder="Masukkan harga emas per gram" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="zakatAmount">Jumlah Zakat yang Harus Dikeluarkan (Rp)</label>
                                                        <input type="number" required value="0" class="form-control" name="nominal-zakat" id="silver-amount" readonly>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <form name="zakat-penghasilan-page" id="zakat-penghasilan-page" action="{{ route('bayar-zakat.step', 2) }}" method="get">
                                        <div class="row">
                                            <div class="col">
                                                <div class="mb-3" hidden id="">
                                                    <label class="form-label">ID</label>
                                                    <input type="text" id="id-zakat-penghasilan" name="kategori" class="form-control" placeholder="0">
                                                </div>
                                                <div class="mb-3" id="">
                                                    <label class="form-label">Penghasilan perbulan</label>
                                                    <input type="number" id="penghasilan-perbulan" required class="form-control" placeholder="0">
                                                </div>
                                                <div class="mb-3" id="">
                                                    <label class="form-label">Penghasilan tambahan perbulan</label>
                                                    <input type="number" id="penghasilan-tambahan" required class="form-control" placeholder="0">
                                                </div>
                                                <div class="mb-3" id="">
                                                    <label class="form-label">Pengeluaran perbulan</label>
                                                    <input type="number" id="pengeluaran-perbulan" required class="form-control" placeholder="0">
                                                </div>

                                                <label class="form-label">Pilih harga beras</label>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" name="opt-rice-price-penghasilan" id="rice-price-1" value="13000">Rp. 13.000
                                                            </label>
                                                        </div>
                                                        <div class="form-check-inline">
                                                            <label class="form-check-label">
                                                                <input type="radio" class="form-check-input" name="opt-rice-price-penghasilan" id="rice-price-2" value="15000">Rp. 15.000
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="mb-3" id="">
                                                    <label class="form-label">Harga beras</label>
                                                    <input type="number" id="harga-beras" required class="form-control" placeholder="0">
                                                </div>

                                                <div class="mb-3" id="">
                                                    <label class="form-label">Nishab (Harga beras x 522 Kg)</label>
                                                    <input type="number" id="nishab" required readonly class="form-control" placeholder="0">
                                                </div>

                                                <div class="mb-3" id="">
                                                    <label class="form-label">Jumlah penghasilan setelah dikurangi pengeluaran</label>
                                                    <input type="number" id="penghasilan-setelah-pengeluaran" required readonly class="form-control" placeholder="0">
                                                </div>

                                                <div class="mb-3" id="">
                                                    <label class="form-label">Jumlah bulan yang akan dibayarkan zakatnya</label>
                                                    <input type="number" id="jumlah-bulan" value="1" min="1" required class="form-control" placeholder="0">
                                                </div>

                                                <div class="mb-3" id="">
                                                    <label class="form-label">Besar zakat hasil perhitungan</label>
                                                    <input type="number" name="nominal-zakat" id="zakat-hasil-perhitungan" required readonly class="form-control" placeholder="0">
                                                </div>

                                            </div>
                                        </div>
                                    </form>
                                    <div class="d-flex justify-content-end">
                                        <button type="submit" id="calc-bayar-zakat-btn" class="button-zakat btn btn-block">Bayar Zakat</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('front_page.ngo-theme.components.footer')
    </div>
    <!-- /.page_wrap -->
    </div>
    <!-- /.body_wrap -->
    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    <div class="custom_html_section"></div>
    @include('front_page.ngo-theme.main_layouts.js')
    @include('front_page.ngo-theme.pages.zakat.js')
</body>

</html>


<script>
    function suggestButton(value) {
        $('#nominal-zakat').val(value)
    }

    function messageAlert(response) {
        const Toast = Swal.mixin({
            toast: true,
            customClass: 'swal-wide',
            position: 'top-end',
            showConfirmButton: false,
            timer: 5000,
            timerProgressBar: true,
            onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        Toast.fire({
            title: response
        });
    }


    function validateForm(event) {
        const nominalZakat = $('#nominal-zakat').val()
        if (nominalZakat == '') {
            messageAlert('Nominal Zakat tidak boleh kosong')
            event.preventDefault();
        }
    }
</script>