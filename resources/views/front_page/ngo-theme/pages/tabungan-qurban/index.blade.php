<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <title>Zakat Agro Bina Alam Mandiri</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
    <link rel="stylesheet" href="{{ asset('themes/pagination/dist/pagination.php')}}" type="text/css">
    @include('front_page.ngo-theme.main_layouts.css')
</head>
<style>
    .button-zakat {
        width: 100%;
        margin-top: 10px;
        padding: 5px;
        text-align: center;
        font-size: 14px;
        background: white;
        border: 1px solid #ddd;
        border-radius: 5px;
        display: inline-block;
        cursor: pointer;
        font-family: Arial;
        font-weight: bold;
        text-decoration: none;
        color: #fff !important;
    }

    .button-zakat:hover {
        color: white !important;
        background-color: #fbce1b;
    }
</style>

<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
    <a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
    <a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            @include('front_page.ngo-theme.main_layouts.header')

            <div class="page_content_wrap page_paddings_no">
                <div class="content_wrap">
                    <div class="container">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-sm-12 col-lg-8 mt-3">
                                <div class="card">
                                    <div class="card-body bg-white">
                                        <form action="#" method="post">
                                            <div class="row">
                                                <div class="col">
                                                    <h2 class="text-center" style="font-size: 25px; margin-top: 15px;">Detail Informasi</h2>
                                                    <label class="form-label">Jumlah Nominal</label>
                                                    <div class="input-group mb-3" id="pendapatan-section">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="basic-addon1">Rp</span>
                                                        </div>
                                                        <input type="number" name="nominal_zakat_field" id="nominal-field" value="0" class="form-control" placeholder="0">
                                                    </div>

                                                    <div hidden class="mb-3">
                                                        <input type="text" name="id_zakat" value="hehe" hidden required id="id-zakat">
                                                    </div>    


                                                    <div class="card">
                                                        <div class="card-body bg-white text-center mt-3" id="select-payment-method">
                                                            <input type="text" name="id_pembayaran" hidden required id="id-pembayaran">
                                                            <input type="text" name="tipe_pembayaran" hidden required id="tipe-pembayaran">
                                                            <input type="text" name="method_payment" hidden required id="method-payment">
                                                            <input type="text" name="no_rek_payment" hidden required id="no-rek-payment">

                                                            <img src="{{ asset('/image/bank.png')}}" alt="" style="max-width: 80px" id="payment-method-image">
                                                            <h5 class="text-center" style="margin-top: 15px;" id="payment-method-text">Silahkan Pilih Metode Pembayaran</h5>
                                                        </div>
                                                    </div>

                                                    <div class="mb-3 mt-5" id="">
                                                        <label class="form-label">Nama Lengkap</label>
                                                        <input type="text" name="nama_lengkap_field" id="nama-lengkap-field" required class="form-control" value="{{Session::get('name')}}" placeholder="Nama Lengkap">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Nomor Handphone</label>
                                                        <input type="text" name="phone_field" id="phone-field" class="form-control" value="{{Session::get('no_telp')}}" required placeholder="6289XXXXXXXX">
                                                    </div>

                                                    <label class="form-label">Email</label>
                                                    <div class="mb-3" id="">
                                                        <input type="email" name="email_field" id="email-field" value="{{Session::get('email')}}" class="form-control" required placeholder="Email">
                                                    </div>

                                                    <button type="submit" id="purchase-zakat" class="button-zakat btn btn-block">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="metode-pembayaran" class="popup">
            <div class="popup-content">
                <div class="head-pop">
                    <div class="title-pop">
                        <p>Metode Pembayaran</p>
                    </div>
                    <span id="mthd-close" class="tutup">&times;</span>
                </div>
                <div class="payment-scroller" id="payment-popup-body">

                </div>
            </div>
        </div>

        <div id="notif-pembayaran" class="popup">
            <div class="popup-content-notif">
                <div class="head-pop">
                    <div class="title-pop">
                        <p id="title_popup">Transfer</p>
                    </div>
                    <span id="simple-pop-close" class="tutup">&times;</span>
                </div>
                <div class="payment-scroller" id="payment-popup-body-notif" style="padding: 10px;">
                    <div class="text-color-popup" style="display: flex; border-radius: 5px; font-size: 15px; padding: 5px; line-height: 40px; text-align: center;">
                        <span style="width: 20%;">
                            <img class="logo-payment" id="moota-logo" src="" alt="">
                        </span>
                        <span id="no_rekening_show" style="width: 60%; text-align: center;">-</span>
                        <span id="salin" style="width: 20%; text-align: right;"><a href="javascript:0;" style="color: white;" onclick="copyToClip('no_rekening_show')">Salin</a></span>
                    </div>
                    <div id="intruksi-pembayaran">
                        <center><br />
                            <span><b>Intruksi Pembayaran</b></span><br>
                            <span>Nominal yang harus anda bayar :</span><br>
                            <span style="font-size: 18px;"><b>Rp. 10.000</b></span><br>
                            <span>*Harap selesaikan pembayaran sesuai dengan nominal yang tertera untuk memudahkan pengecekan!</span><br>
                            <span>*Sesuaikan nominal sampai 3 digit terakhir! (Kelebihan dari nominal akan didonasikan juga)</span><br>
                            <span>*Lakukan pembayaran maksimal 1x24 jam!</span><br>
                            <span>*Silahkan akses link berikut untuk melihat perkembangan donasi anda! (Link dikirim juga ke email anda)</span><br>
                            <span>*Tekan tombol dibawah juka sudah melakukan pembayaran!</span><br><br />
                            <span>
                                <a href="" class="post_readmore">
                                    <button style="padding: 5px; width: 100%; border-radius: 5px;">
                                        <span class="post_readmore_label">Saya Sudah Melakukan Transfer</span>
                                    </button>
                                </a>
                            </span>
                        </center>
                    </div>
                </div>
            </div>
        </div>


        @include('front_page.ngo-theme.components.footer')
    </div>
    <!-- /.page_wrap -->
    </div>
    <!-- /.body_wrap -->
    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    <div class="custom_html_section"></div>
    @include('front_page.ngo-theme.main_layouts.js')
    @include('front_page.ngo-theme.pages.tabungan-qurban.js')
</body>

</html>