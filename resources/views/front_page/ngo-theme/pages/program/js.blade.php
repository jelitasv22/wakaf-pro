<script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}"data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		function tanggal_indonesia($tgl) {
			var date = new Date($tgl);

			var tahun = date.getFullYear();
			var bulan = date.getMonth();
			var tanggal = date.getDate();
			var hari = date.getDay();
			var jam = date.getHours();
			var menit = date.getMinutes();
			var detik = date.getSeconds();
			switch(hari) {
				case 0: hari = "Minggu"; break;
				case 1: hari = "Senin"; break;
				case 2: hari = "Selasa"; break;
				case 3: hari = "Rabu"; break;
				case 4: hari = "Kamis"; break;
				case 5: hari = "Jum'at"; break;
				case 6: hari = "Sabtu"; break;
			}
			switch(bulan) {
				case 0: bulan = "Januari"; break;
				case 1: bulan = "Februari"; break;
				case 2: bulan = "Maret"; break;
				case 3: bulan = "April"; break;
				case 4: bulan = "Mei"; break;
				case 5: bulan = "Juni"; break;
				case 6: bulan = "Juli"; break;
				case 7: bulan = "Agustus"; break;
				case 8: bulan = "September"; break;
				case 9: bulan = "Oktober"; break;
				case 10: bulan = "November"; break;
				case 11: bulan = "Desember"; break;
			}
			var tampilTanggal = hari + ", " + tanggal + " " + bulan + " " + tahun;
			var tampilWaktu = "Jam: " + jam + ":" + menit + ":" + detik;

			return tampilTanggal;
		}

		getPaymentMethod();

		var rupiah = document.getElementById('nominal');
		if(rupiah)
			rupiah.addEventListener('keyup', function (e) {
				rupiah.value = formatRupiah(this.value, '');
			});

		function formatRupiah(angka, prefix) {
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			if (ribuan) {
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		}

		$('#paginate').pagination({
			ajax: function(options, refresh, target){
				$.ajax({
					url: '{{ENV("API_URL")}}/api/ngo-program/program?page='+options.current+'&lang={{$language_ses}}&yayasan={{ENV("YAYASAN_KEY")}}',
					method: 'GET',
					dataType: 'json',
				}).done(function(res){
					refresh({
						total: res.list.total,
						length: res.list.length
					});

					var total  = 0;
					var target = 0;
					var hasil  = 0;
					var persen = 0;

					$('#list_program').html('');
					$.each(res.list.data, function(index, val) {

						if(val.dana_target == null && val.total != 0){
							hasil = 100;
						}else if(val.dana_target != null && val.total != 0){
							persen = val.dana_target/100;
							hasil  = val.total/persen;
						}else{
							hasil = 0;
						}

						if(val.total == null){
							total = 0;
						}else{
							total = val.total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
						}

						if(val.dana_target == null){
							target = 0;
						}else{
							target = val.dana_target.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
						}

						$('#list_program').append('<div class="post_item_excerpt post_type_donation sc_donations_column-1_3">'+
							'<div class="post_featured">'+
								'<img width="570" height="320" src="{{ENV("BACKEND_URL")}}/admin/assets/media/foto-program/'+val.foto+'" class="attachment-thumb_med size-thumb_med" alt="" />'+
							'</div>'+
							'<div class="post_body">'+
								'<div class="post_header entry-header">'+
									'<h4 class="entry-title"><a href="{{ url("program/detail-program") }}/'+val.seo+'" rel="bookmark">'+val.judul+'</a></h4>'+
									'<p>'+tanggal_indonesia(val.tanggal)+'</p>'+
								'</div>'+
								'<div class="post_content entry-content">'+
									'<p>'+val.resume+'</p>'+
									'<div class="post_info_donations">'+
										'<div class="top">'+
											'<span class="post_info_item post_raised"><span class="post_counters_label">Terkumpul</span></span>'+
											'<span class="post_info_item post_goal"><span class="post_counters_label">Target</span></span>'+
										'</div>'+
										'<div class="middle">'+
											'<span style="width: '+hasil+'%;"></span>'+
										'</div>'+
										'<div class="bottom">'+
											'<span class="post_counters_number_raised">'+
												total+
											'</span>'+
											'<span class="post_counters_number_goal">'+
												target+
											'</span>'+
										'</div>'+
									'</div>'+
									'<a href="{{ url("program/donate") }}/'+val.seo+'" class="post_readmore" style="padding: 10px; border-radius: 5px; margin-top: 10px; width: 93%;">'+
										'<span class="post_readmore_label">Donasi Sekarang</span>'+
									'</a>'+
								'</div>'+
							'</div>'+
						'</div>');
					});
				}).fail(function(error){
					console.log(error);
				});
			}
		});
		$('#paginate_donatur_program_detail').pagination({
			ajax: function(options, refresh, target){
				path = window.location.href;
				url = path.split('/');
				$.ajax({
					url: '{{ENV("API_URL")}}/api/ngo-program/program/detail-program?seo='+url[5]+'&page='+options.current+'&lang={{$language_ses}}&yayasan={{ENV("YAYASAN_KEY")}}',
					method: 'GET',
					dataType: 'json',
				}).done(function(res){
					refresh({
						total: res.donatur.total,
						length: res.donatur.length
					});

					var total   = 0;
					var target  = 0;
					var hasil   = 0;
					var persen  = 0;
					var name    = '';
					var tanggal = '';
					var date    = '';

					$('#list_donatur').html('');
					$('#loading-donatur').hide();
					$.each(res.donatur.data, function(index, val) {

						if(val.name == null){
							name = val.nama_lengkap;
						}else{
							name = val.name;
						}

						date = val.created_at.split(' ');
						tanggal = date[0].split('-');

						$('#list_donatur').append('<div style="width: 25%;">'+
							'<img src="{{ENV("BACKEND_URL")}}/admin/assets/media/foto-users/245/Users_5ee71cc25f960.png" style="width: 50px;"><br>'+
							'<span style="font-size: 12px;"><b>'+name+'</b></span><br>'+
							'<span style="font-size: 12px;">'+tanggal[2]+'-'+tanggal[1]+'-'+tanggal[0]+' '+date[1]+'</span><br>'+
							'<span style="font-size: 12px;"><b>Rp. '+val.total_donasi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+'</b></span>'+
						'</div>');
					});
				}).fail(function(error){
					console.log(error);
				});
			}
		});

		$("#selected-method").hide();
		$("#btn-payment-method").on('click', function () {
			$("#metode-pembayaran").addClass('payment-method-active');
			$("#metode-pembayaran").removeClass('payment-method-hide');
		});

		$("#btn-payment-method2").on('click', function () {
			$("#metode-pembayaran").addClass('payment-method-active');
			$("#metode-pembayaran").removeClass('payment-method-hide');
		});

		$("#mthd-close").off('click').on('click', function () {
			$("#metode-pembayaran").addClass('payment-method-hide');
			$("#metode-pembayaran").removeClass('payment-method-active');
		});

		$("#simple-pop-close").off('click').on('click', function () {
			$("#notif-pembayaran").addClass('payment-method-hide');
			$("#notif-pembayaran").removeClass('payment-method-active');

			$('#program_id').val('');
			$('#nominal').val('');
			$('#nama_lengkap').val('');
			$('#no_handphone').val('');
			$('#email').val('');
			$("#payment_vendor").val('');
			$("#payment_type").val('');
			$("#payment_method").val('');
			$('#selected-method-img').val('')
			$('#selected-method-label').val('')
			$('#no_rekening_show').val('');
		});

		$('#donasi_sekarang').click(function(){
			let program_id      = $('#program_id').val();
			let nominal 		= $('#nominal').val().replace(/\./g,'');
			let nama_lengkap	= $('#nama_lengkap').val();
			let no_handphone	= $('#no_handphone').val();
			let email 			= $('#email').val();
			let vendor   		= $("#payment_vendor").val();
			let payment_type	= $("#payment_type").val();
			let payment_method	= $("#payment_method").val();
			let no_rekening     = $('#no_rekening').val();
			let _uid = null;
			let minimal = 1

			
			if ('{{Session::has("uid")}}') {
				_uid = '{{Session::get("uid")}}';
			}

			if(payment_type != 'e_wallet'){
				minimal = 10000
			}

			const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

			if(nominal == null || nominal < minimal){
				messageAlert('Nominal Harus di Isi !, Minimal Donasi Rp. '+minimal)
			}else if(nama_lengkap == ''){
				messageAlert('Nama Lengkap Harus di Isi')
			}else if(no_handphone == ''){
				messageAlert('No Handphone Harus di Isi')
			}else if(email == ''){
				messageAlert('Email Harus di Isi')
			}else if(payment_type == ''){
				messageAlert('Harap Pilih Metode Pembayaran')
			}else if(re.test(String(email).toLowerCase())){
				$.ajax({
					url: '{{ENV("API_URL")}}/api/snap-token-midtrans',
					type: 'POST',
					dataType: 'Json',
					data: {
						key_yayasan     : '{{ENV("YAYASAN_KEY")}}',
						program_id 		: program_id,
						nominal		  	: nominal,
						user_id 		: _uid,
						nama_lengkap  	: nama_lengkap,
						no_handphone  	: no_handphone,
						email		  	: email,
						vendor 			: vendor,
						payment_type	: payment_type,
						payment_method  : payment_method,
						no_rekening     : no_rekening
					},
				})
				.done(function(res) {
					// console.log(res.moota.no_rekening)
					if(payment_type == 'manual_transfer'){
						$("#notif-pembayaran").addClass('payment-method-active');

						let modal_title = $("#modal_title")
						let modal_body = $("#modal_body")

						let type = $("#payment_type").val()
						let method = $("#payment_method").val()

						let nominal = res.moota.nominal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")

						$("#title_popup").html('Transfer '+res.moota.nama_bank)
						$("#moota-logo").attr("src", $("#selected-method-img").attr("src"))
						$("#no_rekening_show").html(res.moota.no_rekening)

						$('#intruksi-pembayaran').html('<center><br />'+
							'<span><b>Intruksi Pembayaran</b></span><br>'+
							'<span>Nominal yang harus anda bayar :</span><br>'+
							'<span style="font-size: 18px;"><b>Rp. '+nominal+'</b></span><br>'+
							'<span>*Harap selesaikan pembayaran sesuai dengan nominal yang tertera untuk memudahkan pengecekan!</span><br>'+
							'<span>*Sesuaikan nominal sampai 3 digit terakhir! (Kelebihan dari nominal akan didonasikan juga)</span><br>'+
							'<span>*Lakukan pembayaran maksimal 1x24 jam!</span><br>'+
							'<span>*Silahkan akses link berikut untuk melihat perkembangan donasi anda! (Link dikirim juga ke email anda)</span><br>'+
							'<span>*Tekan tombol dibawah juka sudah melakukan pembayaran!</span><br><br />'+
							'<span>'+
							'<a href="javascript:0;" class="post_readmore" onclick="confirm_pay(`'+res.moota.no_invoice+'`,`'+res.moota.vendor+'`,`'+res.moota.nominal+'`,`'+res.moota.moota_bank+'`)">'+
							'<button style="padding: 5px; width: 100%; border-radius: 5px;" >'+
							'<span class="post_readmore_label">Saya Sudah Melakukan Transfer</span>'+
							'</button>'+
							'</a>'+
							'</span>'+
						'</center>');

						// $("#mthd-close").click(function(){
						// 	alert("asdsa")
						// })
					}else{
						console.log('masok')
						snap.pay(res.snap_token, {
							onSuccess: function (result) {
								console.log(result);
								$('#snaptoken').val('');
								$.ajax({
									url: '{{ route("donasi.updateStatus") }}',
									type: 'POST',
									dataType: 'json',
									data: {
										"_token": "{{ csrf_token() }}",
										"id": result.order_id,
										"status": result.transaction_status,
										"payment_method": result.payment_method,
										"snap_token": res.snap_token
									},
									success: function(data){
										// hideLoading();
										console.log(data)
										window.location.href = "{{Route('notif.wait')}}";
									}
								});
							},
							onPending: function (result) {
								console.log(result);
								$('#snaptoken').val('');
								window.location.href = "{{Route('notif.wait')}}";
							},
							onError: function (result) {
								console.log(result);
								$('#snaptoken').val('');
								window.location.href = "{{Route('notif.wait')}}";
							},
							onClose: function(){
								$.ajax({
									url: '{{ENV("API_URL")}}/api/delete-snap-token-midtrans',
									type: 'POST',
									dataType: 'json',
									data: {
										"_token": "{{ csrf_token() }}",
										"key_yayasan"     : '{{ENV("YAYASAN_KEY")}}',
										"iDtoken": res.snap_token,
										"status": 1,
									},
									success: function(data){
										alert('Transaksi Dibatalkan')
										location.reload()
									}
								});
							}
						});
					}
				});
			}else{
				messageAlert('Email Tidak Valid')
			}
		});

		$('#konfirmasi_pembayaran').click(function(e){
            e.preventDefault();
            $.ajax({
            	url:'{{ENV("API_URL")}}/api/confirm-payment-donasi',
            	type:"POST",
            	data:new FormData(document.getElementById('data-konfirmasi')),
            	processData:false,
            	contentType:false,
            	cache:false,
            	async:false,
            	success: function(res){
            		messageAlert(res.msg)

            		window.location.href = '{{ url("program") }}';
            	}
            });
        });

        $('#cari').click(function(e){
        	e.preventDefault();
        	$.ajax({
        		url: '{{ENV("API_URL")}}/api/track/transaction/'+$('#no_invoice').val(),
        		type: 'GET',
        		dataType: 'Json',
        	})
        	.done(function(res) {
        		if(res.donasi.name == null){
        			$('#nama_donatur').val(res.donasi.nama_lengkap)
        		}else{
        			$('#nama_donatur').val(res.donasi.name)
        		}

        		$('#id_donasi').val(res.donasi.id)
        		$('#id_yayasan').val(res.donasi.id_yayasan)
        		$('#guest_id').val(res.donasi.guest_id)
        		$('#metode_pembayaran').val(res.donasi.metode_pembayaran)
        		$('#total_donasi').val(res.donasi.total_donasi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
        	});

        });

		function getPaymentMethod()
		{
			var popup = $("#payment-popup-body");
			popup.html("")

			$.ajax({
				Type : 'GET',
				dataType : 'json',
				url : '{{ENV("API_URL")}}/api/payment_vendors/categories',
				success: function(response){

					response.data.forEach(element => {
						$.ajax({
							Type : 'GET',
							dataType : 'json',
							url : '{{ENV("API_URL")}}/api/payment_vendors/'+element.id+'?yayasan={{ENV("YAYASAN_KEY")}}',
							success: function(response2){
								$show_title = false;
								try {
									response2.data.forEach(element2 => {
										if (element2.bank_rekening_moota == null) {
											$show_title = true
											// throw BreakException
										} else {
											if (element2.bank_rekening_moota == null) {
												$show_title = true
												// throw BreakException
											} else {
												var used_for = element2.bank_rekening_moota.used_for.split(',');
												for (var i = 0; i < used_for.length; i++) {
													if(used_for[i].includes('Donasi')){
														$show_title = true
													}else if(used_for[i].includes('Qurban')){
														$show_title = true
													}else if(used_for[i].includes('Zakat')){
														$show_title = true
													}else{
														// throw BreakException
													}
												}
											}
										}
									});
								} catch (e) {
								}

								if (response2.data.length > 0 && $show_title)
									popup.append('<p class="category-title">'+element.category+'</p>');

								response2.data.forEach(element2 => {
									console.log(element2)
									var used_for = ''
									if(element2.bank_rekening_moota){
										used_for = element2.bank_rekening_moota.used_for.split(',');
									}
									if (element2.bank_rekening_moota == null) {
										$show_title = true
										// throw BreakException
									} else {
										for (var i = 0; i < used_for.length; i++) {
											if(used_for[i].includes('Donasi')){
												$show_title = true
											}else if(used_for[i].includes('Qurban')){
												$show_title = true
											}else if(used_for[i].includes('Zakat')){
												$show_title = true
											}else{
												// throw BreakException
											}
										}
									}

									if (element2.bank_rekening_moota == null || (element2.bank_rekening_moota && $show_title)) {
										let vendor = element2.vendor
										let type = element2.payment_type
										let method = (vendor == "midtrans") ? element2.midtrans_code : element2.xendit_code

										if (method == null) method = element2.moota_bank_id

										let category = ""
										if (element2.payment_type == "virtual_account") category = "Virtual Account "
										if (element2.payment_type == "manual_transfer") category = "Transfer "
										let label = category + element2.payment_name

										let no_rek = ''
										if(element2.bank_rekening_moota){
											no_rek  = element2.bank_rekening_moota.no_rekening;
										}
										let img_src = "{{ENV('BACKEND_URL')}}/api/metode/icon/"+element2.id;
										popup.append('<div class="item-method" onclick="choose_method(`'+vendor+'`,`'+type+'`,`'+method+'`,`'+label+'`,`'+img_src+'`,`'+no_rek+'`)">'+
											'<img class="logo-payment" src='+img_src+' alt="">'+
											'<p class="title-payment">'+element2.payment_name+'</p>'+
											'</div>');
									}
								});
							}
						});

					});
				}
			});
		}

	});

	function choose_method(vendor, type, method, label, img_src, no_rek)
	{
		$("#mthd-close").click()
		$("#cc_form").hide()
		$("#ew_form").hide()

		$("#payment_vendor").val(vendor)
		$("#payment_type").val(type)
		$("#payment_method").val(method)
		$('#no_rekening').val(no_rek)

		$("#selected-method-img").attr("src",img_src)
		$("#selected-method-label").html(label)
		$("#selected-method").show()
		$("#btn-payment-method").hide()


		let payment_type = $("#payment_type").val()
		var payment_method = $("#payment_method").val();

		if (payment_type == "credit_card") {
			// if (vendor == "xendit") $("#cc_form").show()
		} else if (payment_type == "e_wallet") {
			if (payment_method == 'gopay' || payment_method == 'DANA')
				$("#ew_form").hide();
			else
				$("#ew_form").show();

		} else if (payment_type == "retail_outlet") {

		} else if (payment_type == "virtual_account") {

		} else if (payment_type == "manual_transfer") {

		}
	}

	function confirm_pay(id, vendor, nominal, moota_bank_id)
	{
		if(vendor == "moota"){
			$.post('{{ENV("API_URL")}}/api/moota/mutation/by-amount',
			{
				_method	: 'POST',
				bank_id: moota_bank_id,
				amount: nominal,
				type: "Donasi"
			},
			function (data, status) {
				$("#notif-pembayaran").removeClass('payment-method-active');
				messageAlert(data.message);

				$('#program_id').val('');
				$('#nominal').val('');
				$('#nama_lengkap').val('');
				$('#no_handphone').val('');
				$('#email').val('');
				$("#payment_vendor").val('');
				$("#payment_type").val('');
				$("#payment_method").val('');
				$('#no_rekening').val('');

			}
			);
		}else{
			$.ajax({
				url: '{{ENV("API_URL")}}/api/confirm-pay-donasi',
				type: 'GET',
				dataType: 'Json',
				data: {
					no_invoice: id
				}
			})
			.done(function(res) {
				messageAlert('Silahkan Lakukan Konfirmasi Pembayaran !');
				$("#notif-pembayaran").removeClass('payment-method-active');

				window.location.href = "{{ url('program/confirm?no_invoice=') }}"+res.confirm.snap_token
			});
		}
	}

	function copyToClip(element_id)
	{
		var temp = $("<input>");
		$("body").append(temp);
		temp.val($("#"+element_id).text()).select();
		document.execCommand("copy");
		temp.remove();
	}

	function messageAlert(response){
		const Toast = Swal.mixin({
			toast: true,
			customClass: 'swal-wide',
			position: 'bottom-end',
			showConfirmButton: false,
			timer: 5000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		Toast.fire({
			title: response
		});
	}
</script>
