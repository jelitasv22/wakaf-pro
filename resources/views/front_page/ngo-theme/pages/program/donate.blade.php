<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
	<title>Donate Agro Bina Alam Mandiri</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">
	<link rel="stylesheet" type="text/css" href="{{ asset('themes/payment/payment.php') }}">
	@include('front_page.ngo-theme.main_layouts.css')
</head>
<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
	<a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
	<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
	<div class="body_wrap">
		<div class="page_wrap">
			<div class="top_panel_fixed_wrap"></div>
			@include('front_page.ngo-theme.main_layouts.header')

			<div class="page_content_wrap page_paddings_no">
				<div class="page_content_wrap page_paddings_no">
					<div class="content_wrap wrapper">
						<div class="content">
							<div id="post-407" class="post_item_single post_type_donation post-407 donation type-donation status-publish has-post-thumbnail hentry donation_category-second-group">
								<div class="post_sidebar">
									<div class="post_featured">
										<img width="570" height="320" src="{{ENV('BACKEND_URL')}}/admin/assets/media/foto-program/{{$detail_program->list->foto}}" class="attachment-thumb_med size-thumb_med" alt="Sponsor Ecology Today">
									</div>
									<?php
									if($detail_program->list->total != null){
										$total  = $detail_program->list->total;
									}else{
										$total = 0;
									}

									if($detail_program->list != null){
										$target = $detail_program->list->dana_target;
									}else{
										$target = 0;
									}

									if ($target != null || $target != 0){
										$persen = $target/100;
										$hasil  = round($total/$persen);
										if($hasil > 100){
											$hasil = 100;
										}
									}else{
										$persen = 100;

										if($total != null || $total != 0){
											$hasil  = 100;
										}else{
											$hasil = 0;
										}
									}
									?>
									<div class="post_info_donations">
										<div class="post_raised">
											<span class="post_raised_title">Terkumpul</span><span class="post_raised_amount">{{$detail_program->list->total}}</span>
										</div>
										<div class="middle"><span style="width: {{$hasil}}%"></span></div>
										<div class="post_goal">
											<span class="post_goal_title">Target</span><span class="post_goal_amount">{{$detail_program->list->dana_target}}</span>
										</div>
									</div>
									<div class="post_info_donations"><br>
										<span class="post_raised_title">{{$detail_program->list->judul}}</span><br>
										<div class="post_info">
											<span class="post_info_item post_date">
												<?php
												if($detail_program->list->tanggal == null){
													echo "(Tanpa Batas Waktu)";
												}else{
													echo Helper::tanggal_indonesia($detail_program->list->tanggal)." ( Batas Waktu Program )";
												}
												?>
											</span>
										</div>
									</div>

									<div class="post_content entry-content">
										{{ $detail_program->list->resume }} . . . <a href="{{ Route('program.detailprogram', $detail_program->list->seo) }}">Read More</a>
									</div>
								</div>

								<div class="post_body">
									<span style="text-align: center; font-size:25px; font-weight: bold;">Donasi</span>
									<div id="sc_donations_form" class="sc_donations_form">
										<form id="sc_donations_form_form" method="post" action="#">
											<div class="sc_donations_form_field">
                                        @if ($donation_type)
                                            @if ($donation_type->type == "kelipatan")
                                                <input type="text" name="Nominal" placeholder="Nominal" id="nominal">
                                                <div class="sugest-box">
                                                    @php
                                                        $value = $donation_type->value;
                                                    @endphp
                                                    @for ($i = 1; $i <= 4; $i++)
                                                        @php
                                                            $value = $value*$i;
                                                        @endphp
                                                        <button type="button" class="button-donasi" style="padding: 5px" onclick="suggestButton({{$value}})">
                                                            {{number_format($value)}}
                                                        </button>
                                                    @endfor
                                                </div>
                                            @else
                                                <div style="display: flex">
                                                    <button type="button" class="button-donasi" style="margin-top: 0px; margin-bottom: 5px; padding: 10px; height: 60px; margin-right: 5px;" onclick="btnMinusOnClick({{$donation_type->value}})">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                    <input type="text" name="Nominal" value="{{$donation_type->value}}" placeholder="Nominal" id="nominal" readonly>
                                                    <button type="button" class="button-donasi" style="margin-top: 0px; margin-bottom: 5px; padding: 10px; height: 60px" onclick="btnPlusOnClick({{$donation_type->value}})">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            @endif
                                        @endif
                                        @if ($nominals)
                                            <input type="text" name="Nominal" placeholder="Nominal" id="nominal">
                                            <div class="sugest-box">
                                                @foreach ($nominals as $val)
                                                    <button type="button" class="button-donasi" style="padding: '5px'" onclick="suggestButton2('{{$val->value}}')">
                                                        {{$val->value}}
                                                    </button>
                                                @endforeach
                                            </div>
                                        @endif
											</div>
											<div class="sc_donations_form_field">
												<label class="sc_donations_form_label">Metode Pembayaran</label>
												<button type="button" id="btn-payment-method" style="width: 100%; padding: 8px; border-radius: 5px">
													Pilih Metode Pembayaran
												</button>
												<input type="hidden" name="program_id" id="program_id" value="{{$detail_program->list->id}}">
												<input name="payment_vendor" id="payment_vendor" type="hidden" value="" />
												<input name="payment_type" id="payment_type" type="hidden" value="" />
												<input name="payment_method" id="payment_method" type="hidden" value="" />
												<input type="hidden" id="no_rekening" name="no_rekening" value="">

												<div class="sc_form_item sc_form_field label_over">
													<div id="selected-method" style="display: flex; width: 100%;">
														<div style="width: 50%;">
															<img src="" alt="" id="selected-method-img" style="max-width:100%;max-height:40px;margin-right:10px;">
															<span style="font-size:14px;" id="selected-method-label">

															</span>
														</div>
														<div style="width: 50%;">
															<button type="button" id="btn-payment-method2" style="width: 50%; padding: 5px; border-radius: 5px; float: right;">
																Ganti
															</button>
														</div>
													</div>
												</div>

												<div class="sc_donations_form_field"><br>
													<label class="sc_donations_form_label" for="">Nama Lengkap</label>
													<input id="nama_lengkap" type="text" value="{{Session::get('name')}}" name="nama_lengkap" placeholder="Nama Lengkap">
												</div>
												<div class="sc_donations_form_field">
													<label class="sc_donations_form_label" for="">No Handphone/ Whatsapp</label>
													<input id="no_handphone" type="text" value="{{Session::get('no_telp	')}}" name="no_handphone" placeholder="No Handphone">
												</div>
												<div class="sc_donations_form_field">
													<label class="sc_donations_form_label" for="">Email</label>
													<input id="email" type="text" name="email" value="{{Session::get('email')}}" placeholder="Email">
												</div>

												<div class="sc_donations_form_field">
													<button type="button" id="donasi_sekarang" style="width: 100%; padding: 10px; border-radius: 5px;">
														Donasi Sekarang
													</button>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="metode-pembayaran" class="popup">
						<div class="popup-content">
							<div class="head-pop">
								<div class="title-pop">
									<p>Metode Pembayaran</p>
								</div>
								<span id="mthd-close" class="tutup">&times;</span>
							</div>
							<div class="payment-scroller" id="payment-popup-body">

							</div>
						</div>
					</div>

					<div id="notif-pembayaran" class="popup">
						<div class="popup-content-notif">
							<div class="head-pop">
								<div class="title-pop">
									<p id="title_popup">Transfer</p>
								</div>
								<span id="simple-pop-close" class="tutup">&times;</span>
							</div>
							<div class="payment-scroller" id="payment-popup-body-notif" style="padding: 10px;">
								<div class="text-color-popup" style="display: flex; border-radius: 5px; font-size: 15px; padding: 5px; line-height: 40px; text-align: center;">
									<span style="width: 20%;">
										<img class="logo-payment" id="moota-logo" src="" alt="">
									</span>
									<span id="no_rekening_show" style="width: 60%; text-align: center;">-</span>
									<span id="salin" style="width: 20%; text-align: right;"><a href="javascript:0;" style="color: white;" onclick="copyToClip('no_rekening_show')">Salin</a></span>
								</div>
								<div id="intruksi-pembayaran">
									<center><br />
										<span><b>Intruksi Pembayaran</b></span><br>
										<span>Nominal yang harus anda bayar :</span><br>
										<span style="font-size: 18px;"><b>Rp. 10.000</b></span><br>
										<span>*Harap selesaikan pembayaran sesuai dengan nominal yang tertera untuk memudahkan pengecekan!</span><br>
										<span>*Sesuaikan nominal sampai 3 digit terakhir! (Kelebihan dari nominal akan didonasikan juga)</span><br>
										<span>*Lakukan pembayaran maksimal 1x24 jam!</span><br>
										<span>*Silahkan akses link berikut untuk melihat perkembangan donasi anda! (Link dikirim juga ke email anda)</span><br>
										<span>*Tekan tombol dibawah juka sudah melakukan pembayaran!</span><br><br />
										<span>
											<a href="" class="post_readmore">
												<button style="padding: 5px; width: 100%; border-radius: 5px;">
													<span class="post_readmore_label">Saya Sudah Melakukan Transfer</span>
												</button>
											</a>
										</span>
									</center>
								</div>
							</div>
						</div>
					</div>

					@include('front_page.ngo-theme.components.footer')
				</div>
			</div>
		</div>
		<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
		<div class="custom_html_section"></div>
		@include('front_page.ngo-theme.main_layouts.js')
        @include('front_page.ngo-theme.pages.program.js')
<script type="text/javascript">
    function btnMinusOnClick(value) {
        var nominal = $('#nominal').val()
        nominal = parseInt(nominal)-parseInt(value)
        if(nominal < parseInt(value)) {
            nominal = parseInt(value)
        }
        $('#nominal').val(nominal);
    }

    function btnPlusOnClick(value) {
        var nominal = $('#nominal').val()
        nominal = parseInt(nominal)+parseInt(value)
        $('#nominal').val(nominal);
    }

    function suggestButton(value) {
        $('#nominal').val(value)
    }

    function suggestButton2(value) {
        $('#nominal').val(parseInt(value.replace(/\./g, '')))
    }
</script>
	</body>
	</html>
