<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
	<title>{{$detail_program->list->judul}}</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">
	<link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
	<link rel="stylesheet" href="{{ asset('themes/pagination/dist/pagination.php')}}" type="text/css">
	@include('front_page.ngo-theme.main_layouts.css')
</head>
<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
	<a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
	<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
	<div class="body_wrap">
		<div class="page_wrap">
			<div class="top_panel_fixed_wrap"></div>
			@include('front_page.ngo-theme.main_layouts.header')

			<div class="page_content_wrap page_paddings_no">
				<div class="content_wrap">
					<div class="content">
						<div class="slider_wrap slider_fullwide slider_engine_revo slider_alias_main">
							<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
								<img src="{{ENV('BACKEND_URL')}}/admin/assets/media/foto-program/{{$detail_program->list->foto}}" style="width: 100%; height: 30%;">
							</div>
						</div>
						<div class="itemscope post_item post_item_single post_featured_default post_format_standard post-2 page type-page status-publish hentry" itemscope itemtype="http://schema.org/Article">
							<div class="post_content" itemprop="articleBody">
								<div class="block">
									<div class="column_container">
										<div class="column-inner">
											<div class="wrapper" style="margin-top: 50px;">
												<?php
												if($detail_program->list->total != null){
													$total  = $detail_program->list->total;
												}else{
													$total = 0;
												}

												if($detail_program->list != null){
													$target = $detail_program->list->dana_target;
												}else{
													$target = 0;
												}

												if ($target != null || $target != 0){
													$persen = $target/100;
													$hasil  = round($total/$persen);
													if($hasil > 100){
														$hasil = 100;
													}
												}else{
													$persen = 100;

													if($total != null || $total != 0){
														$hasil  = 100;
													}else{
														$hasil = 0;
													}
												}
												?>
												<!--Program Detail-->
												<h4>{{$detail_program->list->judul}}</h4>
												<span>
													<?php
													if($detail_program->list->tanggal == null){
														echo "(Tanpa Batas Waktu)";
													}else{
														echo Helper::tanggal_indonesia($detail_program->list->tanggal)." (Batas Waktu Program)";
													}
													?>
												</span><br>
												<div class="post_info_donations">
													<div class="middle">
														<span style="width: {{$hasil}}%;"></span>
													</div>
												</div>
												<span>Terkumpul {{number_format($detail_program->list->total)}} dari {{number_format($detail_program->list->dana_target)}}</span>
												<p>
													{!! $detail_program->list->deskripsi !!}
												</p>
												<center>
													<a href="{{ url('program/donate') }}/{{$detail_program->list->seo}}">
                                                    <button style="border-radius: 5px; width: 200px;">Donasi Sekarang</button>
                                                    </a>
												</center>
                                                <!--END Program Detail-->

												<!--Donatur-->
												<center id="loading-donatur" style="margin-top: 20px;">
													<span>Loading Donatur . . .</span>
												</center>

												<center style="margin-top: 20px;"><b>Donatur</b></center><br>
												<center>
													<div id="list_donatur" style="margin-top: 10px; width: 100%; display: flex;"></div>
												</center>
												<center>
													<ul id="paginate_donatur_program_detail" class="pagination"></ul>
												</center>
												<!--END Donatur-->

												<!--Info Terbaru-->
												<center><b>Info Terbaru</b></center><br>
												<div class="card">
													<div class="card-body">
														@if(count($detail_program->info) < 0)
														<center id="tidak_ada_info">Tidak Ada Info Terbaru</center>
														@endif
														<div class="row" id="list_info">
															<?php foreach ($detail_program->info as $key => $value) { ?>
																<div class="col-lg-12">
																	<p><b>{{$value->judul}}</b></p>
																	<p>{!! $value->content !!}</p>
																	</div>
																<?php } ?>
															</div>
														</div>
													</div>
													<hr>
													<!--END Info Terbaru-->

													Mari berbagi informasi yang insha Allah bermanfaat kepada rekan dan saudara kita. Semoga menjadi amal soleh yang membawa keberkahan untuk anda. klik tombol share di bawah ini.

													<!--Sosmed-->
													<center style="margin-bottom: 50px; margin-top: 10px; float: right;">

														<span><a href="">Facebook</a></span>
														<span><a href="">Twitter</a></span>
														<span><a href="">Whatsapp</a></span>

													</center>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				@include('front_page.ngo-theme.components.footer')
			</div>
			<!-- /.page_wrap -->
		</div>
		<!-- /.body_wrap -->
		<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
		<div class="custom_html_section"></div>
		@include('front_page.ngo-theme.main_layouts.js')
		@include('front_page.ngo-theme.pages.program.js')
	</body>
	</html>
