<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
	<title>Program Agro Bina Alam Mandiri</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- <meta http-equiv="content-type" content="text/html;charset=UTF-8" /> -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no"><link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
	<link rel="stylesheet" href="{{ asset('themes/pagination/dist/pagination.php')}}" type="text/css">
	<link rel="stylesheet" href="{{ asset('themes/ngo-theme-2/assets/css/style.css')}}" type="text/css">
	<!-- <link rel="stylesheet" media="(max-width: 800px)" href="{{ asset('admin/assets/css/demo5/style.bundle.css')}}" /> -->

	@include('front_page.ngo-theme.main_layouts.css')

</head>
<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
	<!-- <main style="min-height : 100vh"> -->
	<a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
	<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
	<div class="body_wrap">
		<div class="page_wrap">
			<div class="top_panel_fixed_wrap"></div>
			@include('front_page.ngo-theme.main_layouts.header')

			

		<div class="container-one">
			
			
			<div class="container clearfix margin-btm">

        		<div class="row justify-content-centerr" >

        			<div class="col-md-7 center">
						<div class="gambarnotif2">
							<img src="{{('image/bell_abu.png')}}" class="w-16">
						</div>
        				<div class="heading-block nobottomborder mb-4">
        					<h4 class="mb-4 not judul">@lang('language.notifikasi_transaksi')</h4>
						</div>
						
        				<!-- <div class="svg-line bottommargin-sm clearfix">
        					<hr style="background-color: red; height: 1px; width: 200px;">
						</div> -->

						<div class="textwaitblade2">
							<p>Terima kasih atas donasi dan partisipasi anda.
							   Setelah anda selesai melakukan transfer dana donasi. 
							   Anda akan mendapatkan konfirmasi melalui e-mail. 
							   Semoga Allah membalas dengan pahala yang berlipat.</p>
						</div>

			
							<div class="thanksbutton">
								<button>Kembali</button> 
								<br>
								<br>
								
							</div>
						
						
					</div>
					
				</div>
				
				</div>
        	</div>

			@include('front_page.ngo-theme.components.footer')
		</div>
		<!-- /.page_wrap -->
	</div>
	<!-- /.body_wrap -->
	<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
	<div class="custom_html_section"></div>

	<!-- </main>Main Wrapper -->

	@include('front_page.ngo-theme.main_layouts.js')
	@include('front_page.ngo-theme.pages.program.js')
</body>
</html>
