<script type="text/javascript">
	$(document).ready(function() {
		function tanggal_indonesia($tgl) {
			var date = new Date($tgl);

			var tahun = date.getFullYear();
			var bulan = date.getMonth();
			var tanggal = date.getDate();
			var hari = date.getDay();
			var jam = date.getHours();
			var menit = date.getMinutes();
			var detik = date.getSeconds();
			switch(hari) {
				case 0: hari = "Minggu"; break;
				case 1: hari = "Senin"; break;
				case 2: hari = "Selasa"; break;
				case 3: hari = "Rabu"; break;
				case 4: hari = "Kamis"; break;
				case 5: hari = "Jum'at"; break;
				case 6: hari = "Sabtu"; break;
			}
			switch(bulan) {
				case 0: bulan = "Januari"; break;
				case 1: bulan = "Februari"; break;
				case 2: bulan = "Maret"; break;
				case 3: bulan = "April"; break;
				case 4: bulan = "Mei"; break;
				case 5: bulan = "Juni"; break;
				case 6: bulan = "Juli"; break;
				case 7: bulan = "Agustus"; break;
				case 8: bulan = "September"; break;
				case 9: bulan = "Oktober"; break;
				case 10: bulan = "November"; break;
				case 11: bulan = "Desember"; break;
			}
			var tampilTanggal = hari + ", " + tanggal + " " + bulan + " " + tahun;
			var tampilWaktu = "Jam: " + jam + ":" + menit + ":" + detik;

			return tampilTanggal;
		}

		$('#paginate').pagination({
			ajax: function(options, refresh, target){
				$.ajax({
					url: '{{ENV("API_URL")}}/api/ngo-posting?page='+options.current+'&lang={{$language_ses}}&yayasan={{ENV("YAYASAN_KEY")}}',
					type: 'GET',
					dataType: 'Json',
				})
				.done(function(res) {
					refresh({
						total: res.paginate.total,
						length: res.paginate.length 
					});

					$('#list_posting').html('');
					$.each(res.paginate.data, function(index, val) {
						$('#list_posting').append('<div class="post_item_excerpt post_type_donation sc_donations_column-1_3">'+
							'<div class="post_featured">'+
								'<img width="570" height="320" src="{{ENV("BACKEND_URL")}}/admin/assets/media/posting/'+val.image+'" class="attachment-thumb_med size-thumb_med" alt="" />'+
							'</div>'+
							'<div class="post_body">'+
								'<div class="post_header entry-header">'+
									'<h4 class="entry-title"><a href="{{ url("posting/detail-posting") }}/'+val.seo+'" rel="bookmark">'+val.judul+'</a></h4>'+
								'</div>'+
								'<div class="post_content entry-content">'+
									'<p>'+tanggal_indonesia(val.created_at)+'</p>'+
									'<p>'+val.content.substr(0, 250)+'</p>'+
									'<a href="{{ url("posting/detail-posting") }}/'+val.seo+'" class="link-more">'+
										'<span>Read More</span>'+
									'</a>'+
								'</div>'+
							'</div>'+
						'</div>');
					});
				}).fail(function(error){
					console.log(error);
				});
			}
		});
	});
</script>
