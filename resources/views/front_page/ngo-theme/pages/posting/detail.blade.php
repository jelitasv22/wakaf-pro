<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
	<title>{{$detail_posting->list->judul}}</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">
	<link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
	@include('front_page.ngo-theme.main_layouts.css')
</head>
<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
	<a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
	<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
	<div class="body_wrap">
		<div class="page_wrap">
			<div class="top_panel_fixed_wrap"></div>
			@include('front_page.ngo-theme.main_layouts.header')
			
			<div class="page_content_wrap page_paddings_no">
				<div class="content_wrap">
					<div class="content">
						<div class="slider_wrap slider_fullwide slider_engine_revo slider_alias_main">
							<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
								<img src="{{ENV('BACKEND_URL')}}/admin/assets/media/posting/{{$detail_posting->list->image}}" style="width: 100%; height: 30%;">
							</div>
						</div>
						<div class="itemscope post_item post_item_single post_featured_default post_format_standard post-2 page type-page status-publish hentry" itemscope itemtype="http://schema.org/Article">
							<div class="post_content" itemprop="articleBody">
								<div class="block">
									<div class="column_container">
										<div class="column-inner">
											<div class="wrapper" style="margin-top: 50px;">
												<!--Program Detail-->
												<h4>{{$detail_posting->list->judul}}</h4>
												<span>{{ Helper::tanggal_indonesia($detail_posting->list->created_at)}}</span>

												{!! $detail_posting->list->content !!}

												<span><b>Update Lainnya<hr></b></span>
												<div id="sc_donations_108588160" class="sc_donations sc_donations_style_excerpt">
													<div class="sc_donations_columns_wrap" id="list_posting">
														
													</div>

													<center>
														<ul id="paginate" class="pagination"></ul>
													</center>
												</div>
												<!--Sosmed-->
												<center style="margin-bottom: 50px; margin-top: 10px; float: right;">

													<span><a href="">Facebook</a></span>
													<span><a href="">Twitter</a></span>
													<span><a href="">Whatsapp</a></span>

												</center>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			@include('front_page.ngo-theme.components.footer')
		</div>
	</div>
	<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
	<div class="custom_html_section"></div>
	@include('front_page.ngo-theme.main_layouts.js')
	@include('front_page.ngo-theme.pages.posting.js')
</body>
</html>
