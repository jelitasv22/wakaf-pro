<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <title>Konsultasi Agro Bina Alam Mandiri</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">




    <style type="text/css">
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background-color: #fff;
        }

        .preloader .loading {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            font: 14px arial;
        }
    </style>
    @include('front_page.ngo-theme.main_layouts.css')
</head>

<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
    <a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
    <a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            @include('front_page.ngo-theme.main_layouts.header')
            <div class="page_content_wrap page_paddings_no">
                <div class="content_wrap">
                    <div class="container">
                        <div class="row ">
                            <div class="col-sm-12 col-lg-8 mt-3">
                                <div class="card">
                                    <div class="card-body bg-white">
                                        <form action="" class="contact-form" method="">
                                            {{csrf_field()}}
                                            <h3 class="mb-4 text-center">Konsultasi Qurban</h3>
                                            <input type="hidden" name="id" id="id" value="{{$list_yayasan->id}}">
                                            <div class="form-group">
                                                <label for="name">Nama</label>
                                                <input type="text" class="form-control" id="name" name="name" placeholder="Nama Anda">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="subjek">Subject</label>
                                                <input type="text" class="form-control" id="subjek" name="subjek" placeholder="Subject Konsultasi">
                                            </div>
                                            <div class="form-group">
                                                <label for="perihal">Perihal</label>
                                                <textarea class="form-control" id="perihal" name="perihal" placeholder="Isi Konsultasi"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="captcha">Captcha</label>
                                                {!! NoCaptcha::renderJs() !!}
                                                {!! NoCaptcha::display() !!}
                                                <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                                            </div>
                                            <br>
                                            <button type="button" class="btn btn-primary btn-block" onclick="sendkonsultasi()">Kirim Konsultasi</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-4 mt-3">
                                <div class="card">
                                    <div class="card-body bg-white text-center">
                                        <h5 class="mb-4">Hubungi Kami</h5>
                                        <div class="">
                                            <img src="{{asset('image/gmail.png')}}" class="contact-icon" alt="Email Icon" style="width: 13%;">
                                            <p class="contact-info">{{$list_yayasan->email}}</p>
                                        </div>
                                        <div class="mt-5">
                                            <img src="{{asset('image/phone.png')}}" class="contact-icon" alt="Phone Icon" style="width: 13%;">
                                            <p class="contact-info">{{$list_yayasan->no_telp}}</p>

                                        </div>
                                        <div class="mt-5">
                                            <img src="{{asset('image/wa.png')}}" class="contact-icon" alt="WhatsApp Icon" style="width: 13%;">
                                            <!-- redirect to whatsapp -->
                                            <?php
                                            // cek jika nomor telepon diawali dengan 0, maka hilangkan 0 dan tambahkan 62
                                            if (substr($list_yayasan->no_wa, 0, 1) == '0') {
                                                $no_telp = '62' . substr($list_yayasan->no_wa, 1);
                                            } else {
                                                $no_telp = $list_yayasan->no_wa;
                                            }

                                            ?>
                                            <a href="https://wa.me/{{$no_telp}}" target="_blank">
                                                <p class="contact-info">{{$list_yayasan->no_wa}}</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            @include('front_page.ngo-theme.components.footer')
        </div>
        <!-- /.page_wrap -->
    </div>
    <!-- /.body_wrap -->

    <div class="modal fade bd-example-modal-lg" id="modalloading" data-backdrop="static" data-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-sm" style="display: flex;
    justify-content: center;
    align-items: center;
    height: 100%;">
            <div class="modal-content" style="width: 60px;">
                <span class="fa fa-spinner fa-spin fa-3x"></span>
            </div>
        </div>
    </div>

    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    <div class="custom_html_section"></div>



    @include('front_page.ngo-theme.main_layouts.js')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>

    <script>
        function messageAlert(response) {
            const Toast = Swal.mixin({
                toast: true,
                customClass: 'swal-wide',
                position: 'center-end',
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });

            Toast.fire({
                title: response
            });
        }

        function sendkonsultasi() {

            if ($('#g-recaptcha-response').val() == "") {
                messageAlert('Captcha Tidak Boleh Kosong')
            } else if ($("#nama").val() == "") {
                messageAlert('nama tidak boleh kosong')
            } else if ($("#email").val() == "") {
                messageAlert('email tidak boleh kosong')
            } else if ($("#subjek").val() == "") {
                messageAlert('subjek tidak boleh kosong')
            } else if ($("#perihal").val() == "") {
                messageAlert('perihal tidak boleh kosong')
            } else {
                $.ajax({
                    type: 'POST', //THIS NEEDS TO BE GET
                    url: '{{ENV("API_URL")}}/api/send_konsultasi', //Route Insert
                    dataType: 'json',
                    data: {
                        "_token": '{{csrf_token()}}',
                        "id_yayasan": $('#id').val(),
                        "name": $('#name').val(),
                        "email": $('#email').val(),
                        "subjek": $('#subjek').val(),
                        "perihal": $('#perihal').val()
                    },
                    beforeSend: function() {
                        showloading();
                        // $hashRate.html(loading);
                        // $valid.html(loading);
                        // $invalid.html(loading);
                        // $invalidp.html(loading);
                    },
                    success: function(data) {
                        hideloading();
                        messageAlert(data);
                        $('#name').val('');
                        $('#email').val('');
                        $('#subjek').val('');
                        $('#perihal').val('');

                        $('#modalloading').modal('hide');
                    }
                });
            }
        }

        function hideloading() {
            $('#modalloading').modal('hide');
        }

        function showloading() {
            $('#modalloading').modal('show');
        }
    </script>
</body>

</html>