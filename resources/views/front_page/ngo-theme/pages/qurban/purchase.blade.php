<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
	<title>Purchase Agro Bina Alam Mandiri </title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no"><link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
	<link rel="stylesheet" type="text/css" href="{{ asset('themes/payment/payment.php') }}">
	@include('front_page.ngo-theme.main_layouts.css')
</head>
<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
	<a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
	<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
	<div class="body_wrap">
		<div class="page_wrap">
			<div class="top_panel_fixed_wrap"></div>
			@include('front_page.ngo-theme.main_layouts.header')
			
			<div class="page_content_wrap page_paddings_no">
				<div class="content_wrap">
					<div class="content">
						<div class="itemscope post_item post_item_single post_featured_default post_format_standard post-2 page type-page status-publish hentry" itemscope itemtype="http://schema.org/Article">
							<div class="post_content" itemprop="articleBody">
								<div class="block">
									<div class="column_container">
										<div class="column-inner">
											<div class="wrapper">
												<div id="sc_donations_108588160" class="sc_donations sc_donations_style_excerpt">
													<h2 class="sc_donations_title sc_item_title" style="font-size: 25px; margin-top: 15px;">Tunaikan Qurban</h2>

													<div id="sc_form_1145226909" class="sc_form sc_form_style_form_2 margin_bottom_huge" style="margin-top: 10px !important;">
														<center>
															<div class="sc_form_fields" style="width: 80%;">
																<form id="sc_form_1145226909_form" class="sc_input_hover_default inited" data-formtype="form_2" method="post" action="#">
																	<div class="sc_form_info">
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Nama</span>
																			<input id="nama_donatur" type="text" name="nama_donatur" placeholder="Nama Donatur">
																		</div>
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Hewan</span>
																			<select class="orderby" name="hewan_qurban" id="hewan_qurban" onchange="change_produk(this.value)">
																				<option value="" selected="" disabled="">- Pilih Hewan Qurban -</option>
																			</select>
																			<input type="hidden" name="id_produk_qurban" id="id_produk_qurban">
																		</div>
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Harga</span>
																			<input id="harga_qurban" type="text" name="harga" placeholder="Harga" readonly="">
																		</div>
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Sedekah Operasional</span>
																			<input id="sedekah_qurban" type="text" name="sedekah" placeholder="Sedekah">
																		</div>
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Email</span>
																			<input id="email" type="text" name="email" placeholder="Email">
																		</div>
																		<div class="sc_form_item sc_form_field label_over">
																			<span>No Handphone</span>
																			<input id="no_handphone" type="text" name="no_handphone" placeholder="No Handphone">
																		</div>
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Nama Pequrban</span>
																			<textarea id="nama_pequrban" name="nama_pequrban" placeholder="Nama Pequrban" rows="5"></textarea>
																		</div>
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Alamat Pequrban</span>
																			<textarea id="alamat_pequrban" name="alamat_pequrban" placeholder="Alamat Pequrban" rows="5"></textarea>
																		</div>

																		<!-- Data Tambahan -->
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Apakah anda ingin meminta bagian?</span><br>
																			<input type="radio" id="minta_bagian_ya" name="minta_bagian" value="1" style="width: auto;"> Ya
																			&emsp;
																			<input type="radio" id="minta_bagian_tidak" name="minta_bagian" value="0" style="width: auto;"> Tidak
																		</div>
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Apakah anda ingin menyaksikan langsung via zoom?</span><br>
																			<input type="radio" id="lihat_via_zoom_ya" name="lihat_via_zoom" value="1" style="width: auto;"> Ya
																			&emsp;
																			<input type="radio" id="lihat_via_zoom_tidak" name="lihat_via_zoom" value="0" style="width: auto;"> Tidak
																		</div>
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Dokumentasi apa yang anda butuhkan?</span><br>
																			<input type="checkbox" id="dokumentasi_foto" name="dokumentasi_foto" value="1" style="width: auto;"> Foto
																			&emsp;
																			<input type="checkbox" id="dokumentasi_video" name="dokumentasi_video" value="1" style="width: auto;"> Video
																		</div>
																		<div class="sc_form_item sc_form_field label_over">
																			<span>Jika keadaan tertentu ketika sapi tidak mencapai 7/7, apakah bersedia di konversi kan ke domba atau sebaliknya?</span><br>
																			<input type="radio" id="bersedia_konversi_ya" name="bersedia_konversi" value="1" style="width: auto;"> Ya
																			&emsp;
																			<input type="radio" id="bersedia_konversi_tidak" name="bersedia_konversi" value="0" style="width: auto;"> Tidak
																		</div>

																		<div class="sc_form_item sc_form_button">
																			<span>Metode Pembayaran</span>
																			<button id="btn-payment-method" style="width: 100%; padding: 8px; border-radius: 5px">
																				Pilih Metode Pembayaran
																			</button>
																			<input name="payment_vendor" id="payment_vendor" type="hidden" value="" />
																			<input name="payment_type" id="payment_type" type="hidden" value="" />
																			<input name="payment_method" id="payment_method" type="hidden" value="" />
																			<input type="hidden" name="no_rekening" id="no_rekening" value="">

																			<div class="sc_form_item sc_form_field label_over">
																				<div id="selected-method" style="display: flex; width: 100%;">
																					<div style="width: 50%;">
																						<img src="" alt="" id="selected-method-img" style="max-width:100%;max-height:40px;margin-right:10px;">
																						<span style="font-size:14px;" id="selected-method-label">
																							
																						</span>
																					</div>
																					<div style="width: 50%;">
																						<button id="btn-payment-method2" style="width: 50%; padding: 5px; border-radius: 5px; float: right;">
																							Ganti
																						</button>
																					</div>
																				</div>
																			</div>

																			<div class="sc_form_item sc_form_field label_over">
																				<span>Dengan Berdonasi Qurban di Yayasan Ini Anda Telah Menyetujui Syarat dan Ketentuan.</span>
																			</div>
																		</div>
																	</div>
																	<div class="sc_form_item sc_form_button" style="margin-top: 10px;">
																		<button id="tunaikan_qurban" style="width: 100%; padding: 10px; border-radius: 5px">
																			Tunaikan Qurban
																		</button>
																	</div>
																</form>
															</div>
														</center>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="metode-pembayaran" class="popup">
				<div class="popup-content">
					<div class="head-pop">
						<div class="title-pop">
							<p>Metode Pembayaran</p>
						</div>
						<span id="mthd-close" class="tutup">&times;</span>
					</div>
					<div class="payment-scroller" id="payment-popup-body">

					</div>
				</div>
			</div>

			<div id="notif-pembayaran" class="popup">
				<div class="popup-content-notif">
					<div class="head-pop">
						<div class="title-pop">
							<p id="title_popup">Transfer</p>
						</div>
						<span id="simple-pop-close" class="tutup">&times;</span>
					</div>
					<div class="payment-scroller" id="payment-popup-body-notif" style="padding: 10px;">
						<div class="text-color-popup" style="display: flex; border-radius: 5px; font-size: 15px; padding: 5px; line-height: 40px; text-align: center;">
							<span style="width: 20%;">
								<img class="logo-payment" id="moota-logo" src="" alt="">
							</span>
							<span id="no_rekening_show" style="width: 60%; text-align: center;"></span>
							<span id="salin" style="width: 20%; text-align: right;"><a href="javascript:0;" style="color: white;" onclick="copyToClip('no_rekening_show')">Salin</a></span>
						</div>
						<div id="intruksi-pembayaran">
							<center><br />
								<span><b>Intruksi Pembayaran</b></span><br>
								<span>Nominal yang harus anda bayar :</span><br>
								<span style="font-size: 18px;"><b>Rp. 10.000</b></span><br>
								<span>*Harap selesaikan pembayaran sesuai dengan nominal yang tertera untuk memudahkan pengecekan!</span><br>
								<span>*Sesuaikan nominal sampai 3 digit terakhir! (Kelebihan dari nominal akan didonasikan juga)</span><br>
								<span>*Lakukan pembayaran maksimal 1x24 jam!</span><br>
								<span>*Silahkan akses link berikut untuk melihat perkembangan donasi anda! (Link dikirim juga ke email anda)</span><br>
								<span>*Tekan tombol dibawah juka sudah melakukan pembayaran!</span><br><br />
								<span>
									<a href="" class="post_readmore">
										<button style="padding: 5px; width: 100%; border-radius: 5px;">
											<span class="post_readmore_label">Saya Sudah Melakukan Transfer</span>
										</button>
									</a>
								</span>
							</center>
						</div>
					</div>
				</div>
			</div>

			
			@include('front_page.ngo-theme.components.footer')
		</div>
	</div>
	<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
	<div class="custom_html_section"></div>
	@include('front_page.ngo-theme.main_layouts.js')
	@include('front_page.ngo-theme.pages.qurban.js')
</body>
</html>
