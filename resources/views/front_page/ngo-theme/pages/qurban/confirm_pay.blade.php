<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
	<title>Konfirmasi Pembayaran</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no"><link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
	@include('front_page.ngo-theme.main_layouts.css')
	@section('css')

	@endsection
</head>
<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
	<a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
	<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
	<div class="body_wrap">
		<div class="page_wrap">
			<div class="top_panel_fixed_wrap"></div>
			@include('front_page.ngo-theme.main_layouts.header')
			
			<div class="page_content_wrap page_paddings_no">
				<div class="content_wrap">
					<div class="content">
						<div class="itemscope post_item post_item_single post_featured_default post_format_standard post-2 page type-page status-publish hentry" itemscope itemtype="http://schema.org/Article">
							<div class="post_content" itemprop="articleBody">
								<div class="block">
									<div class="column_container">
										<div class="column-inner">
											<div class="wrapper">
												<div id="sc_donations_108588160" class="sc_donations sc_donations_style_excerpt">
													<h2 class="sc_donations_title sc_item_title" style="font-size: 25px; margin-top: 15px;">Konfirmasi Pembayaran</h2>

													<div id="sc_form_1145226909" class="sc_form sc_form_style_form_2 margin_bottom_huge" style="margin-top: 10px !important;">
														<form id="data-konfirmasi" class="sc_input_hover_default inited" data-formtype="form_2" method="post" enctype="multipart/form-data">
															{{csrf_field()}}
															<div class="sc_form_info">
																<div class="sc_form_item sc_form_field label_over" style="display: flex;"> 
																	<div style="width: 70%;">
																		<span>No Invoice</span>
																		<input id="no_invoice" type="text" name="no_invoice" placeholder="No Invoice" value="{{$confirm->confirm->no_invoice}}" id="no_invoice">

																		<input type="hidden" name="id_yayasan" value="{{$confirm->confirm->id_yayasan}}">
																		<input type="hidden" name="id_qurban" id="id_qurban" value="{{$confirm->confirm->id}}">
																		<input type="hidden" name="guest_id" id="guest_id" value="{{$confirm->confirm->guest_id}}">
																	</div>
																	<div style="width: 30%;">
																		<button type="button" id="cari" style="width: 95%; padding: 8px; border-radius: 5px; margin-top: 24px; height: 40px; float: right;">
																			Cari
																		</button>
																	</div>
																</div>

																<div class="sc_form_item sc_form_field label_over">
																	<span>Nama Pequrban</span>
																	<textarea id="nama_pequrban" name="nama_pequrban" placeholder="Nama Pequrban" rows="5" readonly="">{{$confirm->confirm->nama_pequrban}}</textarea>
																</div>

																<div class="sc_form_item sc_form_field label_over">
																	<span>Hewan Qurban</span>
																	<input type="text" name="hewan_qurban" placeholder="Hewan Qurban" readonly="" value="{{$confirm->confirm->master_produk_qurban->jenis_hewan}}" id="hewan_qurban">
																</div>

																<div class="sc_form_item sc_form_field label_over">
																	<span>Metode Pembayaran</span>
																	<input type="text" name="metode_pembayaran" placeholder="Metode Pembayaran" readonly="" value="{{$confirm->confirm->metode_pembayaran}}" id="metode_pembayaran">
																</div>

																<div class="sc_form_item sc_form_field label_over">
																	<span>Total Qurban</span>
																	<input type="text" name="total_qurban" placeholder="Total Qurban" readonly="" value="{{number_format($confirm->confirm->total_qurban)}}" id="total_qurban">
																</div>

																<div class="sc_form_item sc_form_field label_over">
																	<span>Nama Pengirim</span>
																	<input type="text" name="nama_pengirim" placeholder="Nama Pengirim" id="nama_pengirim">
																</div>

																<div class="sc_form_item sc_form_field label_over">
																	<span>Bank Pengirim</span>
																	<input type="text" name="bank_pengirim" placeholder="Bank Pengirim" id="bank_pengirim">
																</div>

																<div class="sc_form_item sc_form_field label_over">
																	<span>Tanggal Konfirmasi</span>
																	<input type="date" name="tanggal_transaksi" placeholder="Tanggal Transaksi" value="{{date('Y-m-d')}}" style="background-color: #f2f2f2" id="tanggal_transaksi">
																</div>

																<div class="sc_form_item sc_form_field label_over">
																	<span>Bukti Transfer</span>
																	<input type="file" name="bukti_transfer" placeholder="Bukti Transfer" id="bukti_transfer" style="background-color: #f2f2f2">
																</div>
															</div>
															<center style="margin-top: 5px; margin-bottom: 10px;">
																<button type="submit" id="konfirmasi_pembayaran" style="width: 100%; padding: 8px; border-radius: 5px">
																	Konfirmasi Pembayaran
																</button>
															</center>
														</form>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			@include('front_page.ngo-theme.components.footer')
		</div>
		<!-- /.page_wrap -->
	</div>
	<!-- /.body_wrap -->
	<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
	<div class="custom_html_section"></div>
	@include('front_page.ngo-theme.main_layouts.js')
	@include('front_page.ngo-theme.pages.qurban.js')
</body>
</html>
