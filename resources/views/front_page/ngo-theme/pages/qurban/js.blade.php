<script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}"data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		produk();
 		getPaymentMethod();

		$("#selected-method").hide();
		$("#btn-payment-method").on('click', function () {
			$("#metode-pembayaran").addClass('payment-method-active');
			$("#metode-pembayaran").removeClass('payment-method-hide');
		});

		$("#btn-payment-method2").on('click', function () {
			$("#metode-pembayaran").addClass('payment-method-active');
			$("#metode-pembayaran").removeClass('payment-method-hide');
		});

		$("#mthd-close").off('click').on('click', function () {
			$("#metode-pembayaran").addClass('payment-method-hide');
			$("#metode-pembayaran").removeClass('payment-method-active');
		});

		$("#simple-pop-close").off('click').on('click', function () {
			$("#notif-pembayaran").addClass('payment-method-hide');
			$("#notif-pembayaran").removeClass('payment-method-active');
		});

		var rupiah = document.getElementById('sedekah_qurban');
		if(rupiah)
			rupiah.addEventListener('keyup', function (e) {
				rupiah.value = formatRupiah(this.value, '');
			});

		function formatRupiah(angka, prefix) {
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split = number_string.split(','),
			sisa = split[0].length % 3,
			rupiah = split[0].substr(0, sisa),
			ribuan = split[0].substr(sisa).match(/\d{3}/gi);

			if (ribuan) {
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}

			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
		}

		function produk() {
			$.ajax({
				url: '{{ENV("API_URL")}}/api/produk-qurban?yayasan={{ENV("YAYASAN_KEY")}}',
				type: 'GET',
				dataType: 'Json',
			})
			.done(function(res) {
				$.each(res.produk, function(index, val) {
					$('#hewan_qurban').append('<option value="'+val.id+'">'+val.jenis_hewan+'</option>');
				});
			});			
		}

		function getPaymentMethod()
		{
			var popup = $("#payment-popup-body");
			popup.html("")

			$.ajax({
				Type : 'GET', 
				dataType : 'json',
				url : '{{ENV("API_URL")}}/api/payment_vendors/categories',
				success: function(response){

					response.data.forEach(element => {
						$.ajax({
							Type : 'GET', 
							dataType : 'json',
							url : '{{ENV("API_URL")}}/api/payment_vendors/'+element.id+'?yayasan={{ENV("YAYASAN_KEY")}}',
							success: function(response2){
								$show_title = false;
								try {
									response2.data.forEach(element2 => {
										if (element2.bank_rekening_moota == null) {
											$show_title = true
											// throw BreakException
										} else {
											var used_for = element2.bank_rekening_moota.used_for.split(',');
											if (element2.bank_rekening_moota == null) {
												$show_title = true
												// throw BreakException
											} else {
												for (var i = 0; i < used_for.length; i++) {
													if(used_for[i].includes('Donasi')){
														$show_title = true
													}else if(used_for[i].includes('Qurban')){
														$show_title = true
													}else if(used_for[i].includes('Zakat')){
														$show_title = true
													}else{
														// throw BreakException
													}
												}
											}
										}
									});
								} catch (e) {
								}

								if (response2.data.length > 0 && $show_title)
									popup.append('<p class="category-title">'+element.category+'</p>');

								response2.data.forEach(element2 => {
									console.log(element2)
									var used_for = ''
									if(element2.bank_rekening_moota){
										used_for = element2.bank_rekening_moota.used_for.split(',');
									}
									if (element2.bank_rekening_moota == null) {
										$show_title = true
										// throw BreakException
									} else {
										for (var i = 0; i < used_for.length; i++) {
											if(used_for[i].includes('Donasi')){
												$show_title = true
											}else if(used_for[i].includes('Qurban')){
												$show_title = true
											}else if(used_for[i].includes('Zakat')){
												$show_title = true
											}else{
												// throw BreakException
											}
										}
									}
									if (element2.bank_rekening_moota == null || (element2.bank_rekening_moota && $show_title)) {
										let vendor = element2.vendor
										let type = element2.payment_type
										let method = (vendor == "midtrans") ? element2.midtrans_code : element2.xendit_code
										
										if (method == null) method = element2.moota_bank_id

										let category = ""
										if (element2.payment_type == "virtual_account") category = "Virtual Account "
										if (element2.payment_type == "manual_transfer") category = "Transfer "
										let label = category + element2.payment_name

										let no_rek = ''
										if(element2.bank_rekening_moota){
											no_rek  = element2.bank_rekening_moota.no_rekening;
										}
										let img_src = "{{ENV('BACKEND_URL')}}/api/metode/icon/"+element2.id;
										popup.append('<div class="item-method" onclick="choose_method(`'+vendor+'`,`'+type+'`,`'+method+'`,`'+label+'`,`'+img_src+'`,`'+no_rek+'`)">'+
											'<img class="logo-payment" src='+img_src+' alt="">'+
											'<p class="title-payment">'+element2.payment_name+'</p>'+
											'</div>');
									}
								});
							}
						});

					});
				}
			});
		}

		$('#tunaikan_qurban').on('click', function(){
			let hewan_qurban    = $('#hewan_qurban').val();
			let id_produk_qurban = $("#id_produk_qurban").val();
			let harga_hewan     = $('#harga_qurban').val().replace(/\./g,'');
			let sedekah_qurban  = $('#sedekah_qurban').val().replace(/\./g,'');
			let nama_donatur    = $('#nama_donatur').val();
			let email 		    = $('#email').val();
			let no_handphone    = $('#no_handphone').val();
			let nama_pequrban   = $('#nama_pequrban').val();
			let alamat_pequrban = $('#alamat_pequrban').val();
			let vendor   		= $("#payment_vendor").val();
			let payment_type	= $("#payment_type").val();
			let payment_method	= $("#payment_method").val();
			let no_rekening     = $('#no_rekening').val();
			const additional_data = {
				minta_bagian: $('input[name=minta_bagian]:checked').val(),
				lihat_via_zoom: $('input[name=lihat_via_zoom]:checked').val(),
				dokumentasi_foto: $('input[name=dokumentasi_foto]:checked').val(),
				dokumentasi_video: $('input[name=dokumentasi_video]:checked').val(),
				bersedia_konversi: $('input[name=bersedia_konversi]:checked').val()
			}

			if(hewan_qurban == null){
				messageAlert('Hewan qurban harus di isi!')
			}else if (nama_donatur == null){
				messageAlert('Nama donatur harus di isi!')
			}else if (email == null) {
				messageAlert('Email harus di isi!')
			}else if (no_handphone == null) {
				messageAlert('No Handphone harus di isi!')
			}else if (nama_pequrban == null) {
				messageAlert('Nama Pequrban harus di isi!')
			}else if (alamat_pequrban == null) {
				messageAlert('Alamat Pequrban harus di isi!')
			}else {

				$.ajax({
					url: '{{ENV("API_URL")}}/api/snap-token-midtrans-qurban',
					type: 'POST',
					dataType: 'Json',
					data: {
						_token	   		: '{{csrf_token()}}',
						key_yayasan     : '{{ENV("YAYASAN_KEY")}}',
						id_produk_qurban: id_produk_qurban,
						hewan_qurban 	: hewan_qurban,
						harga_hewan  	: harga_hewan, 
						sedekah_qurban  : sedekah_qurban,
						nama_donatur    : nama_donatur,
						email 			: email,
						no_handphone    : no_handphone,
						nama_pequrban   : nama_pequrban,
						alamat_pequrban : alamat_pequrban,
						vendor 			: vendor,
						payment_type	: payment_type,
						payment_method  : payment_method,
						no_rekening     : no_rekening,
						device			: 'WEB',
						additional_data : additional_data
					},
				})
				.done(function(res) {
					if(payment_type == 'manual_transfer'){
						$("#notif-pembayaran").removeClass('payment-method-hide');
						$("#notif-pembayaran").addClass('payment-method-active');

						let modal_title = $("#modal_title")
						let modal_body = $("#modal_body")

						let type = $("#payment_type").val()
						let method = $("#payment_method").val()

						let nominal = res.moota.nominal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")

						$("#title_popup").html('Transfer '+res.moota.nama_bank)
						$("#moota-logo").attr("src", $("#selected-method-img").attr("src"))
						$("#no_rekening_show").html(res.moota.no_rekening)

						$('#intruksi-pembayaran').html('<center><br />'+
							'<span><b>Intruksi Pembayaran</b></span><br>'+
							'<span>Nominal yang harus anda bayar :</span><br>'+
							'<span style="font-size: 18px;"><b>Rp. '+nominal+'</b></span><br>'+
							'<span>*Harap selesaikan pembayaran sesuai dengan nominal yang tertera untuk memudahkan pengecekan!</span><br>'+
							'<span>*Sesuaikan nominal sampai 3 digit terakhir! (Kelebihan dari nominal akan didonasikan juga)</span><br>'+
							'<span>*Lakukan pembayaran maksimal 1x24 jam!</span><br>'+
							'<span>*Silahkan akses link berikut untuk melihat perkembangan donasi anda! (Link dikirim juga ke email anda)</span><br>'+
							'<span><a href="{{ url("track/transaction?no_invoice=") }}'+res.moota.no_invoice+'" class="btn">Cek Donasi Saya</a><br>'+
							'<span>*Tekan tombol dibawah juka sudah melakukan pembayaran!</span><br><br />'+
							'<span>'+
							'<a href="javascript:0;" class="post_readmore" onclick="confirm_pay(`'+res.moota.no_invoice+'`,`'+res.moota.vendor+'`,`'+res.moota.nominal+'`,`'+res.moota.moota_bank+'`)">'+
							'<button style="padding: 5px; width: 100%; border-radius: 5px;" >'+
							'<span class="post_readmore_label">Saya Sudah Melakukan Transfer</span>'+
							'</button>'+
							'</a>'+
							'</span>'+
						'</center>');
					}else{
						console.log('masok')
						snap.pay(res.snap_token, {
							onSuccess: function (result) {
								console.log(result);
								$('#snaptoken').val('');
								$.ajax({
									url: '{{ route("donasi.updateStatusQurban") }}',
									type: 'POST',
									dataType: 'json',
									data: {
										"_token": "{{ csrf_token() }}",
										"id": result.order_id,
										"status": result.transaction_status,
										"payment_method": result.payment_method,
										"snap_token": data.snap_token
									},
									success: function(data){
										hideLoading();
										console.log(data)
										window.location.href = "{{Route('notif.wait')}}";
									}
								});
							},
							onPending: function (result) {
								console.log(result);
								$('#snaptoken').val('');
								window.location.href = "{{Route('notif.wait')}}";
							},
							onError: function (result) {
								console.log(result);
								$('#snaptoken').val('');
								window.location.href = "{{Route('notif.wait')}}";
							},
							onClose: function(){
								
							}
						});
					}
				});
			}
		});	

		$('#konfirmasi_pembayaran').click(function(e){
            e.preventDefault(); 
            $.ajax({
            	url:'{{ENV("API_URL")}}/api/confirm-payment',
            	type:"POST",
            	data:new FormData(document.getElementById('data-konfirmasi')),
            	processData:false,
            	contentType:false,
            	cache:false,
            	async:false,
            	success: function(res){
            		messageAlert(res.msg)

            		window.location.href = '{{ url("qurban/purchase") }}';
            	}
            });
        });

        $('#cari').click(function(e){
        	e.preventDefault();
        	$.ajax({
        		url: '{{ENV("API_URL")}}/api/track/transaction/'+$('#no_invoice').val(),
        		type: 'GET',
        		dataType: 'Json',
        	})
        	.done(function(res) {
        		$('#id_yayasan').val(res.qurban.id_yayasan)
        		$('#guest_id').val(res.qurban.guest_id)
        		$('#id_qurban').val(res.qurban.id)
				$('#nama_lengkap').val(res.qurban.nama_donatur)
        		$('#nama_pequrban').val(res.qurban.nama_pequrban)
        		$('#hewan_qurban').val(res.qurban.jenis_hewan)
        		$('#metode_pembayaran').val(res.qurban.metode_pembayaran)
        		$('#total_qurban').val(res.qurban.total_qurban.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
        	});
        	
        });

	});
	
	function messageAlert(response){
		const Toast = Swal.mixin({
			toast: true,
			customClass: 'swal-wide',
			position: 'bottom-end',
			showConfirmButton: false,
			timer: 5000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		Toast.fire({
			title: response
		});
	}

	function change_produk(id) {
		$.ajax({
			url: '{{ENV("API_URL")}}/api/produk-qurban',
			type: 'GET',
			dataType: 'Json',
			data: {
				id: id
			},
		})
		.done(function(res) {
			$('#harga_qurban').val(res.produk.harga_hewan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
			$("#id_produk_qurban").val(res.produk.id)
		});			
	}

	function copyToClip(element_id)
	{
		var temp = $("<input>");
		$("body").append(temp);
		temp.val($("#"+element_id).text()).select();
		document.execCommand("copy");
		temp.remove();
	}

	function choose_method(vendor, type, method, label, img_src, no_rek)
	{
		$("#mthd-close").click()
		$("#cc_form").hide()
		$("#ew_form").hide()

		$("#payment_vendor").val(vendor)
		$("#payment_type").val(type)
		$("#payment_method").val(method)
		$('#no_rekening').val(no_rek)

		$("#selected-method-img").attr("src",img_src)
		$("#selected-method-label").html(label)
		$("#selected-method").show()
		$("#btn-payment-method").hide()


		let payment_type = $("#payment_type").val()
		var payment_method = $("#payment_method").val();

		if (payment_type == "credit_card") {
			// if (vendor == "xendit") $("#cc_form").show()
		} else if (payment_type == "e_wallet") {
			if (payment_method == 'gopay' || payment_method == 'DANA')
				$("#ew_form").hide();
			else
				$("#ew_form").show();

		} else if (payment_type == "retail_outlet") {

		} else if (payment_type == "virtual_account") {

		} else if (payment_type == "manual_transfer") {

		}
	}

	function confirm_pay(id, vendor, nominal, moota_bank_id)
	{
		if(vendor == "moota"){
			$.post('{{ENV("API_URL")}}/api/moota/mutation/by-amount',
			{
				_method	: 'POST',
				bank_id: moota_bank_id,
				amount: nominal,
				type: "Qurban"
			},
			function (data, status) {
				messageAlert(data.message);

				$('#hewan_qurban').val('');
				$('#harga_qurban').val('');
				$('#sedekah_qurban').val('');
				$('#nama_donatur').val('');
				$('#email').val('');
				$('#no_handphone').val('');
				$('#nama_pequrban').val('');
				$('#alamat_pequrban').val('');
				$("#payment_vendor").val('');
				$("#payment_type").val('');
				$("#payment_method").val('');
				$('#no_rekening').val('');

			}
			);
		}else{

			$.ajax({
				url: '{{ENV("API_URL")}}/api/confirm-pay-qurban',
				type: 'GET',
				dataType: 'Json',
				data: {
					no_invoice: id
				}
			})
			.done(function(res) {
				messageAlert('Silahkan Lakukan Konfirmasi Pembayaran !');
				$("#notif-pembayaran").removeClass('payment-method-active');

				window.location.href = "{{ url('qurban/confirm?no_invoice=') }}"+res.confirm.no_invoice
			});

		}
	}
</script>