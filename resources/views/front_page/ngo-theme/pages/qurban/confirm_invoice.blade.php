<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
<head>
	<title>Invoice Pembayaran</title>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no"><link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
	@include('front_page.ngo-theme.main_layouts.css')
</head>
<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">
	<a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
	<a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
	<div class="body_wrap">
		<div class="page_wrap">
			<div class="top_panel_fixed_wrap"></div>
			@include('front_page.ngo-theme.main_layouts.header')
			
			<div class="page_content_wrap page_paddings_no">
				<div class="content_wrap">
					<div class="content">
						<div class="itemscope post_item post_item_single post_featured_default post_format_standard post-2 page type-page status-publish hentry" itemscope itemtype="http://schema.org/Article">
							<div class="post_content" itemprop="articleBody">
								<div class="block">
									<div class="column_container">
										<div class="column-inner">
											<div class="wrapper">
												<div id="sc_donations_108588160" class="sc_donations sc_donations_style_excerpt">
													<h2 class="sc_donations_title sc_item_title" style="font-size: 25px; margin-top: 15px;">Invoice Pembayaran</h2>

													<div id="sc_form_1145226909" class="sc_form sc_form_style_form_2 margin_bottom_huge" style="margin-top: 10px !important;">
														<table>
															<tr>
																<th style="color: black; font-weight: bold; font-size: 0.875em; width: 50%;">No Invoice</th>
																<td style="color: black; font-weight: normal;">{{$confirm->confirm->no_invoice}}</td>
															</tr>
															<tr>
																<th style="color: black; font-weight: bold; width: 50%;">Nama Pequrban</th>
																<td style="color: black;">{{$confirm->confirm->nama_pequrban}}</td>
															</tr>
															<tr>
																<th style="color: black; font-weight: bold; width: 50%;">Hewan Qurban</th>
																<td style="color: black;">{{$confirm->confirm->master_produk_qurban->jenis_hewan}}</td>
															</tr>
															<tr>
																<th style="color: black; font-weight: bold; width: 50%;">Metode Pembayaran</th>
																<td style="color: black;">{{$confirm->confirm->metode_pembayaran}}</td>
															</tr>
															<tr>
																<th style="color: black; font-weight: bold; width: 50%;">Total Qurban</th>
																<td style="color: black;">Rp. {{number_format($confirm->confirm->total_qurban)}}</td>
															</tr>
															<tr>
																<th style="color: black; font-weight: bold; width: 50%;">Status Pembayaran</th>
																<td style="color: black;">{{$confirm->confirm->status}}</td>
															</tr>
														</table>
													</div>

													<h4 class="sc_donations_title sc_item_title" style="font-size: 25px; margin-top: 5px; text-align: left;">Intruksi Pembayaran</h4>
													<p>Silahkan Lakukan Konfirmasi Pembayaran dengan Cara Klik Tombol Konfirmasi di Bawah Ini. Mohon Lengkapi Data Konfirmasi Pembayaran Anda.</p>

													<center style="margin-top: 10px; margin-bottom: 10px;">
														<a href="{{ url('qurban/confirm-pay?no_invoice=') }}{{$confirm->confirm->no_invoice}}" class="post_readmore" style="padding: 10px; border-radius: 5px; margin-top: 10px; width: 98%;">
															<span class="post_readmore_label">Konfirmasi Pembayaran</span>
														</a>
													</center>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			@include('front_page.ngo-theme.components.footer')
		</div>
		<!-- /.page_wrap -->
	</div>
	<!-- /.body_wrap -->
	<a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
	<div class="custom_html_section"></div>
	@include('front_page.ngo-theme.main_layouts.js')
</body>
</html>
