<script>
    let riwayatTabunganCurrentItem = 1
    let riwayatTabunganTotalItem = 0

    let realisasiQurbanCurrentItem = 1
    let realisasiQurbanTotalItem = 0

    const riwayatTabunganBtn = $("button#riwayatTabunganBtn")
    const realisasiQurbanBtn = $("button#realisasiQurbanBtn")


    var status_berhasil = ""
    var status_pending = ""
    var status_gagal = ""

    function getStatusBerhasil() {
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/parameter/status_berhasil",
            success: (response) => {
                status_berhasil = response.data.value
            }
        })
    }

    function getStatusPending() {
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/parameter/status_pending",

            success: (response) => {
                status_pending = response.data.value
            }
        })
    }

    function getStatusGagal() {
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/parameter/status_gagal",
            success: (response) => {
                status_gagal = response.data.value
            }
        })
    }



    function changeMenuTabunganQurban(item){
        const riwayatTabungaContainerRef = $("#riwayat-tabungan-content")
        const realisasiQurbanContainerRef = $("#realisasi-qurban-content")
        
        if(item == 'tabungan'){
            riwayatTabungaContainerRef.removeClass('d-none')
            realisasiQurbanContainerRef.addClass('d-none')
            riwayatTabunganBtn.removeAttr('style')
            realisasiQurbanBtn.attr('style', 'color: #000; background-color: #ddd;')

            initTabunganQurbanItem()
        } else if(item == 'realisasi'){
            riwayatTabungaContainerRef.addClass('d-none')
            realisasiQurbanContainerRef.removeClass('d-none')
            realisasiQurbanBtn.removeAttr('style')
            riwayatTabunganBtn.attr('style', 'color: #000; background-color: #ddd;')

            getRealisasiQurban()
        }

    }

    function riwayatTabunganPrevPage(event) {
        event.preventDefault()
        riwayatTabunganCurrentItem -= 1
        getRiwayatTabungan()
    }

    function riwayatTabunganNextPage(event) {
        event.preventDefault()
        riwayatTabunganCurrentItem += 1
        getRiwayatTabungan()
    }

    function riwayatTabunganSelectPage(event, num) {
        event.preventDefault()
        if (num == riwayatTabunganCurrentItem) {
            return
        }

        riwayatTabunganCurrentItem = num
        getRiwayatTabungan()
    }

    function realisasiQurbanPrevPage(event) {
        event.preventDefault()
        realisasiQurbanCurrentItem -= 1
        getRealisasiQurban()
    }

    function realisasiQurbanNextPage(event) {
        event.preventDefault()
        realisasiQurbanCurrentItem += 1
        getRealisasiQurban()
    }

    function realisasiQurbanSelectPage(event, num) {
        event.preventDefault()
        if (num == realisasiQurbanCurrentItem) {
            return
        }

        realisasiQurbanCurrentItem = num
        getRealisasiQurban()
    }

    function riwayatTabunganPagination() {
        const riwayatTabunganPaginationRef = $('ul#riwayat_tabungan_pagination')
        riwayatTabunganPaginationRef.empty()
        
        if (riwayatTabunganCurrentItem == 1) {
            riwayatTabunganPaginationRef.append(`<li class="page-item disabled"><a class="page-link" href="#" tabindex="-1">Previous</a></li>`)
        } else {
            riwayatTabunganPaginationRef.append(`<li class="page-item"><a class="page-link" href="#" onclick="riwayatTabunganPrevPage(event)" tabindex="-1">Previous</a></li>`)
        }

        for (let index = 0; index < riwayatTabunganTotalItem; index++) {
            riwayatTabunganPaginationRef.append(`<li class="page-item"><a class="page-link ${index + 1 == riwayatTabunganCurrentItem ? 'bg-warning text-white' : 'text-dark'}" href="#" onclick="riwayatTabunganSelectPage(event, ${index + 1})">${index + 1}</a></li>`)
        }

        if (riwayatTabunganCurrentItem == riwayatTabunganTotalItem) {
            riwayatTabunganPaginationRef.append(`<li class="page-item disabled"><a class="page-link" href="#">Next</a></li>`)
        } else {
            riwayatTabunganPaginationRef.append(`<li class="page-item"><a class="page-link" href="#" onclick="riwayatTabunganNextPage(event)">Next</a></li>`)
        }
    }

    function realisasiQurbanPagination() {
        const realisasiQurbanPaginationRef = $('ul#realisasi_qurban_pagination')
        realisasiQurbanPaginationRef.empty()
        
        if (realisasiQurbanCurrentItem == 1) {
            realisasiQurbanPaginationRef.append(`<li class="page-item disabled"><a class="page-link" href="#" tabindex="-1">Previous</a></li>`)
        } else {
            realisasiQurbanPaginationRef.append(`<li class="page-item"><a class="page-link" href="#" onclick="realisasiQurbanPrevPage(event)" tabindex="-1">Previous</a></li>`)
        }

        for (let index = 0; index < riwayatTabunganTotalItem; index++) {
            realisasiQurbanPaginationRef.append(`<li class="page-item"><a class="page-link ${index + 1 == realisasiQurbanCurrentItem ? 'bg-warning text-white' : 'text-dark'}" href="#" onclick="riwayatTabunganSelectPage(event, ${index + 1})">${index + 1}</a></li>`)
        }

        if (realisasiQurbanCurrentItem == riwayatTabunganTotalItem) {
            realisasiQurbanPaginationRef.append(`<li class="page-item disabled"><a class="page-link" href="#">Next</a></li>`)
        } else {
            realisasiQurbanPaginationRef.append(`<li class="page-item"><a class="page-link" href="#" onclick="realisasiQurbanNextPage(event)">Next</a></li>`)
        }
    }

    function getRiwayatTabungan() {
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/tabungan-qurban/history",
            headers: {
                "Authorization": "{{ Session::get('token') }}"
            },
            data: {
                page: riwayatTabunganCurrentItem,
                perpage: 10
            },
            success: (response) => {
                riwayatTabunganTotalItem = response.last_page
                $("tbody#riwayat-tabungan-items").empty()
                $.each(response.data, (i, val) => {
                    let statusHtml = ''
                    if (val.status == 'success') {
                        statusHtml = '<span class="badge text-light bg-success">Success</span>'
                    } else if (val.status == 'pending') {
                        statusHtml = '<span class="badge text-light bg-warning">Pending</span>'
                    } else {
                        statusHtml = `<span class="badge text-light bg-danger">${val.status}</span>`
                    }

                    $("tbody#riwayat-tabungan-items").append(`
                        <tr class="text-dark">
                            <td>${tanggal_indonesia(val.created_at)}</td>
                            <td>Rp. ${formatRupiah(val.nominal_tabungan.toString())}</td>
                            <td>${statusHtml}</td>
                        </tr>
                        `)
                })
                riwayatTabunganPagination()
            }
        })
    }

    function getRealisasiQurban(){
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/tabungan-qurban/history-realisasi",
            headers: {
                "Authorization": "{{ Session::get('token') }}"
            },
            data: {
                page: realisasiQurbanCurrentItem,
                perpage: 10
            },
            success: (response) => {
                realisasiQurbanTotalItem = response.last_page
                $("tbody#realisasi-qurban-items").empty()
                $.each(response.data, (i, val) => {
                    let statusHtml = ''
                    if (val.qurban.status == 'success') {
                        statusHtml = '<span class="badge text-light bg-success">Success</span>'
                    } else if (val.qurban.status == 'pending') {
                        statusHtml = '<span class="badge text-light bg-warning">Pending</span>'
                    } else {
                        statusHtml = `<span class="badge text-light bg-danger">${val.qurban.status}</span>`
                    }
                    $("tbody#realisasi-qurban-items").append(`
                        <tr class="text-dark">
                            <td>${tanggal_indonesia(val.created_at)}</td>
                            <td>${val.produk_qurban.jenis_hewan}</td>
                            <td>Rp. ${formatRupiah((val.total_qurban).toString())}</td>
                            <td>${statusHtml}</td>
                        </tr>
                        `)
                })
                realisasiQurbanPagination()
            }
        })
    }

    async function initTabunganQurbanItem() {
        let idProdukQurban = 0
        
        await $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/tabungan-qurban",
            headers: {
                "Authorization": "{{ Session::get('token') }}"
            },
            success: (response) => {
                const responseData = response.data
                idProdukQurban = responseData.id_produk_qurban
                $("input#target-tabungan").val(formatRupiah(responseData.nominal_target.toString(), ""))
                $("input#nominal-tabungan-qurban").val('Rp. ' + formatRupiah(responseData.nominal_tabungan.toString(), ""))
            }
        })

        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/produk-qurban",
            headers: {
                "Authorization": "{{ Session::get('token') }}"
            },
            data: {
                yayasan: "{{ENV('YAYASAN_KEY')}}"
            },
            success: (response) => {
                $("select#produk-qurban-list").empty()
                $("select#produk-qurban-list").append(`
                    <option value="0">-- Belum dipilih --</option>
                    `)
                $.each(response.produk, (i, val) => {
                    $("select#produk-qurban-list").append(`
                        <option value="${val.id}" ${idProdukQurban == val.id ? 'selected' : ''}>${val.jenis_hewan}</option>
                        `)
                })
            }
        })

        if(idProdukQurban != 0){
            $("a#realisasi-qurban-link").attr('href', "{{ route('tabungan-qurban.realisasi') }}?produk=" + idProdukQurban + '&produk=tabungan')
        }
        getRiwayatTabungan()
    }

    function changeQurbanTarget(id){
        $.ajax({
			url: '{{ENV("API_URL")}}/api/produk-qurban',
			type: 'GET',
			dataType: 'Json',
			data: {
				id: id
			},
		})
		.done(function(res) {
			$("input#target-tabungan").val(res.produk.harga_hewan.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
		});
    }

    function updateTabunganQurban(event){
        event.preventDefault()
        const idProdukQurban = $("select#produk-qurban-list").val()
        const targetTabungan = $("input#target-tabungan").val().replace(/\./g, '')

        $.ajax({
            method: "POST",
            url: "{{ENV('API_URL')}}/api/tabungan-qurban/update",
            headers: {
                "Authorization": "{{ Session::get('token') }}"
            },      
            data: {
                id_produk: idProdukQurban,
                nominal_target: targetTabungan
            },
            success: function(res){
                messageAlert('Berhasil menyimpan perubahan')
            } 
        })
    }

    function messageAlert(response) {
		const Toast = Swal.mixin({
			toast: true,
			customClass: 'swal-wide',
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		Toast.fire({
			title: response
		});
	}

    $(document).ready(function() {
        const urlParams = new URLSearchParams(window.location.search);
        let selectedMenu = urlParams.get('menu')

        if (selectedMenu == 'tabungan-qurban') {
            initTabunganQurbanItem()
        }

        $("#menu-tabungan-qurban-item").on('click', function(event) {
            event.preventDefault()
            initTabunganQurbanItem()
        })

        $("input#target-tabungan").on('keyup', function(e) {
            e.target.value = formatRupiah(this.value, '');
        })

        $("select#produk-qurban-list").on('change', function(){
            changeQurbanTarget(this.value)
        })

        riwayatTabunganBtn.on('click', function(){
            changeMenuTabunganQurban('tabungan')
        })

        realisasiQurbanBtn.on('click', function(){
            changeMenuTabunganQurban('realisasi')
        })
    })
</script>