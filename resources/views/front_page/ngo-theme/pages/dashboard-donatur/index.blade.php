<?php


?>
<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

<head>
    <title>Zakat Agro Bina Alam Mandiri</title>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel='shortcut icon' href='favicon.ico' type='image/x-icon' />
    <link rel="stylesheet" href="{{ asset('themes/pagination/dist/pagination.php')}}" type="text/css">

    @include('front_page.ngo-theme.main_layouts.css')
</head>
<style>
    .button-zakat {
        width: 100%;
        margin-top: 10px;
        padding: 5px;
        text-align: center;
        font-size: 14px;
        background: white;
        border: 1px solid #ddd;
        border-radius: 5px;
        display: inline-block;
        cursor: pointer;
        font-family: Arial;
        font-weight: bold;
        text-decoration: none;
        color: #fff !important;
    }

    .button-zakat:hover {
        color: white !important;
        background-color: #fbce1b;
    }

    input[type="date"] {
        color: #8a8a8a;
        border-color: #f2f2f2;
        background-color: #f2f2f2;
    }

    @media screen and (max-width: 767px) {
        body {
            overflow-x: hidden;
        }

        .row {
            margin-right: -14px !important;
        }

        .profile-info {
            display: flex;
            align-items: center;
            justify-content: left;
        }

        #profile-image {
            max-width: 80px !important;
        }

        .profile-details {
            margin-left: 10px;
            text-align: left;
        }

        .camera-icon {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            max-width: 40px;
            border-radius: 50%;
            background-color: #fff;
            padding: 5px;
        }

    }
</style>

<body class="home page page-template-default body_style_wide top_style_header_1 body_filled article_style_stretch layout_single-standard template_single-standard scheme_original top_panel_show top_panel_above sidebar_hide sidebar_outer_hide">


    <a id="toc_home" class="sc_anchor" title="Home" data-description="&lt;i&gt;Return to Home&lt;/i&gt; - &lt;br&gt;navigate to home page of the site" data-icon="icon-home" data-url="http://charity-is-hope.themerex.net/" data-separator="yes"></a>
    <a id="toc_top" class="sc_anchor" title="To Top" data-description="&lt;i&gt;Back to top&lt;/i&gt; - &lt;br&gt;scroll to top of the page" data-icon="icon-double-up" data-url="" data-separator="yes"></a>
    <div class="body_wrap">
        <div class="page_wrap">
            <div class="top_panel_fixed_wrap"></div>
            @include('front_page.ngo-theme.main_layouts.header')
			@include('front_page.ngo-theme.components.messages')
            <div class="container">
                <div class="row bg-white">
                    <div class="col-lg-3 col-md-4 col-sm-12">
                        <div class="row">
                            <div class="col">
                                <div class="card shadow-sm bg-white mt-5">
                                    <div class="card-body">
                                        <div class="profile text-center">
                                            <div class="profile-info">
                                                <img src="{{ asset('/image/icon-people.png')}}" alt="" style="max-width: 120px" id="profile-image">
                                                <div class="profile-details">
                                                    <h5 id="profile-name" class="mt-3">{{$user_profile->name}}</h5>
                                                    <h5 id="profile-phone" class="mb-3" style="font-size: 15px;">{{$user_profile->no_telp}}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <ul class="list-group shadow-sm mt-5">
                                    <a href="#" id="menu-profile-item" class="list-group-item list-group-item-action" onclick="" style="color: #000;">Profile</a>
                                    <a href="#" id="menu-tabungan-qurban-item" class="list-group-item list-group-item-action" style="color: #000;">Tabungan Qurban</a>
                                    <a href="#" id="menu-histori-donasi-item" class="list-group-item list-group-item-action" onclick="" style="color: #000;">Histori Donasi</a>
                                    <a href="#" id="menu-histori-zakat-item" class="list-group-item list-group-item-action" onclick="" style="color: #000;">Histori Zakat</a>
                                    <a href="#" id="menu-histori-qurban-item" class="list-group-item list-group-item-action" onclick="" style="color: #000;">Histori Qurban</a>
                                    <a href="{{Route('bayar_zakat.index')}}" id="me nu-bayar-zakat-item" class="list-group-item list-group-item-action" style="color: #000;">Tunaikan Zakat</a>
                                    <a href="#" id="menu-change-password-item" class="list-group-item list-group-item-action" style="color: #000;">Ganti Password</a>
                                    <a href="{{Route('dashboard.logout')}}" id="menu-change-password-item" class="list-group-item list-group-item-action bg-danger text-white" >Logout</a>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-12">


                        <!-- START PROFILE PAGE -->
                        <div id="profile-page" class="">
                            <div class="col-sm-12 col-lg-12 mt-5" style="padding: 0;">
                                <div class="card">
                                    <div class="card-body bg-white">
                                        <form action="{{Route('dashboard.update_profile')}}" method="post">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col">
                                                    <h5 class="mt-2 text-center">Informasi Akun</h5>
                                                    <div class="mb-3 mt-5" id="">
                                                        <label class="form-label">Nama Lengkap</label>

                                                        <input type="text" name="nama_lengkap_field" value="{{$user_profile->name}}" required class="form-control border" placeholder="Nama Lengkap">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Email</label>
                                                        <input type="email" name="email_field" class="form-control" readonly value="{{$user_profile->email}}" placeholder="Email">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Username</label>

                                                        <input type="text" name="username_field" readonly value="{{$user_profile->username}}" class="form-control" placeholder="Username">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Nomor Handphone</label>
                                                        <input type="text" name="phone_field" class="form-control" value="{{$user_profile->no_telp}}" required placeholder="6289XXXXXXXX">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Bio</label>

                                                        <input type="text" name="bio_field" required value="{{$user_profile->bio}}" class="form-control" placeholder="Bio">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Alamat</label>
                                                        <input type="text" name="alamat_field" value="{{$user_profile->alamat}}" required class="form-control" placeholder="Alamat">
                                                    </div>

                                                    <button type="submit" class="button-zakat btn btn-block">Ganti Profile</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- END PROFILE PAGE -->

                        <!-- START TABUNGAN QURBAN PAGE -->
                        <div id="tabungan-qurban-page">
                            <div class="col-sm-12 col-lg-12 mt-5" style="padding: 0;">
                                <div class="card">
                                    <div class="card-body bg-white">
                                        <h5 class="mt-2 text-center">Tabungan Qurban</h5>
                                        <div class="row mx-2 pb-4 mb-4 align-item-center" style="border-bottom: 1px solid #dfdfdf;">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col">
                                                        <label>Produk Qurban</label>
                                                        <select name="produk_qurban" id="produk-qurban-list" class="form-select w-100 mb-1"></select>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <label>Target Tabungan</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Rp.</span>
                                                            </div>
                                                            <input type="text" id="target-tabungan" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row mb-3">
                                                                    <div class="col text-center">
                                                                        <h6>Tabungan Saat Ini</h6>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <input type="text" id="nominal-tabungan-qurban" class="form-control" disabled>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 text-right">
                                                <button type="button" class="btn" onclick="updateTabunganQurban(event)">Simpan Perubahan</button>
                                            </div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-md-6 text-center">
                                                <button type="button" id="riwayatTabunganBtn" class="btn">Riwayat Tabungan</button>
                                            </div>
                                            <div class="col-md-6 text-center">
                                                <button type="button" id="realisasiQurbanBtn" class="btn" style="color: #000; background-color: #ddd;">Realisasi Qurban</button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <!-- Riwayat tabungan -->
                                                <div class="row" id="riwayat-tabungan-content">
                                                    <div class="col-12 text-center">
                                                        <h5>Riwayat Tabungan</h5>
                                                    </div>
                                                    <div class="col-12 text-right">
                                                        <a href="{{ route('tabungan-qurban.purchase') }}" id="tambah-tabungan-link" class="btn btn-warning text-light mb-2">+ Tambah Tabungan</a>
                                                    </div>
                                                    <div class="col-12">
                                                        <table class="table table-bordered w-100">
                                                            <thead>
                                                                <tr class="text-dark">
                                                                    <th>Tanggal</th>
                                                                    <th>Nominal</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="riwayat-tabungan-items">
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="col text-center">
                                                            <ul class="pagination justify-content-center" id="riwayat_tabungan_pagination">

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Realisasi qurban -->
                                                <div class="row d-none" id="realisasi-qurban-content">
                                                    <div class="col-12 text-center">
                                                        <h5>Realisasi Qurban</h5>
                                                    </div>
                                                    <div class="col-12 text-right">
                                                        <a href="{{ Route('tabungan-qurban.realisasi') }}?produk=tabungan" id="realisasi-qurban-link" class="btn btn-warning text-light mb-2">+ Realisasi</a>
                                                    </div>
                                                    <div class="col-12">
                                                        <table class="table table-bordered w-100">
                                                            <thead>
                                                                <tr class="text-dark">
                                                                    <th>Tanggal</th>
                                                                    <th>Hewan Qurban</th>
                                                                    <th>Nominal</th>
                                                                    <th>Status</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="realisasi-qurban-items">
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="col text-center">
                                                            <ul class="pagination justify-content-center" id="realisasi_qurban_pagination">

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END TABUNGAN QURBAN PAGE -->


                        <!-- START CHANGE PASSWORD PAGE -->

                        <div id="change-password-page">
                            <div class="col-sm-12 col-lg-12 mt-5" style="padding: 0;">
                                <div class="card">
                                    <div class="card-body bg-white">
                                        <form action="{{Route('dashboard.update_password')}}" method="post">
                                            {{ csrf_field() }}
                                            <div class="row">
                                                <div class="col">
                                                    <h5 class="mt-2 text-center">Ganti Password</h5>
                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Password Lama</label>

                                                        <input type="password" name="old_password_field" required class="form-control" placeholder="Password Lama">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Password Baru</label>
                                                        <input type="password" name="new_password_field" required class="form-control" placeholder="Password Baru">
                                                    </div>

                                                    <div class="mb-3" id="">
                                                        <label class="form-label">Ketik Ulang Password Baru</label>

                                                        <input type="password" name="confirm_password_field" required class="form-control" placeholder="Ketik Ulang Password Baru">
                                                    </div>
                                                    <button type="submit" class="button-zakat btn btn-block">Ganti Password</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- END CHANGE PASSWORD PAGE -->

                        <!-- START HISTORI DONASI PAGE -->

                        <div id="histori-donasi-page">
                            <div class="col-sm-12 col-lg-12 mt-5" style="padding: 0;">
                                <div class="row">
                                    <div class="col">

                                        <div class="card">
                                            <div class="card-body bg-white">
                                                <h5 class="mt-2 text-center">Histori Donasi Anda</h5>
                                                <div class="row" id="histori-donasi-items"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col text-center">
                                        <ul class="pagination justify-content-center" id="donasi_pagination">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- END HISTORY DONASI PAGE  -->

                        <!-- START HISTORY ZAKAT PAGE  -->

                        <div id="histori-zakat-page">
                            <div class="col-sm-12 col-lg-12 mt-5" style="padding: 0;">
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-body bg-white">
                                                <h5 class="mt-2 text-center">Histori Zakat Anda</h5>
                                                <div class="row" id="histori-zakat-items"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col text-center">
                                        <ul class="pagination justify-content-center" id="zakat_pagination">

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- END HISTORY ZAKAT PAGE -->

                        <!-- START HISTORY QURBAN PAGE -->
                        <div id="histori-qurban-page">
                            <div class="col-sm-12 col-lg-12 mt-5" style="padding: 0;">
                                <div class="row">
                                    <div class="col">
                                        <div class="card">
                                            <div class="card-body bg-white">
                                                <h5 class="mt-2 text-center">Histori Qurban Anda</h5>
                                                <div class="row" id="histori-qurban-items">


                                                    <!-- END ITEM COMPONENT  -->

                                                </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col text-center">
                                                <ul class="pagination justify-content-center" id="qurban_pagination">

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- END HISTORY QURBAN PAGE -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    @include('front_page.ngo-theme.components.footer')
    </div>
    <!-- /.page_wrap -->
    </div>
    <!-- /.body_wrap -->
    <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
    <div class="custom_html_section"></div>
    @include('front_page.ngo-theme.main_layouts.js')
    @include('front_page.ngo-theme.pages.dashboard-donatur.helper')
    @include('front_page.ngo-theme.pages.dashboard-donatur.zakat_pagination')
    @include('front_page.ngo-theme.pages.dashboard-donatur.qurban_pagination')
    @include('front_page.ngo-theme.pages.dashboard-donatur.donasi_pagination')
    @include('front_page.ngo-theme.pages.dashboard-donatur.tabungan_qurban_data')

    @include('front_page.ngo-theme.pages.dashboard-donatur.js')
</body>


<!-- END OF FILE -->

</html>