<script>
    let zakat_current_page = 1
    let zakat_total_page = 0
    const paginationRef = $('ul#zakat_pagination')
    const itemContainerRef = $('div#histori-zakat-items')

    var status_berhasil = ""
    var status_pending = ""
    var status_gagal = ""

    function getStatusBerhasil() {
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/parameter/status_berhasil",
            success: (response) => {
                status_berhasil = response.data.value
                console.log(status_berhasil)
            }
        })
    }

    function getStatusPending() {
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/parameter/status_pending",

            success: (response) => {
                status_pending = response.data.value
                console.log(status_pending)
            }
        })
    }

    function getStatusGagal() {
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/parameter/status_gagal",
            success: (response) => {
                status_gagal = response.data.value
                console.log(status_gagal)
            }
        })
    }

    function zakatPrevPage() {
        zakat_current_page -= 1
        initZakatItem()
    }

    function zakatNextPage() {
        zakat_current_page += 1
        initZakatItem()
    }

    function zakatSelectPage(page) {
        zakat_current_page = page
        initZakatItem()
    }

    function zakatSetPagination(){
        paginationRef.empty()

        if(zakat_current_page == 1){
            paginationRef.append(`<li class="page-item disabled"><a class="page-link" href="#" onclick="zakatPrevPage()" tabindex="-1">Previous</a></li>`)
        } else{
            paginationRef.append(`<li class="page-item"><a class="page-link" href="#" onclick="zakatPrevPage()" tabindex="-1">Previous</a></li>`)
        }

        for (let index = 0; index < zakat_total_page; index++) {
            paginationRef.append(`<li class="page-item"><a class="page-link ${index + 1 == zakat_current_page ? 'bg-warning text-white' : 'text-dark'}" href="#" onclick="zakatSelectPage(${index + 1})">${index + 1}</a></li>`)
        }

        if(zakat_current_page == zakat_total_page){
            paginationRef.append(`<li class="page-item disabled"><a class="page-link" href="#" onclick="zakatNextPage()">Next</a></li>`)
        } else{
            paginationRef.append(`<li class="page-item"><a class="page-link" href="#" onclick="zakatNextPage()">Next</a></li>`)
        }
    }

    function zakatSetItems(items) {
        itemContainerRef.empty()
        $.each(items, (index, item) => {
            var statushtml = ""
            var statusArrSuccess = status_berhasil.split(',');
            var statusArrPending = status_pending.split(',');
            var statusArrGagal = status_gagal.split(',');
            if (statusArrSuccess.includes(item.status)) {
                statushtml = `
                <div name="status-qurban-item" id="status-qurban-item" class="bg-success rounded p-2">               
                    <p name="status-qurban-text" id="status-qurban-text" class="m-0 text-white">Berhasil</p>
                </div>
                `
            } else if (statusArrPending.includes(item.status)) {
                statushtml = `
                <div name="status-qurban-item" id="status-qurban-item" class="bg-warning rounded p-2">               
                    <p name="status-qurban-text" id="status-qurban-text" class="m-0 text-white">Pending</p>
                </div>
                `
            } else if (statusArrGagal.includes(item.status)) {
                statushtml = `
                <div name="status-qurban-item" id="status-qurban-item" class="bg-danger rounded p-2">               
                    <p name="status-qurban-text" id="status-qurban-text" class="m-0 text-white">Gagal</p>
                </div>
                `
            } else {
                statushtml = `
                <div name="status-qurban-item" id="status-qurban-item" class="bg-warning rounded p-2">               
                    <p name="status-qurban-text" id="status-qurban-text" class="m-0 text-white">Pending</p>
                </div>
                `
            }
            itemContainerRef.append(`
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card shadow-sm mt-3">
                    <div class="card-body">
                        <h3 style="font-size: x-large;" name="nominal-zakat-text" id="nominal-zakat-text" class="card-title text-center">Rp. ${formatRupiah(item.total_zakat.toString(), 'Rp')}</h3>
                        <p name="deskripsi-zakat-text" id="deskripsi-zakat-text" class="card-text">${item.detail.category.category}</p>
                        <div class="d-flex justify-content-between align-items-center mt-3">
                            ${statushtml}
                            <div name="tanggal-zakat-item" id="tanggal-zakat-item" class="ml-3">
                                <p name="tanggal-zakat-text" id="tanggal-zakat-text" class="m-0">${tanggal_indonesia(item.created_at)}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `)
        })
    }

    function initZakatItem() {
        getStatusBerhasil()
        getStatusPending()
        getStatusGagal()
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/zakat/history",
            headers: {
                "Authorization": "{{ Session::get('token') }}"
            },
            data: {
                page: zakat_current_page,
                perpage: 9,
            },
            success: (response) => {
                zakat_total_page = response.last_page
                console.log(response)   

                zakatSetPagination()
                zakatSetItems(response.data)
            }
        })
    }
</script>