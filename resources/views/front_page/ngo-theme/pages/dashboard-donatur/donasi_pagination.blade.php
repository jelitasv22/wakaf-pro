<script>
    let donasi_current_page = 1
    let donasi_total_page = 0
    const donasiPaginationRef = $('ul#donasi_pagination')
    const donasiItemContainerRef = $('div#histori-donasi-items')

    var status_berhasil = ""
    var status_pending = ""
    var status_gagal = ""

    function getStatusBerhasil() {
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/parameter/status_berhasil",
            success: (response) => {
                status_berhasil = response.data.value
                console.log(status_berhasil)
            }
        })
    }

    function getStatusPending() {
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/parameter/status_pending",

            success: (response) => {
                status_pending = response.data.value
                console.log(status_pending)
            }
        })
    }

    function getStatusGagal() {
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/parameter/status_gagal",
            success: (response) => {
                status_gagal = response.data.value
                console.log(status_gagal)
            }
        })
    }

    function donasiPrevPage() {
        donasi_current_page -= 1
        initDonasiItem()
    }

    function donasiNextPage() {
        donasi_current_page += 1
        initDonasiItem()
    }

    function donasiSelectPage(page) {
        donasi_current_page = page
        initDonasiItem()
    }

    function donasiSetPagination(){
        donasiPaginationRef.empty()

        if(donasi_current_page == 1){
            donasiPaginationRef.append(`<li class="page-item disabled"><a class="page-link" href="#" onclick="donasiPrevPage()" tabindex="-1">Previous</a></li>`)
        } else{
            donasiPaginationRef.append(`<li class="page-item"><a class="page-link" href="#" onclick="donasiPrevPage()" tabindex="-1">Previous</a></li>`)
        }

        for (let index = 0; index < donasi_total_page; index++) {
            donasiPaginationRef.append(`<li class="page-item"><a class="page-link ${index + 1 == donasi_current_page ? 'bg-warning text-white' : 'text-dark'}" href="#" onclick="donasiSelectPage(${index + 1})">${index + 1}</a></li>`)
        }

        if(donasi_current_page == donasi_total_page){
            donasiPaginationRef.append(`<li class="page-item disabled"><a class="page-link" href="#" onclick="donasiNextPage()">Next</a></li>`)
        } else{
            donasiPaginationRef.append(`<li class="page-item"><a class="page-link" href="#" onclick="donasiNextPage()">Next</a></li>`)
        }
    }

    function donasiSetItems(items) {
        donasiItemContainerRef.empty()
        var statusArrSuccess = status_berhasil.split(',');
            var statusArrPending = status_pending.split(',');
            var statusArrGagal = status_gagal.split(',');
        $.each(items, (index, item) => {
            var statushtml = ""

            if (statusArrSuccess.includes(item.status)) {
                statushtml = `
                <div name="status-qurban-item" id="status-qurban-item" class="bg-success rounded p-2">               
                    <p name="status-qurban-text" id="status-qurban-text" class="m-0 text-white">Berhasil</p>
                </div>
                `
            } else if (statusArrPending.includes(item.status)) {
                statushtml = `
                <div name="status-qurban-item" id="status-qurban-item" class="bg-warning rounded p-2">               
                    <p name="status-qurban-text" id="status-qurban-text" class="m-0 text-white">Pending</p>
                </div>
                `
            } else if (statusArrGagal.includes(item.status)) {
                statushtml = `
                <div name="status-qurban-item" id="status-qurban-item" class="bg-danger rounded p-2">               
                    <p name="status-qurban-text" id="status-qurban-text" class="m-0 text-white">Gagal</p>
                </div>
                `
            } else {
                statushtml = `
                <div name="status-qurban-item" id="status-qurban-item" class="bg-warning rounded p-2">               
                    <p name="status-qurban-text" id="status-qurban-text" class="m-0 text-white">Pending</p>
                </div>
                `
            }
            donasiItemContainerRef.append(`
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="card shadow-sm mt-3">
                    <div class="card-body">
                        <h3 style="font-size: x-large;" name="nominal-qurban-text" id="nominal-qurban-text" class="card-title text-center">Rp. ${formatRupiah(item.total_donasi.toString(), 'Rp')}</h3>
                        <p name="deskripsi-qurban-text" id="deskripsi-qurban-text" class="card-text">${item.detail.program.program_child.judul}</p>
                        <div class="d-flex justify-content-between align-items-center mt-3">
                            ${statushtml}
                            <div name="tanggal-qurban-item" id="tanggal-qurban-item" class="ml-3">
                                <p name="tanggal-qurban-text" id="tanggal-qurban-text" class="m-0">${tanggal_indonesia(item.created_at)}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `)
        })
    }

    function initDonasiItem() {
        getStatusBerhasil()
        getStatusPending()
        getStatusGagal()
        $.ajax({
            method: "GET",
            url: "{{ENV('API_URL')}}/api/program/history",
            headers: {
                "Authorization": "{{ Session::get('token') }}"
            },
            data: {
                page: donasi_current_page,
                perpage: 9,
            },
            success: (response) => {
                donasi_total_page = response.last_page
                console.log(response)   

                donasiSetPagination()
                donasiSetItems(response.data)
            }
        })
    }
</script>