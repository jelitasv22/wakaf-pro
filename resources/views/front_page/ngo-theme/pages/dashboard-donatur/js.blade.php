<script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}" data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		function hideAllMenu(){
			$("#change-password-page").hide()
			$("#histori-donasi-page").hide()
			$("#histori-zakat-page").hide()
			$("#histori-qurban-page").hide()
			$("#profile-page").hide()
			$("#tabungan-qurban-page").hide()

			$("#menu-profile-item").removeClass("bg-warning text-white")
			$("#menu-histori-donasi-item").removeClass("bg-warning text-white")
			$("#menu-tabungan-qurban-item").removeClass("bg-warning text-white")
			$("#menu-histori-zakat-item").removeClass("bg-warning text-white")
			$("#menu-histori-qurban-item").removeClass("bg-warning text-white")
			$("#menu-change-password-item").removeClass("bg-warning text-white")
		}

		function setActiveMenu(name, extra=null){
			hideAllMenu()
			$(`#${name}-page`).show()
			$(`#menu-${name}-item`).addClass("bg-warning text-white")
			const cleanUrl = window.location.href.split('?')[0]
			let modifiedUrl = cleanUrl + `?menu=${name}`
			if(extra){
				modifiedUrl += extra
			}

			window.history.pushState({}, "", modifiedUrl)
		}

		const urlParams = new URLSearchParams(window.location.search);
		let selectedMenu = urlParams.get('menu')
		if(!selectedMenu){
			selectedMenu = 'profile'
		}

		hideAllMenu()

		const availableMenu = ['profile', 'change-password', 'tabungan-qurban', 'histori-donasi', 'histori-zakat', 'histori-qurban']
		switch(selectedMenu){
			case 'histori-zakat':
				setActiveMenu('histori-zakat')
				initZakatItem()
				break
			case 'histori-qurban': 
				setActiveMenu('histori-qurban')
				initQurbanItem()
				break
			case 'histori-donasi':
				setActiveMenu('histori-donasi')
				initDonasiItem()
				break
			case 'tabungan-qurban':
				setActiveMenu('tabungan-qurban', '&produk=tabungan')
				break
			default: 
				for(let i = 0; i < availableMenu.length; i++){
					if(selectedMenu == availableMenu[i]){
						setActiveMenu(availableMenu[i])
						break
					}
				}
				break
		}

		// Format rupiah
		var rupiah = document.getElementById('nominal');
		if (rupiah){
			rupiah.addEventListener('keyup', function(e) {
				rupiah.value = formatRupiah(this.value, '');
			});
		}

		$("#menu-profile-item").on('click', function(event) {
			event.preventDefault()
			hideAllMenu()
			setActiveMenu('profile')
		});

		$("#menu-histori-donasi-item").on('click', function(event) {
			event.preventDefault()
			setActiveMenu('histori-donasi')
			initDonasiItem()
		});

		$("#menu-histori-zakat-item").on('click', function(event) {
			event.preventDefault()
			setActiveMenu('histori-zakat')
			initZakatItem()
		});

		$("#menu-histori-qurban-item").on('click', function(event) {
			event.preventDefault()
			setActiveMenu('histori-qurban')
			initQurbanItem()
		});

		$("#menu-tabungan-qurban-item").on('click', function(event){
			event.preventDefault()
			setActiveMenu('tabungan-qurban', '&produk=tabungan')
		})

		$("#menu-change-password-item").on('click', function(event) {
			event.preventDefault()
			setActiveMenu('change-password')
		});
	});

	//--------------------------- GET STATUS --------------------------------



	function copyToClip(element_id) {
		var temp = $("<input>");
		$("body").append(temp);
		temp.val($("#" + element_id).text()).select();
		document.execCommand("copy");
		temp.remove();
	}

	function messageAlert(response) {
		const Toast = Swal.mixin({
			toast: true,
			customClass: 'swal-wide',
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		Toast.fire({
			title: response
		});
	}

	function messageAlertError(response) {
		const Toast = Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: response
		})
	}




	@if(Session::get('message'))
	messageAlert("{{ Session::get('message') }}") 
	{{Session::forget('message')}}
	@endif


	@if(Session::get('error'))
	messageAlertError("{{ Session::get('error') }}")
	{{Session::forget('error')}}
	@endif
</script>