<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<title>Konfirmasi Transaksi</title>
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/icons.min.css')}}">
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/bootstrap-select.min.css')}}">
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/animate.min.css')}}">
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/fancybox.min.css')}}">
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/perfect-scrollbar.css')}}">
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/style.css')}}">
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/responsive.css')}}">
	<link rel="stylesheet" media="screen" href="{{asset('themes/ngo-theme-2/assets/css/color.css')}}">

	<!-- REVOLUTION STYLE SHEETS -->
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/revolution/settings.css')}}">
	<!-- REVOLUTION LAYERS STYLES -->
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/revolution/layers.css')}}">
	<!-- REVOLUTION NAVIGATION STYLES -->
	<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/revolution/navigation.css')}}">

	<link href="{{asset('admin/assets/vendors/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{ asset('themes/pagination/dist/pagination.css')}}" type="text/css">
	<link rel="stylesheet" type="text/css" href="{{ asset('themes/payment/payment.css') }}">
	<link rel="icon" href="{{ENV('BACKEND_URL')}}/admin/assets/media/logo_yayasan/{{$yayasan->data->logo_primary}}" sizes="32x32" type="image/png">
	<style type="text/css">
		.pagination > li > a:hover,
		.pagination > li > span:hover,
		.pagination > li > a:focus,
		.pagination > li > span:focus {
			color:#9a292d;
		}

		.pagination > .active > a,
		.pagination > .active > span,
		.pagination > .active > a:hover,
		.pagination > .active > span:hover,
		.pagination > .active > a:focus,
		.pagination > .active > span:focus {
			background-color:#9a292d;
			border-color:#9a292d;
		}

		.swal2-popup.swal2-toast.swal2-show {
			background-color: #9a292d;
		}

		.swal2-popup.swal2-toast .swal2-title {
			color: white;
		}
</style>

</head>
<body itemscope>
	<main>
		<section>
			<div class="block">
				<div class="container">
					<div class="cus-wrp remove-ext5">
						<div class="cnt-inr" style="background-image: url('{{ asset('themes/ngo-theme-2/assets/images/pattern-bg2.png') }}');">
							<center>
								<img src="{{ENV('BACKEND_URL')}}/admin/assets/media/logo_yayasan/{{$yayasan->data->logo_primary}}" style="margin-bottom: 20px; width: 200px;">
							</center>
							<div class="cnt-frm">
								<span>Masukkan no invoice yang anda dapat pada kolom di bawah.</span>
								<div class="sc_form_item sc_form_field label_over" style="display: flex;"> 
									<div style="width: 70%;">
										<span>No Invoice</span>
										<input id="no_invoice" type="text" name="no_invoice" placeholder="No Invoice" value="" id="no_invoice">
									</div>
									<div style="width: 30%;">
										<button type="button" class="thm-btn2" id="cari" style="width: 95%; padding: 8px; border-radius: 5px; margin-top: 24px; height: 55px; float: right;">
											Cari
										</button>
									</div>
								</div>
								<br>
								<span>Transaksi Anda</span>
								<table class="table table-striped table-bordered table-responsive-md">
									<thead>
										<tr>
											<th style="width: 50%;">No Invoice</th>
											<td id="no_invoices">
												<?php
													if($list->qurban == null){
														echo $list->donasi->snap_token;
													}else{
														echo $list->qurban->no_invoice;
													}
												?>
											</td>
										</tr>
										<tr>
											<th style="width: 50%;">Metode Pembayaran</th>
											<td id="metode_pembayaran">
												<?php
													if($list->qurban == null){
														echo $list->donasi->metode_pembayaran;
													}else{
														echo $list->qurban->metode_pembayaran;
													}
												?>
											</td>
										</tr>
										<tr>
											<th style="width: 50%;">Nominal</th>
											<td id="nominal">
												<?php
													if($list->qurban == null){
														echo number_format($list->donasi->total_donasi);
													}else{
														echo number_format($list->qurban->total_qurban);
													}
												?>
											</td>
										</tr>
										<tr>
											<th style="width: 50%;">Status Pembayaran</th>
											<td id="status_pembayaran">
												<?php
													if($list->qurban == null){
														echo $list->donasi->status;
													}else{
														echo $list->qurban->status;
													}
												?>
											</td>
										</tr>
										<tr>
											<th style="width: 50%;">Waktu Transaksi</th>
											<td id="waktu_transaksi">
												<?php
													if($list->qurban == null){
														echo $list->donasi->created_at;
													}else{
														echo $list->qurban->created_at;
													}
												?>
											</td>
										</tr>
									</thead>
								</table>
								
								<button type="button" class="thm-btn2" onclick="confirm_pay('{{$list->confirm->no_invoice}}', '{{$list->confirm->vendor}}', '{{$list->confirm->nominal}}', '{{$list->confirm->moota_bank_id}}')" style="width: 100%; padding: 8px; border-radius: 5px; margin-top: 24px; margin-bottom: 20px; height: 40px; float: right;">
									Konfirmasi Pembayaran Saya
								</button>
								
								<span>Data Donatur</span>
								<table class="table table-striped table-bordered table-responsive-md">
									<thead>
										<tr>
											<th style="width: 50%;">Nama</th>
											<td id="nama_donatur">
												<?php
												if($list->donasi == null){
													if($list->qurban->nama_lengkap == null){
														echo $list->qurban->name;
													}else{
														echo $list->qurban->nama_lengkap;
													}
												}else{
													if($list->donasi->nama_lengkap == null){
														echo $list->donasi->name;
													}else{
														echo $list->donasi->nama_lengkap;
													}
												}
												?>
											</td>
										</tr>
										<tr>
											<th style="width: 50%;">No Handphone/Telepon</th>
											<td id="no_telp">
												<?php
												if($list->donasi == null){
													if($list->qurban->no_telp == null){
														echo $list->qurban->no_telp_users;
													}else{
														echo $list->qurban->no_telp;
													}
												}else{
													if($list->donasi->no_telp == null){
														echo $list->donasi->no_telp_users;
													}else{
														echo $list->donasi->no_telp;
													}
												}
												?>
											</td>
										</tr>
										<tr>
											<th style="width: 50%;">Email</th>
											<td id="email">
												<?php
												if($list->donasi == null){
													if($list->qurban->email == null){
														echo $list->qurban->email_users;
													}else{
														echo $list->qurban->email;
													}
												}else{
													if($list->donasi->email == null){
														echo $list->donasi->email_users;
													}else{
														echo $list->donasi->email;
													}
												}
												?>
											</td>
										</tr>
										<tr>
											<th style="width: 50%;">Alamat</th>
											<td id="alamat">
												<?php
												if($list->donasi == null){
													if($list->qurban->alamat_users == null){
														echo $list->qurban->alamat_users;
													}else{
														echo $list->qurban->alamat;
													}
												}else{
													if($list->donasi->alamat_users == null){
														echo $list->donasi->alamat_users;
													}else{
														echo $list->donasi->alamat;
													}
												}
												?>
											</td>
										</tr>
									</thead>
								</table>
								<br>
								<span>Riwayat Transaksi</span>
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>Waktu Transaksi</th>
											<th>Nominal</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td id="waktu">
												<?php
													if($list->qurban == null){
														echo $list->donasi->created_at;
													}else{
														echo $list->qurban->created_at;
													}
												?>
											</td>
											<td id="total">
												<?php
													if($list->qurban == null){
														echo number_format($list->donasi->total_donasi);
													}else{
														echo number_format($list->qurban->total_qurban);
													}
												?>
											</td>
											<td id="status_transaksi">
												<?php
													if($list->qurban == null){
														echo $list->donasi->status;
													}else{
														echo $list->qurban->status;
													}
												?>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section style="margin-bottom: 100px;"></section>
</main><!-- Main Wrapper -->

@include('front_page.ngo-theme-2.main_layouts.js')
<script type="text/javascript">
	$(document).ready(function() {
		$('#cari').click(function(event) {
			$.ajax({
				url: '{{ENV("API_URL")}}/api/track/transaction/'+$('#no_invoice').val(),
				type: 'GET',
				dataType: 'Json',
			})
			.done(function(res) {
				if(res.qurban != null){
					$('#no_invoices').html(res.qurban.no_invoice)
					$('#metode_pembayaran').html(res.qurban.metode_pembayaran)
					$('#nominal').html(res.qurban.total_qurban.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
					$('#status').html(res.qurban.status)
					$('#waktu_transaksi').html(res.qurban.created_at)
					$('#waktu').html(res.qurban.created_at)
					$('#total').html(res.qurban.total_qurban.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
					$('#status_transaksi').html(res.qurban.status)
				}else{
					$('#no_invoices').html(res.donasi.snap_token)
					$('#metode_pembayaran').html(res.donasi.metode_pembayaran)
					$('#nominal').html(res.donasi.total_donasi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
					$('#status').html(res.donasi.status)
					$('#waktu_transaksi').html(res.donasi.created_at)
					$('#waktu').html(res.donasi.created_at)
					$('#total').html(res.donasi.total_donasi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","))
					$('#status_transaksi').html(res.donasi.status)
				}

				if(res.qurban.name != null){
					$('#name').html(res.qurban.name)
				}else{
					$('#name').html(res.donasi.nama_lengkap)
				}

				if(res.qurban.no_telp != null){
					$('#no_telp').html(res.qurban.no_telp)
				}else{
					$('#no_telp').html(res.donasi.no_telp_users)
				}

				if(res.qurban.email != null){
					$('#email').html(res.qurban.email)
				}else{
					$('#email').html(res.donasi.email)
				}

				if(res.qurban.alamat != null){
					$('#alamat').html(res.qurban.alamat)
				}else{
					$('#alamat').html(res.donasi.alamat_users)
				}
			});
		});
	});

	function messageAlert(response){
		const Toast = Swal.mixin({
			toast: true,
			customClass: 'swal-wide',
			position: 'top-end',
			showConfirmButton: false,
			timer: 8000,
			timerProgressBar: true,
			onOpen: (toast) => {
				toast.addEventListener('mouseenter', Swal.stopTimer)
				toast.addEventListener('mouseleave', Swal.resumeTimer)
			}
		});

		Toast.fire({
			title: response
		});
	}

	function confirm_pay(id, vendor, nominal, moota_bank_id)
	{
		console.log(vendor)
		if(vendor == "moota"){
			$.post('{{ENV("API_URL")}}/api/moota/mutation/by-amount',
			{
				_method	: 'POST',
				bank_id: moota_bank_id,
				amount: nominal,
				type: "Donasi"
			},
			function (data, status) {
				$("#notif-pembayaran").removeClass('payment-method-active');
				messageAlert(data.message);

				$('#program_id').val('');
				$('#nominal').val('');
				$('#nama_lengkap').val('');
				$('#no_handphone').val('');
				$('#email').val('');
				$("#payment_vendor").val('');
				$("#payment_type").val('');
				$("#payment_method").val('');
				$('#no_rekening').val('');

			}
			);
		}else{
			$.ajax({
				url: '{{ENV("API_URL")}}/api/confirm-pay-donasi',
				type: 'GET',
				dataType: 'Json',
				data: {
					no_invoice: id
				}
			})
			.done(function(res) {
				messageAlert('Silahkan Lakukan Konfirmasi Pembayaran !');
				$("#notif-pembayaran").removeClass('payment-method-active');

				window.location.href = "{{ url('program/confirm?no_invoice=') }}"+res.confirm.snap_token
			});
		}
	}
</script>
</body>	
</html>