
<div class="container">
    <div class="row">
        <div class="col-lg-12 mt-5 mt-lg-0">
            <h3 class="mb-2">
                @lang('language.update_artikel')
            </h3>
            <div class="svg-line mb-2 clearfix">
                <hr style="background-color: red; height: 1px; width: 250px; position: absolute;">
            </div>
            <p class="mb-5">
               @lang('language.update_artikel_deskripsi')
            </p>
            <div class="clear"></div>
            @foreach($posting as $post)
            <div class="col-md-3" style="float: left;">
                <div class="feature-box media-box">
                    <div class="fbox-media">
                        <a href="{{Route('update.detail', $post->seo)}}">
                            <div class="img-artikel" style="background-image: url({{asset('admin/assets/media/posting/'.$post->image)}});height:150px !important;">
                            </div>
                        </a>
                    </div>
                    <div class="fbox-desc">
                        <div class="badge alert-danger">{{$post->category}}</div>
                        <a href="{{Route('update.detail', $post->seo)}}"><h4>{{$post->judul}}</h4></a>
                        <ul class="entry-meta clearfix">
                            <li><i class="icon-calendar3"></i> {{$post->created_at}}</li>
                        </ul><br>
                        {{substr(strip_tags($post->content), 0, 200)}}<br>
                        <a href="{{Route('update.detail', $post->seo)}}" class="more-link">
                            Read More
                        </a><br><br><br>
                        
                        </div>
    
                </div>
            </div>
            
            @endforeach
        </div>
       
        
    </div>
    <div class="row" style="margin-top: 20px;">
        <div class="col-12">

            
        </div>
    </div>
</div>