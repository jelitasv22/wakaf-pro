<style type="text/css">
 .mdi-36px {
     font-size: 39px;
     margin-right: 10px
 }
</style>
<div class="footer-widgets-wrap dark clearfix" style="background: radial-gradient(rgba(0,45,64,.5), rgba(0,45,64,.1), rgba(0,45,64,.5)), url('{{asset('themes/theme1/demos/nonprofit/images/others/footer.jpg')}}') repeat center center / cover;  padding: 150px 0">
    <div class="divcenter center" style="max-width: 700px;">
        <h2 class="display-2 t700 text-white mb-0 ls1 font-secondary mb-4">
            <i class="icon-heart d-block mb-3">
            </i>
            @if($footer_text != null)
            {{$footer_text->judul}}
            @endif
        </h2>
        <a href="programs" class="button button-rounded button-xlarge button-white bg-white button-light text-dark shadow nott ls0 ml-0 mt-5">
            @lang('language.donasi')
        </a>
        <br>
        <a href='https://play.google.com/store/apps/details?id=com.mizanamanah.apps&hl=in&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'>
            <img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/ style="width: 32%;" title="Get it on Google Play">
        </a>

        <a href="javascript:0;" title="Sedang Dalam Pengembangan">
            <img src="{{asset('admin/assets/media/icons/app_store.png')}}" style="width: 25%; border: none;">
        </a>
    </div>
</div>