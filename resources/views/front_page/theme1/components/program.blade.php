<div class="container clearfix" style="margin-bottom: 150px;">
    <div class="col-md-12 center">
        <br>
        <div class="heading-block nobottomborder mb-4">
            <h2 class="mb-4 nott">
                @lang('language.program')
            </h2>
        </div>
        <div class="svg-line bottommargin-sm clearfix">
            <hr style="background-color: red; height: 1px; width: 200px;">
        </div>
        <p>
            @lang('language.program_deskripsi')
        </p>
    </div>
    <div class="box-program">
        <?php $hasil =0; $persen=0; $date =""; $id_progam = ''; $total_capture= 0; $total = 0;?>

        @foreach($program_home as $prog)                        
        <?php 
        $total  = $prog->total;
        $target = $prog->dana_target;

        if ($target != null || $target != 0){
            $persen = $target/100;
            $hasil  = round($total/$persen);
            if($hasil > 100){
                $hasil = 100;
            }
        }else{
            $persen = 100;

            if($total != null || $total != 0){
                $hasil  = 100; 
            }else{
                $hasil = 0;
            }
        }

        $create = date_create($prog->tanggal);
        $date = date_format($create, 'd F Y');
        ?>
        <div class="card-program">
            <div class="img-card" style="background-image: url({{asset('admin/assets/media/foto-program')}}/{{$prog->foto}}">
                <div class="label">{{$prog->category}}</div>
            </div>
            <p class="title-card">{{$prog->judul}}</p>
            <ul class="skills mb-3" style="padding: 10px;">
                <li data-percent="{{$hasil}}">
                    <div class="progress skills-animated" style="width: 2%;">
                    </div>
                </li>
            </ul>
            @if($prog->dana_target != null)
            <h6 class="padding-left-right" style="padding-right: 10px !important; padding-left: 10px !important;">
                Terkumpul {{'Rp. '.number_format($prog->total)}} dari {{'Rp. '.number_format($prog->dana_target)}} ({{$hasil}}%)
            </h6>
            @else
            <h6 class="padding-left-right" style="padding-right: 10px !important; padding-left: 10px !important;">
                Terkumpul {{'Rp. '.number_format($prog->total)}} ({{$hasil}}%)
            </h6>
            @endif
            <p class="padding-left-right mb-0">
                {{$prog->resume}}
            </p>
            <a href="{{Route('program.detailprogram', $prog->seo)}}" class=" padding-left-right">
                Read More
            </a>
            <div class="footer-card">
                <div class="batas-waktu-box">
                    <p class="mb-1">Batas Waktu</p>
                    @if($prog->tanggal == null)
                    <p class="mb-1"><b>Tanpa Batas Waktu</b></p>
                    @else
                    <p class="mb-1"><b>{{$date}}</b></p>
                    @endif
                </div>
                <a href="{{Route('program.donasi', $prog->seo)}}" ><p class="card-btn">@lang('language.btn_donasi1')</p></a><br><br>
            </div>    
        </div>
        @endforeach

        <div class="clear"></div>
        <div class="col-md-12">
            <a class="more-link" href="programs/pencarian?search_query_first=&search_query_last=all" style="float: right;">Program Lainnya</a><br><br><br>
        </div>

        <div class="col-lg-12">
            <div class="js-player" data-plyr-provider="youtube" data-plyr-embed-id="{{$links}}"></div>
        </div>
    </div>
</div>
