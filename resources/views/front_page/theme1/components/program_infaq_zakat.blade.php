<div class="container clearfix" style="margin-bottom: 50px;">
	<div class="col-md-12 center">
		<br>
		<div class="heading-block nobottomborder mb-4">
			<h2 class="mb-4 nott">
				Program @if($is_ramadhan) Ramadhan, @endif Infaq dan Zakat
			</h2>
		</div>
		<div class="svg-line bottommargin-sm clearfix">
			<hr style="background-color: red; height: 1px; width: 200px;">
		</div>
		<p>
			Program @if($is_ramadhan) Ramadhan, @endif Infaq dan Zakat Pilihan
		</p>
	</div>
	<div class="tabs tabs-alt tabs-justify clearfix ui-tabs ui-corner-all ui-widget ui-widget-content" id="tab-10">

		<ul class="tab-nav clearfix ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header" role="tablist">
			@if($is_ramadhan)
			<li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active" aria-controls="tabs-36" aria-labelledby="ui-id-24" aria-selected="true" aria-expanded="true" style="width: 150px;">
				<a href="#tabs-36" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-24">Ramadhan</a>
			</li>
			@endif
			<li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="tabs-37" aria-labelledby="ui-id-25" aria-selected="true" aria-expanded="true" style="width: 75px;">
				<a href="#tabs-37" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-25">Infaq</a>
			</li>
			<li role="tab" tabindex="-1" class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" aria-controls="tabs-38" aria-labelledby="ui-id-26" aria-selected="false" aria-expanded="false" style="width: 75px;">
				<a href="#tabs-38" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-26">Zakat</a>
			</li>
		</ul>
		<br>
		<div class="tab-container">
			@if($is_ramadhan)
			<div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tabs-36" aria-labelledby="ui-id-24" role="tabpanel" aria-hidden="false" style="">
				<div class="">
					<div class="box-program">
						<?php $hasil =0; $persen=0; $date =""; $id_progam = ''; $total_capture= 0; $total = 0;?>

						<?php foreach($program_ramadan as $ramadan){ 
							$total  = $ramadan->total;
							$target = $ramadan->dana_target;

							if ($target != null || $target != 0){
								$persen = $target/100;
								$hasil  = round($total/$persen);
								if($hasil > 100){
									$hasil = 100;
								}
							}else{
								$persen = 100;

								if($total != null || $total != 0){
									$hasil  = 100; 
								}else{
									$hasil = 0;
								}
							}

							$create = date_create($ramadan->tanggal);
							$date = date_format($create, 'd F Y');                    
							?>

							<div class="card-program">
								<div class="img-card" style="background-image: url({{asset('admin/assets/media/foto-program')}}/{{$ramadan->foto}}">
									<div class="label">{{$ramadan->category}}</div>
								</div>
								<p class="title-card">{{$ramadan->judul}}</p>
								<ul class="skills mb-3" style="padding: 10px;">
									<li data-percent="{{$hasil}}">
										<div class="progress skills-animated" style="width: 2%;">
										</div>
									</li>
								</ul>
								@if($ramadan->dana_target != null)
								<h6 class="padding-left-right" style="padding-right: 10px !important; padding-left: 10px !important;">
									Terkumpul {{'Rp. '.number_format($ramadan->total)}} dari {{'Rp. '.number_format($ramadan->dana_target)}} ({{$hasil}}%)
								</h6>
								@else
								<h6 class="padding-left-right" style="padding-right: 10px !important; padding-left: 10px !important;">
									Terkumpul {{'Rp. '.number_format($ramadan->total)}} ({{$hasil}}%)
								</h6>
								@endif
								<p class="padding-left-right mb-0">
									{{$ramadan->resume}}
								</p>
								<a href="{{Route('program.detailprogram', $ramadan->seo)}}" class=" padding-left-right">
									Read More
								</a>
								<div class="batas-waktu-box">
									<p class="mb-1">Batas Waktu</p>
									@if($ramadan->tanggal == null)
                                        <p class="mb-1"><b>Tanpa Batas Waktu</b></p>
                                    @else
                                        <p class="mb-1"><b>{{$date}}</b></p>
                                    @endif
								</div>
								<a href="{{Route('program.donasi', $ramadan->seo)}}" ><p class="card-btn">@lang('language.btn_donasi1')</p></a><br><br>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			@endif

			<div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tabs-37" aria-labelledby="ui-id-25" role="tabpanel" aria-hidden="false">
				<div class="">
					<div class="box-program">
						<?php $hasil =0; $persen=0; $date =""; $id_progam = ''; $total_capture= 0; $total = 0;?>

						<?php foreach($program_infaq as $infaq){ 
							$total  = $infaq->total;
							$target = $infaq->dana_target;

							if ($target != null || $target != 0){
								$persen = $target/100;
								$hasil  = round($total/$persen);
								if($hasil > 100){
									$hasil = 100;
								}
							}else{
								$persen = 100;

								if($total != null || $total != 0){
									$hasil  = 100; 
								}else{
									$hasil = 0;
								}
							}

							$create = date_create($infaq->tanggal);
							$date = date_format($create, 'd F Y');                    
							?>

							<div class="card-program">
								<div class="img-card" style="background-image: url({{asset('admin/assets/media/foto-program')}}/{{$infaq->foto}}">
									<div class="label">{{$infaq->category}}</div>
								</div>
								<p class="title-card">{{$infaq->judul}}</p>
								<ul class="skills mb-3" style="padding: 10px;">
									<li data-percent="{{$hasil}}">
										<div class="progress skills-animated" style="width: 2%;">
										</div>
									</li>
								</ul>
								@if($infaq->dana_target != null)
								<h6 class="padding-left-right" style="padding-right: 10px !important; padding-left: 10px !important;">
									Terkumpul {{'Rp. '.number_format($infaq->total)}} dari {{'Rp. '.number_format($infaq->dana_target)}} ({{$hasil}}%)
								</h6>
								@else
								<h6 class="padding-left-right" style="padding-right: 10px !important; padding-left: 10px !important;">
									Terkumpul {{'Rp. '.number_format($infaq->total)}} ({{$hasil}}%)
								</h6>
								@endif
								<p class="padding-left-right mb-0">
									{{$infaq->resume}}
								</p>
								<a href="{{Route('program.detailprogram', $infaq->seo)}}" class=" padding-left-right">
									Read More
								</a>
								<div class="footer-card">
								<div class="batas-waktu-box">
									<p class="mb-1">Batas Waktu</p>
									@if($infaq->tanggal == null)
                                        <p class="mb-1"><b>Tanpa Batas Waktu</b></p>
                                    @else
                                        <p class="mb-1"><b>{{$date}}</b></p>
                                    @endif
								</div>
								<a href="{{Route('program.donasi', $infaq->seo)}}" ><p class="card-btn">@lang('language.btn_donasi1')</p></a><br><br>
								</div>
							</div>

						<?php } ?>
					</div>
				</div>
			</div>
			<div class="tab-content clearfix ui-tabs-panel ui-corner-bottom ui-widget-content" id="tabs-38" aria-labelledby="ui-id-26" role="tabpanel" aria-hidden="true">
				<div class="">
					<div class="box-program">
						<?php foreach($program_zakat as $zakat) { 

							$total  = $zakat->total;
							$target = $zakat->dana_target;

							if ($target != null || $target != 0){
								$persen = $target/100;
								$hasil  = round($total/$persen);
								if($hasil > 100){
									$hasil = 100;
								}
							}else{
								$persen = 100;

								if($total != null || $total != 0){
									$hasil  = 100; 
								}else{
									$hasil = 0;
								}
							}

							$create = date_create($zakat->tanggal);
							$date = date_format($create, 'd F Y');

							?>

							<div class="card-program">
								<div class="img-card" style="background-image: url({{asset('admin/assets/media/foto-program')}}/{{$zakat->foto}}">
									<div class="label">{{$zakat->category}}</div>
								</div>
								<p class="title-card">{{$zakat->judul}}</p>
								<ul class="skills mb-3" style="padding: 10px;">
									<li data-percent="{{$hasil}}">
										<div class="progress skills-animated" style="width: 2%;">
										</div>
									</li>
								</ul>
								@if($zakat->dana_target != null)
								<h6 class="padding-left-right" style="padding-right: 10px !important; padding-left: 10px !important;">
									Terkumpul {{'Rp. '.number_format($zakat->total)}} dari {{'Rp. '.number_format($zakat->dana_target)}} ({{$hasil}}%)
								</h6>
								@else
								<h6 class="padding-left-right" style="padding-right: 10px !important; padding-left: 10px !important;">
									Terkumpul {{'Rp. '.number_format($zakat->total)}} ({{$hasil}}%)
								</h6>
								@endif
								<p class="padding-left-right mb-0">
									{{$zakat->resume}}
								</p>
								<a href="{{Route('program.detailprogram', $zakat->seo)}}" class=" padding-left-right">
									Read More
								</a>
								<div class="footer-card">
								<div class="batas-waktu-box">
									<p class="mb-1">Batas Waktu</p>
									@if($zakat->tanggal == null)
                                        <p class="mb-1"><b>Tanpa Batas Waktu</b></p>
                                    @else
                                        <p class="mb-1"><b>{{$date}}</b></p>
                                    @endif
								</div>
								<a href="{{Route('program.donasi', $zakat->seo)}}" ><p class="card-btn">@lang('language.btn_donasi1')</p></a><br><br>
								</div>
							</div>

						<?php } ?>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>