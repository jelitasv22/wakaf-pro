<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
	<!--css include from main_layouts-->
	@include('front_page.theme1.main_layouts.css')
	<!--end css-->
	<script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}" data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
	<title>Form Zakat | Mizan Amanah</title>

</head>

<body class="stretched">

    <!-- Document Wrapper
    	============================================= -->
    	<div id="" class="clearfix">

    		<!-- Header ============================================= -->
    		@include('front_page.theme1.main_layouts.header')
    		<!-- #header end -->

        <!-- Content
        	============================================= -->
        	<section id="content" style="overflow: visible">

        		<div class="content-wrap">

        			<div class="container clearfix">

        				<div class="row justify-content-center">
        					<div class="col-md-7 ">
        						<div class="heading-block nobottomborder mb-4 center">
        							<h4 class="mb-4 nott">Tunaikan Zakat</h4>
        						</div>
        						<div class="svg-line bottommargin-sm clearfix center">
        							<hr style="background-color: red; height: 1px; width: 50%;">
        						</div>
        					</div>
        				</div>
        				<div class="form-widget" style="margin-top: 20px;">

        					<div class="form-result"></div>

        					<div class="row justify-content-center">
        						<div class="col-lg-6 ">
        							<form class="row" id="payment-form" action="{{Route('donasi.snaptokenZakat')}}" method="post"
        							enctype="multipart/form-data">
        							<input type="hidden" name="_token" value="{!! csrf_token() !!}">
        							<input type="hidden" name="result_type" id="result-type" value="">
        							<input type="hidden" name="result_data" id="result-data" value="">
        							<input type="hidden" name="user_id" value="{{session::get('id')}}" id="user_id">
        							<div class="col-12 form-group">
        								<label>Kategori Zakat</label>
        								<select class="form-control" name="category_id" id="category_id" onchange="get_kategori()" required="">
        									<option selected="" disabled="">- Kategori -</option>
        									@foreach($category as $val)
        									<?php if($kategori == $val->id){
        										$selected = 'selected';
        									}else{
        										$selected = '';
        									}
        									?>
        									<option value="{{$val->id}}" <?=$selected?> >{{$val->category}}</option>
        									@endforeach
        								</select>
        								<input type="hidden" name="category_produk" id="category_produk" value="{{$data_kategori}}">
        							</div>
        							<div class="col-12 form-group">
        								<label>Jumlah Zakat</label>
        								<input type="text" name="donation" class="form-control required" value="{{$jumlah_zakat}}"
        								placeholder="Jumlah Zakat" id="donationtext">
        							</div>

									<!-- Xendit Payment Method -->
									<div class="col-12 form-group">
										<label>Metode Pembayaran</label>
										<div class="btn-payment-method" id="btn-payment-method" style="width:100%;">Pilih Metode Pembayaran</div>

										<input name="payment_vendor" id="payment_vendor" type="hidden" value="" />
										<input name="payment_type" id="payment_type" type="hidden" value="" />
										<input name="payment_method" id="payment_method" type="hidden" value="" />

										<div id="selected-method" class="row">
											<!-- <input class="form-control" name="payment_method_label" id="payment_method_label" value="" readonly /> -->
											<div class="col-md-8">
												<img src="" alt="" id="selected-method-img" style="max-width:100%;max-height:40px;margin-right:10px;">
												<span style="font-size:14px;" id="selected-method-label"></span>
											</div>

											<div class="col-md-4">
												<div class="btn-payment-method form-full" id="btn-payment-method2">Ganti</div>
											</div>
										</div>

										<br/>
										<!-- <select class="form-control" name="payment_type" id="payment_type" onchange="choose_method()" required="">
											<option value="credit_card">Kartu Kredit</option>
											<option value="e_wallet">E-Wallets</option>
											<option value="retail_outlet">Ritel Outlet</option>
											<option value="virtual_account">Virtual Akun</option>
										</select> -->
									</div>
									<!-- <div class="col-12 form-group">
										<select class="form-control" name="payment_method" id="payment_method"></select>
									</div> -->

									<!-- Credit Card -->
									<div class="col-12 form-group row" id="cc_form">
										<!-- <div class="col-12 form-group">
											<label>Nomor Akun</label>
											<input type="text" class="form-control" id="cc_account_number">
										</div>
										<div class="col-4 form-group">
											<label>Exp. Month</label>
											<input type="number" class="form-control" id="cc_exp_month">
										</div>
										<div class="col-4 form-group">
											<label>Exp. Year</label>
											<input type="number" class="form-control" id="cc_exp_year">
										</div>
										<div class="col-4 form-group">
											<label>CVN</label>
											<input type="password" class="form-control" id="cc_cvn">
										</div> -->
									</div>
									<!-- E-Wallets -->
									<div class="col-12 form-group row" id="ew_form">
										<div class="col-12 form-group" id="ew_phone_form">
											<label>Nomor HP Akun E-Wallet</label>
											<input type="text" class="form-control" id="ew_phone_number">
										</div>
										<div class="col-12 form-group">
											<input type="checkbox" id="ew_same_number" onclick="handleChecked(this)" />
											<label for="ew_same_number">Gunakan nomor yang sama dengan nomor pada data diri.</label>
										</div>
									</div>
									<!-- End of Xendit Payment Method -->
                                    <!-- Metode Pembayaran -->
                                    <div id="metode-pembayaran" class="popup">

                                        <!-- popup metode pembayaran -->
                                        <div class="popup-content">
                                            <div class="head-pop">
                                                <div class="title-pop">
                                                    <p>Metode Pembayaran</p>
                                                </div>
                                                <span id="mthd-close" class="tutup">&times;</span>
                                            </div>
                                            <div class="payment-scroller" id="payment-popup-body">
                                            </div>
                                        </div>

                                    </div>
                                    <!-- Virtual Akun -->
                                    <div id="virtual-akun" class="popup">

                                        <!-- popup metode pembayaran -->
                                        <div class="popup-content">
                                            <div class="head-pop">
                                                <div class="title-pop">
                                                    <p>Virtual Akun</p>
                                                </div>
                                                <span id="virtual-close" class="tutup" onclick="cancelTransaction()">&times;</span>
                                            </div>
                                            <div class="payment-scroller">
                                                <br>
                                                <p class="center b600">Instruksi Pembayaran</p>
                                                <p class="center ">Transfer sesuai nominal di bawah ini :</p>
                                                <p class="center b600 nominal" id="virtual-akun-total">Rp 10.000</p>
                                                <p class="center ">ke rekening <b id="virtual-akun-bank">BNI Virtual Account</b>:</p>
                                                <br>
                                                <div class="no-rek-virtual">
                                                    <img class="logo-payment" id="virtual-akun-trans-logo"
                                                        src="{{asset('themes/theme1/images/bni.png')}}" alt="">
                                                    <p class="center b600 rek-virtual" id="virtual-akun-trans-code">9982433423489238</p>
                                                    <p class="center red-text" onclick="copyToClip('virtual-akun-trans-code')">SALIN</p>
                                                </div>
                                                <p class="small-text" id="virtual-akun-caution">Transfer sebelum 15 MEI 2020 16.28 WIB atau donasi
                                                    kamu otomatis dibatalkan oleh sistem</p>
                                                <br><br>
                                                <p class="center b600">Panduan Pembayaran</p><br>

												<div id="virtual-akun-step-bni">
													<div class="accordion-panduan" id="ac1">
														<p class="title-accordion">BNI Mobile Banking</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac1" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
																Masuk ke aplikasi BNI Mobile dan input PIN kamu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Pilih Menu Transfer
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
																Pilih menu Virtual Account Billing dan masukan nomor Virtual
																Account diatas lalu klik lanjut
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
																Cek nama dan nominal pembayaran, apabila telah sesuai klik OK.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
																Masukan PIN BNI Mobile Kamu dan klik OK.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
																Transaksi Selesai, Mohon simpan nomor invoice sebagai bukti pembayaran.
															</p>
														</div>
													</div>
													<div class="accordion-panduan" id="ac2">
														<p class="title-accordion">BNI Internet Banking</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac2" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masuk ke Halaman website internet banking BNI (ibank.bni.co.id)
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Masuk ke akun kamu dengan mengisi User ID dan PIN internet banking kamu
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															Pilih menu “Transfer” lalu pilih “Virtual Account Billing”
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Masukan nomor Virtual Account diatas lalu pilih “Lanjut”
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Konfirmasi dengan input Otentikasi Token
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
															Transaksi selesai, mohon simpan nomor invoice sebagai bukti pembayaran
															</p>
														</div>
													</div>
													<div class="accordion-panduan" id="ac3">
														<p class="title-accordion">ATM BNI</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac3" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masukan kartu ATM BNI dan PIN ATM BNI kamu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Pilih “Menu Lainnya”
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">

																Pilih menu “VIRTUAL ACCOUNT BILLING” Masukan nomor Virtual Account diatas.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
																Cek nama dan nominal pembayaran, apabila telah sesuai pilih “Benar”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">

																Transaksi selesai, pilih “Tidak” untuk tidak melanjutkan transaksi lain.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">

																	Terakhir jangan lupa ambil kartu ATM BNI kamu.
															</p>
														</div>
													</div>
												</div>

												<div id="virtual-akun-step-bri">
													<div class="accordion-panduan" id="ac4">
														<p class="title-accordion">BRI Mobile Banking</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac4" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masuk ke aplikasi BRI Mobile dan input PIN kamu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Pilih menu “Pembayaran” lalu pilih “BRIVA”
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															Masukan nomor BRI Virtual Account diatas beserta jumlah pembayaran.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Masukkan PIN BRI Mobile kamu dan klik OK.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Transaksi selesai, mohon simpan nomor invoice sebagai bukti pembayaran.
															</p>
														</div>
													</div>
													<div class="accordion-panduan" id="ac5">
														<p class="title-accordion">BRI Internet Banking</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac5" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masuk ke Halaman website internet banking BRI (ib.bri.co.id)
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Masuk ke akun kamu dengan mengisi User ID dan PIN internet banking kamu
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															Pilih menu “Pembayaran” lalu pilih “BRIVA”
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Masukan nomor Virtual Account diatas yang tertera lalu masukan nominal yang akan dibayarkan
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Masukan Password dan Mtoken Kamu
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
															Transaksi selesai, mohon simpan nomor invoice sebagai bukti pembayaran
															</p>
														</div>
													</div>
													<div class="accordion-panduan" id="ac6">
														<p class="title-accordion">ATM BRI</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac6" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masukan kartu ATM BRI dan PIN ATM BRI kamu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Pilih “Menu Lainnya” lalu pilih menu “Pembayaran”
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															Pilih “BRIVA” lalu masukan nomor Virtual Account diatas lalu tekan “Benar”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Cek nama dan nominal pembayaran, apabila telah sesuai pilih “Ya”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Transaksi selesai, pilih “Tidak” untuk tidak melanjutkan transaksi lain.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
															Terakhir jangan lupa ambil Kartu ATM BRI kamu.
															</p>
														</div>
													</div>
												</div>

												<div id="virtual-akun-step-mandiri">
													<div class="accordion-panduan" id="ac7">
														<p class="title-accordion">Mandiri m-Banking (Mandiri Online)</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac7" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Pilih menu "Pembayaran" lalu pilih menu "Multipayment".
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Pilih penyedia jasa "Plink Pay"
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															Masukkan "Nomor Virtual Account" dan "Nominal" yang akan dibayarkan, lalu pilih "Lanjut"
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Setelah muncul tagihan, pilih "Konfirmasi"
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Masukkan PIN Mandiri Online.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
															Transaksi selesai, simpan bukti bayar kamu.
															</p>
														</div>
													</div>
													<div class="accordion-panduan" id="ac8">
														<p class="title-accordion">Mandiri Internet Banking</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac8" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masuk ke halaman website internet banking Mandiri (https://ib.bankmandiri.co.id)
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Masuk ke akun kamu dengan mengisi User ID dan PIN internet Banking kamu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															Pilih menu "Bayar" lalu pilih "Multi Payment"
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Pada bagian "Dari Rekening" masukkan rekening kamu
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Pada bagian "Penyedia Jasa" pilih "Plink Pay" lalu klik "Lanjut"
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
															Masukkan nomor Virtual Account diatas yang tertera. Hindari mencentang "Simpan Daftar Transfer" karena jumlah pembayaran Anda sebelumnya akan ikut tersimpan dan dapat mengganggu proses pembayaran berikutnya.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">7</div>
															<p class="desc-panel">
															Klik "Lanjutkan" dan periksa informasi pembayaran. Jika sudah benar, beri centang
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">8</div>
															<p class="desc-panel">
															Masukkan PIN token kamu dan klik "Kirim".
															</p>
														</div>
													</div>
													<div class="accordion-panduan" id="ac9">
														<p class="title-accordion">ATM Mandiri</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac9" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masukkan kartu ATM Mandiri dan PIN ATM Mandiri kamu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Pilih menu Bayar/Beli > Lainnya > Lainnya
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															Pilih menu Multipayment
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Masukkan kode perusahaan/institusi
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Masukkan nomor Virtual Account diatas lalu klik benar
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
															Cek nama dan nominal pembayaran, apabila telah sesuai masukkan angka 1 lalu pilih
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">7</div>
															<p class="desc-panel">
															Periksa konfirmasi pembayaran, jika sudah benar pilih ‘YA’
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">8</div>
															<p class="desc-panel">
															Transaksi selesai, pilih Tidak untuk tidak melanjutkan transaksi lain.
															</p>
														</div>
													</div>
												</div>

												<div id="virtual-akun-step-bca">
													<div class="accordion-panduan" id="ac10">
														<p class="title-accordion">BCA Mobile Banking (m-BCA)</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac10" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masuk ke aplikasi mobile banking BCA, pilih m-Banking dan input PIN kamu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Pilih menu “m-Transfer” lalu pilih menu “BCA Virtual Account”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															MKlik “Input No. Virtual Account” dan masukkan nomor Virtual Account diatas lalu
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Cek nama dan nominal pembayaran, apabila telah sesuai klik “OK”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Masukkan PIN m-BCA kamu dan klik “OK”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
															Transaksi selesai, mohon simpan nomor invoice sebagai bukti pembayaran.
															</p>
														</div>
													</div>
													<div class="accordion-panduan" id="ac11">
														<p class="title-accordion">BCA Internet Banking</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac11" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masuk ke halaman website KlikBCA (https://klikbca.com) dan pilih menu “login Individual”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Masuk ke akun KlikBCA kamu dengan mengisi User ID dan PIN Internet Banking kamu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															Pilih menu “Transfer ke BCA Virtual Account”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Masukkan nomor Virtual Account yang tertera diatas lalu pilih “Lanjutkan”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Cek nama dan nominal pembayaran, apabila telah sesuai pilih “Lanjutkan”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
															Masukkan respon KeyBCA APPLI 1 yang muncol pada Token BCA lalu pilih “Kirim”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">7</div>
															<p class="desc-panel">
															Transaksi selesai, mohon simpan nomor invoice sebagai bukti pembayaran.
															</p>
														</div>
													</div>
													<div class="accordion-panduan" id="ac12">
														<p class="title-accordion">ATM BCA</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac12" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masukkan kartu ATM BCA dan PIN ATM BCA kamu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Pilih menu “Transaksi Lainnya” lalu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															Pilih menu “Transfer” lalu pilih “Ke Rek. BCA Virtual Account”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Klik Input No Virtual Account dan masukkan nomor Virtual Account diatas lalu klik “OK”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Cek nama dan nominal pembayaran, apabila telah sesuai pilih “Benar”.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
															Cek dan perhatikan konfirmasi pembayaran dari layar ATM, jika sudah benar pilih “Ya”, atau pilih “Tidak” jika data di layar masih salah.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">7</div>
															<p class="desc-panel">
															Transaksi selesai, pilih “Tidak” untuk tidak melanjutkan transaksi lain.
															</p>
														</div>
													</div>
												</div>

												<div id="virtual-akun-step-permata">
													<div class="accordion-panduan" id="ac13">
														<p class="title-accordion">ATM PERMATA</p>
														<i class="fa fa-plus"></i>
													</div>
													<div id="panel-ac13" class="panel-ac">
														<div class="item-panel">
															<div class="poin-panel">1</div>
															<p class="desc-panel">
															Masukkan kartu ATM BCA dan PIN ATM BCA kamu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">2</div>
															<p class="desc-panel">
															Pilih menu “Transaksi Lainnya” lalu.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">3</div>
															<p class="desc-panel">
															Pilih Pembayaran Lainnya.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">4</div>
															<p class="desc-panel">
															Pilih Akun Virtual.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">5</div>
															<p class="desc-panel">
															Masukkan 16 digit No. Akun dan tekan Benar.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">6</div>
															<p class="desc-panel">
															Jumlah yang harus dibayar, nomor akun, dan nama pedagang akan muncul di halaman konfirmasi pembayaran. Jika informasinya benar, tekan Benar.
															</p>
														</div>
														<div class="item-panel">
															<div class="poin-panel">7</div>
															<p class="desc-panel">
															Pilih akun pembayaran Anda dan tekan Benar.
															</p>
														</div>
													</div>
												</div>
											</div>
											<br><br>

											<div class="btn-popup-confirm" id="virtual-akun-btn-label">
												Transaksi Selesai
											</div>
											<br>
                                        </div>

                                    </div>
                                    <!-- DANA -->
                                    <div id="simple-pop" class="popup">

                                        <!-- popup metode pembayaran -->
                                        <div class="popup-content">
                                            <div class="head-pop">
                                                <div class="title-pop">
                                                    <p id="simple-pop-title">DANA</p>
                                                </div>
                                                <span id="simple-pop-close" class="tutup" onclick="cancelTransaction()">&times;</span>
                                            </div>
                                            <div class="payment-scroller">
                                                <br>
												<center><img src="" alt="" id="simple-pop-logo" style="max-width:100%;max-height:60px;"></center>
                                                <div class="no-rek-virtual" id="simple-pop-trans">
                                                    <img class="logo-payment" id="simple-pop-trans-logo" src="" alt="">
													<p class="center b600 rek-virtual" id="simple-pop-trans-code">Kode Transaksi</p>
													<p class="center red-text" onclick="copyToClip('simple-pop-trans-code')">SALIN</p>
												</div>

                                                <p class="center b600">Instruksi Pembayaran</p>
                                                <p class="center ">Nominal yang akan anda donasikan :</p>
                                                <p class="center b600 nominal" id="simple-pop-total">Rp 10.000</p>
                                                <p class="center " id="simple-pop-guide">klik tombol untuk lanjutkan pembayaran</p>
                                                <br>
                                                <div class="btn-popup-confirm" id="simple-pop-btn-label">
													Lanjutkan ke DANA
												</div>
                                                <br><br>

                                            </div>
                                        </div>

									</div>
									<!-- Credit Card -->
                                    <div id="credit-card" class="popup">

                                        <!-- popup metode pembayaran -->
                                        <div class="popup-content">
                                            <div class="head-pop">
                                                <div class="title-pop">
                                                    <p id="simple-pop-title">Credit Card</p>
                                                </div>
                                                <span id="credit-close" class="tutup" onclick="cancelTransaction()">&times;</span>
                                            </div>
                                            <div class="payment-scroller">
                                                <br>
												<center><img src="" alt="" id="credit-card-logo" style="max-width:100%;max-height:60px;"></center>
												<p class="center b600">Instruksi Pembayaran</p>
												<div id="credit-card-info">
													<p class="center ">Nominal yang akan anda donasikan :</p>
													<p class="center b600 nominal" id="credit-card-total">Rp 10.000</p>
													<p class="center " id="credit-card-guide">Klik tombol untuk lanjutkan pembayaran!</p>
												</div>
												<br>
												<div class="form-box-credit" id="credit-card-form">
													<label for="">Card Number</label><br>
													<input type="text" id="cc_account_number" class="credit-form form-full">
													<div class="flex-form">
														<div class="item">
															<label for="">Exp.Month</label><br>
															<input type="number" id="cc_exp_month" class="credit-form half-form">
														</div>
														<div class="item">
															<label for="">&nbsp;Exp.Year</label><br>
															<input type="number" id="cc_exp_year" class="credit-form half-form">
														</div>
														<div class="item">
															<label for="">CVV/CVN</label><br>
															<input type="password" id="cc_cvn" class="credit-form half-form">
														</div>
													</div>
												</div>
												<br>
                                                <div class="btn-popup-confirm" id="credit-card-btn-label">
													Lanjutkan Pembayaran
												</div>
                                                <br>

                                            </div>
                                        </div>

                                    </div>

        							@if(session::get('id') == null)
        							<div class="col-12" style="margin-top: 5px; margin-bottom:20px; text-align:center;">
        								<a href="{{Route('front_page.login')}}">Silahkan
        								Login atau Isi data di bawah ini</a>
        							</div>
        							@endif
									<input type="hidden" name="metode_pembayaran" id="metode_pembayaran" value="0">
        							<div class="col-12 form-group">
        								<label>Nama Lengkap</label>
        								<?php if(session::has('name') != null){ ?>
        									<input type="text" name="nama" class="form-control required" value="{{session::get('name')}}"
        									placeholder="Masukan Nama Lengkap" id="nama" readonly="true">
        								<?php } else { ?>
        									<input type="text" name="nama" class="form-control required" value="{{session::get('name')}}"
        									placeholder="Masukan Nama Lengkap" id="nama">
        								<?php } ?>
        							</div>
        							<div class="col-12 form-group">
        								<label>No Handphone / Whatsapp</label>
        								<?php if(session::has('no_telp') != null) { ?>
        									<input type="number" name="no_telp" class="form-control required" value="{{session::get('no_telp')}}" placeholder="Masukan No Handphone" id="no_telp" readonly="true">
        								<?php } else { ?>
        									<input type="number" name="no_telp" class="form-control required" value="{{session::get('no_telp')}}" placeholder="Masukan No Handphone" id="no_telp">
        								<?php } ?>
        							</div>
        							<div class="col-12">
        								<div class="form-group">
        									<label>Alamat</label>
        									<?php if(session::has('alamat') != null){ ?>
        										<textarea name="alamat" id="alamat" class="form-control required" cols="30" rows="5" readonly="true">{{session::get('alamat')}}</textarea>
        									<?php } else { ?>
        										<textarea name="alamat" id="alamat" class="form-control required" cols="30" rows="5">{{session::get('alamat')}}</textarea>
        									<?php } ?>
        								</div>
        							</div>
        							<div class="col-12 form-group">
        								<label>Email:</label>
        								<?php if(session::has('email') != null) {?>
        									<input type="email" name="email" class="form-control required" value="{{session::get('email')}}"
        									placeholder="Masukan Alamat Email" id="email" readonly="true">
        								<?php } else { ?>
        									<input type="email" name="email" class="form-control required" value="{{session::get('email')}}"
        									placeholder="Masukan Alamat Email" id="email">
        								<?php } ?>
        							</div>
        							<div class="col-12 hidden">
        								<input type="text" id="event-registration-botcheck"
        								name="event-registration-botcheck" value="" />
        							</div>
        							<div class="col-12 center">
        								<button type="button" class="btn btn-program btn-danger" onclick="simpan()">Lanjutkan Pembayaran</button>
        							</div>

        							<input type="hidden" name="prefix" value="event-registration-">
        							<input type="hidden" name="komentar" value="" id="komentar">
        							<input type="hidden" name="anonim" value="0" id="anonim">
        							<input type="hidden" name="verifikasi" value="1" id="verifikasi">
        						</form>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>

    </div>

</div>

</div>

</div>

<!-- Xendit Payment Method -->
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="modal_title">Modal Header</h4>
				<button type="button" class="close" data-dismiss="modal" onclick="cancelTransaction()">&times;</button>
			</div>
			<div class="modal-body" id="modal_body">
				<p>This is a large modal.</p>
			</div>
			<div class="modal-footer">
				<button id="close_modal" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- Trigger the modal with a button -->
<button id="show_modal" type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#myModal">Open
	Large Modal</button>

<div class="btn-payment-method" id="btn-virtual-akun">Virtual Akun</div>
<div class="btn-payment-method" id="btn-simple-pop">Simple Popup</div>
<div class="btn-payment-method" id="btn-credit-card">Credit Card</div>
<!-- Loading -->
<div class="modal fade" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="loadMeLabel">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-body text-center">
				<div class="spinner-border" role="status">
				</div>
				<span class="">Loading...</span>
				<h4 id="loading-caution" style="margin:10px;">Harap selesaikan transaksi segera!</h4>
			</div>
		</div>
	</div>
</div>
<!-- Trigger loading with loading -->
<button id="show_loading" type="button" class="btn btn-info btn-md">Show Loading</button>
<!-- End of Xendit Payment Method -->

</section>
</div>


</section><!-- #content end -->

    <!-- Footer
    	============================================= -->
    	<footer id="footer" style="background-color: #002D40;">

    		<div class="container">

            <!-- Footer Widgets
            	============================================= -->

            </div>

        <!-- Copyrights
        	============================================= -->
        	@include('front_page.theme1.main_layouts.footer')
        	<!-- @include('front_page.theme1.components.floating_contact') -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
    	============================================= -->
    	<div id="gotoTop" class="icon-angle-up" style="bottom: 100px;"></div>
    	<!--css include from main_layouts-->
    	@include('front_page.theme1.main_layouts.js')
    	<!--end css-->

    	<script>

			var transaction_id = 0;
			var cc_first_step = true;

    		jQuery(document).ready(function ($) {
    			var elementParent = $('.floating-contact-wrap');
    			$('.floating-contact-btn').off('click').on('click', function () {
    				elementParent.toggleClass('active');
    			});

				$("#btn-payment-method").on('click', function () {
					$("#metode-pembayaran").addClass('payment-method-active');
					$("#metode-pembayaran").removeClass('payment-method-hide');
				});
				$("#btn-payment-method2").on('click', function () {
					$("#metode-pembayaran").addClass('payment-method-active');
					$("#metode-pembayaran").removeClass('payment-method-hide');
				});

				$("#mthd-close").off('click').on('click', function () {
					$("#metode-pembayaran").addClass('payment-method-hide');
					$("#metode-pembayaran").removeClass('payment-method-active');

				});

				$("#btn-virtual-akun").on('click', function () {
					$("#virtual-akun").addClass('payment-method-active');
					$("#virtual-akun").removeClass('payment-method-hide');
				});
				$("#virtual-close").off('click').on('click', function () {
					$("#virtual-akun").addClass('payment-method-hide');
					$("#virtual-akun").removeClass('payment-method-active');
				});

				$("#btn-simple-pop").on('click', function () {
					$("#simple-pop").addClass('payment-method-active');
					$("#simple-pop").removeClass('payment-method-hide');
				});
				$("#simple-pop-close").off('click').on('click', function () {
					$("#simple-pop").addClass('payment-method-hide');
					$("#simple-pop").removeClass('payment-method-active');
				});

				$("#btn-credit-card").on('click', function () {
					$("#credit-card").addClass('payment-method-active');
					$("#credit-card").removeClass('payment-method-hide');
				});
				$("#credit-close").off('click').on('click', function () {
					$("#credit-card").addClass('payment-method-hide');
					$("#credit-card").removeClass('payment-method-active');
				});
				// BNI
				$("#ac1").on('click', function () {
					$("#panel-ac1").toggleClass("display-block");
				});
				$("#ac2").on('click', function () {
					$("#panel-ac2").toggleClass("display-block");
				});
				$("#ac3").on('click', function () {
					$("#panel-ac3").toggleClass("display-block");
				});
				// BRI
				$("#ac4").on('click', function () {
					$("#panel-ac4").toggleClass("display-block");
				});
				$("#ac5").on('click', function () {
					$("#panel-ac5").toggleClass("display-block");
				});
				$("#ac6").on('click', function () {
					$("#panel-ac6").toggleClass("display-block");
				});
				// MANDIRI
				$("#ac7").on('click', function () {
					$("#panel-ac7").toggleClass("display-block");
				});
				$("#ac8").on('click', function () {
					$("#panel-ac8").toggleClass("display-block");
				});
				$("#ac9").on('click', function () {
					$("#panel-ac9").toggleClass("display-block");
				});
				// BCA
				$("#ac10").on('click', function () {
					$("#panel-ac10").toggleClass("display-block");
				});
				$("#ac11").on('click', function () {
					$("#panel-ac11").toggleClass("display-block");
				});
				$("#ac12").on('click', function () {
					$("#panel-ac12").toggleClass("display-block");
				});
				// Permata
				$("#ac13").on('click', function () {
					$("#panel-ac13").toggleClass("display-block");
				});
				getPaymentMethod();

				// Xendit Payment Method
				// choose_method();
				$("#btn-virtual-akun").hide()
				$("#btn-simple-pop").hide()
				$("#btn-credit-card").hide()

				$("#show_modal").hide();
				$("#show_loading").hide();
				$("#selected-method").hide()

				$("#cc_form").hide()
				$("#ew_form").hide()

				$("#show_loading").on("click", function(e) {
					e.preventDefault();
					$("#loadMe").modal({
						backdrop: "static", //remove ability to close modal with click
						keyboard: false, //remove option to close with keyboard
						show: true //Display loader!
					});
					// setTimeout(function() {
					// 	$("#loadMe").modal("hide");
					// }, 3500);
				});
				// End of Xendit Payment Method
    		});

    		var rupiah = document.getElementById('donationtext');
    		rupiah.addEventListener('keyup', function(e){
                // tambahkan 'Rp.' pada saat form di ketik
                // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
                rupiah.value = formatRupiah(this.value, 'Rp. ');
            });

    		/* Fungsi formatRupiah */
    		function formatRupiah(angka, prefix){
    			var number_string = angka.replace(/[^,\d]/g, '').toString(),
    			split             = number_string.split(','),
    			sisa              = split[0].length % 3,
    			rupiah            = split[0].substr(0, sisa),
    			ribuan            = split[0].substr(sisa).match(/\d{3}/gi);

                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                	separator = sisa ? '.' : '';
                	rupiah += separator + ribuan.join('.');
                }

                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }

            var cal = $('#calendar').calendario({
            	onDayClick: function ($el, $contentEl, dateProperties) {

            		for (var key in dateProperties) {
            			console.log(key + ' = ' + dateProperties[key]);
            		}

            	},
            	caldata: canvasEvents
            }),
            $month = $('#calendar-month').html(cal.getMonthName()),
            $year = $('#calendar-year').html(cal.getYear());

            $('#calendar-next').on('click', function () {
            	cal.gotoNextMonth(updateMonthYear);
            });
            $('#calendar-prev').on('click', function () {
            	cal.gotoPreviousMonth(updateMonthYear);
            });
            $('#calendar-current').on('click', function () {
            	cal.gotoNow(updateMonthYear);
            });

            function updateMonthYear() {
            	$month.html(cal.getMonthName());
            	$year.html(cal.getYear());
            };

            function simpan(){
				var id = $('#metode_pembayaran').val();
				if (id == 1) {
					$("#payment-form").submit();

					setTimeout(function () {
						window.location.href = "{{Route('notif.konfirmasi2')}}";
					}, 300);

				}else{

					var validation = 0;
					var validationText = "";

					var zakat = 0;
					if($("#donationtext").val()!=''){
						zakat = $("#donationtext").val().substr(4).replace(/\./g,'');
					} else if($("input[name='donation']:checked").val() != null){
						zakat = $("input[name='donation']:checked").val();
					} else {
						validation++;
						validationText = validationText + "Nilai zakat tidak boleh kosong\n";
					}

					$("#donationtext").val(zakat);

					if($("#nama").val()==''){ validation++; validationText = validationText + "Nama Lengkap tidak boleh kosong\n"; }
					if($("#no_telp").val()==''){ validation++; validationText = validationText + "No Telepon tidak boleh kosong\n"; }
					if($("#email").val()==''){ validation++; validationText = validationText + "Email tidak boleh kosong\n"; }
					if($("#verifikasi").val()==''){ validation++; validationText = validationText + "Verifikasi tidak boleh kosong\n"; }


					if(validation>0){
						alert(validationText);
					} else {

						showLoading();
						if ($("#payment_type").val() == "e_wallet" &&
							$("#payment_method").val() == "OVO")
						{
							$("#loading-caution").html("Periksa aplikasi OVO anda dan selesaikan transaksi dalam 30 detik!")
							hideLoading();

							$("#simple-pop-title").html($("#payment_method").val())
							$("#simple-pop-total").html(formatRupiah(zakat, "Rp. "))

							$("#simple-pop-trans").hide()
							$("#simple-pop-logo").show()
							$("#simple-pop-logo").attr("src", $("#selected-method-img").attr("src"))

							$("#simple-pop-guide").html("<center>Periksa aplikasi OVO anda dan segera selesaikan transaksi dalam 30 detik!</center>")
							$("#simple-pop-guide").append("<div style='margin-left:20px;'>"
														+ "<br/><b>Notes :</b>"
														+ "<br/>Apabila transaksi gagal, silahkan coba cara berikut :"
														+ "<br/>1. Masukkan kembali nomor telepon dan coba kirim kembali pembayaran."
														+ "<br/>2. Bersihkan cache aplikasi OVO anda."
														+ "<br/>3. Apabila transaksi masih gagal, silahkan coba kembali dengan metode pembayaran yang lain."
														+ "<br/>Anda TIDAK akan dikenakan biaya dua kali untuk mencoba kembali pembayaran."
														+ "</div>")
							$("#simple-pop-guide").attr("class","")
							$("#simple-pop-btn-label").html("Saya Sudah Bayar")
							$("#simple-pop-btn-label").attr("onclick","confirm_pay(0)")

							$("#btn-simple-pop").click();

						} else {
							$("#loading-caution").html("Harap selesaikan transaksi segera!")
						}
						$("#loading-caution").show()

						// Xendit Payment Method
						var card_data = null;
						if ($("#payment_type").val() == "credit_card") {
							card_data = {
								"account_number": $("#cc_account_number").val(),
								"exp_month": $("#cc_exp_month").val(),
								"exp_year": $("#cc_exp_year").val(),
								"cvn": $("#cc_cvn").val()
							}
						}
						// End of Xendit Payment Method

						if ($("#payment_vendor").val() == "xendit" &&
							$("#payment_type").val() == "credit_card" &&
							cc_first_step)
						{
							cc_first_step = false
							$("#btn-credit-card").click();

							$("#credit-card-info").hide();
							$("#credit-card-form").show();
							$("#credit-card-btn-label").attr("onclick","simpan()")

							return
						}

						$(this).attr("disabled", "disabled");
						$.post("{{ route('donasi.snaptokenZakat') }}",
						{
							_method: 'POST',
							_token: '{{ csrf_token() }}',
							jumlah_zakat 	  : zakat,
							nama_produk		  : $('#category_produk').val(),
							nama 			  : $("#nama").val(),
							email 			  : $("#email").val(),
							no_telp			  : $("#no_telp").val(),
							user_id 		  : $('#user_id').val(),
							category_id		  : $('#category_id').val(),
							// Xendit Payment Method
							vendor			  : $("#payment_vendor").val(),
							payment_type	  : $("#payment_type").val(),
							payment_method	  : $("#payment_method").val(),
							card_data		  : card_data,
							phone 			  : ($("#ew_phone_number").val() != "") ? $("#ew_phone_number").val() : $("#no_telp").val(),
							device			  : "Web"
							// End of Xendit Payment Method
						})
						.done(function (data, status) {
							console.log(data)
							if (data.status == 'error') {
								alert(data.message);
							} else {
								hideLoading();

								if (data.snap_token != null) {
									if ($("#payment_type").val() == "credit_card") {
										$("#credit-card").addClass('payment-method-hide');
										$("#credit-card").removeClass('payment-method-active');
									}

									snap.pay(data.snap_token, {
										onSuccess: function (result) {
											console.log(result);
											$.ajax({
												url: '{{ route("donasi.updateStatusZakat") }}',
												type: 'POST',
												dataType: 'json',
												data: {
													"_token": "{{ csrf_token() }}",
													"id": result.order_id,
													"status": result.transaction_status,
													"payment_method": result.payment_method,
													"snap_token": data.snap_token
												},
												success: function(data){
													hideLoading();
													window.location.href = "{{Route('notif-zakat.wait')}}";
												}
											});
										},
										onPending: function (result) {
											hideLoading();
											console.log(result);
											window.location.href = "{{Route('notif-zakat.wait')}}";
										},
										onError: function (result) {
											hideLoading();
											console.log(result);
											window.location.href = "{{Route('notif-zakat.wait')}}";
										},
										onClose: function(){
											hideLoading();
											iDtoken = data.snap_token;
											$.ajax({
												url: '{{ route("donasi.deleteZakat") }}',
												type: 'POST',
												dataType: 'json',
												data: {
													"_token": "{{ csrf_token() }}",
													"iDtoken": iDtoken,
												},
												success : function(data){
													// location.reload();
												}
											});
										}
									});
								} else {
									// Xendit Payment Method
									let modal_title = $("#modal_title")
									let modal_body = $("#modal_body")

									let type = $("#payment_type").val()
									let method = $("#payment_method").val()

									if (type == "credit_card") {
										$("#btn-credit-card").click();

										let cc = data.data_cc
										transaction_id = cc.transaction_id

										$("#credit-card-info").show();
										$("#credit-card-form").hide();

										$("#credit-card-logo").attr("src", $("#selected-method-img").attr("src"))
										$("#credit-card-guide").html("Klik tombol untuk melanjutkan pembayaran!")

										$("#credit-card-total").html(formatRupiah(zakat, "Rp. "))
										$("#credit-card-btn-label").html("<a href='"+cc.payer_authentication_url+"' onclick='confirm_pay(`step_cc_"+cc.id+"`)' target='_blank' style='color:white;'>Lajutkan Pembayaran</a>")
										$("#credit-card-btn-label").attr("onclick","confirm_pay('step_cc_"+cc.id+"')")

									} else if (type == "e_wallet") {
										let ew = data.data_ew
										transaction_id = ew.transaction_id
										$("#simple-pop-title").html(method)
										$("#simple-pop-total").html(formatRupiah(zakat, "Rp. "))

										$("#simple-pop-trans").hide()
										$("#simple-pop-logo").show()
										$("#simple-pop-logo").attr("src", $("#selected-method-img").attr("src"))

										if (method != "OVO") {
											$("#simple-pop-guide").html("Klik tombol untuk melanjutkan pembayaran!")
											$("#simple-pop-btn-label").html("<a href='"+ew.checkout_url+"' style='color:white;'>Lajutkan Pembayaran</a>")
											$("#simple-pop-btn-label").attr("onclick","")
										} else { // OVO
											// Popup OVO di awal, beda dari yg lain

											// $("#simple-pop-guide").html("<center>Periksa aplikasi OVO anda dan segera selesaikan transaksi dalam 30 detik!</center>")
											// $("#simple-pop-guide").append("<div style='margin-left:20px;'>"
											// 							+ "<br/><b>Notes :</b>"
											// 							+ "<br/>Apabila transaksi gagal, silahkan coba cara berikut :"
											// 							+ "<br/>1. Masukkan kembali nomor telepon dan coba kirim kembali pembayaran."
											// 							+ "<br/>2. Bersihkan cache aplikasi OVO anda."
											// 							+ "<br/>3. Apabila transaksi masih gagal, silahkan coba kembali dengan metode pembayaran yang lain."
											// 							+ "<br/>Anda TIDAK akan dikenakan biaya dua kali untuk mencoba kembali pembayaran."
											// 							+ "</div>")
											// $("#simple-pop-guide").attr("class","")
											// $("#simple-pop-btn-label").html("Saya Sudah Bayar")
											// $("#simple-pop-btn-label").attr("onclick","confirm_pay("+ew.id+")")
										}
										$("#btn-simple-pop").click();

									} else if (type == "retail_outlet") {
										let ro = data.data_ro
										transaction_id = ro.transaction_id
										$("#simple-pop-title").html(method)
										$("#simple-pop-total").html(formatRupiah(zakat, "Rp. "))

										$("#simple-pop-logo").hide()
										$("#simple-pop-trans").show()
										$("#simple-pop-trans-logo").attr("src", $("#selected-method-img").attr("src"))
										$("#simple-pop-trans-code").html(ro.payment_code)

										$("#simple-pop-guide").html("Harap simpan kode pembayaran di atas untuk melakukan pembayaran!")
										$("#simple-pop-btn-label").html("Selesaikan Pembayaran")
										$("#simple-pop-btn-label").attr("onclick","confirm_pay("+ro.id+")")

										$("#btn-simple-pop").click();

									} else if (type == "virtual_account") {
										$("#btn-virtual-akun").click();

										let va = data.data_va
										transaction_id = va.transaction_id

										$("#virtual-akun-total").html(formatRupiah(zakat, "Rp. "))
										$("#virtual-akun-bank").html($("#selected-method-label").text())

										$("#virtual-akun-trans-logo").attr("src", $("#selected-method-img").attr("src"))
										$("#virtual-akun-trans-code").html(va.account_number)
										$("#virtual-akun-caution").html("Batas transfer maksimal 1 hari dihitung dari sekarang atau donasi kamu otomatis dibatalkan oleh sistem")

										$("#virtual-akun-btn-label").html("Transaksi Selesai")
										$("#virtual-akun-btn-label").attr("onclick","confirm_pay("+va.id+")")
										showStepVA()
									}
									// End of Xendit Payment Method
								}
							}
						})
						.fail(function(xhr, status, error) {
							hideLoading();

							if ($("#payment_type").val() == "e_wallet" || $("#payment_type").val() == "retail_outlet") {
								$("#simple-pop").addClass('payment-method-hide');
								$("#simple-pop").removeClass('payment-method-active');

							} else if ($("#payment_type").val() == "virtual_account") {
								$("#virtual-akun").addClass('payment-method-hide');
								$("#virtual-akun").removeClass('payment-method-active');

							} else if ($("#payment_type").val() == "credit_card") {
								$("#credit-card").addClass('payment-method-hide');
								$("#credit-card").removeClass('payment-method-active');

							}

							// console.log(xhr, status, error)
							$("#show_modal").click();
							let modal_title = $("#modal_title")
							let modal_body = $("#modal_body")
							let message = xhr.responseJSON.message

							if (message == "Payment was not authorized")
								message = "Coba lagi dan segera selesaikan pembayaran."

							modal_title.html("PEMBAYARAN GAGAL!")
							modal_body.html("<p>"+message+"</p>")

							$.post("{{ route('transaksi.logerror') }}", {
								_token: '{{ csrf_token() }}',
								status_code: xhr.status,
								status_message: error + ", " + xhr.responseJSON.message
							})
						});
						return false;
					}
				}

            };

            function get_kategori()
            {
            	var category_id = $('#category_id').val();
            	$.ajax({
            		Type : 'GET',
            		dataType : 'json',
            		url : '{{url("get_data_kategori")}}/'+category_id,
            		success: function(data){
            			$('#category_produk').val(data.category)
            		}
            	});
            }

			// Xendit Payment Method
			function confirm_pay(id)
			{
				if (id == "close") {
					window.location.href = "{{Route('notif.wait')}}";
					return true
				}

				showLoading()

				let type = $("#payment_type").val()
				let method = $("#payment_method").val()

				if (type == "credit_card") {
					if (id.toString().includes("step_cc_")) {
						$("#credit-card-guide").html("Klik tombol untuk konfirmasi pembayaran anda!")

						$("#credit-card-btn-label").html("Saya Sudah Bayar")
						$("#credit-card-btn-label").attr("onclick","confirm_pay("+id.split('_')[2]+")")

						hideLoading()
					} else {
						$.post('{{url("api/credit_card/charge")}}/'+id,
							{
								_method	: 'POST',
								_token	: '{{ csrf_token() }}',
								card_cvn: $("#cc_cvn").val()
							},
							function (data, status) {
								console.log(data)
								if (data.status == 'error') {
									alert(data.message);
									hideLoading();
									window.location.href = "{{Route('notif.wait')}}";
								} else {
									alert(data.message);
									hideLoading();
									window.location.href = "{{Route('notif.wait')}}";
								}
							}
						);
					}
				} else if (type == "e_wallet") {
					hideLoading();
					window.location.href = "{{Route('notif.wait')}}";
				} else if (type == "retail_outlet") {
					hideLoading();
					window.location.href = "{{Route('notif.wait')}}";
				} else if (type == "virtual_account") {
					hideLoading();
					window.location.href = "{{Route('notif.wait')}}";
				}
				$("#close_modal").click()
			}

			function getPaymentMethod()
			{
				showLoading()

				var popup = $("#payment-popup-body");
				popup.html("")

				$.ajax({
					Type : 'GET',
					dataType : 'json',
					url : 'https://api.mizanamanah.or.id/api/payment_vendors/categories',
					success: function(response){

						response.data.forEach(element => {
							$.ajax({
								Type : 'GET',
								dataType : 'json',
								url : 'https://api.mizanamanah.or.id/api/payment_vendors/'+element.id,
								success: function(response2){

									if (response2.data.length > 0)
										popup.append('<p class="category-title">'+element.category+'</p>');

									response2.data.forEach(element2 => {
										let vendor = element2.vendor
										let type = element2.payment_type
										let method = (vendor == "midtrans") ? element2.midtrans_code : element2.xendit_code

										let category = ""
										if (element2.payment_type == "virtual_account") category = "Virtual Account "
										if (element2.payment_type == "manual_transfer") category = "Transfer "
										let label = category + element2.payment_name

										let img_src = "https://mizanamanah.or.id/api/metode/icon/"+element2.id
										popup.append('<div class="item-method" onclick="choose_method(`'+vendor+'`,`'+type+'`,`'+method+'`,`'+label+'`,`'+img_src+'`)">'+
														'<img class="logo-payment" src='+img_src+' alt="">'+
														'<p class="title-payment">'+element2.payment_name+'</p>'+
													'</div>');
									});
								}
							});

						});
						hideLoading()
					}
				});
			}

			function choose_method(vendor, type, method, label, img_src)
			{
				$("#mthd-close").click()
				$("#cc_form").hide()
				$("#ew_form").hide()

				$("#payment_vendor").val(vendor)
				$("#payment_type").val(type)
				$("#payment_method").val(method)
				// $("#payment_method_label").val(label)

				$("#selected-method-img").attr("src",img_src)
				$("#selected-method-label").html(label)
				$("#selected-method").show()
				$("#btn-payment-method").hide()


				let payment_type = $("#payment_type").val()
				var payment_method = $("#payment_method").val();

				if (payment_type == "credit_card") {
					// if (vendor == "xendit") $("#cc_form").show()

				} else if (payment_type == "e_wallet") {
					if (payment_method == 'gopay' || payment_method == 'DANA')
						$("#ew_form").hide();
					else
						$("#ew_form").show();

				} else if (payment_type == "retail_outlet") {

				} else if (payment_type == "virtual_account") {

				}
			}

			function showLoading()
			{
				$("#show_loading").click()
				$("#loading-caution").hide()
			}
			function hideLoading()
			{
				$("#loadMe").modal("hide");
			}

			function copyToClip(element_id)
			{
				var temp = $("<input>");
				$("body").append(temp);
				temp.val($("#"+element_id).text()).select();
				document.execCommand("copy");

				alert("Copied the text: " + temp.val());
				temp.remove();
			}
			function handleChecked(cb)
			{
				if (cb.checked) {
					$("#ew_phone_form").hide()
					$("#ew_phone_number").attr("disabled",true)
					$("#ew_phone_number").val("")
				} else {
					$("#ew_phone_form").show()
					$("#ew_phone_number").attr("disabled",false)
				}
			}

			function cancelTransaction()
			{
				var result = confirm("Apakah andah yakin untuk membatalkan transaksi?");
				if (result) {
					$.ajax({
						url: '{{ route("donasi.deleteZakat") }}',
						type: 'POST',
						dataType: 'json',
						data: {
							"_token": "{{ csrf_token() }}",
							"iDtransaction": transaction_id,
							"status": 1,
						},
						success : function(data){
							// location.reload();
						}
					});
				} else {
					if ($("#payment_type").val() == "e_wallet" || $("#payment_type").val() == "retail_outlet")
						setTimeout(() => $("#btn-simple-pop").click(), 100);
					else if (type == "credit_card")
						setTimeout(() => $("#btn-credit-card").click(), 100);
					else if (type == "virtual_account")
						setTimeout(() => $("#btn-virtual-akun").click(), 100);
				}
			}
			function showStepVA()
			{
				$("#virtual-akun-step-bni").hide()
				$("#virtual-akun-step-bri").hide()
				$("#virtual-akun-step-mandiri").hide()
				$("#virtual-akun-step-bca").hide()
				$("#virtual-akun-step-permata").hide()

				let method = $("#payment_method").val()
				if (method == "BNI")
					$("#virtual-akun-step-bni").show()
				else if (method == "BRI")
					$("#virtual-akun-step-bri").show()
				else if (method == "MANDIRI")
					$("#virtual-akun-step-mandiri").show()
				else if (method == "BCA")
					$("#virtual-akun-step-bca").show()
				else if (method == "PERMATA")
					$("#virtual-akun-step-permata").show()
			}
			// End of Xendit Payment Method
        </script>


    </body>

    </html>
