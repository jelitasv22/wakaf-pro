<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="SemiColonWeb" />
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,900|Caveat+Brush" rel="stylesheet"
type="text/css" />
<link rel="stylesheet" href="{{asset('themes/theme1/css/bootstrap.css')}}" type="text/css" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="{{asset('themes/theme1/style.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('themes/theme1/paymentmethod.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('themes/theme1/statistik.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('themes/theme1/css/swiper.css')}}" type="text/css" />

<link rel="stylesheet" href="{{asset('themes/theme1/css/dark.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('themes/theme1/css/font-icons.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('themes/theme1/css/animate.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('themes/theme1/css/magnific-popup.css')}}" type="text/css" />

<link rel="stylesheet" href="{{asset('themes/theme1/css/responsive.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('themes/theme1/css/calendar.css')}}" type="text/css" />

<!-- <link rel="stylesheet" href="{{asset('themes/theme1/css/colors.php?color=e91d24')}}" type="text/css" /> -->
<link rel="stylesheet" href="{{asset('themes/theme1/demos/nonprofit/css/fonts.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('themes/theme1/demos/nonprofit/nonprofit.css')}}" type="text/css" />

<!-- <link rel="stylesheet" type="text/css" href="{{asset('themes/theme1/css/component/datepicker.css')}}}"> -->
<link href="{{asset('admin/assets/vendors/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('admin/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="{{asset('themes/theme1/images/mizan-bulat.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('themes/theme1/images/mizan-bulat.png')}}">
<style type="text/css">
	@media only screen and (max-width: 425px) {
		#menu_item {
			width: 100% !important;
			height: 30px !important;
			text-align: left;
			margin: 0px;
			padding: 0px;
		}
	}
</style>
<script type='text/javascript'>
	function loadCSS(e, t, n) { "use strict"; var i = window.document.createElement("link"); var o = t || window.document.getElementsByTagName("script")[0]; i.rel = "stylesheet"; i.href = e; i.media = "only x"; o.parentNode.insertBefore(i, o); setTimeout(function () { i.media = n || "all" }) }
	loadCSS("https://cdn.plyr.io/3.4.7/plyr.css");
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PTZQ9TQ');</script>
<!-- End Google Tag Manager -->
<meta name='viewport' content='initial-scale=1, viewport-fit=cover'>
