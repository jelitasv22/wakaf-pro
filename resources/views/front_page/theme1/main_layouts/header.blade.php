<header id="header" class="clearfix static-sticky">
	<div id="header-wrap">
		<div class="container clearfix">
			<div id="primary-menu-trigger">
				<i class="icon-reorder"></i>
			</div>

			<!-- Logo ============================================= -->
			<div id="logo">
				<a href="{{url('home')}}" class="standard-logo" data-dark-logo="images/mizan-panjang.png">
					<img src="{{asset('themes/theme1/images/logo-laz-mizan-amanah.png')}}" alt="Canvas Logo" width="140px;" style="margin-top: 0px; margin-bottom: 20px;">
				</a>
				<a href="{{url('home')}}" class="retina-logo" data-dark-logo="images/mizan-panjang.png">
					<img src="{{asset('themes/theme1/images/mizan-panjang.png')}}" alt="Canvas Logo">
				</a>
			</div>
			<!-- #logo end -->

			<nav id="primary-menu" class="d-lg-flex d-xl-flex justify-content-xl-between justify-content-lg-between fnone with-arrows">
				<div id="menu-web">
					<ul class="align-self-start">
						<li><span class="menu-bg col-auto align-self-start d-flex"></span></li>
						@foreach($header as $head)
						<li>
							@if($head->url != "#")
								@if($head->judul_menu != 'Qurban')
									<a href="{{Route('front_page.'.$head->url)}}">
										<div>{{$head->judul_menu}}</div>
									</a>
								@else
									<a href="{{$head->url}}" target="blank">
										<div>{{$head->judul_menu}}</div>
									</a>
								@endif
							@else

							<a href="javascript:0;">{{$head->judul_menu}} <i class="icon-angle-down1"></i></a>
							<ul>
								@foreach($header_body as $body)
									@if($body->id_sub_menu == $head->id)
										@if($body->judul_menu != 'Konser Amal')
										<li>
											<a href="{{ Route('front_page.'.$body->url) }}">{{$body->judul_menu}}</a>
										</li>
										@else
										<li>
											<a href="{{$body->url}}" target="blank">{{$body->judul_menu}}</a>
										</li>
										@endif
									@endif
								@endforeach
							</ul>

							@endif
						</li>
						@endforeach
					</ul>
				</div>

				<div id="menu-mobile">
					<ul class="align-self-start">
						<li><span class="menu-bg col-auto align-self-start d-flex"></span></li>
						@foreach($header as $head)
						<li>
							@if($head->url != "#")
								@if($head->judul_menu != 'Qurban')
									<a href="{{Route('front_page.'.$head->url)}}">
										<div>{{$head->judul_menu}}</div>
									</a>
								@else
									<a href="{{$head->url}}" target="blank">
										<div>{{$head->judul_menu}}</div>
									</a>
								@endif
							@else

							@foreach($header_body as $body)
							@if($body->id_sub_menu == $head->id)
							<li>
								<a href="{{ Route('front_page.'.$body->url) }}">{{$body->judul_menu}}</a>
							</li>
							@endif
							@endforeach

							@endif
						</li>
						@endforeach

						@if(Session::get('id') != NULL)
						<li>
							<a href="{{Route('front_page.logout')}}">
								<div>@lang('language.keluar')</div>
							</a>
						</li>
						<li>
							<a href="{{Route('front_page.dash_donatur')}}">
								<div>Dashboard Donatur</div>
							</a>
						</li>
						@else
						<li>
							<a href="{{Route('front_page.login')}}">
								<div>@lang('language.masuk')</div>
							</a>
						</li>
						<li>
							<a href="{{Route('front_page.register')}}">
								<div>@lang('language.daftar_akun')</div>
							</a>
						</li>
						@endif
					</ul>
				</div>

				<div id="menu-web-login">
					<ul class="not-dark align-self-end">
						<li>
							@if(Session::get('id') != null)
							@if(file_exists(asset("admin/assets/media/foto-users/245/session::get('foto')")))
							<a href="#"><img style="width: 30px; border-radius: 50%;" src="{{asset('admin/assets/media/foto-users/245')}}/{{session::get('foto')}}"><i class="icon-angle-down1"></i>
							</a>
							@else
							<a href="#"><i class="icon-user-circle"></i><i class="icon-angle-down1"></i></a>
							@endif
							@else
							<a href="#"><i class="icon-user-circle"></i><i class="icon-angle-down1"></i></a>
							@endif
							<ul>
								@if(Session::get('id') != NULL)
								<li>
									<a href="{{Route('front_page.logout')}}">
										<div>@lang('language.keluar')</div>
									</a>
								</li>
								<li>
									<a href="{{Route('front_page.dash_donatur')}}">
										<div>Dashboard Donatur</div>
									</a>
								</li>
								@else
								<li>
									<a href="{{Route('front_page.login')}}">
										<div>@lang('language.masuk')</div>
									</a>
								</li>
								<li>
									<a href="{{Route('front_page.register')}}">
										<div>@lang('language.daftar_akun')</div>
									</a>
								</li>
								@endif
							</ul>
						</li>
					</ul>
				</div>
			</nav><!-- #primary-menu end -->
		</div>
	</div>
</header>