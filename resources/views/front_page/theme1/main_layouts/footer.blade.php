<div id="copyrights" style="padding-top: 40px; padding-bottom: 0px;">

	<div class="container clearfix">

		<div class="col_full nobottommargin center">
			<div class="copyrights-menu copyright-links clearfix">
				@foreach($footer as $foot)
				<a href="{{url($foot->url)}}" id="menu_item">{{$foot->judul_menu}}</a>
				@endforeach

			</div>

			<div class="copyrights-menu copyright-links clearfix">
				<ul class="nav" style="display: block;">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false" style="margin: 0px;" id="menu_item">
							<span style="color: white;">Syarat dan Ketentuan</span>
						</a>
						<ul class="dropdown-menu" style="width: 250px;">
							@foreach($footer_sub as $foot_sub)

							<a class="dropdown-item" href="{{url($foot_sub->url)}}" style="color: black; margin: 0px;">{{$foot_sub->judul_menu}}</a>

							@endforeach
						</ul>
					</li>
				</ul>
			</div>
			
			<div class="copyrights-menu copyright-links clearfix">
				<form action="{{Route('front_page.switch_language')}}" method="post" id="switch_lang">
					{{ csrf_field() }}
					<input type="hidden" id="lang" name="lang">
					<a href="#" id="id" onclick="cek_lang_id()" style="margin-right: 0px;" title="Indonesia">
						@if($language == 1 && $language != 2)
						<img src="{{asset('themes/theme1/images/id.png')}}" style="width:25px;">
						@else
						<img src="{{asset('themes/theme1/images/id.png')}}" style="width:25px; -webkit-filter:grayscale(100%); filter: grayscale(100%);">
						@endif
					</a>

					<a href="#" id="en" onclick="cek_lang_en()" style="margin-right: 0px;" title="English"> 
						@if($language == 2 && $language != 1)
						<img src="{{asset('themes/theme1/images/en.png')}}" style="width:25px;">
						@else
						<img src="{{asset('themes/theme1/images/en.png')}}" style="width:25px; -webkit-filter:grayscale(100%); filter: grayscale(100%);">
						@endif
					</a>
				</form>
			</div>
		</div>

		<div class="si-share clearfix" style="margin-bottom: 30px; border: 0px;">
			<span style="color: white;">
				Jl. Ulujami Raya No.111 Kel. Ulujami Kec. Pesanggrahan Jakarta Selatan - Telp. (021) 2765 9993<br>
				Copyrights © {{date('Y')}} All Rights Reserved by Mizan Amanah.
			</span>
			<div>
				<a href="{{ Helper::sosmed_facebook() }}" class="social-icon si-borderless si-text-color si-facebook" title="Facebook" style="background-color: white; border-radius: 50px;" target="_blank">
					<i class="icon-facebook"></i>
					<i class="icon-facebook"></i>
				</a>
				<a href="{{ Helper::sosmed_twitter() }}" class="social-icon si-borderless si-text-color si-twitter" title="Twitter" style="background-color: white; border-radius: 50px; margin-left: 2px;" target="_blank">
					<i class="icon-twitter"></i>
					<i class="icon-twitter"></i>
				</a>
				<a href="{{ Helper::sosmed_instagram() }}" class="social-icon si-borderless si-text-color si-instagram" title="Instagram" style="background-color: white; border-radius: 50px; margin-left: 2px;" target="_blank">
					<i class="icon-instagram2"></i>
					<i class="icon-instagram2"></i>
				</a>

				<a href="https://www.youtube.com/c/MizanAmanahLaznas" class="social-icon si-borderless si-text-color si-youtube" title="Youtube" style="background-color: white; border-radius: 50%; margin-left: 2px;" target="_blank">
					<i class="icon-youtube"></i>
					<i class="icon-youtube"></i> 
				</a>
			</div>
		</div>
	</div>

</div>

<script>
	function cek_lang_id(){
        // alert("id")
        $("#lang").val("id")
        $("#switch_lang").submit()
    }

    function cek_lang_en(){
        // alert("en")
        $("#lang").val("en")
        $("#switch_lang").submit()
    }
</script>