<!DOCTYPE html>
<html dir="ltr" lang="en-US">

<head>
	<!--css include from main_layouts-->
	@include('front_page.theme1.main_layouts.css')
	<!--end css-->
	<title>Lembaga Amil Zakat Nasional | Mizan Amanah</title>

</head>
<body class="stretched">
	<div id="" class="clearfix">
		@include('front_page.theme1.main_layouts.header')
		@include('front_page.theme1.components.slider')
		<section id="content" style="overflow: visible">
			<div class="content-wrap p-0">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1382 58" width="100%" height="60" preserveAspectRatio="none" style="position: absolute; top: -58px; left:0; z-index: 1">
					<path style="fill:#FFF;" d="M1.52.62s802.13,127,1380,0v56H.51Z" />
				</svg>
				<br><br>	
				<div class="container">
					<?php 
					$total_zakat = number_format($total_nilai_zakat->total_zakat);
					$total_donasi = number_format($total_nilai_donasi->total_donasi);
					$total_donatur = number_format($total_donatur_tergabung);
					?>
					<div class="statistik-box">
						<div class="item-statistik">
							<p class="nominal-text"><?=str_replace(',','.', $total_zakat)?></p>
							<p class="title-text">Zakat Terkumpul</p>
						</div>
						<div class="item-statistik">
							<p class="nominal-text"><?=str_replace(',','.', $total_donasi)?></p>
							<p class="title-text">Donasi Terkumpul</p>
						</div>
						<div class="item-statistik">
							<p class="nominal-text"><?=str_replace(',','.', $total_donatur)?></p>
							<p class="title-text">Donatur Tergabung</p>
						</div>
					</div>
				</div>
				<br><br><br><br>
				@include('front_page.theme1.components.program_infaq_zakat')

				@include('front_page.theme1.components.program')

				@include('front_page.theme1.components.update_artikel')

				@include('front_page.theme1.components.kisah_sukses')
				<br><br><br><br>
				@include('front_page.theme1.components.mitra_perusahaan')
				<div class="clear"></div>  
			</div>
		</section>

		<footer id="footer" style="background-color: #002D40; ">
			<div class="container">
				@include('front_page.theme1.components.footer_widget')
			</div>
			@include('front_page.theme1.main_layouts.footer')
		</footer>

	</div>

	<!-- <div id="gotoTop" class="icon-angle-up" style="bottom: 100px;"></div> -->

	@include('front_page.theme1.main_layouts.js')
</body>

</html>