<script src="{{asset('themes/theme1/js/jquery.js')}}"></script>
<script src="{{asset('themes/theme1/js/plugins.js')}}"></script>
<script src="{{asset('themes/theme1/demos/nonprofit/js/events.js')}}"></script>
<script src="{{asset('themes/theme1/js/functions.js')}}"></script>
<script src="{{asset('themes/theme1/js/components/bs-datatable.js')}}"></script>
<script src="{{asset('themes/theme1/js/components/moment.js')}}"></script>
<script src="{{asset('themes/theme1/js/components/datepicker.js')}}"></script>
<script src="{{asset('admin/assets/vendors/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/vendors/custom/js/vendors/sweetalert2.init.js')}}" type="text/javascript"></script>

<!-- Google Tag Manager (noscript) -->
<noscript>
	<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTZQ9TQ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- end google tag manager -->

<!--bitrix live chat-->
<script>
	(function(w,d,u){
		var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
		var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
	})(window,document,'https://cdn.bitrix24.com/b13054133/crm/site_button/loader_1_73lho4.js');
</script>
<!-- end live chat -->

<!-- Global site tag (gtag.js) - Google Ads: 955472882 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-955472882"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'AW-955472882');
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-74860308-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-74860308-1');
</script>

<!-- Facebook Pixel Code -->
	<script type="text/javascript">

		$(document).ready(function(){
			$("#tanggal_konfirmasi").datepicker({
				dateFormat : "Y/m/d",
				todayHighlight: true,
				autoclose: true,
				pickerPosition: 'bottom-left'
			});
		});

		document.addEventListener("DOMContentLoaded",
			function() {
				var div, n,
				v = document.getElementsByClassName("youtube-player");
				for (n = 0; n < v.length; n++) {
					div = document.createElement("div");
					div.setAttribute("data-id", v[n].dataset.id);
					div.innerHTML = loadThumb(v[n].dataset.id);
					div.onclick = loadIframe;
					v[n].appendChild(div);
				}
			});

		function loadThumb(id) {
			var thumb = '<img src="https://i.ytimg.com/vi/ID/hqdefault.jpg">',
			play = '<div class="play"></div>';
			return thumb.replace("ID", id) + play;
		}

		function loadIframe() {
			var iframe = document.createElement("iframe");
			var embed = "https://www.youtube.com/embed/ID?autoplay=1";
			iframe.setAttribute("src", embed.replace("ID", this.dataset.id));
			iframe.setAttribute("frameborder", "0");
			iframe.setAttribute("allowfullscreen", "1");
			this.parentNode.replaceChild(iframe, this);
		}


	</script>

	<script type='text/javascript' src='https://cdn.plyr.io/3.4.7/plyr.js'></script>
	<script type="text/javascript">
		const players = Array.from(document.querySelectorAll('.js-player')).map(p => new Plyr(p));
		const plyrs = Array.from(document.querySelectorAll('.js-plyr')).map(plyrs => new Plyr(plyrs));
	</script>

	<script type="text/javascript">
		$(document).ready(function() {
			const width = $(window).width()

			if(width <= 768){
				$('#menu-web').hide();
				$('#menu-mobile').show();
				$('#menu-web-login').hide();
			}else{
				$('#menu-web').show();
				$('#menu-mobile').hide();
			}
		});

	</script>
