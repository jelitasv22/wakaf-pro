<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Helping Hand - Fund rise for great events</title>

    @include('front_page.ngo-theme-3.main_layouts.css')


</head>
<body>
   
    @include('front_page.ngo-theme-3.main_layouts.header2')
   
    <section class="row blog-content page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                                       
                    <article class="post post-type-image row">
                        <div class="row featured-contents">
                            <a href="single.html"><img src="images/post2/1.jpg" alt=""></a>
                        </div>
                        <div class="row article-body">
                            <h3 class="post-title"><a href="single.html">SEMINAR FOR Childrens to know about FUTURE</a></h3>
                            <ul class="post-meta nav">
                                <li class="post-date"><i class="fa fa-calendar-o"></i> <a href="#">june 29, 2016</a></li>       
                                <li class="post-comments"><i class="fa fa-comments"></i> <a href="#">1 comment</a></li>
                                <li class="posted-by"><i class="fa fa-user"></i>posted by: <a href="#">admin</a></li>
                                <li class="category"><i class="fa fa-folder"></i>category: <a href="#">education</a>, <a href="#">food</a></li>
                            </ul>
                            <div class="post-excerpt row">
                                <p>Quisque eros leo, pellentesque id leo non, scelerisque hendrerit mauris. Integer dapibus purus in aliquet vehicula. In laoreet justo ac sapien malesuada laoreet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            </div>
                            <a href="single.html" class="btn-primary btn-outline dark">read more</a>
                        </div>
                    </article>
                    
                    <article class="post post-type-text row">
                        <div class="row article-body">
                            <h3 class="post-title"><a href="single.html">EVENT FOR PROVIDING PENS TO THE CHILDRENS</a></h3>
                            <ul class="post-meta nav">
                                <li class="post-date"><i class="fa fa-calendar-o"></i> <a href="#">june 29, 2016</a></li>       
                                <li class="post-comments"><i class="fa fa-comments"></i> <a href="#">1 comment</a></li>
                                <li class="posted-by"><i class="fa fa-user"></i>posted by: <a href="#">admin</a></li>
                                <li class="category"><i class="fa fa-folder"></i>category: <a href="#">education</a>, <a href="#">food</a></li>
                            </ul>
                            <div class="post-excerpt row">
                                <p>Quisque eros leo, pellentesque id leo non, scelerisque hendrerit mauris. Integer dapibus purus in aliquet vehicula. In laoreet justo ac sapien malesuada laoreet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            </div>
                            <a href="single.html" class="btn-primary btn-outline dark">read more</a>
                        </div>
                    </article>
                    
                    <article class="post post-type-gallery row">
                        <div class="row featured-contents">
                            <div class="post-gallery">
                                <div class="item"><img src="images/post2/2.jpg" alt=""></div>
                                <div class="item"><img src="images/post2/1.jpg" alt=""></div>
                            </div>
                        </div>
                        <div class="row article-body">
                            <h3 class="post-title"><a href="single.html">EVENT FOR PROVIDING PENS TO THE CHILDRENS</a></h3>
                            <ul class="post-meta nav">
                                <li class="post-date"><i class="fa fa-calendar-o"></i> <a href="#">june 29, 2016</a></li>       
                                <li class="post-comments"><i class="fa fa-comments"></i> <a href="#">1 comment</a></li>
                                <li class="posted-by"><i class="fa fa-user"></i>posted by: <a href="#">admin</a></li>
                                <li class="category"><i class="fa fa-folder"></i>category: <a href="#">education</a>, <a href="#">food</a></li>
                            </ul>
                            <div class="post-excerpt row">
                                <p>Quisque eros leo, pellentesque id leo non, scelerisque hendrerit mauris. Integer dapibus purus in aliquet vehicula. In laoreet justo ac sapien malesuada laoreet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            </div>
                            <a href="single.html" class="btn-primary btn-outline dark">read more</a>
                        </div>
                    </article>
                    
                    <article class="post post-type-video row">
                        <div class="row featured-contents">
                            <iframe src="https://www.youtube.com/embed/EDZqaB4qXIc" allowfullscreen></iframe>
                        </div>
                        <div class="row article-body">
                            <h3 class="post-title"><a href="single.html">EVENT FOR PROVIDING PENS TO THE CHILDRENS</a></h3>
                            <ul class="post-meta nav">
                                <li class="post-date"><i class="fa fa-calendar-o"></i> <a href="#">june 29, 2016</a></li>       
                                <li class="post-comments"><i class="fa fa-comments"></i> <a href="#">1 comment</a></li>
                                <li class="posted-by"><i class="fa fa-user"></i>posted by: <a href="#">admin</a></li>
                                <li class="category"><i class="fa fa-folder"></i>category: <a href="#">education</a>, <a href="#">food</a></li>
                            </ul>
                            <div class="post-excerpt row">
                                <p>Quisque eros leo, pellentesque id leo non, scelerisque hendrerit mauris. Integer dapibus purus in aliquet vehicula. In laoreet justo ac sapien malesuada laoreet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            </div>
                            <a href="single.html" class="btn-primary btn-outline dark">read more</a>
                        </div>
                    </article>
                    
                    <article class="post post-type-audio row">
                        <div class="row featured-contents">
                            <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/157942432&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                        </div>
                        <div class="row article-body">
                            <h3 class="post-title"><a href="single.html">Donec at urna tincidunt, maximus turpis</a></h3>
                            <ul class="post-meta nav">
                                <li class="post-date"><i class="fa fa-calendar-o"></i> <a href="#">june 29, 2016</a></li>       
                                <li class="post-comments"><i class="fa fa-comments"></i> <a href="#">1 comment</a></li>
                                <li class="posted-by"><i class="fa fa-user"></i>posted by: <a href="#">admin</a></li>
                                <li class="category"><i class="fa fa-folder"></i>category: <a href="#">education</a>, <a href="#">food</a></li>
                            </ul>
                            <div class="post-excerpt row">
                                <p>Quisque eros leo, pellentesque id leo non, scelerisque hendrerit mauris. Integer dapibus purus in aliquet vehicula. In laoreet justo ac sapien malesuada laoreet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                            </div>
                            <a href="single.html" class="btn-primary btn-outline dark">read more</a>
                        </div>
                    </article>
                    
                    <article class="post post-type-quote row">                        
                        <div class="row article-body">
                            <blockquote>
                                <p>“EVENT FOR PROVIDING PENS TO THE CHILDRENS”</p>
                                <footer>Source of quote</footer>
                            </blockquote>
                        </div>
                    </article>
                    
                    <article class="post post-type-link row">
                        <div class="row article-body">
                            <h3 class="post-title"><a href="single.html">Donec at urna tincidunt, maximus turpis</a></h3>
                            <ul class="post-meta nav">
                                <li class="post-date"><i class="fa fa-calendar-o"></i> <a href="#">june 29, 2016</a></li>       
                                <li class="post-comments"><i class="fa fa-comments"></i> <a href="#">1 comment</a></li>
                                <li class="posted-by"><i class="fa fa-user"></i>posted by: <a href="#">admin</a></li>
                                <li class="category"><i class="fa fa-folder"></i>category: <a href="#">education</a>, <a href="#">food</a></li>
                            </ul>
                            <div class="post-excerpt row">
                                <p>Curabitur eget felis gravida, molestie eros in, pretium eros. Aenean tempus justo vitae nisl volutpat, viverra euismod urna hendrerit. Duis a purus ligula. In nec suscipit tellus. Morbi dapibus leo eros, at facilisis est auctor vel. Etiam nibh justo, hendrerit sit amet finibus sed, placerat at libero.</p>
                            </div>
                            <a href="single.html" class="btn-primary btn-outline dark">read more</a>
                        </div>
                    </article>
                    
                    <ul class="pagination">
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                    </ul>
                    
                </div>
                <div class="col-md-4 sidebar post-sidebar">
                    <div class="row m0 widget widget-search">
                        <h4 class="widget-title">search</h4>
                        <form action="#" class="row m0 search-form" method="get" role="search">
                            <div class="input-group">
                                <input type="search" class="form-control" placeholder="Search here">
                                <span class="input-group-addon"><button type="submit"><i class="fa fa-search"></i></button></span>
                            </div>
                        </form>
                    </div>
                    
                    <div class="row m0 widget widget-category">
                        <h4 class="widget-title">categories</h4>
                        <ul class="nav">
                            <li><a href="#">education</a></li>
                            <li><a href="#">environment</a></li>
                            <li><a href="#">building homes</a></li>
                            <li><a href="#">donating clothes</a></li>
                            <li><a href="#">food</a></li>
                            <li><a href="#">OTHERS</a></li>
                        </ul>
                    </div>
                    
                    <div class="row m0 widget widget-recent-posts">
                        <h4 class="widget-title">recent post</h4>
                        <div class="media recent-post">
                            <div class="media-left"><a href="single.html"><img src="images/post2/recent1.jpg" alt=""></a></div>
                            <div class="media-body">
                                <h5 class="title"><a href="#">EVENT FOR PROVIDING PENS TO THE CHILDRENS monthly base</a></h5>
                                <h5 class="date"><i class="fa fa-calendar-o"></i><a href="#">June 29, 2015</a></h5>
                            </div>
                        </div>
                        <div class="media recent-post">
                            <div class="media-left"><a href="single.html"><img src="images/post2/recent2.jpg" alt=""></a></div>
                            <div class="media-body">
                                <h5 class="title"><a href="#">MEDICAL CHECKUP CAMP FOR CHILDRENS IN AFRICA</a></h5>
                                <h5 class="date"><i class="fa fa-calendar-o"></i><a href="#">June 28, 2015</a></h5>
                            </div>
                        </div>
                        <div class="media recent-post">
                            <div class="media-left"><a href="single.html"><img src="images/post2/recent3.jpg" alt=""></a></div>
                            <div class="media-body">
                                <h5 class="title"><a href="#">EVENT FOR GATHERING BOOKS FOR CHILDRENS</a></h5>
                                <h5 class="date"><i class="fa fa-calendar-o"></i><a href="#">June 27, 2015</a></h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row m0 widget widget-tags">
                        <h4 class="widget-title">Tags</h4>
                        <a href="#" class="tag">causes</a>
                        <a href="#" class="tag">donate</a>
                        <a href="#" class="tag">child</a>
                        <a href="#" class="tag">care</a>
                        <a href="#" class="tag">children help</a>
                        <a href="#" class="tag">donate</a>
                        <a href="#" class="tag">child</a>
                        <a href="#" class="tag">causes</a>
                        <a href="#" class="tag">donate</a>
                        <a href="#" class="tag">child</a>
                        <a href="#" class="tag">care</a>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>
    
    @include('front_page.ngo-theme-3.main_layouts.footer')

    
    @include('front_page.ngo-theme-3.main_layouts.js')
    
</body>
</html>