<script type="text/javascript">
	$(document).ready(function() {
		$('#paginate').pagination({
			ajax: function(options, refresh, target){
				$.ajax({
					url: '{{ENV("API_URL")}}/api/ngo-posting?page='+options.current+'&lang={{$language_ses}}&yayasan={{ENV("YAYASAN_KEY")}}',
					type: 'GET',
					dataType: 'Json',
				})
				.done(function(res) {
					refresh({
						total: res.paginate.total,
						length: res.paginate.length 
					});

					$('#list_posting').html('');
					$.each(res.paginate.data, function(index, val) {
						$('#list_posting').append('<div class="col-md-4 col-sm-6 col-lg-4">'+
							'<div class="blg-bx">'+
								'<div class="blg-thmb">'+
									'<a href="" title="" itemprop="url"><img src="{{ENV("BACKEND_URL")}}/admin/assets/media/posting/'+val.image+'" alt="" itemprop="image"></a>'+
									'<span class="blg-dat"><a href="#" title="" itemprop="url">'+val.created_at+'</a></span>'+
								'</div>'+
								'<div class="blg-inf">'+
									'<h4 itemprop="headline"><a href="{{ url("posting/detail-posting") }}/'+val.seo+'" title="" itemprop="url">'+val.judul+'</a></h4>'+
									'<p>'+val.content.substr(0, 250)+'. . .</p>'+
									'<a href="{{ url("posting/detail-posting") }}/'+val.seo+'" class="link-more">'+
										'<span>Read More</span>'+
									'</a>'+
								'</div>'+
							'</div>'+
						'</div>');
					});
				}).fail(function(error){
					console.log(error);
				});
			}
		});
	});
</script>