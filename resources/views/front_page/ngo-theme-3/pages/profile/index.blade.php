<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header2')

		<div class="gray-bg3 brdcrmb-wrp">
            <div class="container">
                <div class="brdcrmb-inr flex justify-content-between">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('home') }}" title="" itemprop="url">Home</a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
        </div><!-- Breadcrumbs Wrap -->
        <section>
            <div class="block">
                <div class="container">
                    <h2 class="sc_donations_title sc_item_title" style="font-size: 25px; margin-top: 15px;">{{$profile->page->judul_page}}</h2>

                    {!! $profile->page->konten_page !!}
                </div>
            </div>
        </section>


        @include('front_page.ngo-theme-2.main_layouts.footer')

    </main><!-- Main Wrapper -->

    @include('front_page.ngo-theme-2.main_layouts.js')
</body>	
</html>