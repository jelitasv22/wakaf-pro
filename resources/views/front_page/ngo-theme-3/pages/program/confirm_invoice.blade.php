<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header2')

		
		<div class="gray-bg3 brdcrmb-wrp" >
			<div class="container">
				<div class="brdcrmb-inr flex justify-content-between">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('home') }}" title="" itemprop="url">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('program') }}" title="" itemprop="url">Program</a></li>
						<li class="breadcrumb-item active">Invoice</li>
					</ol>
				</div>
			</div>
		</div><!-- Breadcrumbs Wrap -->
		<section>
			<div class="block">
				<div class="container">
					<h4 style="text-align: center; margin-bottom: 20px;">Invoice Pembayaran</h4>
					<table class="table table-striped">
						<tr>
							<th style="color: black; font-weight: bold; font-size: 0.875em; width: 50%;">No Invoice</th>
							<td style="color: black; font-weight: normal;">{{$confirm->confirm->snap_token}}</td>
						</tr>
						<tr>
							<th style="color: black; font-weight: bold; font-size: 0.875em; width: 50%;">Program</th>
							<td style="color: black; font-weight: normal;">{{$confirm->confirm->judul}}</td>
						</tr>
						<tr>
							<th style="color: black; font-weight: bold; width: 50%;">Nama Donatur</th>
							<td style="color: black;">
								<?php
								if($confirm->confirm->name == null){
									echo $confirm->confirm->nama_lengkap;
								}else{
									echo $confirm->confirm->name;
								}
								?>
							</td>
						</tr>
						<tr>
							<th style="color: black; font-weight: bold; width: 50%;">Metode Pembayaran</th>
							<td style="color: black;">{{$confirm->confirm->metode_pembayaran}}</td>
						</tr>
						<tr>
							<th style="color: black; font-weight: bold; width: 50%;">Total Donasi</th>
							<td style="color: black;">Rp. {{number_format($confirm->confirm->total_donasi)}}</td>
						</tr>
						<tr>
							<th style="color: black; font-weight: bold; width: 50%;">Status Pembayaran</th>
							<td style="color: black;">{{$confirm->confirm->status}}</td>
						</tr>
					</table>

					<h4 class="sc_donations_title sc_item_title" style="font-size: 25px; margin-top: 5px; text-align: left;">Intruksi Pembayaran</h4>
					<p>Silahkan Lakukan Konfirmasi Pembayaran dengan Cara Klik Tombol Konfirmasi di Bawah Ini. Mohon Lengkapi Data Konfirmasi Pembayaran Anda.</p>

					<center style="margin-top: 10px; margin-bottom: 10px;">
						<a href="{{ url('program/confirm-pay?no_invoice=') }}{{$confirm->confirm->snap_token}}" class="thm-btn2" style="padding: 10px; border-radius: 5px; margin-top: 10px; width: 100%;">
							<span class="post_readmore_label">Konfirmasi Pembayaran</span>
						</a>
					</center>

				</div>
			</div>
		</section>

		@include('front_page.ngo-theme-2.main_layouts.footer')

	</main><!-- Main Wrapper -->

	@include('front_page.ngo-theme-2.main_layouts.js')
</body>	
</html>