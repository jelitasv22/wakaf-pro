<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Helping Hand - Fund rise for great events</title>

    @include('front_page.ngo-theme-3.main_layouts.css')


</head>
<body>
   
    @include('front_page.ngo-theme-3.main_layouts.header2')
   
    <section class="row gallery-content">
        <div class="container">
            <div class="row sectionTitle text-center">
                <h6 class="label label-default">how you could help</h6>
                <h3>WE NEED YOUR HELP TO HELP OTHERS, SEE OUR CAUSES gallery</h3>
            </div>
            
            <div class="row m0 filters_row">
                <ul class="gallery-filter causes-filter nav pull-left">
                    <li data-filter class="active">ALL</li>
                    <li data-filter=".education">EDUCATION</li>
                    <li data-filter=".environment">ENVIRONMENT</li>
                    <li data-filter=".buiding-homes">BUILDING HOMES</li>
                    <li data-filter=".donating-clothes">DONATING CLOTHES</li>
                    <li data-filter=".food">food</li>
                    <li data-filter=".others">others</li>
                </ul>
                
                <ul class="nav pull-right view-filter">
                    <li class="active"><a href="cause-listing-grid.html"><i class="glyphicon glyphicon-th"></i></a></li>
                    <li><a href="cause-listing-list.html"><i class="fa fa-bars"></i></a></li>
                </ul>
            </div>
            
            <div class="row">
                <div class="causes_container popup-gallery">
                    <div class="col-sm-6 col-md-4 grid-sizer"></div>
                    <div class="col-sm-6 col-md-4 cause-item education">
                        <div class="images_row row m0">
                            <div id="causes_carousel_inner" class="carousel slide" data-ride="carousel" data-interval="3000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active"><img src="images/cause01.jpg" alt=""></div>
                                    <div class="item"><img src="images/cause02.jpg" alt=""></div>
                                </div>

                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#causes_carousel_inner" data-slide-to="0" class="active"></li>
                                    <li data-target="#causes_carousel_inner" data-slide-to="1"></li>
                                </ol>
                            </div>
                            <a href="#donate_box" class="btn-primary">donate now</a>
                        </div>
                        <div class="cause_excepts row m0">
                            <h4 class="cuase_title"><a href="single-cause.html">GIVE EDUCATION TO CHILDRENS</a></h4>
                            <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                            <div class="row fund_progress m0">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                        <div class="percentage"><span class="counter">68</span>%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row fund_raises m0">                                
                                <div class="pull-left raised amount_box">
                                    <h6>raised</h6>
                                    <h3>$65,360</h3>
                                </div>
                                <div class="pull-left goal amount_box">
                                    <h6>goal</h6>
                                    <h3>$124,500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 cause-item environment">
                        <div class="images_row row m0">
                            <img src="images/cause02.jpg" alt="">
                            <a href="#donate_box" class="btn-primary">donate now</a>
                        </div>
                        <div class="cause_excepts row m0">
                            <h4 class="cuase_title"><a href="single-cause.html">HELP SENIOR CITIZENS</a></h4>
                            <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                            <div class="row fund_progress m0">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                        <div class="percentage"><span class="counter">68</span>%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row fund_raises m0">                                
                                <div class="pull-left raised amount_box">
                                    <h6>raised</h6>
                                    <h3>$65,360</h3>
                                </div>
                                <div class="pull-left goal amount_box">
                                    <h6>goal</h6>
                                    <h3>$124,500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 cause-item buiding-homes">
                        <div class="images_row row m0">
                            <img src="images/cause03.jpg" alt="">
                            <a href="#donate_box" class="btn-primary">donate now</a>
                        </div>
                        <div class="cause_excepts row m0">
                            <h4 class="cuase_title"><a href="single-cause.html">HELP US PRINTING 100 T-SHIRTS</a></h4>
                            <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                            <div class="row fund_progress m0">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                        <div class="percentage"><span class="counter">68</span>%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row fund_raises m0">                                
                                <div class="pull-left raised amount_box">
                                    <h6>raised</h6>
                                    <h3>$65,360</h3>
                                </div>
                                <div class="pull-left goal amount_box">
                                    <h6>goal</h6>
                                    <h3>$124,500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 cause-item donating-clothes">
                        <div class="images_row row m0">
                            <img src="images/cause02.jpg" alt="">
                            <a href="#donate_box" class="btn-primary">donate now</a>
                        </div>
                        <div class="cause_excepts row m0">
                            <h4 class="cuase_title"><a href="single-cause.html">HELP SENIOR CITIZENS</a></h4>
                            <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                            <div class="row fund_progress m0">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                        <div class="percentage"><span class="counter">68</span>%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row fund_raises m0">                                
                                <div class="pull-left raised amount_box">
                                    <h6>raised</h6>
                                    <h3>$65,360</h3>
                                </div>
                                <div class="pull-left goal amount_box">
                                    <h6>goal</h6>
                                    <h3>$124,500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 cause-item food">
                        <div class="images_row row m0">
                            <img src="images/cause03.jpg" alt="">
                            <a href="#donate_box" class="btn-primary">donate now</a>
                        </div>
                        <div class="cause_excepts row m0">
                            <h4 class="cuase_title"><a href="single-cause.html">HELP US PRINTING 100 T-SHIRTS</a></h4>
                            <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                            <div class="row fund_progress m0">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                        <div class="percentage"><span class="counter">68</span>%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row fund_raises m0">                                
                                <div class="pull-left raised amount_box">
                                    <h6>raised</h6>
                                    <h3>$65,360</h3>
                                </div>
                                <div class="pull-left goal amount_box">
                                    <h6>goal</h6>
                                    <h3>$124,500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 cause-item others">
                        <div class="images_row row m0">
                            <img src="images/cause02.jpg" alt="">
                            <a href="#donate_box" class="btn-primary">donate now</a>
                        </div>
                        <div class="cause_excepts row m0">
                            <h4 class="cuase_title"><a href="single-cause.html">HELP SENIOR CITIZENS</a></h4>
                            <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                            <div class="row fund_progress m0">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                        <div class="percentage"><span class="counter">68</span>%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row fund_raises m0">                                
                                <div class="pull-left raised amount_box">
                                    <h6>raised</h6>
                                    <h3>$65,360</h3>
                                </div>
                                <div class="pull-left goal amount_box">
                                    <h6>goal</h6>
                                    <h3>$124,500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 cause-item food">
                        <div class="images_row row m0">
                            <img src="images/cause03.jpg" alt="">
                            <a href="#donate_box" class="btn-primary">donate now</a>
                        </div>
                        <div class="cause_excepts row m0">
                            <h4 class="cuase_title"><a href="single-cause.html">HELP US PRINTING 100 T-SHIRTS</a></h4>
                            <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                            <div class="row fund_progress m0">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                        <div class="percentage"><span class="counter">68</span>%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row fund_raises m0">                                
                                <div class="pull-left raised amount_box">
                                    <h6>raised</h6>
                                    <h3>$65,360</h3>
                                </div>
                                <div class="pull-left goal amount_box">
                                    <h6>goal</h6>
                                    <h3>$124,500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 cause-item education">
                        <div class="images_row row m0">
                            <div id="causes_carousel_inner2" class="carousel slide" data-ride="carousel" data-interval="3000">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active"><img src="images/cause01.jpg" alt=""></div>
                                    <div class="item"><img src="images/cause02.jpg" alt=""></div>
                                </div>

                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#causes_carousel_inner2" data-slide-to="0" class="active"></li>
                                    <li data-target="#causes_carousel_inner2" data-slide-to="1"></li>
                                </ol>
                            </div>
                            <a href="#donate_box" class="btn-primary">donate now</a>
                        </div>
                        <div class="cause_excepts row m0">
                            <h4 class="cuase_title"><a href="single-cause.html">GIVE EDUCATION TO CHILDRENS</a></h4>
                            <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                            <div class="row fund_progress m0">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                        <div class="percentage"><span class="counter">68</span>%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row fund_raises m0">                                
                                <div class="pull-left raised amount_box">
                                    <h6>raised</h6>
                                    <h3>$65,360</h3>
                                </div>
                                <div class="pull-left goal amount_box">
                                    <h6>goal</h6>
                                    <h3>$124,500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 cause-item environment">
                        <div class="images_row row m0">
                            <img src="images/cause02.jpg" alt="">
                            <a href="#donate_box" class="btn-primary">donate now</a>
                        </div>
                        <div class="cause_excepts row m0">
                            <h4 class="cuase_title"><a href="single-cause.html">HELP SENIOR CITIZENS</a></h4>
                            <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                            <div class="row fund_progress m0">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                        <div class="percentage"><span class="counter">68</span>%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row fund_raises m0">                                
                                <div class="pull-left raised amount_box">
                                    <h6>raised</h6>
                                    <h3>$65,360</h3>
                                </div>
                                <div class="pull-left goal amount_box">
                                    <h6>goal</h6>
                                    <h3>$124,500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="gallery-pagination list-unstyled">
                    <li class="prev"><a href="#">prev</a></li>
                    <li class="page-no first-no active"><a href="#">1</a></li>
                    <li class="page-no last-no"><a href="#">2</a></li>
                    <li class="next"><a href="#">next</a></li>
                </ul>
            </div>
        </div>
    </section>
    
    @include('front_page.ngo-theme-3.main_layouts.footer')

    
    @include('front_page.ngo-theme-3.main_layouts.js')
    
</body>
</html>