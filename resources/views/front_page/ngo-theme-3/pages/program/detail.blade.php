<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Helping Hand - Fund rise for great events</title>

    @include('front_page.ngo-theme-3.main_layouts.css')


</head>
<body>
   
    @include('front_page.ngo-theme-3.main_layouts.header2')
   
    
    <section class="row gallery-content">
        <div class="container">
           
            <div class="row">
                <div class="col-md-6 single-project single-cause">
                    <div class="featured-content">
                        <div class="item"><img src="images/gallery/project.jpg" alt=""></div>
                        <div class="item"><img src="images/gallery/project.jpg" alt=""></div>
                    </div>
                    <div class="row m0 project_title">
                        <h2 class="hhh h1 pull-left">give education to children</h2>
                        <a href="#donate_box" class="btn-primary pull-right">donate now</a>
                    </div>
                        
                    <div class="row progressBarRow">
                        <div class="row m0">                    
                            <div class="progress_barBox row m0">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                        <div class="percentage"><span class="counter">68</span>%</div>
                                    </div>
                                </div>
                            </div>

                            <div class="fund_raises style2 row m0">
                                <div class="col-xs-4 amount_box">
                                    <h6>RAISED</h6>
                                    <h3>$65,360</h3>
                                </div>
                                <div class="col-xs-4 amount_box text-center">                            
                                    <h6>days left</h6>
                                    <h3>293</h3>
                                </div>
                                <div class="col-xs-4 amount_box text-right">
                                    <h6>goal</h6>
                                    <h3>$124,500</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <h4>Lorem ipsum dolor sit coopnsectetur adipiscing elit ellentesque fu ture stuffs also goes placerat vel augue.</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque massa ipsum, efficitur a fermen tum sed, suscipit sit amet arcu. Ut ut finibus tortor, eu ultrices turpis. Mauris vitae elit nec diam elementum elementum. Mauris ante quam, consequat ac nibh placerat, lacinia sollicitudin mi. Duis facilisis nibh quam, sit amet interdum tellus sollicitudin tempor. Curabitur aliquam erat in nisl lobortis, ut pellentesque lectus viverra. Aenean sodales aliquet arcu at aliquam. </p>
                    <p>Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. Aenean imperdiet lacus sit amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. Quisque tempor dolor sit amet tellus malesuada, malesuada iaculis eros dignissim. Aenean vitae diam id lacus fringilla maximus. Mauris auctor efficitur nisl, non blandit urna fermentum nec. Vestibulum non leo libero.</p>
                    <div class="row featureswithImage">
                        <div class="col-sm-6">
                            <img src="images/gallery/list-image.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="col-sm-6">
                            <h4 class="list-heading">Lorem ipsum dolor sit coopnse.</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipi scing elit. Pellentesque massa.</p>
                            <ul class="list-unstyled">
                                <li>Feature Point 1 will appear here</li>
                                <li>Some Poiner goes here</li>
                                <li>Your 100% donation goes to field</li>
                                <li>You dont need to worry about the payment you make</li>
                            </ul>
                        </div>
                    </div>
                    <div class="text-block row">Vestibulum quam nisi, pretium a nibh sit amet, consectetur hendrerit mi. amet elit porta, et malesuada erat bibendum. Cras sed nunc massa. tellus malesuada, malesuada iaculis eros dignissim.</div>
                    <div class="row featureswithImage">
                        <div class="col-sm-6 col-sm-push-6">
                            <img src="images/gallery/list-image2.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="col-sm-6 col-sm-pull-6">
                            <h4 class="list-heading">Lorem ipsum dolor sit coopnse.</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipi scing elit. Pellentesque massa.</p>
                            <ul class="list-unstyled">
                                <li>Feature Point 1 will appear here</li>
                                <li>Some Poiner goes here</li>
                                <li>Your 100% donation goes to field</li>
                                <li>You dont need to worry about the payment you make</li>
                            </ul>
                        </div>
                    </div>
                    <h4>Lorem ipsum dolor sit coopnsectetur adipiscing elit ellentesque future stuffs also goes placerat vel augue</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque massa ipsum, efficitur a fermen tum sed, suscipit sit amet arcu. Ut ut finibus tortor, eu ultrices turpis. Mauris vitae elit nec diam elementum elementum. Mauris ante quam, consequat ac nibh placerat, lacinia sollicitudin mi. Duis facilisis nibh quam, sit amet interdum tellus sollicitudin tempor. Curabitur aliquam erat in nisl lobortis, ut pellentesque lectus viverra. Aenean sodales aliquet arcu at aliquam. </p>
                    
                    <h2 class="hhh h2">few latest donors</h2>
                    
                    <div class="row latest-donors">
                        <div class="col-xs-3 donor style2">
                            <img src="images/donors/1.jpg" alt="">
                            <div class="row inner">                        
                                <h5 class="name">Johnathan Doe</h5>
                                <h5 class="amount">donated - $5000</h5>
                            </div>
                        </div>
                        <div class="col-xs-3 donor style2">
                            <img src="images/donors/2.jpg" alt="">
                            <div class="row inner">                        
                                <h5 class="name">michell flintoff</h5>
                                <h5 class="amount">donated - $5000</h5>
                            </div>
                        </div>
                        <div class="col-xs-3 donor style2">
                            <img src="images/donors/3.jpg" alt="">
                            <div class="row inner">                        
                                <h5 class="name">donny chang</h5>
                                <h5 class="amount">donated - $5000</h5>
                            </div>
                        </div>
                        <div class="col-xs-3 donor style2">
                            <img src="images/donors/4.jpg" alt="">
                            <div class="row inner">                        
                                <h5 class="name">Angelina Yeager</h5>
                                <h5 class="amount">donated - $5000</h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row shareOnRow m0">
                        <ul class="list-unstyled pull-left">
                            <li>SHARE IT ON</li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                        <a href="#donate_box" class="btn-primary pull-right">donate now</a> 
                    </div>
                </div>
                <div class="col-md-6 sidebar cause-sidebar">
                    <div class="row m0 widget widget-similar-project widget-similar">
                        <h4 class="hhh h2">Donasi</h4>
                        <p>Nominal</p>
                        <input type="text" class="form-control input-donasi" placeholder="Username atau Email"> <br>
                        <p>Metode Pembayaran</p>
                        <a href="#donate_box" class="btn-primary btn-outline btn-block hidden-sm hidden-xs">pilih metode pembayaran</a>
                        <p style="margin-top: 20px;">Nama Lengkap</p>
                        <input type="text" class="form-control input-donasi" placeholder="Nama Lengkap"> <br>
                        <p>No Handphone / WhatsApp</p>
                        <input type="text" class="form-control input-donasi" placeholder="No Handphone / WhatsApp"> <br>
                        <p>Email</p>
                        <input type="email" class="form-control input-donasi" placeholder="Email"> <br>
                        <a href="#donate_box" class="btn-primary btn-outline btn-block hidden-sm hidden-xs">donasi sekarang</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    @include('front_page.ngo-theme-3.main_layouts.footer')
    

    @include('front_page.ngo-theme-3.main_layouts.js')
    
</body>
</html>