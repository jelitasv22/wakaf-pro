<!--Featured Slider-->
<section class="row featured_events">
    <div class="item">
        <div class="img-slider" style="background-image: url({{asset('themes/ngo-theme-3/images/recent_cause.jpg')}});"></div>
        <div class="row caption m0">
               <div class="caption_row">
                <div class="container">
                    <div class="event_box featured_event_box row m0">
                        
                        <h4 class="event_link"><a href="single-event.html">GIVE EDUCATION TO THE ONE’S WHO REALLY NEEDED</a></h4>
                        
                        <div class="row m0">
                            <a href="#donate_box" class="btn-primary">donate now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="item">
        <div class="img-slider" style="background-image: url({{asset('themes/ngo-theme-3/images/recent_cause2.jpg')}});"></div>
        <div class="row caption m0">
            <div class="caption_row">
             <div class="container">
                 <div class="event_box featured_event_box row m0">
                     
                     <h4 class="event_link"><a href="single-event.html">GIVE EDUCATION TO THE ONE’S WHO REALLY NEEDED</a></h4>
                     
                     <div class="row m0">
                         <a href="#donate_box" class="btn-primary">donate now</a>
                     </div>
                 </div>
             </div>
         </div>
     </div>
    </div>
    <div class="item">
        <div class="img-slider" style="background-image: url({{asset('themes/ngo-theme-3/images/recent_cause3.jpg')}});"></div>
        <div class="row caption m0">
            <div class="caption_row">
             <div class="container">
                 <div class="event_box featured_event_box row m0">
                     
                     <h4 class="event_link"><a href="single-event.html">GIVE EDUCATION TO THE ONE’S WHO REALLY NEEDED</a></h4>
                     
                     <div class="row m0">
                         <a href="#donate_box" class="btn-primary">donate now</a>
                     </div>
                 </div>
             </div>
         </div>
     </div>
    </div>
</section>