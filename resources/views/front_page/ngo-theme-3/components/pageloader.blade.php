<div class="page-loader">
    <div class="loader">
        <?xml version="1.0" encoding="utf-8"?>
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: rgb(255, 255, 255); display: block; shape-rendering: auto;" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
        <path fill="none" stroke="#9a292d" stroke-width="8" stroke-dasharray="42.76482137044271 42.76482137044271" d="M24.3 30C11.4 30 5 43.3 5 50s6.4 20 19.3 20c19.3 0 32.1-40 51.4-40 C88.6 30 95 43.3 95 50s-6.4 20-19.3 20C56.4 70 43.6 30 24.3 30z" stroke-linecap="round" style="transform:scale(0.8);transform-origin:50px 50px">
          <animate attributeName="stroke-dashoffset" repeatCount="indefinite" dur="1s" keyTimes="0;1" values="0;256.58892822265625"></animate>
        </path>
        </svg>
    </div>
</div><!-- Page Loader -->
<div class="wb-top-donate-wrap blue-layer opc9 flex">
    <span class="wb-cls-btn"><i class="fa fa-times"></i></span>
    <div class="fixed-bg patern-bg blue-bg back-blend-multiply" style="background-image: url({{asset('themes/ngo-theme-2/assets/images/pattern-bg1.jpg')}});"></div>
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-5 col-sm-12 col-lg-5">
                <div class="wb-top-donate-title">
                    <i class="flaticon-money"></i>
                    <h2 itemprop="headline">Donate</h2>
                    <p itemprop="description">Foundation has spent more than $4 billion on life-changing</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-lg-6">
                <div class="wb-top-donate-list-wrap">
                    <ul class="wb-top-donate-list">
                        <li><span>$10</span><i class="fa fa-arrow-right"></i></li>
                        <li><span>$20</span><i class="fa fa-arrow-right"></i></li>
                        <li><span>$30</span><i class="fa fa-arrow-right"></i></li>
                        <li><span>$40</span><i class="fa fa-arrow-right"></i></li>
                    </ul>
                    <span><i>$0</i></span>
                    <a href="donate-now.html" title="Start Donation" itemprop="url">Start Donation<i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div><!-- Top Donate Wrap -->