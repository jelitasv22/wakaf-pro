<!--Our Casuses-->
<section class="row our_casuses">
    <div class="container">
        <div class="row sectionTitle text-center">
            <h6 class="label label-default">Program</h6>
            <h3>Lihat Program Kami dan Berdonasi</h3>
        </div>
        <div class="row">
            <div class="causes_carousel">
                <div class="item">
                    <div class="images_row row m0">
                        <div id="causes_carousel_inner" class="carousel slide" data-ride="carousel" data-interval="3000">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active"><img src="images/cause01.jpg" alt=""></div>
                                <div class="item"><img src="images/cause02.jpg" alt=""></div>
                            </div>

                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#causes_carousel_inner" data-slide-to="0" class="active"></li>
                                <li data-target="#causes_carousel_inner" data-slide-to="1"></li>
                            </ol>
                        </div>
                        <a href="#donate_box" class="btn-primary">donate now</a>
                    </div>
                    <div class="cause_excepts row m0">
                        <h4 class="cuase_title"><a href="single-cause.html">GIVE EDUCATION TO CHILDRENS</a></h4>
                        <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                        <div class="row fund_progress m0">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                    <div class="percentage"><span class="counter">68</span>%</div>
                                </div>
                            </div>
                        </div>
                        <div class="row fund_raises m0">                                
                            <div class="pull-left raised amount_box">
                                <h6>raised</h6>
                                <h3>$65,360</h3>
                            </div>
                            <div class="pull-left goal amount_box">
                                <h6>goal</h6>
                                <h3>$124,500</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="images_row row m0">
                        <img src="images/cause02.jpg" alt="">
                        <a href="#donate_box" class="btn-primary">donate now</a>
                    </div>
                    <div class="cause_excepts row m0">
                        <h4 class="cuase_title"><a href="single-cause.html">HELP SENIOR CITIZENS</a></h4>
                        <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                        <div class="row fund_progress m0">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                    <div class="percentage"><span class="counter">68</span>%</div>
                                </div>
                            </div>
                        </div>
                        <div class="row fund_raises m0">                                
                            <div class="pull-left raised amount_box">
                                <h6>raised</h6>
                                <h3>$65,360</h3>
                            </div>
                            <div class="pull-left goal amount_box">
                                <h6>goal</h6>
                                <h3>$124,500</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="images_row row m0">
                        <img src="images/cause03.jpg" alt="">
                        <a href="#donate_box" class="btn-primary">donate now</a>
                    </div>
                    <div class="cause_excepts row m0">
                        <h4 class="cuase_title"><a href="single-cause.html">HELP US PRINTING 100 T-SHIRTS</a></h4>
                        <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                        <div class="row fund_progress m0">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                    <div class="percentage"><span class="counter">68</span>%</div>
                                </div>
                            </div>
                        </div>
                        <div class="row fund_raises m0">                                
                            <div class="pull-left raised amount_box">
                                <h6>raised</h6>
                                <h3>$65,360</h3>
                            </div>
                            <div class="pull-left goal amount_box">
                                <h6>goal</h6>
                                <h3>$124,500</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="images_row row m0">
                        <img src="images/cause02.jpg" alt="">
                        <a href="#donate_box" class="btn-primary">donate now</a>
                    </div>
                    <div class="cause_excepts row m0">
                        <h4 class="cuase_title"><a href="single-cause.html">HELP SENIOR CITIZENS</a></h4>
                        <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                        <div class="row fund_progress m0">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                    <div class="percentage"><span class="counter">68</span>%</div>
                                </div>
                            </div>
                        </div>
                        <div class="row fund_raises m0">                                
                            <div class="pull-left raised amount_box">
                                <h6>raised</h6>
                                <h3>$65,360</h3>
                            </div>
                            <div class="pull-left goal amount_box">
                                <h6>goal</h6>
                                <h3>$124,500</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="images_row row m0">
                        <img src="images/cause03.jpg" alt="">
                        <a href="#donate_box" class="btn-primary">donate now</a>
                    </div>
                    <div class="cause_excepts row m0">
                        <h4 class="cuase_title"><a href="single-cause.html">HELP US PRINTING 100 T-SHIRTS</a></h4>
                        <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                        <div class="row fund_progress m0">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                    <div class="percentage"><span class="counter">68</span>%</div>
                                </div>
                            </div>
                        </div>
                        <div class="row fund_raises m0">                                
                            <div class="pull-left raised amount_box">
                                <h6>raised</h6>
                                <h3>$65,360</h3>
                            </div>
                            <div class="pull-left goal amount_box">
                                <h6>goal</h6>
                                <h3>$124,500</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="images_row row m0">
                        <img src="images/cause02.jpg" alt="">
                        <a href="#donate_box" class="btn-primary">donate now</a>
                    </div>
                    <div class="cause_excepts row m0">
                        <h4 class="cuase_title"><a href="single-cause.html">HELP SENIOR CITIZENS</a></h4>
                        <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                        <div class="row fund_progress m0">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                    <div class="percentage"><span class="counter">68</span>%</div>
                                </div>
                            </div>
                        </div>
                        <div class="row fund_raises m0">                                
                            <div class="pull-left raised amount_box">
                                <h6>raised</h6>
                                <h3>$65,360</h3>
                            </div>
                            <div class="pull-left goal amount_box">
                                <h6>goal</h6>
                                <h3>$124,500</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="images_row row m0">
                        <img src="images/cause03.jpg" alt="">
                        <a href="#donate_box" class="btn-primary">donate now</a>
                    </div>
                    <div class="cause_excepts row m0">
                        <h4 class="cuase_title"><a href="single-cause.html">HELP US PRINTING 100 T-SHIRTS</a></h4>
                        <p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>
                        <div class="row fund_progress m0">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100">
                                    <div class="percentage"><span class="counter">68</span>%</div>
                                </div>
                            </div>
                        </div>
                        <div class="row fund_raises m0">                                
                            <div class="pull-left raised amount_box">
                                <h6>raised</h6>
                                <h3>$65,360</h3>
                            </div>
                            <div class="pull-left goal amount_box">
                                <h6>goal</h6>
                                <h3>$124,500</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>