  
    <section class="row latest_news">
        <div class="container">
            <div class="row sectionTitle text-center">
                <h6 class="label label-default">Artikel</h6>
                <h3>Update terbaru kegiatan kami</h3>
            </div>
            <div class="row">
                <div class="latest-post col-md-3 col-sm-6">                    
                    <div class="row m0 featured_cont">
                        <img src="images/posts/1.jpg" alt="" class="img-responsive">
                        <i class="fa fa-youtube-play"></i>
                    </div>
                    <h5 class="post-title"><a href="#">EVENT FOR PROVIDING PENS TO THE CHILDRENS</a></h5>
                    <h6 class="post-meta"><a href="#">JOHN DOE</a><a href="#">06 June 2015</a></h6>
                    <div class="post-excerpts row m0">Phasellus in egestas libero, et cong lacus. Cras vel lacus nisi. Duis null metus, tincidunt at tortor.</div>
                    <a href="single.html" class="btn-primary btn-outline">read more</a>
                </div>
                <div class="latest-post col-md-3 col-sm-6">                    
                    <div class="row m0 featured_cont">
                        <img src="images/posts/2.jpg" alt="" class="img-responsive">
                    </div>
                    <h5 class="post-title"><a href="#">EVENT FOR PROVIDING PENS TO THE CHILDRENS</a></h5>
                    <h6 class="post-meta"><a href="#">JOHN DOE</a><a href="#">06 June 2015</a></h6>
                    <div class="post-excerpts row m0">Phasellus in egestas libero, et cong lacus. Cras vel lacus nisi. Duis null metus, tincidunt at tortor.</div>
                    <a href="single.html" class="btn-primary btn-outline">read more</a>
                </div>
                <div class="latest-post col-md-3 col-sm-6">                    
                    <div class="row m0 featured_cont">
                        <img src="images/posts/3.jpg" alt="" class="img-responsive">
                    </div>
                    <h5 class="post-title"><a href="#">EVENT FOR PROVIDING PENS TO THE CHILDRENS</a></h5>
                    <h6 class="post-meta"><a href="#">JOHN DOE</a><a href="#">06 June 2015</a></h6>
                    <div class="post-excerpts row m0">Phasellus in egestas libero, et cong lacus. Cras vel lacus nisi. Duis null metus, tincidunt at tortor.</div>
                    <a href="single.html" class="btn-primary btn-outline">read more</a>
                </div>
                <div class="latest-post col-md-3 col-sm-6">                    
                    <div class="row m0 featured_cont">
                        <img src="images/posts/4.jpg" alt="" class="img-responsive">
                    </div>
                    <h5 class="post-title"><a href="#">EVENT FOR PROVIDING PENS TO THE CHILDRENS</a></h5>
                    <h6 class="post-meta"><a href="#">JOHN DOE</a><a href="#">06 June 2015</a></h6>
                    <div class="post-excerpts row m0">Phasellus in egestas libero, et cong lacus. Cras vel lacus nisi. Duis null metus, tincidunt at tortor.</div>
                    <a href="single.html" class="btn-primary btn-outline">read more</a>
                </div>
            </div>
        </div>
    </section>