<section>
    <div class="block gray-layer opc7">
        <div class="fixed-bg gray-bg back-blend-multiply" style="background-image: url(assets/images/prlx-bg11.jpg);"></div>
        <div class="container">
            <div class="we-hlp-wrp">
                <div class="row align-items-center">
                    <div class="col-md-4 col-sm-12 col-lg-4">
                        <div class="we-hlp-titl">
                            <h2 itemprop="headline">Partner</h2>
                            <p itemprop="description">Mereka adalah partner kami</p>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-lg-8">
                        <div class="cntry-hlp-wrp">
                            <div class="cntry-hlp-car owl-carousel">
                                <div class="cntry-hlp-bx">
                                    <div class="cntry-hlp-tp">
                                        <h6 itemprop="headline">Philippines</h6>
                                        <img src="assets/images/resources/flg-img1.jpg" alt="flg-img1.jpg" itemprop="image">
                                    </div>
                                    <div class="cntry-hlp-md">
                                        <span>Population: <i>741.4 Million</i></span>
                                        <span>Poverty: <i>17.3 percent</i></span>
                                        <strong>Donated:<span>$123010</span></strong>
                                    </div>
                                    <a class="thm-btn2" href="donate-now.html" title="" itemprop="url">Donate Now</a>
                                </div>
                                <div class="cntry-hlp-bx">
                                    <div class="cntry-hlp-tp">
                                        <h6 itemprop="headline">Europe</h6>
                                        <img src="assets/images/resources/flg-img2.jpg" alt="flg-img2.jpg" itemprop="image">
                                    </div>
                                    <div class="cntry-hlp-md">
                                        <span>Population: <i>741.4 Million</i></span>
                                        <span>Poverty: <i>17.3 percent</i></span>
                                        <strong>Donated:<span>$123010</span></strong>
                                    </div>
                                    <a class="thm-btn2" href="donate-now.html" title="" itemprop="url">Donate Now</a>
                                </div>
                                <div class="cntry-hlp-bx">
                                    <div class="cntry-hlp-tp">
                                        <h6 itemprop="headline">UAE</h6>
                                        <img src="assets/images/resources/flg-img3.jpg" alt="flg-img3.jpg" itemprop="image">
                                    </div>
                                    <div class="cntry-hlp-md">
                                        <span>Population: <i>741.4 Million</i></span>
                                        <span>Poverty: <i>17.3 percent</i></span>
                                        <strong>Donated:<span>$123010</span></strong>
                                    </div>
                                    <a class="thm-btn2" href="donate-now.html" title="" itemprop="url">Donate Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- Where We Help Wrap -->
        </div>
    </div>
</section>