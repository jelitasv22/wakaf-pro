<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Helping Hand - Fund rise for great events</title>

    @include('front_page.ngo-theme-3.main_layouts.css')


</head>
<body>
   
    
    <div class="base-login">
        <div class="kontener">
            <div class="login-logo">
                <img src="{{asset('themes/ngo-theme-3/images/logo.png')}}" alt="" width="50%">
            </div>
            <div class="form-login">
                <h3 style="font-weight: bold;">Login</h3><br>
                <input type="text" class="form-control lgn-frm" placeholder="Username atau Email"><br>
                <input type="password" class="form-control lgn-frm" placeholder="Password"><br>
                <input type="submit" value="submit" class="btn-primary">
            </div>
        </div>
    </div>
    
    
    @include('front_page.ngo-theme-3.main_layouts.js')
    
</body>
</html>