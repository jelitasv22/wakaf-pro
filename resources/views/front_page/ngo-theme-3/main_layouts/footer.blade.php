<footer class="row footer">
      
    <div class="row copyright_area m0">
        <div class="container">
            <div class="copy_inner row">
                <div class="col-sm-7 copyright_text">Copyright 2020. All Rights Reserved by <a href="#">Helping Hands</a>.</div>
                <div class="col-sm-5 footer-nav">
                    <ul class="nav">
                        <li><a href="#">Terms of Use</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Desclaimer</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>