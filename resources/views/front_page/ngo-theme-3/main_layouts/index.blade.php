<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Helping Hand - Fund rise for great events</title>

    @include('front_page.ngo-theme-3.main_layouts.css')


</head>
<body>
   
    @include('front_page.ngo-theme-3.main_layouts.header')
   
    
    @include('front_page.ngo-theme-3.components.slider')
    
    
    @include('front_page.ngo-theme-3.components.statistik')
    

    @include('front_page.ngo-theme-3.components.program')

    
    @include('front_page.ngo-theme-3.components.posting')
            

    @include('front_page.ngo-theme-3.main_layouts.footer')

    
    @include('front_page.ngo-theme-3.main_layouts.js')
    
</body>
</html>