<!--Fonts-->
<link href='https://fonts.googleapis.com/css?family=Karla:400,400italic,700italic,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

<!--Bootstrap-->
<link rel="stylesheet" href="{{asset('themes/ngo-theme-3/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('themes/ngo-theme-3/css/bootstrap-theme.min.css')}}">
<!--Font Awesome-->
<link rel="stylesheet" href="{{asset('themes/ngo-theme-3/css/font-awesome.min.css')}}">
<!--Owl Carousel-->
<link rel="stylesheet" href="{{asset('themes/ngo-theme-3/vendors/owl.carousel/owl.carousel.css')}}">
<!--Magnific Popup-->
<link rel="stylesheet" href="{{asset('themes/ngo-theme-3/vendors/magnific-popup/magnific-popup.css')}}">

<!--Theme Styles-->
<link rel="stylesheet" href="{{asset('themes/ngo-theme-3/css/style.css')}}">
<link rel="stylesheet" href="{{asset('themes/ngo-theme-3/css/theme.css')}}">

<!--[if lt IE 9]>
	
	<script src="js/html5shiv.min.js"></script>
	<script src="js/respond.min.js"></script>
<![endif]-->