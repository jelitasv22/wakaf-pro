<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
        <title>Langkah Indonesia Mandiri</title>

       
        @include('front_page.ngo-theme-2.main_layouts.css')
    </head>
    <body itemscope>
        <main>
            @include('front_page.ngo-theme-2.main_layouts.header')
            
            @include('front_page.ngo-theme-2.components.pageloader')

            @include('front_page.ngo-theme-2.components.slider')
            
            @include('front_page.ngo-theme-2.components.statistik')
            
		    @include('front_page.ngo-theme-2.components.program')
           
            <!-- <section>
                <div class="block black-layer opc7 top-spac70 bottom-spac70">
                    <div class="fixed-bg" style="background-image: url(assets/images/prlx-bg1.jpg);"></div>
                    <div class="container">
                        <div class="dnt-sec">
                            <div class="row align-items-center">
                                <div class="col-md-9 col-sm-12 col-lg-9">
                                    <h3 itemprop="headline">Tunaikan Qurban Sekarang Juga di Yayasan Langkah Indonesia Mandiri.</h3>
                                </div>
                                <div class="col-md-3 col-sm-12 col-lg-3">
                                    <a class="thm-btn" href="" title="" itemprop="url">Tunaikan Qrban<span></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> -->
          
		    @include('front_page.ngo-theme-2.components.posting')
            
            @include('front_page.ngo-theme-2.main_layouts.footer')
            
        </main><!-- Main Wrapper -->

        @include('front_page.ngo-theme-2.main_layouts.js')

       
    </body>	
</html>