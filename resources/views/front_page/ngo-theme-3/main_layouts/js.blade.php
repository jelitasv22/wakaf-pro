<script src="{{asset('themes/ngo-theme-3/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-3/js/bootstrap.min.js')}}"></script>
<!--Magnific Popup-->
<script src="{{asset('themes/ngo-theme-3/vendors/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<!--Owl Carousel-->
<script src="{{asset('themes/ngo-theme-3/vendors/owl.carousel/owl.carousel.min.js')}}"></script>
<!--CounterUp-->
<script src="{{asset('themes/ngo-theme-3/vendors/couterup/jquery.counterup.min.js')}}"></script>
<!--WayPoints-->
<script src="{{asset('themes/ngo-theme-3/vendors/waypoint/waypoints.min.js')}}"></script>
<!--Theme Script-->    
<script src="{{asset('themes/ngo-theme-3/js/theme.js')}}"></script>