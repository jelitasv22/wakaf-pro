<nav class="navbar navbar-default navbar-static-top navbar2">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>            
        <div class="collapse navbar-collapse" id="mainNav">                
            <ul class="nav navbar-nav">
                <li class="dropdown active">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home</a>
                </li>
                <li><a href="donation-checkout.html">Profile</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Update</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Program</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Qurban</a>
                </li>
            </ul>
            <ul class="nav social_navbar navbar-right">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></a>
                    <ul class="dropdown-menu">
                        <li>
                            <form action="#" method="get" role="form" class="search_form">
                                <input type="search" class="form-control" placeholder="Type and press enter">
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<header class="row header1">
    <div class="container">
        <div class="logo pull-left">
            <a href="#"><img src="{{asset('images/logo.png')}}" alt=""></a>
        </div>
        <form action="#" method="post" role="form" class="pull-left language_picker">
            <select name="language" class="selectpicker">
                <option value="en">en</option>
                <option value="bn">bn</option>
                <option value="ar">ar</option>
            </select>
        </form>
        <a href="#donate_box" class="btn-primary btn-outline hidden-sm hidden-xs pull-right">donate now</a>
        
        <div class="pull-right emergency-contact">
            <div class="pull-left">
                <span><i class="fa fa-envelope-o"></i></span>
                <div class="infos_col">
                    <h6>email us at</h6>
                    <a href="mailto:info@helpinghands.com"><h5>info@helpinghands.com</h5></a>
                </div>
            </div>
            <div class="pull-left">
                <span class="rotate"><i class="fa fa-phone"></i></span>
                <div class="infos_col">
                    <h6>call us now</h6>
                    <h4>700 (123) 4567</h4>
                </div>
            </div>
        </div>
    </div>        
</header>
