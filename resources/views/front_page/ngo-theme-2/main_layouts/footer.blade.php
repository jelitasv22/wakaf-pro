<footer>
    <div class="block dark-layer opc3">
        <div class="fixed-bg drk-bg back-blend-darken" style="background-image: url({{asset('themes/ngo-theme-2/assets/images/prlx-bg2.jpg')}});"></div>
        <div class="container">
            <div class="ftr-dta-wrp remove-ext10">
                <!-- <div class="row"> -->
                    <!-- <div class="col-md-4 col-sm-6 col-lg-4">
                        <div class="wdgt-bx">
                            <div class="logo"><a href="{{ url('/') }}" title="Home" itemprop="url"><img src="{{ENV('BACKEND_URL')}}/admin/assets/media/logo_yayasan/{{$yayasan->data->logo_secondary}}" alt="" itemprop="image" ></a></div>
                        </div>
                    </div> -->
                    <div class="cpyrgt-footer text-center"> 
                        <p class="cpyrgt-p" itemprop="description">&copy; Copyright {{date('Y')}} <a href="" title="" itemprop="url">Langkah Indonesia Mandiri</a> All rights reserved.</p>
                    </div>
                <!-- </div> -->
            </div><!-- Footer Data Wrap -->
        </div>
    </div>
</footer><!-- Footer -->