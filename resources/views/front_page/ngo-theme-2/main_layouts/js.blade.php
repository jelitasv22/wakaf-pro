<script src="{{asset('themes/ngo-theme-2/assets/js/jquery.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/fancybox.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/wow.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/perfectscrollbar.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/scroll-up-bar.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/custom-scripts.js')}}"></script>

<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/jquery.themepunch.revolution.min.js')}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->    
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/extensions/revolution.extension.carousel.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/extensions/revolution.extension.migration.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/extensions/revolution.extension.navigation.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/extensions/revolution.extension.parallax.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/extensions/revolution.extension.video.min.js')}}"></script>
<script src="{{asset('themes/ngo-theme-2/assets/js/revolution/revolution-init.js')}}"></script>
<script type="text/javascript" src="{{asset('themes/pagination/dist/pagination.min.js')}}"></script>
<script src="{{asset('admin/assets/vendors/general/sweetalert2/dist/sweetalert2.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
		$.ajax({
			url: '{{ENV("API_URL")}}/api/ngo-statistik?yayasan={{ENV("YAYASAN_KEY")}}',
			type: 'GET',
			dataType: 'Json',
		})
		.done(function(res) {
			if(res.donasi.total_donasi == null){
				$('#donasi_terkumpul_statistik').html('Rp. 0')
			}else{
				$('#donasi_terkumpul_statistik').html('Rp. '+res.donasi.total_donasi.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "."))
			}

			$('#donasi_terkumpul_statistik2').html(res.donatur)
		});
		
	});
</script>