<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/icons.min.css')}}">
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/animate.min.css')}}">
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/fancybox.min.css')}}">
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/perfect-scrollbar.css')}}">
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/style.css')}}">
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/responsive.css')}}">
<link rel="stylesheet" media="screen" href="{{asset('themes/ngo-theme-2/assets/css/color.css')}}">

<!-- REVOLUTION STYLE SHEETS -->
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/revolution/settings.css')}}">
<!-- REVOLUTION LAYERS STYLES -->
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/revolution/layers.css')}}">
<!-- REVOLUTION NAVIGATION STYLES -->
<link rel="stylesheet" href="{{asset('themes/ngo-theme-2/assets/css/revolution/navigation.css')}}">

<link href="{{asset('admin/assets/vendors/general/sweetalert2/dist/sweetalert2.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('themes/pagination/dist/pagination.css')}}" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('themes/payment/payment.css') }}">
<style type="text/css">
	.pagination > li > a:hover,
	.pagination > li > span:hover,
	.pagination > li > a:focus,
	.pagination > li > span:focus {
		color:#9a292d;
	}

	.pagination > .active > a,
	.pagination > .active > span,
	.pagination > .active > a:hover,
	.pagination > .active > span:hover,
	.pagination > .active > a:focus,
	.pagination > .active > span:focus {
		background-color:#9a292d;
		border-color:#9a292d;
	}

	.swal2-popup.swal2-toast.swal2-show {
		background-color: #9a292d;
	}

	.swal2-popup.swal2-toast .swal2-title {
		color: white;
	}
</style>
@yield('css')
