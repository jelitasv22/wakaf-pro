<header class="style1 stick flex">
    <div class="container">
        <div class="wb-lgmnu-wrp flex justify-content-between">
            <div class="logo"><a href="{{url('/')}}" title="Home" itemprop="url"><img src="{{ENV('BACKEND_URL')}}/admin/assets/media/logo_yayasan/{{$yayasan->data->logo_primary}}" alt="logo.png" itemprop="image" style="width: 100px;"></a></div><!-- Logo -->
            <nav>
                <div>
                    <ul>
                        <?php foreach ($menu->list as $key => $header) { ?>
                            <li><a class="link-menu" href="{{ url($header->url) }}" title="" itemprop="url">{{ $header->judul_menu }}</a></li>

                            <?php 
                            if($header->judul_menu == "#"){ ?>
                                <?php foreach ($menu->body as $body) { ?>
                                    <li class="menu-item-has-children">
                                        <a class="link-menu" href="{{$header->url}}" title="" itemprop="url">{{ $header->judul_menu }}</a>
                                        <ul>
                                            <li><a href="{{ url($body->url) }}" title="" itemprop="url">{{ $body->judul_menu }}</a></li>
                                        </ul>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </nav>
        </div><!-- Logo Menu Wrap -->
    </div>
</header><!-- Header -->

<div class="sticky-header flex">
    <div class="container">
        <div class="wb-lgmnu-wrp flex justify-content-between">
            <div class="logo"><a href="index.html" title="Home" itemprop="url"><img src="{{asset('themes/ngo-theme-2/assets/images/logolima-text.png')}}" alt="logo2.png" itemprop="image" width="175px"></a></div><!-- Logo -->
            <nav>
                <div>
                    <ul>
                     <?php foreach ($menu->list as $key => $header) { ?>
                        <li><a class="link-menu" href="{{ url($header->url) }}" title="" itemprop="url">{{ $header->judul_menu }}</a></li>

                        <?php 
                        if($header->judul_menu == "#"){ ?>
                            <?php foreach ($menu->body as $body) { ?>
                                <li class="menu-item-has-children">
                                    <a class="link-menu" href="{{$header->url}}" title="" itemprop="url">{{ $header->judul_menu }}</a>
                                    <ul>
                                        <li><a href="{{ url($body->url) }}" title="" itemprop="url">{{ $body->judul_menu }}</a></li>
                                    </ul>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </nav>
    </div><!-- Logo Menu Wrap -->
</div>
</div><!-- Sticky Header -->
<div class="rspns-hdr">
    <div class="rspns-lg-mnu-br">
        <div class="container">
            <div class="logo"><a href="index.html" title="Home" itemprop="url"><img src="{{asset('themes/ngo-theme-2/assets/images/logolima-text.png')}}" alt="logo2.png" itemprop="image" width="175px"></a></div>
            <span class="rspns-mnu-btn"><i class="fa fa-align-justify"></i></span>
        </div>
        <div class="rspns-mnu">
            <span class="rspns-cls-btn"><i class="fa fa-times"></i></span>
            <ul>
                <?php foreach ($menu->list as $key => $header) { ?>
                    <li><a class="link-menu" href="{{ url($header->url) }}" title="" itemprop="url">{{ $header->judul_menu }}</a></li>

                    <?php 
                    if($header->judul_menu == "#"){ ?>
                        <?php foreach ($menu->body as $body) { ?>
                            <li class="menu-item-has-children">
                                <a class="link-menu" href="{{$header->url}}" title="" itemprop="url">{{ $header->judul_menu }}</a>
                                <ul>
                                    <li><a href="{{ url($body->url) }}" title="" itemprop="url">{{ $body->judul_menu }}</a></li>
                                </ul>
                            </li>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</div><!-- Responsive Header -->