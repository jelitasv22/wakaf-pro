<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="40x40" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main style="min-height : 100vh">
		@include('front_page.ngo-theme-2.main_layouts.header2')
			<div class="gambarnotif">
				<img src="{{('image/bell_abu.png')}}" class="w-15">
			</div>
			<div class="container clearfix">
        		<div class="row justify-content-center" style="height: 60vh; align-items: center;">

        			<div class="col-md-7 center">
						
        				<div class="heading-block nobottomborder mb-4bell">
        					<h4 class="mb-4 nott">@lang('language.notifikasi_transaksi')</h4>
        				</div>
        				<div class="svg-line bottommargin-sm clearfix">
        					<!-- <hr style="background-color: red; height: 1px; width: 200px;"> -->
						</div>
						<div class="textwaitblade">
							<p>Terima kasih atas donasi dan partisipasi anda. 
							   Setelah anda selesai melakukan transfer dana donasi.
							   Anda akan mendapatkan konfirmasi melalui e-mail. 
							   Semoga Allah membalas dengan pahala yang berlipat.</p>
						</div>
						
						<div class="payment-method">
							<button>Kembali</button>
						</div>
        			</div>
        		</div>
        	</div>

		@include('front_page.ngo-theme-2.main_layouts.footer')

	</main><!-- Main Wrapper -->

	@include('front_page.ngo-theme-2.main_layouts.js')
	@include('front_page.ngo-theme-2.pages.program.js')
</body>	
</html>