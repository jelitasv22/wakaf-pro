<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header_2')

		<section>
			<div class="block">
				<div class="container">
					<div class="sec-ttl text-center">
						<div class="sec-ttl-inr">
							<h2 itemprop="headline">Qurban</h2>
							<i class="flaticon-sweat thm-clr"></i>
						</div>
					</div>
					<div class="blg-wrp remove-ext9">
						<div class="row">
							<h1>Comming Soon</h1>
						</div>
					</div>
				</div>
			</div>
		</section>

		@include('front_page.ngo-theme-2.main_layouts.footer')

	</main><!-- Main Wrapper -->

	@include('front_page.ngo-theme-2.main_layouts.js')
</body>	
</html>