

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header2')

        <div class="gray-bg3 brdcrmb-wrp">
            <div class="container">
                <div class="brdcrmb-inr flex justify-content-between">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}" title="" itemprop="url">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('posting') }}" title="" itemprop="url">Update</a></li>
                        <li class="breadcrumb-item active">Detail Update</li>
                    </ol>
                </div>
            </div>
        </div><!-- Breadcrumbs Wrap -->
        <section>
            <div class="block">
                <div class="container">
                    <div class="blg-dtl-thmb">
                        <img src="{{ENV('BACKEND_URL')}}/admin/assets/media/posting/Posting_5f0880203658a.jpg" alt="blg-dtl-img2-1.jpg" itemprop="image">
                    </div>
                    <div class="blog-dtl-wrp">
                        <div class="blg-inf">
                            <span class="blg-dat thm-bg"><a href="#" title="" itemprop="url">{{ $detail_posting->list->category }}</a></span>
                            <span class="blg-tgs"><i class="fa fa-tag"></i><a href="#" title="" itemprop="url"></a>{{ $detail_posting->list->created_at }}</span>
                            <h1 itemprop="headline">{{$detail_posting->list->judul}}</h1>
                        </div>
                        <div class="blg-dtl-desc">
                            <p itemprop="description">{!! $detail_posting->list->content !!}</p>
                        </div>
                        <span><b>Update Lainnya<hr></b></span>
                        <div id="sc_donations_108588160" class="sc_donations sc_donations_style_excerpt">
                            <div class="sc_donations_columns_wrap" id="list_posting">

                            </div>

                            <center>
                                <ul id="paginate" class="pagination"></ul>
                            </center>
                        </div>
                        <!--Sosmed-->
                        <center style="margin-bottom: 50px; margin-top: 10px; float: right;">

                            <span><a href="">Facebook</a></span>
                            <span><a href="">Twitter</a></span>
                            <span><a href="">Whatsapp</a></span>

                        </center>
                    </div><!-- Blog Detail Wrap -->
                </div>
            </div>
        </section>
        @include('front_page.ngo-theme-2.main_layouts.footer')

    </main><!-- Main Wrapper -->

    @include('front_page.ngo-theme-2.main_layouts.js')
    @include('front_page.ngo-theme-2.pages.posting.js')
</body>	
</html>