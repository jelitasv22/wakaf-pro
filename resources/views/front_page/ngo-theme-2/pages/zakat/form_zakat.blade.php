<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header2')

		<div class="gray-bg3 brdcrmb-wrp">
			<div class="container">
				<div class="brdcrmb-inr flex justify-content-between">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('home') }}" title="" itemprop="url">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('zakat') }}"> Zakat </a></li>
						<li class="breadcrumb-item active">Purchase</li>
					</ol>
				</div>
			</div>
		</div><!-- Breadcrumbs Wrap -->
		<section>
			<div class="block">
				<div class="container">
					<div class="blg-wrp style3 remove-ext3">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-lg-12">
								<h4 class="sc_donations_title sc_item_title">Purchase Zakat</h4>
								<div class="cnt-inr" style="background-image: url('{{ asset('themes/ngo-theme-2/assets/images/pattern-bg2.png') }}');">
									<div class="cnt-frm">

										<span class="dnt-fld-inr">
											<div class="row mrg">
												<div class="slc-wrp">
													<label>Kategori</label>
													<select class="form-control" name="category_id" id="category_id">
														<option value="" selected="" disabled="">- Kategori -</option>
														@foreach($category_zakat->data as $row)
														<option value="{{ $row->id }}" <?php if($category_id == $row->id){ ?> selected <?php } ?> >{{ $row->category }}</option>
														@endforeach
													</select>
												</div>

												<div class="slc-wrp">
													<label>Jumlah Zakat</label>
													<input type="text" name="jumlah_zakat" class="form-control" id="jumlah_zakat" placeholder="Rp. 0" value="{{ str_replace(',','.', number_format($jumlah_zakat)) }}">
												</div>

												<div class="slc-wrp">
													<label>Metode Pembayaran</label>
													<button type="button" id="btn-payment-method" class="thm-btn2" style="width: 100%; padding: 8px; border-radius: 5px">
														Pilih Metode Pembayaran
													</button>
													<input name="payment_vendor" id="payment_vendor" type="hidden" value="" />
													<input name="payment_type" id="payment_type" type="hidden" value="" />
													<input name="payment_method" id="payment_method" type="hidden" value="" />
													<input type="hidden" id="no_rekening" name="no_rekening" value="">
													<div id="selected-method" style="display: flex; width: 100%;">
														<div style="width: 50%;">
															<img src="" alt="" id="selected-method-img" style="max-width:100%;max-height:40px;margin-right:10px;">
															<span style="font-size:14px;" id="selected-method-label">

															</span>
														</div>
														<div style="width: 50%;">
															<button type="button" id="btn-payment-method2" class="thm-btn2" style="width: 50%; padding: 5px; border-radius: 5px; float: right;">
																Ganti
															</button>
														</div>
													</div>
												</div>

												<div class="slc-wrp">
													<label>Nama Lengkap</label>
													<input type="text" name="nama_lengkap" class="form-control" id="nama_lengkap" placeholder="Nama Lengkap">
												</div>

												<div class="slc-wrp">
													<label>No Handphone</label>
													<input type="text" name="no_handphone" class="form-control" id="no_handphone" placeholder="No Handphone">
												</div>

												<div class="slc-wrp">
													<label>Email</label>
													<input type="text" name="email" class="form-control" id="email" placeholder="Email">
												</div>

												<div class="slc-wrp">
													<button type="button" class="thm-btn2" id="lanjutkan_pembayaran_zakat" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">
														Lanjutkan Pembayaran
													</button>
												</div>
											</div>

										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<div id="metode-pembayaran" class="popup">
				<div class="popup-content">
					<div class="head-pop" style="background-color: #9a292d;">
						<div class="title-pop">
							<p style="color: white;">Metode Pembayaran</p>
						</div>
						<span id="mthd-close" class="tutup">&times;</span>
					</div>
					<div class="payment-scroller" id="payment-popup-body">

					</div>
				</div>
			</div>

			<div id="notif-pembayaran" class="popup">
				<div class="popup-content-notif">
					<div class="head-pop" style="background-color: #9a292d;">
						<div class="title-pop">
							<p id="title_popup" style="color: white;">Transfer</p>
						</div>
						<span id="simple-pop-close" class="tutup">&times;</span>
					</div>
					<div class="payment-scroller" id="payment-popup-body-notif" style="padding: 10px;">
						<div class="text-color-popup" style="display: flex; border-radius: 5px; font-size: 15px; padding: 5px; line-height: 40px; text-align: center; background-color: #9a292d;">
							<span style="width: 20%;">
								<img class="logo-payment" id="moota-logo" src="" alt="">
							</span>
							<span id="no_rekening_show" style="width: 60%; text-align: center;">-</span>
							<span id="salin" style="width: 20%; text-align: right;"><a href="javascript:0;" style="color: white;" onclick="copyToClip('no_rekening_show')">Salin</a></span>
						</div>
						<div id="intruksi-pembayaran">
							<center><br />
								<span><b>Intruksi Pembayaran</b></span><br>
								<span>Nominal yang harus anda bayar :</span><br>
								<span style="font-size: 18px;"><b>Rp. 10.000</b></span><br>
								<span>*Harap selesaikan pembayaran sesuai dengan nominal yang tertera untuk memudahkan pengecekan!</span><br>
								<span>*Sesuaikan nominal sampai 3 digit terakhir! (Kelebihan dari nominal akan didonasikan juga)</span><br>
								<span>*Lakukan pembayaran maksimal 1x24 jam!</span><br>
								<span>*Silahkan akses link berikut untuk melihat perkembangan donasi anda! (Link dikirim juga ke email anda)</span><br>
								<span>*Tekan tombol dibawah juka sudah melakukan pembayaran!</span><br><br />
								<span>
									<a href="">
										<button style="padding: 5px; width: 100%; border-radius: 5px;" class="thm-btn2">
											<span class="post_readmore_label">Saya Sudah Melakukan Transfer</span>
										</button>
									</a>
								</span>
							</center>
						</div>
					</div>
				</div>
			</div>



			@include('front_page.ngo-theme-2.main_layouts.footer')

		</main><!-- Main Wrapper -->

		@include('front_page.ngo-theme-2.main_layouts.js')
		@include('front_page.ngo-theme-2.pages.zakat.js')
	</body>	
	</html>