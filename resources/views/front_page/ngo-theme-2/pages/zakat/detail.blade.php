<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header2')

		<div class="gray-bg3 brdcrmb-wrp">
			<div class="container">
				<div class="brdcrmb-inr flex justify-content-between">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('home') }}" title="" itemprop="url">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('zakat') }}">Zakat</a></li>
						<li class="breadcrumb-item active">Detail</li>
					</ol>
				</div>
			</div>
		</div><!-- Breadcrumbs Wrap -->
		<section>
			<div class="block">
				<div class="container">
					<div class="blg-wrp style3 remove-ext3">
						<div class="row">
							@if($seo == 'zakat-penghasilan')
							<div class="col-md-12 col-sm-12 col-lg-12">
								<h4 class="sc_donations_title sc_item_title" style="text-align: center; font-weight: bold; margin-bottom: 15px;">Zakat Penghasilan</h4>
								<div class="cnt-inr" style="background-image: url('{{ asset('themes/ngo-theme-2/assets/images/pattern-bg2.png') }}');">

									<div class="row">
										<div class="col-lg-3"></div>
										<div class="col-lg-6">
											<div class="w3-row">
												<a href="javascript:void(0)" onclick="openCity(event, 'hitung-zakat-penghasilan');">
													<div class="w3-third tablink w3-bottombar w3-border-red w3-padding">Hitung</div>
												</a>
												<a href="javascript:void(0)" onclick="openCity(event, 'bayar-zakat-penghasilan');">
													<div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding">Bayar</div>
												</a>
											</div>
										</div>
									</div>
									<br><br>
									<form action="{{ Route('zakat.purchase') }}" method="POST">
										{{ csrf_field() }}
										<div id="hitung-zakat-penghasilan" class="w3-container city" style="display:block;">
											<div class="cnt-frm">
												<span class="dnt-fld-inr">

													<div class="row mrg">

														<input type="hidden" name="qty_beras" id="qty_beras" value="{{ $setup_zakat->qty_beras->value }}">
														<input type="hidden" name="harga_emas" id="harga_emas" value="{{ $setup_zakat->harga_emas->value }}">
														<input type="hidden" name="harga_perak" id="harga_perak" value="{{ $setup_zakat->harga_perak->value }}">
														<input type="hidden" name="category_id" value="80">
														<div class="col-lg-3"></div>
														<div class="col-md-6 col-sm-12 col-lg-6">
															<div class="slc-wrp">
																<label>Penghasilan Perbulan</label>
																<input type="text" name="penghasilan_perbulan" id="penghasilan_perbulan" placeholder="Penghasilan Perbulan" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>

															<div class="slc-wrp">
																<label>Bonus/THR/Penghasilan Lainnya</label>
																<input type="text" name="penghasilan_lainnya" id="penghasilan_lainnya" placeholder="Penghasilan Lainnya" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>

															<div class="slc-wrp">
																<label>Harga Beras</label>
																<input type="text" name="harga_beras_penghasilan" id="harga_beras_penghasilan" placeholder="Harga Beras" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>

															<div class="slc-wrp">
																<label>Total</label>
																<input type="text" name="jumlah_zakat_purchase_detail" id="total_zakat_penghasilan" placeholder="Rp. 0" readonly="">
															</div>

															<div class="slc-wrp">
																<a class="thm-btn2" id="hitung_zakat_penghasilan" href="javascript:0;" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">Hitung</a>
															</div>
															<div class="slc-wrp">
																<button type="submit" class="thm-btn2" id="bayar-zakat-penghasilan-hitung" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px; display: none;">
																	Bayar
																</button>
															</div>
														</div>
													</div>
												</span>
											</div>
										</div>
									</form>

									<form action="{{ Route('zakat.purchase') }}" method="POST">
										{{ csrf_field() }}
										<div id="bayar-zakat-penghasilan" class="w3-container city" style="display:none">
											<div class="cnt-frm">
												<span class="dnt-fld-inr">
													<input type="hidden" name="qty_beras" id="qty_beras" value="{{ $setup_zakat->qty_beras->value }}">
													<input type="hidden" name="harga_emas" id="harga_emas" value="{{ $setup_zakat->harga_emas->value }}">
													<input type="hidden" name="harga_perak" id="harga_perak" value="{{ $setup_zakat->harga_perak->value }}">
													<input type="hidden" name="category_id" value="80">
													<div class="row mrg">
														<div class="col-lg-3"></div>
														<div class="col-lg-6">
															<div class="slc-wrp">
																<label>Jumlah Zakat</label>
																<input type="text" name="jumlah_zakat_purchase_detail" id="jumlah_zakat" placeholder="Rp. 0" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>
															<div class="slc-wrp">
																<button type="submit" class="thm-btn2" id="bayar_zakat" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">
																	Bayar
																</button>
															</div>
														</div>
													</div>
												</span>
											</div>
										</div>
									</form>
								</div>
							</div>
							@elseif($seo == 'zakat-fitrah')
							<div class="col-md-12 col-sm-12 col-lg-12">
								<h4 class="sc_donations_title sc_item_title" style="text-align: center; font-weight: bold; margin-bottom: 15px;">Zakat Fitrah</h4>
								<div class="cnt-inr" style="background-image: url('{{ asset('themes/ngo-theme-2/assets/images/pattern-bg2.png') }}');">

									<div class="row">
										<div class="col-lg-3"></div>
										<div class="col-lg-6">
											<div class="w3-row">
												<a href="javascript:void(0)" onclick="openCity(event, 'hitung-zakat-penghasilan');">
													<div class="w3-third tablink w3-bottombar w3-border-red w3-padding">Hitung</div>
												</a>
												<a href="javascript:void(0)" onclick="openCity(event, 'bayar-zakat-penghasilan');">
													<div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding">Bayar</div>
												</a>
											</div>
										</div>
									</div>
									<br><br>
									<form action="{{ Route('zakat.purchase') }}" method="POST">
										{{ csrf_field() }}
										<div id="hitung-zakat-penghasilan" class="w3-container city" style="display:block;">
											<div class="cnt-frm">
												<span class="dnt-fld-inr">

													<div class="row mrg">

														<input type="hidden" name="qty_beras" id="qty_beras" value="{{ $setup_zakat->qty_beras->value }}">
														<input type="hidden" name="harga_emas" id="harga_emas" value="{{ $setup_zakat->harga_emas->value }}">
														<input type="hidden" name="harga_perak" id="harga_perak" value="{{ $setup_zakat->harga_perak->value }}">
														<input type="hidden" name="category_id" value="81">
														<div class="col-lg-3"></div>
														<div class="col-md-6 col-sm-12 col-lg-6">
															<div class="slc-wrp">
																<label>Harga Beras</label>
																<input type="text" name="harga_beras_fitrah" id="harga_beras_fitrah" placeholder="Harga Beras" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>
															<div class="slc-wrp">
																<label>Jumlah Zakat</label>
																<input type="text" name="jumlah_zakat_purchase_detail" id="total_zakat_fitrah" placeholder="Rp. 0" readonly="">
															</div>
															<div class="slc-wrp">
																<a class="thm-btn2" id="hitung_zakat_fitrah" href="javascript:0;" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">Hitung</a>
															</div>
															<div class="slc-wrp">
																<button type="submit" class="thm-btn2" id="bayar-zakat-penghasilan-hitung" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px; display: none;">
																	Bayar
																</button>
															</div>
														</div>
													</div>
												</span>
											</div>
										</div>
									</form>

									<form action="{{ Route('zakat.purchase') }}" method="POST">
										{{ csrf_field() }}
										<div id="bayar-zakat-penghasilan" class="w3-container city" style="display:none">
											<div class="cnt-frm">
												<span class="dnt-fld-inr">
													<input type="hidden" name="qty_beras" id="qty_beras" value="{{ $setup_zakat->qty_beras->value }}">
													<input type="hidden" name="harga_emas" id="harga_emas" value="{{ $setup_zakat->harga_emas->value }}">
													<input type="hidden" name="harga_perak" id="harga_perak" value="{{ $setup_zakat->harga_perak->value }}">
													<input type="hidden" name="category_id" value="81">
													<div class="row mrg">
														<div class="col-lg-3"></div>
														<div class="col-lg-6">
															<div class="slc-wrp">
																<label>Jumlah Zakat</label>
																<input type="text" name="jumlah_zakat_purchase_detail" id="jumlah_zakat" placeholder="Rp. 0" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>
															<div class="slc-wrp">
																<button type="submit" class="thm-btn2" id="bayar_zakat" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">
																	Bayar
																</button>
															</div>
														</div>
													</div>
												</span>
											</div>
										</div>
									</form>
								</div>
							</div>
							@elseif($seo == 'zakat-emas')
							<div class="col-md-12 col-sm-12 col-lg-12">
								<h4 class="sc_donations_title sc_item_title" style="text-align: center; font-weight: bold; margin-bottom: 15px;">Zakat Emas</h4>
								<div class="cnt-inr" style="background-image: url('{{ asset('themes/ngo-theme-2/assets/images/pattern-bg2.png') }}');">

									<div class="row">
										<div class="col-lg-3"></div>
										<div class="col-lg-6">
											<div class="w3-row">
												<a href="javascript:void(0)" onclick="openCity(event, 'hitung-zakat-penghasilan');">
													<div class="w3-third tablink w3-bottombar w3-border-red w3-padding">Hitung</div>
												</a>
												<a href="javascript:void(0)" onclick="openCity(event, 'bayar-zakat-penghasilan');">
													<div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding">Bayar</div>
												</a>
											</div>
										</div>
									</div>
									<br><br>
									<form action="{{ Route('zakat.purchase') }}" method="POST">
										{{ csrf_field() }}
										<div id="hitung-zakat-penghasilan" class="w3-container city" style="display:block;">
											<div class="cnt-frm">
												<span class="dnt-fld-inr">

													<div class="row mrg">

														<input type="hidden" name="qty_beras" id="qty_beras" value="{{ $setup_zakat->qty_beras->value }}">
														<input type="hidden" name="harga_emas" id="harga_emas" value="{{ $setup_zakat->harga_emas->value }}">
														<input type="hidden" name="harga_perak" id="harga_perak" value="{{ $setup_zakat->harga_perak->value }}">
														<input type="hidden" name="category_id" value="82">
														<div class="col-lg-3"></div>
														<div class="col-md-6 col-sm-12 col-lg-6">
															<div class="slc-wrp">
																<label>Jumlah Emas (/gr)</label>
																<input type="text" name="jumlah_emas" id="jumlah_emas" placeholder="Jumlah Emas" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>
															<div class="slc-wrp">
																<label>Jumlah Emas Yang di Gunakan(/gr)</label>
																<input type="text" name="jumlah_emas_yang_digunakan" id="jumlah_emas_yang_digunakan" placeholder="Jumlah Emas Yang di Gunakan" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>
															<div class="slc-wrp">
																<label>Total</label>
																<input type="text" name="jumlah_zakat_purchase_detail" id="total_zakat_emas" placeholder="Rp. 0" readonly="">
															</div>
															<div class="slc-wrp">
																<a class="thm-btn2" id="hitung_zakat_emas" href="javascript:0;" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">Hitung</a>
															</div>
															<div class="slc-wrp">
																<button type="submit" class="thm-btn2" id="bayar-zakat-penghasilan-hitung" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px; display: none;">
																	Bayar
																</button>
															</div>
														</div>
													</div>
												</span>
											</div>
										</div>
									</form>

									<form action="{{ Route('zakat.purchase') }}" method="POST">
										{{ csrf_field() }}
										<div id="bayar-zakat-penghasilan" class="w3-container city" style="display:none">
											<div class="cnt-frm">
												<span class="dnt-fld-inr">
													<input type="hidden" name="qty_beras" id="qty_beras" value="{{ $setup_zakat->qty_beras->value }}">
													<input type="hidden" name="harga_emas" id="harga_emas" value="{{ $setup_zakat->harga_emas->value }}">
													<input type="hidden" name="harga_perak" id="harga_perak" value="{{ $setup_zakat->harga_perak->value }}">
													<input type="hidden" name="category_id" value="82">
													<div class="row mrg">
														<div class="col-lg-3"></div>
														<div class="col-lg-6">
															<div class="slc-wrp">
																<label>Jumlah Zakat</label>
																<input type="text" name="jumlah_zakat_purchase_detail" id="jumlah_zakat" placeholder="Rp. 0" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>
															<div class="slc-wrp">
																<button type="submit" class="thm-btn2" id="bayar_zakat" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">
																	Bayar
																</button>
															</div>
														</div>
													</div>
												</span>
											</div>
										</div>
									</form>
								</div>
							</div>
							@else
							<div class="col-md-12 col-sm-12 col-lg-12">
								<h4 class="sc_donations_title sc_item_title" style="text-align: center; font-weight: bold; margin-bottom: 15px;">Zakat Emas</h4>
								<div class="cnt-inr" style="background-image: url('{{ asset('themes/ngo-theme-2/assets/images/pattern-bg2.png') }}');">

									<div class="row">
										<div class="col-lg-3"></div>
										<div class="col-lg-6">
											<div class="w3-row">
												<a href="javascript:void(0)" onclick="openCity(event, 'hitung-zakat-penghasilan');">
													<div class="w3-third tablink w3-bottombar w3-border-red w3-padding">Hitung</div>
												</a>
												<a href="javascript:void(0)" onclick="openCity(event, 'bayar-zakat-penghasilan');">
													<div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding">Bayar</div>
												</a>
											</div>
										</div>
									</div>
									<br><br>
									<form action="{{ Route('zakat.purchase') }}" method="POST">
										{{ csrf_field() }}
										<div id="hitung-zakat-penghasilan" class="w3-container city" style="display:block;">
											<div class="cnt-frm">
												<span class="dnt-fld-inr">

													<div class="row mrg">

														<input type="hidden" name="qty_beras" id="qty_beras" value="{{ $setup_zakat->qty_beras->value }}">
														<input type="hidden" name="harga_emas" id="harga_emas" value="{{ $setup_zakat->harga_emas->value }}">
														<input type="hidden" name="harga_perak" id="harga_perak" value="{{ $setup_zakat->harga_perak->value }}">
														<input type="hidden" name="category_id" value="83">
														<div class="col-lg-3"></div>
														<div class="col-md-6 col-sm-12 col-lg-6">
															<div class="slc-wrp">
																<label>Jumlah Perak (/gr)</label>
																<input type="text" name="jumlah_perak" id="jumlah_perak" placeholder="Jumlah Perak" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>
															<div class="slc-wrp">
																<label>Jumlah Perak Yang di Gunakan (/gr)</label>
																<input type="text" name="jumlah_perak_yang_digunakan" id="jumlah_perak_yang_digunakan" placeholder="Jumlah Perak Yang di Gunakan" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>
															<div class="slc-wrp">
																<label>Total</label>
																<input type="text" name="jumlah_zakat_purchase_detail" id="total_zakat_perak" placeholder="Rp. 0" readonly="">
															</div>
															<div class="slc-wrp">
																<a class="thm-btn2" id="hitung_zakat_perak" href="javascript:0;" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">Hitung</a>
															</div>
															<div class="slc-wrp">
																<button type="submit" class="thm-btn2" id="bayar-zakat-penghasilan-hitung" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px; display: none;">
																	Bayar
																</button>
															</div>
														</div>
													</div>
												</span>
											</div>
										</div>
									</form>

									<form action="{{ Route('zakat.purchase') }}" method="POST">
										{{ csrf_field() }}
										<div id="bayar-zakat-penghasilan" class="w3-container city" style="display:none">
											<div class="cnt-frm">
												<span class="dnt-fld-inr">
													<input type="hidden" name="qty_beras" id="qty_beras" value="{{ $setup_zakat->qty_beras->value }}">
													<input type="hidden" name="harga_emas" id="harga_emas" value="{{ $setup_zakat->harga_emas->value }}">
													<input type="hidden" name="harga_perak" id="harga_perak" value="{{ $setup_zakat->harga_perak->value }}">
													<input type="hidden" name="category_id" value="83">
													<div class="row mrg">
														<div class="col-lg-3"></div>
														<div class="col-lg-6">
															<div class="slc-wrp">
																<label>Jumlah Zakat</label>
																<input type="text" name="jumlah_zakat_purchase_detail" id="jumlah_zakat" placeholder="Rp. 0" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
															</div>
															<div class="slc-wrp">
																<button type="submit" class="thm-btn2" id="bayar_zakat" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">
																	Bayar
																</button>
															</div>
														</div>
													</div>
												</span>
											</div>
										</div>
									</form>
								</div>
							</div>
							@endif
						</div>
					</div>

					<div class="tabs-detail">
						<div class="container-tabs-title">
							<div id="tab1" class="item-tabs-active">
								Hukum
							</div>
							<div id="tab2" class="item-tabs">
								Ketentuan
							</div>
							<div id="tab3" class="item-tabs">
								Cara Hitung
							</div>
						</div>
					</div>
					<div id="content1" class="content-tabs">
						@if($hukum_zakat != null)
							{!! $hukum_zakat->konten_page !!}
						@else
							Tidak Ada Konten
						@endif
					</div>
					<div id="content2" class="content-tabs">
						@if($ketentuan_zakat != null)
							{!! $ketentuan_zakat->konten_page !!}
						@else
							Tidak Ada Konten
						@endif
					</div>
					<div id="content3" class="content-tabs">
						@if($cara_hitung_zakat != null)
							{!! $cara_hitung_zakat->konten_page !!}
						@else
							Tidak Ada Konten
						@endif
					</div>
				</div>

			</div>
		</section>


		@include('front_page.ngo-theme-2.main_layouts.footer')

	</main><!-- Main Wrapper -->

	@include('front_page.ngo-theme-2.main_layouts.js')
	@include('front_page.ngo-theme-2.pages.zakat.js')
</body>	
</html>