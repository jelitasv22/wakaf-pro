<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header2')

		
		<div class="gray-bg3 brdcrmb-wrp" >
			<div class="container">
				<div class="brdcrmb-inr flex justify-content-between">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('home') }}" title="" itemprop="url">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('program') }}" title="" itemprop="url">Zakat</a></li>
						<li class="breadcrumb-item active">Konfirmasi Pembayaran</li>
					</ol>
				</div>
			</div>
		</div><!-- Breadcrumbs Wrap -->
		<section>
			<div class="block">
				<div class="container">
					<div class="cnt-frm">
						<form id="data-konfirmasi" class="sc_input_hover_default inited" data-formtype="form_2" method="post" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="sc_form_item sc_form_field label_over" style="display: flex;"> 
								<div style="width: 70%;">
									<span>No Invoice</span>
									<input id="no_invoice" type="text" name="no_invoice" placeholder="No Invoice" value="{{$confirm->confirm->snap_token}}" id="no_invoice">

									<input type="hidden" name="id_yayasan" id="id_yayasan" value="{{$confirm->confirm->id_yayasan}}">
									<input type="hidden" name="id_zakat" id="id_zakat" value="{{$confirm->confirm->id}}">
									<input type="hidden" name="guest_id" id="guest_id" value="{{$confirm->confirm->guest_id}}">
								</div>
								<div style="width: 30%;">
									<button type="button" class="thm-btn2" id="cari" style="width: 95%; padding: 8px; border-radius: 5px; margin-top: 24px; height: 55px; float: right;">
										Cari
									</button>
								</div>
							</div>
							<div class="sc_form_item sc_form_field label_over">
								<span>Nama Donatur</span>
								<?php
								if($confirm->confirm->name == null){
									$name = $confirm->confirm->nama_lengkap;
								}else{
									$name = $confirm->confirm->name;
								}
								?>
								<input type="text" id="nama_donatur" name="nama_donatur" placeholder="Nama Donatur" value="{{$name}}" readonly="">
							</div>

							<div class="sc_form_item sc_form_field label_over">
								<span>Metode Pembayaran</span>
								<input type="text" name="metode_pembayaran" id="metode_pembayaran" placeholder="Metode Pembayaran" readonly="" value="{{$confirm->confirm->metode_pembayaran}}">
							</div>

							<div class="sc_form_item sc_form_field label_over">
								<span>Total Zakat</span>
								<input type="text" name="total_zakat" placeholder="Total Zakat" readonly="" value="{{number_format($confirm->confirm->total_zakat)}}" id="total_zakat">
							</div>

							<div class="sc_form_item sc_form_field label_over">
								<span>Nama Pengirim</span>
								<input type="text" name="nama_pengirim" placeholder="Nama Pengirim" id="nama_pengirim">
							</div>

							<div class="sc_form_item sc_form_field label_over">
								<span>Bank Pengirim</span>
								<input type="text" name="bank_pengirim" placeholder="Bank Pengirim" id="bank_pengirim">
							</div>

							<div class="sc_form_item sc_form_field label_over">
								<span>Tanggal Transaksi</span>
								<input type="date" name="tanggal_transaksi" placeholder="Tanggal Transaksi" value="{{date('Y-m-d')}}" style="background-color: #f2f2f2" id="tanggal_transaksi">
							</div>

							<div class="sc_form_item sc_form_field label_over">
								<span>Bukti Transfer</span>
								<input type="file" name="bukti_transfer" placeholder="Bukti Transfer" id="bukti_transfer" style="background-color: #f2f2f2">
							</div>
						</div>
						<center style="margin-top: 5px; margin-bottom: 10px;">
							<button type="button" class="thm-btn2" id="konfirmasi_pembayaran" style="width: 100%; padding: 8px; border-radius: 5px">
								Konfirmasi Pembayaran
							</button>
						</center>
					</form>
				</div>
			</div>
		</div>
	</section>

	@include('front_page.ngo-theme-2.main_layouts.footer')

</main><!-- Main Wrapper -->

	@include('front_page.ngo-theme-2.main_layouts.js')
	@include('front_page.ngo-theme-2.pages.program.js')
</body>	
</html>