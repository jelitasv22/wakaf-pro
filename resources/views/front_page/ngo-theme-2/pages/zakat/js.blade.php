<script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}"data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
<script type="text/javascript">

	function openCity(evt, type) {
		var i, x, tablinks;
		x = document.getElementsByClassName("city");
		for (i = 0; i < x.length; i++) {
			x[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablink");
		for (i = 0; i < x.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
		}
		document.getElementById(type).style.display = "block";
		evt.currentTarget.firstElementChild.className += " w3-border-red";
	}

	function tandaPemisahTitik(b){
		var _minus = false;
		if (b<0) _minus = true;
		b = b.toString();
		b=b.replace(".","");
		b=b.replace("-","");
		c = "";
		panjang = b.length;
		j = 0;
		for (i = panjang; i > 0; i--){
			j = j + 1;
			if (((j % 3) == 1) && (j != 1)){
				c = b.substr(i-1,1) + "." + c;
			} else {
				c = b.substr(i-1,1) + c;
			}
		}
		if (_minus) c = "-" + c ;
		return c;
	}

	function numbersonly(ini, e){
		if (e.keyCode>=49){
			if(e.keyCode<=57){
				a = ini.value.toString().replace(",","");
				b = a.replace(/[^\d]/g,"");
				b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
				ini.value = tandaPemisahTitik(b);
				return false;
			}
			else if(e.keyCode<=105){
				if(e.keyCode>=96){
					a = ini.value.toString().replace(",","");
					b = a.replace(/[^\d]/g,"");
					b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
					ini.value = tandaPemisahTitik(b);
					return false;
				}
				else {
					return false;
				}
			}
			else {
				return false; 
			}
		}else if (e.keyCode==48){
			a = ini.value.replace(",","") + String.fromCharCode(e.keyCode);
			b = a.replace(/[^\d]/g,"");
			if (parseFloat(b)!=0){
				ini.value = tandaPemisahTitik(b);
				return false;
			} else {
				return false;
			}
		}else if (e.keyCode==95){
			a = ini.value.replace(",","") + String.fromCharCode(e.keyCode-48);
			b = a.replace(/[^\d]/g,"");
			if (parseFloat(b)!=0){
				ini.value = tandaPemisahTitik(b);
				return false;
			} else {
				return false;
			}
		}else if (e.keyCode==8 || e.keycode==46){
			a = ini.value.replace(",","");
			b = a.replace(/[^\d]/g,"");
			b = b.substr(0,b.length -1);
			if (tandaPemisahTitik(b)!=""){
				ini.value = tandaPemisahTitik(b);
			} else {
				ini.value = "";
			}

			return false;
		} else if (e.keyCode==9){
			return true;
		} else if (e.keyCode==17){
			return true;
		} else {
			return false;
		}
	}

	$(document).ready(function() {

		$("#content2").hide();
		$("#content3").hide();

		$("#tab1").on('click', function () {
			$("#tab1").addClass('item-tabs-active');
			$("#tab1").removeClass('item-tabs');
			$("#tab2").addClass('item-tabs');
			$("#tab2").removeClass('item-tabs-active');
			$("#tab3").addClass('item-tabs');
			$("#tab3").removeClass('item-tabs-active');
			
			$("#content1").show();
			$("#content2").hide();
			$("#content3").hide();
		});

		$("#tab2").on('click', function () {
			$("#tab2").addClass('item-tabs-active');
			$("#tab2").removeClass('item-tabs');
			$("#tab1").addClass('item-tabs');
			$("#tab1").removeClass('item-tabs-active');
			$("#tab3").addClass('item-tabs');
			$("#tab3").removeClass('item-tabs-active');
			
			$("#content2").show();
			$("#content1").hide();
			$("#content3").hide();
		});

		$("#tab3").on('click', function () {
			$("#tab3").addClass('item-tabs-active');
			$("#tab3").removeClass('item-tabs');
			$("#tab1").addClass('item-tabs');
			$("#tab1").removeClass('item-tabs-active');
			$("#tab2").addClass('item-tabs');
			$("#tab2").removeClass('item-tabs-active');
			
			$("#content3").show();
			$("#content1").hide();
			$("#content2").hide();
		});
		getPaymentMethod();
		$("#selected-method").hide();
		$("#btn-payment-method").on('click', function () {
			$("#metode-pembayaran").addClass('payment-method-active');
			$("#metode-pembayaran").removeClass('payment-method-hide');
		});

		$("#btn-payment-method2").on('click', function () {
			$("#metode-pembayaran").addClass('payment-method-active');
			$("#metode-pembayaran").removeClass('payment-method-hide');
		});

		$("#mthd-close").off('click').on('click', function () {
			$("#metode-pembayaran").addClass('payment-method-hide');
			$("#metode-pembayaran").removeClass('payment-method-active');
		});

		$("#simple-pop-close").off('click').on('click', function () {
			$("#notif-pembayaran").addClass('payment-method-hide');
			$("#notif-pembayaran").removeClass('payment-method-active');
		});

		function getPaymentMethod()
		{
			var popup = $("#payment-popup-body");
			popup.html("")

			$.ajax({
				Type : 'GET', 
				dataType : 'json',
				url : '{{ENV("API_URL")}}/api/payment_vendors/categories',
				success: function(response){

					response.data.forEach(element => {
						$.ajax({
							Type : 'GET', 
							dataType : 'json',
							url : '{{ENV("API_URL")}}/api/payment_vendors/'+element.id+'?yayasan={{ENV("YAYASAN_KEY")}}',
							success: function(response2){
								$show_title = false;
								try {
									response2.data.forEach(element2 => {
										if (element2.bank_rekening_moota == null) {
											$show_title = true
											// throw BreakException
										} else {
											if (element2.bank_rekening_moota == null) {
												$show_title = true
												// throw BreakException
											} else {
												var used_for = element2.bank_rekening_moota.used_for.split(',');
												for (var i = 0; i < used_for.length; i++) {
													if(used_for[i].includes('Donasi')){
														$show_title = true
													}else if(used_for[i].includes('Qurban')){
														$show_title = true
													}else if(used_for[i].includes('Zakat')){
														$show_title = true
													}else{
														// throw BreakException
													}
												}
											}
										}
									});
								} catch (e) {
								}

								if (response2.data.length > 0 && $show_title)
									popup.append('<p class="category-title">'+element.category+'</p>');

								response2.data.forEach(element2 => {
									console.log(element2)
									var used_for = ''
									if(element2.bank_rekening_moota){
										used_for = element2.bank_rekening_moota.used_for.split(',');
									}
									if (element2.bank_rekening_moota == null) {
										$show_title = true
										// throw BreakException
									} else {
										for (var i = 0; i < used_for.length; i++) {
											if(used_for[i].includes('Donasi')){
												$show_title = true
											}else if(used_for[i].includes('Qurban')){
												$show_title = true
											}else if(used_for[i].includes('Zakat')){
												$show_title = true
											}else{
												// throw BreakException
											}
										}
									}

									if (element2.bank_rekening_moota == null || (element2.bank_rekening_moota && $show_title)) {
										let vendor = element2.vendor
										let type = element2.payment_type
										let method = (vendor == "midtrans") ? element2.midtrans_code : element2.xendit_code
										
										if (method == null) method = element2.moota_bank_id

											let category = ""
										if (element2.payment_type == "virtual_account") category = "Virtual Account "
											if (element2.payment_type == "manual_transfer") category = "Transfer "
												let label = category + element2.payment_name

											let no_rek = ''
											if(element2.bank_rekening_moota != null){
												no_rek  = element2.bank_rekening_moota.no_rekening;
											}
											let img_src = "{{ENV('BACKEND_URL')}}/api/metode/icon/"+element2.id;
											popup.append('<div class="item-method" onclick="choose_method(`'+vendor+'`,`'+type+'`,`'+method+'`,`'+label+'`,`'+img_src+'`,`'+no_rek+'`)">'+
												'<img class="logo-payment" src='+img_src+' alt="">'+
												'<p class="title-payment">'+element2.payment_name+'</p>'+
												'</div>');
										}
									});
							}
						});

					});
				}
			});
		}

		$('#zakat-penghasilan').show();
		$('#zakat-fitrah').hide();
		$('#zakat-emas').hide();
		$('#zakat-perak').hide();

		$('#category_id').val(80)
		$('#kategori').change(function(event) {
			var id = this.value;


			if(id == 80){
				$('#zakat-penghasilan').show();
				$('#zakat-fitrah').hide();
				$('#zakat-emas').hide();
				$('#zakat-perak').hide();

				$('#category_id').val(id);
			}else if(id == 81){
				$('#zakat-penghasilan').hide();
				$('#zakat-fitrah').show();
				$('#zakat-emas').hide();
				$('#zakat-perak').hide();

				$('#category_id').val(id);
			}else if(id == 82){
				$('#zakat-penghasilan').hide();
				$('#zakat-fitrah').hide();
				$('#zakat-emas').show();
				$('#zakat-perak').hide();

				$('#category_id').val(id);
			}else{
				$('#zakat-penghasilan').hide();
				$('#zakat-fitrah').hide();
				$('#zakat-emas').hide();
				$('#zakat-perak').show();

				$('#category_id').val(id);
			}

		});

		$('#hitung_zakat_penghasilan').click(function(event) {
			$('#bayar-zakat-penghasilan-hitung').show();
			const penghasilan_perbulan = Number($('#penghasilan_perbulan').val().replace(/\./g, ''));
			const penghasilan_lainnya  = Number($('#penghasilan_lainnya').val().replace(/\./g, ''));
			const harga_beras 		   = Number($('#harga_beras_penghasilan').val().replace(/\./g, ''));
			const qty_beras 		   = Number($('#qty_beras').val().replace(/\./g, ''));

			const total  = (penghasilan_perbulan+penghasilan_lainnya) * (2.5/100);
			const nishab = qty_beras * harga_beras;

			notes = 'Nishab Zakat Anda Adalah Rp. '+nishab.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')+', Jumlah Zakat Yang Harus Anda Bayar Adalah Rp. '+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')+', Jika Jumlah Zakat Kurang dari Nishab Maka Anda Belum Wajib Zakat.';

			$('#total_zakat_penghasilan').val('Rp. '+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))
			$('#jumlah_zakat_side').html('Rp. '+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))
			$('#notes').html(notes)

			$('#jumlah_zakat_purchase').val(total).substr(4).replace(/\./g, '')
		});

		$('#hitung_zakat_fitrah').click(function(event) {
			$('#bayar-zakat-penghasilan-hitung').show();
			const harga_beras = Number($('#harga_beras_fitrah').val().replace(/\./g, ''))

			const total = (harga_beras*2.5);

			notes = 'Jumlah Zakat Yang Harus Anda Bayar Adalah Rp. '+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

			$('#total_zakat_fitrah').val(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))
			$('#jumlah_zakat_side').html('Rp. '+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))
			$('#notes').html(notes)

			$('#jumlah_zakat_purchase').val(total).substr(4).replace(/\./g, '')
		});

		$('#hitung_zakat_emas').click(function(event) {
			$('#bayar-zakat-penghasilan-hitung').show();
			const jumlah_emas    = Number($('#jumlah_emas').val().replace(/\./g, ''))
			const emas_digunakan = Number($('#jumlah_emas_yang_digunakan').val().replace(/\./g, ''))
			const harga_emas     = Number($('#harga_emas').val().replace(/\./g, ''))

			const total = (jumlah_emas - emas_digunakan) * (2.5/100) * harga_emas;

			notes = 'Jumlah Zakat Yang Harus Anda Bayar Adalah Rp. '+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

			$('#total_zakat_emas').val(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))
			$('#jumlah_zakat_side').html('Rp. '+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))
			$('#notes').html(notes)

			$('#jumlah_zakat_purchase').val(total).substr(4).replace(/\./g, '')
		});

		$('#hitung_zakat_perak').click(function(event) {
			$('#bayar-zakat-penghasilan-hitung').show();
			const jumlah_perak    = Number($('#jumlah_perak').val().replace(/\./g, ''))
			const perak_digunakan = Number($('#jumlah_perak_yang_digunakan').val().replace(/\./g, ''))
			const harga_perak     = Number($('#harga_perak').val().replace(/\./g, ''))

			const total = (jumlah_perak - perak_digunakan) * (2.5/100) * harga_perak;

			notes = 'Jumlah Zakat Yang Harus Anda Bayar Adalah Rp. '+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');

			$('#total_zakat_perak').val(total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))
			$('#jumlah_zakat_side').html('Rp. '+total.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'))
			$('#notes').html(notes)

			$('#jumlah_zakat_purchase').val(total).substr(4).replace(/\./g, '')
		});

		$('#lanjutkan_pembayaran_zakat').click(function(event) {
			const category_id  = $('#category_id').val();
			const jumlah_zakat = $('#jumlah_zakat').val();
			const nama_lengkap = $('#nama_lengkap').val();
			const no_handphone = $('#no_handphone').val();
			const email 	   = $('#email').val();
			const method       = $('#payment_method').val();
			const vendor   	   = $("#payment_vendor").val();
			const payment_type = $("#payment_type").val();
			const no_rekening  = $('#no_rekening').val();

			var atps=email.indexOf("@");
			var dots=email.lastIndexOf(".");

			var validasiAngka = /^[0-9]+$/;

			if(category_id == ''){
				messageAlert('Kategori Harus di Isi')
			}else if(jumlah_zakat == ''){
				messageAlert('Jumlah Zakat Harus di Isi')
			}else if(nama_lengkap == ''){
				messageAlert('Nama Lengkap Harus di Isi')
			}else if(no_handphone.match(validasiAngka) == null || no_handphone == ''){
				messageAlert('No Handphone Harus di Isi')
			}else if(atps<1 || dots<atps+2 || dots+2>=email.length){
				messageAlert('Alamat email tidak valid.');
			}else if(method == ''){
				messageAlert('Anda Belum Memilih Metode Pembayaran ')
			}else{
				$.ajax({
					url: '{{ env("API_URL") }}/api/snap-token-midtrans-zakat',
					type: 'POST',
					dataType: 'Json',
					data: {
						csrf_token     : '{{ csrf_token() }}',
						jumlah_zakat   : jumlah_zakat.replace(/\./g, ''),
						nama_produk    : 'Penghasilan',
						nama           : nama_lengkap,
						email          : email,
						no_telp 	   : no_handphone,
						user_id  	   : 1,
						vendor         : vendor,
						payment_type   : payment_type,
						payment_method : method,
						phone 		   : no_handphone,
						category_id    : category_id,
						device 		   : 'WEB',
						key_yayasan    : '{{ env("YAYASAN_KEY") }}',
						no_rekening    : no_rekening
					},
				})
				.done(function(res) {
					console.log(res)
					if(payment_type == 'manual_transfer'){
						$("#notif-pembayaran").addClass('payment-method-active');

						let modal_title = $("#modal_title")
						let modal_body = $("#modal_body")

						let type = $("#payment_type").val()
						let method = $("#payment_method").val()

						let nominal = res.moota.nominal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")

						$("#title_popup").html('Transfer '+res.moota.nama_bank)
						$("#moota-logo").attr("src", $("#selected-method-img").attr("src"))
						$("#no_rekening_show").html(res.moota.no_rekening)

						$('#intruksi-pembayaran').html('<center><br />'+
							'<span><b>Intruksi Pembayaran</b></span><br>'+
							'<span>Nominal yang harus anda bayar :</span><br>'+
							'<span style="font-size: 18px;"><b>Rp. '+nominal+'</b></span><br>'+
							'<span>*Harap selesaikan pembayaran sesuai dengan nominal yang tertera untuk memudahkan pengecekan!</span><br>'+
							'<span>*Sesuaikan nominal sampai 3 digit terakhir! (Kelebihan dari nominal akan didonasikan juga)</span><br>'+
							'<span>*Lakukan pembayaran maksimal 1x24 jam!</span><br>'+
							'<span>*Silahkan akses link berikut untuk melihat perkembangan donasi anda! (Link dikirim juga ke email anda)</span><br>'+
							'<span>*Tekan tombol dibawah juka sudah melakukan pembayaran!</span><br><br />'+
							'<span>'+
							'<a href="javascript:0;" onclick="confirm_pay(`'+res.moota.no_invoice+'`,`'+res.moota.vendor+'`,`'+res.moota.nominal+'`,`'+res.moota.moota_bank+'`)">'+
							'<button style="padding: 5px; width: 100%; border-radius: 5px;" class="thm-btn2">'+
							'<span class="post_readmore_label">Saya Sudah Melakukan Transfer</span>'+
							'</button>'+
							'</a>'+
							'</span>'+
							'</center>');
					}else{
						snap.pay(res.snap_token, {
							onSuccess: function (result) {
								console.log(result);
								$('#snaptoken').val('');
								$.ajax({
									url: '{{ route("donasi.updateStatus") }}',
									type: 'POST',
									dataType: 'json',
									data: {
										"_token": "{{ csrf_token() }}",
										"id": result.order_id,
										"status": result.transaction_status,
										"payment_method": result.payment_method,
										"snap_token": res.snap_token
									},
									success: function(data){
										hideLoading();
										console.log(data)
										window.location.href = "{{Route('notif.wait')}}";
									}
								});
							},
							onPending: function (result) {
								console.log(result);
								$('#snaptoken').val('');
								window.location.href = "{{Route('notif.wait')}}";
							},
							onError: function (result) {
								console.log(result);
								messageAlert('maaf ada kesalahan, mohon hubungi administrator')
								// $('#snaptoken').val('');
								// window.location.href = "{{Route('notif.wait')}}";
							},
							onClose: function(){
								$.ajax({
									url: '{{ENV("API_URL")}}/api/delete-snap-token-midtrans',
									type: 'POST',
									dataType: 'json',
									data: {
										"_token": "{{ csrf_token() }}",
										"key_yayasan"     : '{{ENV("YAYASAN_KEY")}}',
										"iDtoken": res.snap_token,
										"status": 1,
									},
									success: function(data){
										messageAlert('Transaksi Dibatalkan')
										location.reload()
									}
								});
							}
						});
					}
				});
			}
		});

		$('#konfirmasi_pembayaran').click(function(e){
			e.preventDefault(); 
			$.ajax({
				url:'{{ENV("API_URL")}}/api/confirm-payment-zakat',
				type:"POST",
				data:new FormData(document.getElementById('data-konfirmasi')),
				processData:false,
				contentType:false,
				cache:false,
				async:false,
				success: function(res){
					messageAlert(res.msg)

					window.location.href = '{{ url("zakat") }}';
				}
			});
		});

});

function choose_method(vendor, type, method, label, img_src, no_rek)
{
	$("#mthd-close").click()
	$("#cc_form").hide()
	$("#ew_form").hide()

	$("#payment_vendor").val(vendor)
	$("#payment_type").val(type)
	$("#payment_method").val(method)
	$('#no_rekening').val(no_rek)

	$("#selected-method-img").attr("src",img_src)
	$("#selected-method-label").html(label)
	$("#selected-method").show()
	$("#btn-payment-method").hide()


	let payment_type = $("#payment_type").val()
	var payment_method = $("#payment_method").val();

	if (payment_type == "credit_card") {

	} else if (payment_type == "e_wallet") {
		if (payment_method == 'gopay' || payment_method == 'DANA')
			$("#ew_form").hide();
		else
			$("#ew_form").show();

	} else if (payment_type == "retail_outlet") {

	} else if (payment_type == "virtual_account") {

	} else if (payment_type == "manual_transfer") {

	}
}

function confirm_pay(id, vendor, nominal, bank)
{
	if(vendor == "moota"){
		$.post('{{ENV("API_URL")}}/api/moota/mutation/by-amount',
		{
			_method	: 'POST',
			bank_id: moota_bank_id,
			amount: nominal,
			type: "Qurban"
		},
		function (data, status) {
			messageAlert(data.message);

			$('#category_id').val();
			$('#jumlah_zakat').val();
			$('#nama_lengkap').val();
			$('#no_handphone').val();
			$('#email').val();
			$('#payment_method').val();
			$("#payment_vendor").val();
			$("#payment_type").val();
			$('#no_rekening').val();
		}
		);
	}else{

		$.ajax({
			url: '{{ENV("API_URL")}}/api/confirm-pay-zakat',
			type: 'GET',
			dataType: 'Json',
			data: {
				no_invoice: id
			}
		})
		.done(function(res) {

			messageAlert('Silahkan Lakukan Konfirmasi Pembayaran !');
			$("#notif-pembayaran").removeClass('payment-method-active');

			window.location.href = "{{ url('zakat/confirm?no_invoice=') }}"+res.confirm.snap_token
		});

	}
}

function copyToClip(element_id)
{
	var temp = $("<input>");
	$("body").append(temp);
	temp.val($("#"+element_id).text()).select();
	document.execCommand("copy");
	temp.remove();
}

function messageAlert(response){
	const Toast = Swal.mixin({
		toast: true,
		customClass: 'swal-wide',
		position: 'top-end',
		showConfirmButton: false,
		timer: 5000,
		timerProgressBar: true,
		onOpen: (toast) => {
			toast.addEventListener('mouseenter', Swal.stopTimer)
			toast.addEventListener('mouseleave', Swal.resumeTimer)
		}
	});

	Toast.fire({
		title: response
	});
}
</script>