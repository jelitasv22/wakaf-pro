<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header2')

		<div class="gray-bg3 brdcrmb-wrp">
			<div class="container">
				<div class="brdcrmb-inr flex justify-content-between">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('home') }}" title="" itemprop="url">Home</a></li>
						<li class="breadcrumb-item active">Zakat</li>
					</ol>
				</div>
			</div>
		</div><!-- Breadcrumbs Wrap -->
		<section>
			<div class="block">
				<div class="container">
					<h2 class="sc_donations_title sc_item_title" style="font-size: 35px; text-align: center; margin-bottom: 10px;">Bayar Zakat</h2>
					<div class="blg-wrp style3 remove-ext3">
						<div class="row">
							@foreach($category_zakat->image as $row)
							<div class="col-md-3 col-sm-6 col-lg-3">
								<a href="{{ url('zakat/detail', $row->seo) }}">
									<div class="blg-bx3" style="border-radius: 10px; display: flex; flex-direction: column; justify-content: center; align-items: center;">
										<img src="{{ env('BACKEND_URL') }}/admin/assets/media/icon-kategori/{{ $row->gambar }}" style="width: 50%; text-align: center;"><br>
										<h4 class="thm-clr" style="text-align: center;">{{ str_replace('Zakat', '', $row->category) }}</h4>
									</div>
								</a>
							</div>
							@endforeach
						</div>
					</div>
					<hr>
					<div class="blg-wrp style3 remove-ext3">
						<div class="row">
							<div class="col-md-12 col-sm-12 col-lg-12">
								<h4 class="sc_donations_title sc_item_title">Kalkulator</h4>
								<div class="cnt-inr" style="background-image: url('{{ asset('themes/ngo-theme-2/assets/images/pattern-bg2.png') }}');">
									<div class="cnt-frm">
										<span class="dnt-fld-inr">

											<div class="slc-wrp">
												<label>Kategori</label>
												<div class="dropdown bootstrap-select">
													<select class="" tabindex="-98" name="kategori" id="kategori">
														@foreach($category_zakat->data as $row)
														<option value="{{ $row->id }}">{{ $row->category }}</option>
														@endforeach
													</select>
												</div>
											</div>

											<div class="row mrg">
												<input type="hidden" name="qty_beras" id="qty_beras" value="{{ $setup_zakat->qty_beras->value }}">
												<input type="hidden" name="harga_emas" id="harga_emas" value="{{ $setup_zakat->harga_emas->value }}">
												<input type="hidden" name="harga_perak" id="harga_perak" value="{{ $setup_zakat->harga_perak->value }}">
												<div class="col-md-5 col-sm-5 col-lg-5">
													<div id="zakat-penghasilan">
														<div class="slc-wrp">
															<label>Penghasilan Perbulan</label>
															<input type="text" name="penghasilan_perbulan" id="penghasilan_perbulan" placeholder="Penghasilan Perbulan" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
														</div>

														<div class="slc-wrp">
															<label>Bonus/THR/Penghasilan Lainnya</label>
															<input type="text" name="penghasilan_lainnya" id="penghasilan_lainnya" placeholder="Penghasilan Lainnya" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
														</div>

														<div class="slc-wrp">
															<label>Harga Beras</label>
															<input type="text" name="harga_beras_penghasilan" id="harga_beras_penghasilan" placeholder="Harga Beras" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
														</div>

														<div class="slc-wrp">
															<label>Total</label>
															<input type="text" name="total_zakat_penghasilan" id="total_zakat_penghasilan" placeholder="Rp. 0" readonly="">
														</div>

														<div class="slc-wrp">
															<a class="thm-btn2" id="hitung_zakat_penghasilan" href="javascript:0;" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">Hitung</a>
														</div>
													</div>

													<div id="zakat-fitrah">
														<div class="slc-wrp">
															<label>Harga Beras</label>
															<input type="text" name="harga_beras_fitrah" id="harga_beras_fitrah" placeholder="Harga Beras" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
														</div>
														<div class="slc-wrp">
															<label>Jumlah Zakat</label>
															<input type="text" name="total_zakat_fitrah" id="total_zakat_fitrah" placeholder="Rp. 0" readonly="">
														</div>
														<div class="slc-wrp">
															<a class="thm-btn2" id="hitung_zakat_fitrah" href="javascript:0;" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">Hitung</a>
														</div>
													</div>

													<div id="zakat-emas">
														<div class="slc-wrp">
															<label>Jumlah Emas (/gr)</label>
															<input type="text" name="jumlah_emas" id="jumlah_emas" placeholder="Jumlah Emas" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
														</div>
														<div class="slc-wrp">
															<label>Jumlah Emas Yang di Gunakan(/gr)</label>
															<input type="text" name="jumlah_emas_yang_digunakan" id="jumlah_emas_yang_digunakan" placeholder="Jumlah Emas Yang di Gunakan" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
														</div>
														<div class="slc-wrp">
															<label>Total</label>
															<input type="text" name="total_zakat_emas" id="total_zakat_emas" placeholder="Rp. 0" readonly="">
														</div>
														<div class="slc-wrp">
															<a class="thm-btn2" id="hitung_zakat_emas" href="javascript:0;" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">Hitung</a>
														</div>
													</div>

													<div id="zakat-perak">
														<div class="slc-wrp">
															<label>Jumlah Perak (/gr)</label>
															<input type="text" name="jumlah_perak" id="jumlah_perak" placeholder="Jumlah Perak" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
														</div>
														<div class="slc-wrp">
															<label>Jumlah Perak Yang di Gunakan (/gr)</label>
															<input type="text" name="jumlah_perak_yang_digunakan" id="jumlah_perak_yang_digunakan" placeholder="Jumlah Perak Yang di Gunakan" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);">
														</div>
														<div class="slc-wrp">
															<label>Total</label>
															<input type="text" name="total_zakat_perak" id="total_zakat_perak" placeholder="Rp. 0" readonly="">
														</div>
														<div class="slc-wrp">
															<a class="thm-btn2" id="hitung_zakat_perak" href="javascript:0;" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">Hitung</a>
														</div>
													</div>
												</div>
												<div class="col-lg-1"></div>
												<div class="col-md-6 col-sm-6 col-lg-6">
													<div class="card" style="height: 95%; border: 0px;">

														<div class="card-body cnt-inr" style="background-image: url('{{ asset('themes/ngo-theme-2/assets/images/pattern-bg2.png') }}');">

															<form action="{{ Route('zakat.purchase') }}" method="POST">
																{{ csrf_field() }}
																<h4 class="text-center" style="margin-top: 20%;">Jumlah Zakat</h4>
																<h3 class="text-center" id="jumlah_zakat_side">Rp. 0</h3>

																<span id="notes"></span>

																<input type="hidden" name="category_id" id="category_id">
																<input type="hidden" name="jumlah_zakat_purchase" id="jumlah_zakat_purchase">

																<button type="submit" class="thm-btn2" id="bayar_zakat" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">
																	Bayar
																</button>
															</form>

														</div>

													</div>																
												</div>
											</div>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>


			@include('front_page.ngo-theme-2.main_layouts.footer')

		</main><!-- Main Wrapper -->

		@include('front_page.ngo-theme-2.main_layouts.js')
		@include('front_page.ngo-theme-2.pages.zakat.js')
	</body>	
	</html>