<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header2')


		<div class="gray-bg3 brdcrmb-wrp" >
			<div class="container">
				<div class="brdcrmb-inr flex justify-content-between">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="{{ url('home') }}" title="" itemprop="url">Home</a></li>
						<li class="breadcrumb-item"><a href="{{ url('program') }}" title="" itemprop="url">Program</a></li>
						<li class="breadcrumb-item active">Donasi Program</li>
					</ol>
				</div>
			</div>
		</div><!-- Breadcrumbs Wrap -->
		<section>
			<div class="block">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="cus-dtl-thmb">
								<img src="{{ENV('BACKEND_URL')}}/admin/assets/media/foto-program/{{$detail_program->list->foto}}" alt="cus-dtl-img1-1.jpg" itemprop="image">
							</div>
							<div class="cus-dtl-wrp" style="padding: 0.5rem;">
								<?php
								if($detail_program->list->total != null){
									$total  = $detail_program->list->total;
								}else{
									$total = 0;
								}

								if($detail_program->list != null){
									$target = $detail_program->list->dana_target;
								}else{
									$target = 0;
								}

								if ($target != null || $target != 0){
									$persen = $target/100;
									$hasil  = round($total/$persen);
									if($hasil > 100){
										$hasil = 100;
									}
								}else{
									$persen = 100;

									if($total != null || $total != 0){
										$hasil  = 100;
									}else{
										$hasil = 0;
									}
								}
								?>
								<div class="cus-dtl-inf">
									<span class="cus-cat"><a class="bg-clr2" href="#" title="" itemprop="url">{{$detail_program->list->category}}</a></span>
									<ul class="cus-mta">
										<li><i class="fa fa-calendar"></i>{{$detail_program->list->created_at}}</li>
									</ul>
									<h1 itemprop="headline">
										<a href="{{ url('program/detail-program', $detail_program->list->seo) }}">{{$detail_program->list->judul}}</a>
									</h1>
									<div class="progress">
										<div class="progress-bar thm-bg wdth{{$hasil}}"><span>{{$hasil}}%</span></div>
									</div>
									<span class="cus-amt"><i class="thm-clr">{{ number_format($detail_program->list->total) }}</i> dari {{ number_format($detail_program->list->dana_target) }}</span>
								</div>
								<p itemprop="description">{!! $detail_program->list->deskripsi !!}</p>
							</div><!-- Cause Detail Wrap -->
						</div>

						<div class="col-lg-6">
							<div class="cnt-inr" style="background-image: url('{{ asset('themes/ngo-theme-2/assets/images/pattern-bg2.png') }}');">
								<h4>Donasi</h4>
								<div class="cnt-frm">
									<label>Nominal</label>
									<span class="dnt-fld2">
                                        @if ($donation_type)
                                            @if ($donation_type->type == "kelipatan")
                                                <input type="text" name="Nominal" placeholder="Nominal" id="nominal">
                                                <div class="sugest-box">
                                                    @php
                                                        $value = $donation_type->value;
                                                    @endphp
                                                    @for ($i = 1; $i <= 4; $i++)
                                                        @php
                                                            $value = $value*$i;
                                                        @endphp
                                                        <button class="button-donasi" style="padding: '5px'" onclick="suggestButton({{$value}})">
                                                            {{number_format($value)}}
                                                        </button>
                                                    @endfor
                                                </div>
                                            @else
                                                <div style="display: flex">
                                                    <button class="button-donasi" style="margin-top: 0px; margin-bottom: 5px; padding: 10px; height: 60px; margin-right: 5px;" onclick="btnMinusOnClick({{$donation_type->value}})">
                                                        <i class="fa fa-minus"></i>
                                                    </button>
                                                    <input type="text" name="Nominal" value="{{$donation_type->value}}" placeholder="Nominal" id="nominal" readonly>
                                                    <button class="button-donasi" style="margin-top: 0px; margin-bottom: 5px; padding: 10px; height: 60px" onclick="btnPlusOnClick({{$donation_type->value}})">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            @endif
                                        @endif
                                        @if ($nominals)
                                            <input type="text" name="Nominal" placeholder="Nominal" id="nominal">
                                            <div class="sugest-box">
                                                @foreach ($nominals as $val)
                                                    <button class="button-donasi" style="padding: '5px'" onclick="suggestButton2('{{$val->value}}')">
                                                        {{$val->value}}
                                                    </button>
                                                @endforeach
                                            </div>
                                        @endif
									</span>
									<label>Metode Pembayaran</label>
									<button type="button" id="btn-payment-method" class="thm-btn2" style="width: 100%; padding: 8px; border-radius: 5px">
										Pilih Metode Pembayaran
									</button>
									<input type="hidden" name="program_id" id="program_id" value="{{$detail_program->list->id}}">
									<input name="payment_vendor" id="payment_vendor" type="hidden" value="" />
									<input name="payment_type" id="payment_type" type="hidden" value="" />
									<input name="payment_method" id="payment_method" type="hidden" value="" />
									<input type="hidden" id="no_rekening" name="no_rekening" value="">
									<div id="selected-method" style="display: flex; width: 100%;">
										<div style="width: 50%;">
											<img src="" alt="" id="selected-method-img" style="max-width:100%;max-height:40px;margin-right:10px;">
											<span style="font-size:14px;" id="selected-method-label">

											</span>
										</div>
										<div style="width: 50%;">
											<button type="button" id="btn-payment-method2" class="thm-btn2" style="width: 50%; padding: 5px; border-radius: 5px; float: right;">
												Ganti
											</button>
										</div>
									</div>
									<br>
									<label>Nama Lengkap</label>
									<span class="dnt-fld2">
										<input type="text" name="nama_lengkap" placeholder="Nama Lengkap" id="nama_lengkap">
									</span>

									<label>No Handphone/ Whatsapp</label>
									<span class="dnt-fld2">
										<input id="no_handphone" type="text" name="no_handphone" placeholder="No Handphone">
									</span>

									<label>Email</label>
									<span class="dnt-fld2">
										<input id="email" type="text" name="email" placeholder="Email">
									</span>
									<br>
									<a class="thm-btn2" id="donasi_sekarang" href="javascript:0;" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 20px;">Donasi Sekarang</a>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</section>

		<div id="metode-pembayaran" class="popup">
			<div class="popup-content">
				<div class="head-pop" style="background-color: #9a292d;">
					<div class="title-pop">
						<p style="color: white;">Metode Pembayaran</p>
					</div>
					<span id="mthd-close" class="tutup">&times;</span>
				</div>
				<div class="payment-scroller" id="payment-popup-body">

				</div>
			</div>
		</div>

		<div id="notif-pembayaran" class="popup">
			<div class="popup-content-notif">
				<div class="head-pop" style="background-color: #9a292d;">
					<div class="title-pop">
						<p id="title_popup" style="color: white;">Transfer</p>
					</div>
					<span id="simple-pop-close" class="tutup">&times;</span>
				</div>
				<div class="payment-scroller" id="payment-popup-body-notif" style="padding: 10px;">
					<div class="text-color-popup" style="display: flex; border-radius: 5px; font-size: 15px; padding: 5px; line-height: 40px; text-align: center; background-color: #9a292d;">
						<span style="width: 20%;">
							<img class="logo-payment" id="moota-logo" src="" alt="">
						</span>
						<span id="no_rekening_show" style="width: 60%; text-align: center;">-</span>
						<span id="salin" style="width: 20%; text-align: right;"><a href="javascript:0;" style="color: white;" onclick="copyToClip('no_rekening_show')">Salin</a></span>
					</div>
					<div id="intruksi-pembayaran">
						<center><br />
							<span><b>Intruksi Pembayaran</b></span><br>
							<span>Nominal yang harus anda bayar :</span><br>
							<span style="font-size: 18px;"><b>Rp. 10.000</b></span><br>
							<span>*Harap selesaikan pembayaran sesuai dengan nominal yang tertera untuk memudahkan pengecekan!</span><br>
							<span>*Sesuaikan nominal sampai 3 digit terakhir! (Kelebihan dari nominal akan didonasikan juga)</span><br>
							<span>*Lakukan pembayaran maksimal 1x24 jam!</span><br>
							<span>*Silahkan akses link berikut untuk melihat perkembangan donasi anda! (Link dikirim juga ke email anda)</span><br>
							<span>*Tekan tombol dibawah juka sudah melakukan pembayaran!</span><br><br />
							<span>
								<a href="" class="post_readmore">
									<button style="padding: 5px; width: 100%; border-radius: 5px;">
										<span class="post_readmore_label">Saya Sudah Melakukan Transfer</span>
									</button>
								</a>
							</span>
						</center>
					</div>
				</div>
			</div>
		</div>

		@include('front_page.ngo-theme-2.main_layouts.footer')

	</main><!-- Main Wrapper -->

	@include('front_page.ngo-theme-2.main_layouts.js')
    @include('front_page.ngo-theme-2.pages.program.js')
<script type="text/javascript">
    function btnMinusOnClick(value) {
        var nominal = $('#nominal').val()
        nominal = parseInt(nominal)-parseInt(value)
        if(nominal < parseInt(value)) {
            nominal = parseInt(value)
        }
        $('#nominal').val(nominal);
    }

    function btnPlusOnClick(value) {
        var nominal = $('#nominal').val()
        nominal = parseInt(nominal)+parseInt(value)
        $('#nominal').val(nominal);
    }

    function suggestButton(value) {
        $('#nominal').val(value)
    }

    function suggestButton2(value) {
        $('#nominal').val(parseInt(value.replace(/\./g, '')))
    }
</script>
</body>
</html>
