<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header2')


        <div class="gray-bg3 brdcrmb-wrp" >
            <div class="container">
                <div class="brdcrmb-inr flex justify-content-between">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('home') }}" title="" itemprop="url">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{ url('program') }}" title="" itemprop="url">Program</a></li>
                        <li class="breadcrumb-item active">Detail Program</li>
                    </ol>
                    <!-- <form class="pg-srch-frm">
                        <input type="text" placeholder="Search All Resources">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form> -->
                </div>
            </div>
        </div><!-- Breadcrumbs Wrap -->
        <section>
            <div class="block">
                <div class="container">
                    <div class="cus-dtl-thmb">
                        <img src="{{ENV('BACKEND_URL')}}/admin/assets/media/foto-program/{{$detail_program->list->foto}}" alt="cus-dtl-img1-1.jpg" itemprop="image">
                        <div class="cus-dtl-dnt">Donasi Sekarang <a class="thm-btn" href="{{ url('program/donate') }}/{{$detail_program->list->seo}}" title="" itemprop="url">Donasi<span></span></a></div>
                    </div>
                    <div class="cus-dtl-wrp">
                        <?php
                        if($detail_program->list->total != null){
                            $total  = $detail_program->list->total;
                        }else{
                            $total = 0;
                        }

                        if($detail_program->list != null){
                            $target = $detail_program->list->dana_target;
                        }else{
                            $target = 0;
                        }

                        if ($target != null || $target != 0){
                            $persen = $target/100;
                            $hasil  = round($total/$persen);
                            if($hasil > 100){
                                $hasil = 100;
                            }
                        }else{
                            $persen = 100;

                            if($total != null || $total != 0){
                                $hasil  = 100;
                            }else{
                                $hasil = 0;
                            }
                        }
                        ?>
                        <div class="cus-dtl-inf">
                            <span class="cus-cat"><a class="bg-clr2" href="#" title="" itemprop="url">{{$detail_program->list->category}}</a></span>
                            <ul class="cus-mta">
                                <li><i class="fa fa-calendar"></i>{{$detail_program->list->created_at}}</li>
                                <!-- <li><i class="flaticon-user"></i>0 Donors</li> -->
                            </ul>
                            <h1 itemprop="headline">{{$detail_program->list->judul}}</h1>
                            <div class="progress">
                                <div class="progress-bar thm-bg wdth{{$hasil}}"><span>{{$hasil}}%</span></div>
                            </div>
                            <span class="cus-amt"><i class="thm-clr">{{ number_format($detail_program->list->total) }}</i> dari {{ number_format($detail_program->list->dana_target) }}</span>
                        </div>
                        <p itemprop="description">{!! $detail_program->list->deskripsi !!}</p>
                    </div><!-- Cause Detail Wrap -->
                </div>
            </div>
        </section>
        <section>
            <div class="block black-layer opc7" style="background-color: white;">
                <div class="fixed-bg"></div>
                <div class="container">
                    <div class="no-chld-wrp text-center">
                        <div class="no-chld-inr">
                            <h3 itemprop="headline" style="color: black;">Info Terbaru</h3><hr>
                            @if(count($detail_program->info) < 0)
                            <center id="tidak_ada_info">
                                <p itemprop="description">Tidak Ada Info Terbaru</p>
                            </center>
                            @endif
                            <div class="row" id="list_info">
                                <?php foreach ($detail_program->info as $key => $value) { ?>
                                    <div class="col-lg-12">
                                        <p itemprop="description" style="color: black;"><b>{{$value->judul}}</b></p>
                                        <p>{!! $value->content !!}</p>
                                    </div>
                                <?php } ?>
                            </div>

                            <br>
                            Mari berbagi informasi yang insha Allah bermanfaat kepada rekan dan saudara kita. Semoga menjadi amal soleh yang membawa keberkahan untuk anda. klik tombol share di bawah ini.

                            <a class="thm-btn2" href="{{ Route('program.donate', $detail_program->list->seo) }}" title="" itemprop="url" style="text-align: center; width: 100%; margin-top: 50px;">Donasi Sekarang</a>

                            <!--Sosmed-->
                            <center style="margin-bottom: 10px; margin-top: 50px; float: right;">

                                <span><a href="">Facebook</a></span>
                                <span><a href="">Twitter</a></span>
                                <span><a href="">Whatsapp</a></span>

                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section>
            <div class="block gray-layer opc7" style="padding-bottom: 10px; padding-top: 5px">
                <div class="fixed-bg gray-bg back-blend-multiply" style="background-image: url(assets/images/prlx-bg11.jpg);"></div>
                <div class="container">
                    <div class="we-hlp-wrp">
                        <div class="row align-items-center">
                            <div class="col-md-4 col-sm-12 col-lg-4">
                                <div class="we-hlp-titl">
                                    <h2 itemprop="headline">Daftar Donatur</h2>
                                    <p itemprop="description">disamping ini adalah daftar donatur yang membantu program ini</p>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-12 col-lg-8">
                                <div class="cntry-hlp-wrp">
                                    <div class="cntry-hlp-car owl-carousel">
                                        <?php foreach ($detail_program->donatur2 as $key => $value) { ?>

                                            <div class="cntry-hlp-bx">
                                                <div class="cntry-hlp-tp">
                                                    <h6 itemprop="headline">
                                                        @if($value->name == null)
                                                            {{$value->nama_lengkap}}
                                                        @else
                                                            {{$value->name}}
                                                        @endif
                                                    </h6>
                                                    <img src="{{ENV('BACKEND_URL')}}/admin/assets/media/icons/icon-people.png" alt="" itemprop="image">
                                                </div>
                                                <div class="cntry-hlp-md">
                                                    <strong>Donasi:<span>{{ number_format($value->total_donasi) }}</span></strong>
                                                </div>
                                            </div>

                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Where We Help Wrap -->
                </div>
            </div>
        </section>

        @include('front_page.ngo-theme-2.main_layouts.footer')

    </main><!-- Main Wrapper -->

    @include('front_page.ngo-theme-2.main_layouts.js')
</body>
</html>
