<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
	<title>Langkah Indonesia Mandiri</title>


	@include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
	<main>
		@include('front_page.ngo-theme-2.main_layouts.header2')

		<div class="gray-bg3 brdcrmb-wrp">
            <div class="container">
                <div class="brdcrmb-inr flex justify-content-between">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('home') }}" title="" itemprop="url">Home</a></li>
                        <li class="breadcrumb-item active">Program</li>
                    </ol>
                </div>
            </div>
        </div><!-- Breadcrumbs Wrap -->
        <section>
            <div class="block">
                <div class="container">
                    <div class="cus-wrp remove-ext5">
                        <h2 class="text-center">Program</h2>
                        <div class="row" id="list_program">
                            <span style="font-weight: bold; text-align: center; width: 100%;">Loading Program . . . </span>
                        </div>
                    </div><!-- Causes Style 1 -->
                    <div class="pgntin-wrp text-center">
                        <ul id="paginate" class="pagination"></ul><!-- Pagination -->
                    </div><!-- Pagination Wrap -->
                </div>
            </div>
        </section>


		@include('front_page.ngo-theme-2.main_layouts.footer')

	</main><!-- Main Wrapper -->

	@include('front_page.ngo-theme-2.main_layouts.js')
    @include('front_page.ngo-theme-2.pages.program.js')
</body>	
</html>