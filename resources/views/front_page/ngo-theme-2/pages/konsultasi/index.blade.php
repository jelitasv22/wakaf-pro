<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="icon" href="{{asset('themes/ngo-theme-2/assets/images/logolima.png')}}" sizes="32x32" type="image/png">
    <title>Langkah Indonesia Mandiri</title>


    @include('front_page.ngo-theme-2.main_layouts.css')
</head>
<body itemscope>
    <main>
        @include('front_page.ngo-theme-2.main_layouts.header2')

        <div class="gray-bg3 brdcrmb-wrp">
            <div class="container">
                <div class="brdcrmb-inr flex justify-content-between">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ url('home') }}" title="" itemprop="url">Home</a></li>
                        <li class="breadcrumb-item active">Konsultasi</li>
                    </ol>
                </div>
            </div>
        </div><!-- Breadcrumbs Wrap -->
        <section>
            <div class="block">
                <div class="container">
                    <h2 class="sc_donations_title sc_item_title" style="font-size: 25px;">Konsultasi</h2>
                    <br>
                    @csrf
                        <input type="hidden" name="id" id="id" value="{{$list_yayasan->id}}">
                        <div class="cnt-frm">
                            <label>Nama</label>
                            <span class="dnt-fld2">
                                <input type="text" name="name" placeholder="nama" id="name">
                            </span>
                            <label>Email</label>
                            <span class="dnt-fld2">
                                <input type="email" name="email" placeholder="email" id="email">
                            </span>
                            <label>Subject</label>
                            <span class="dnt-fld2">
                                <input type="text" name="subject" placeholder="subject konsultasi" id="subject">
                            </span>
                            <label>Perihal</label>
                            <span class="dnt-fld2">
                                <textarea type="text" name="perihal" placeholder="perihal" id="perihal"></textarea>
                            </span>
                            <center style="padding-bottom: 20px;">
                                <div class="form-group" >
                                    <label for="captcha">Captcha</label>
                                    {!! NoCaptcha::renderJs() !!}
                                    {!! NoCaptcha::display() !!}
                                    <span class="text-danger" >{{ $errors->first('g-recaptcha-response') }}</span>
                                </div>
                                <button type="button" href="javascript:0" class="thm-btn2" onclick="sendkonsultasi()" >Kirim Konsultasi</button>
                                </center>
                        </div>

                         <center>
                            <div><h3 style="padding-bottom: 20px;">Contact Us</h3>
                                <h5 style="display: inline-grid;width: 30%"><img src="{{asset('image/gmail.png')}}" style="width:10%;">{{$list_yayasan->email}}</h5>
                                <h5 style="display: inline-grid;width: 30%"><img src="{{asset('image/phone.png')}}" style="width:10%;">{{$list_yayasan->no_telp}}</h5>
                                <h5 style="display: inline-grid;width: 30%"><img src="{{asset('image/wa.png')}}" style="width:10%;">{{$list_yayasan->no_wa}}</h5>     
                            </div>
                        </center>
                
                    <div class="modal fade bd-example-modal-lg" id="modalloading" data-backdrop="static" data-keyboard="false" tabindex="-1">
                        <div class="modal-dialog modal-sm" style="display: flex;
                    justify-content: center;
                    align-items: center;
                    height: 100%;">
                            <div class="modal-content" style="width: 60px;">
                                <span class="fa fa-spinner fa-spin fa-3x"></span>
                            </div>
                        </div>
                    </div>
                
                </div>
            </div>
        </section>



        @include('front_page.ngo-theme-2.main_layouts.footer')

    </main><!-- Main Wrapper -->

    @include('front_page.ngo-theme-2.main_layouts.js')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>

    <script>


        function messageAlert(response){
            const Toast = Swal.mixin({
                toast: true,
                customClass: 'swal-wide',
                position: 'center-end',
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });

            Toast.fire({
                title: response
            });
        }

        function sendkonsultasi(){

            if($('#g-recaptcha-response').val() == ""){
                messageAlert('Captcha Tidak Boleh Kosong')
            }else if($("#name").val() == ""){
                messageAlert('nama tidak boleh kosong')
            }else if($("#email").val() == ""){
                messageAlert('email tidak boleh kosong')
            }else if($("#subjek").val() == ""){
                messageAlert('subjek tidak boleh kosong')
            }else if($("#perihal").val() == ""){
                messageAlert('perihal tidak boleh kosong')
            }else{
                $.ajax({
                    type: 'POST', //THIS NEEDS TO BE GET
                    url: '{{ENV("API_URL")}}/api/send_konsultasi',//Route Insert
                    dataType: 'json',
                    data: {
                        "_token"        : '{{csrf_token()}}',
                        "id_yayasan"    : $('#id').val(),
                        "name"          : $('#name').val(),
                        "email"         : $('#email').val(),
                        "subjek"        : $('#subjek').val(),
                        "perihal"       : $('#perihal').val()
                    },
                    beforeSend: function() {
                        showloading();
                    // $hashRate.html(loading);
                    // $valid.html(loading);
                    // $invalid.html(loading);
                    // $invalidp.html(loading);
                    },
                    success: function (data) {
                        hideloading();
                        messageAlert(data);
                        $('#name').val('');
                        $('#email').val('');
                        $('#subjek').val('');
                        $('#perihal').val('');

                        $('#modalloading').modal('hide');
                    }
                });
            }
        }
        function hideloading(){
            $('#modalloading').modal('hide');
        }
        function showloading(){
            $('#modalloading').modal('show');
        }
    </script>
</body> 
</html>