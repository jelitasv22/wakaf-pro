<section>
    <div class="block gray-layer opc97">
        <div class="fixed-bg patern-bg" style="background-image: url(assets/images/pattern-bg2.png);"></div>
        <div class="container">
            <div class="sec-ttl text-center">
                <div class="sec-ttl-inr">
                    <h2 itemprop="headline">Program</h2>
                    <i class="flaticon-sweat thm-clr"></i>
                </div>
            </div><!-- Sec Title -->
            <div class="cus-wrp remove-ext5">
                <div class="row">
                    <?php foreach ($program->list as $key => $value) { ?>
                        <?php 
                        $total  = $value->total;
                        $target = $value->dana_target;

                        if ($target != null || $target != 0){
                            $persen = $target/100;
                            $hasil  = round($total/$persen);
                            if($hasil > 100){
                                $hasil = 100;
                            }
                        }else{
                            $persen = 100;

                            if($total != null || $total != 0){
                                $hasil  = 100; 
                            }else{
                                $hasil = 0;
                            }
                        }
                        ?>
                        <div class="col-md-4 col-sm-6 col-lg-4">
                            <div class="cus-bx">
                                <div class="cus-thmb">
                                    <img src="{{ENV('BACKEND_URL')}}/admin/assets/media/foto-program/{{$value->foto}}" alt="" itemprop="image">
                                    <span class="cus-cat"><a class="bg-clr1" href="#" title="" itemprop="url">{{$value->category}}</a></span>
                                </div>
                                <div class="cus-inf">
                                    <h4 itemprop="headline"><a href="{{ url('program/detail-program', $value->seo) }}" title="" itemprop="url">{{$value->judul}}</a></h4>
                                    <div class="progress">
                                        <div class="progress-bar thm-bg wdth{{$hasil}}"><span>{{$hasil}}</span></div>
                                    </div>
                                    <span class="cus-amt">
                                        <i class="thm-clr">
                                            <?php 
                                            if($value->total == null){ echo 0; }else{ echo str_replace(',','.', number_format($value->total)); }
                                            ?>

                                        </i>
                                        dari <?php 
                                        if($value->dana_target == null){ echo 0; }else{ echo str_replace(',','.',number_format($value->dana_target)); }
                                        ?>
                                    </span>
                                    <p>{{$value->resume}}...</p>
                                    <a class="thm-btn2" href="{{ Route('program.donate', $value->seo) }}" title="" itemprop="url" style="text-align: center; width: 100%;">Donasi Sekarang</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div><!-- Causes Style 1 -->
            <div class="vw-mr text-center">
                <a class="thm-btn" href="{{ url('program') }}" title="" itemprop="url">View All Program<span></span></a>
            </div><!-- View More -->
        </div>
    </div>
</section>