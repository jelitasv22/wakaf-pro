<section>
    <div class="block no-padding">
        <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="notgeneric125" data-source="gallery" style="background-color:transparent;padding:0px;">
            <div id="rev_slider_4_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
                <ul>
                    <?php foreach ($slider->list as $key => $value) { ?>
                    <li data-index="rs-1" style="display: flex; justify-content: center;" data-transition="random" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-title="Slide Title" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-rotate="0"  data-fstransition="random" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{ENV('BACKEND_URL')}}/admin/assets/media/slider/{{$value->gambar}}" alt="slide1.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>                        
                        <h2 class="judul-slider">{{$value->judul}}</h2>
                        <div class="btn-slider"><a href="{{$value->link}}">{{$value->title_button}}</a></div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div><!-- END REVOLUTION SLIDER -->
    </div>
</section>  