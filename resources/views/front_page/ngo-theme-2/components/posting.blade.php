<section>
    <div class="block">
        <div class="container">
            <div class="sec-ttl text-center">
                <div class="sec-ttl-inr">
                    <h2 itemprop="headline">Updates</h2>
                    <i class="flaticon-sweat thm-clr"></i>
                </div>
            </div><!-- Sec Title -->
            <div class="blg-wrp remove-ext9">
                <div class="row">
                    <?php foreach ($posting->list as $key => $value) { ?>
                    <div class="col-md-4 col-sm-6 col-lg-4">
                        <div class="blg-bx">
                            <div class="blg-thmb">
                                <a href="" title="" itemprop="url">
                                    <img src="{{ENV('BACKEND_URL')}}/admin/assets/media/posting/{{$value->image}}" alt="" itemprop="image">
                                </a>
                                <span class="blg-dat"><a href="#" title="" itemprop="url">{{$value->category}}</a></span>
                            </div>
                            <div class="blg-inf">
                                <h4 itemprop="headline"><a href="{{ url('posting/detail-posting', $value->seo) }}" title="" itemprop="url">{!! $value->judul !!}</a></h4>
                                {!! substr($value->content, 0, 200) !!}...
                            </div>
                        </div>
                    </div>
                    <?php } ?>  
                </div>
            </div><!-- Blog Wrap Style 1 -->
            <div class="vw-mr text-center">
                <a class="thm-btn" href="{{ url('posting') }}" title="" itemprop="url">View All Posts<span></span></a>
            </div><!-- View More -->
        </div>
    </div>
</section>