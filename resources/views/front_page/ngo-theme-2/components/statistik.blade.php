<section style="padding-top: 50px;">
    <div class="block remove-gap">
        <div class="container">
            <div class="fct-wrp text-center remove-ext3">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-lg-6">
                        <div class="fct-bx">
                            <i class="flaticon-assistance"></i>
                            <h6 class="thm-clr" itemprop="headline" id="donasi_terkumpul_statistik"></h6>
                            <span>Donasi Terkumpul</span>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-6">
                        <div class="fct-bx">
                            <i class="flaticon-world"></i>
                            <h6 class="clr3" itemprop="headline" id="donasi_terkumpul_statistik2"></h6>
                            <span>Donatur Tergabung</span>
                        </div>
                    </div>
                </div>
            </div><!-- Facts Wrap Style 1 -->
        </div>
    </div>
</section>